#!/usr/env python3
# -*- coding: utf-8 -*-
import imgen
imgen.setup()

import matplotlib.pyplot as plt
import numpy as np

X = np.linspace(0, 4, 200)

plt.plot(X, np.tanh(X), label=r'$M / M_s$')
plt.plot(X, - X * np.tanh(X) + np.log(2*np.cosh(X)), label=r'$s / k_B$')

plt.xlabel(r'$x$')
plt.ylabel(r'$y$')
plt.legend()

# Save
imgen.done(__file__)
