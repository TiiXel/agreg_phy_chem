#!/usr/env python3
# -*- coding: utf-8 -*-
import imgen
imgen.setup()

import matplotlib.pyplot as plt
import numpy as np
from numpy import tanh, cosh, log
from labellines import labelLine, labelLines

muBperkB = 0.67  # SI
T = np.linspace(0, 1, 200)

for B in np.linspace(0.1, 1, 5):
    x = muBperkB * B/T
    S = - x * tanh(x) + log(2*cosh(x))
    plt.plot(T, S, label='B = {:0.1f}T'.format(B))
labelLines(plt.gca().get_lines())

plt.xlabel(r'$T$')
plt.ylabel(r'$s(T) / k_B$')
plt.axhline(y=0.4, color='k', linestyle='dashed', label='isoS')

# Save
imgen.done(__file__)
