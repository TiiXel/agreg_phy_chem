#!/usr/env python3
# -*- coding: utf-8 -*-
import imgen
args = imgen.setup()
savefig = args.save
savefig = True

##############################################################################
# Used to save this special imgen into multiple pages
from os.path import realpath, dirname, join, basename
savedir_ = join(dirname(dirname(realpath(__file__))), 'documents/')
savename_ = 'LP20.pdf'
savepath_ = join(savedir_, savename_)

from matplotlib.backends.backend_pdf import PdfPages
pdf = PdfPages(savepath_)
##############################################################################

import time as time
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle

if not savefig:
    plt.ion()
plt.gca().set_aspect('equal')
plt.xticks([])
plt.yticks([])

r_rotor = 1.0  # rayon rotor
e_entrefer = 0.7  # entrefer
e_stator = 0.4  # épaisseur stator

champ_offset = e_stator/5
N_champ = 10

col_rotor = 'firebrick'
col_rotor_out = 'coral'
col_stator = 'grey'
col_stator_out = 'k'
col_spire = 'yellow'
col_champ = 'blue'
col_ampere = 'k'

inset_ampere = 0.05
Theta_ampere = 0.3

lim = (r_rotor+e_entrefer+e_stator)*1.2
plt.xlim([-lim, lim])
plt.ylim([-lim, lim])

def wait(figs):
    if not savefig:
        plt.figure(1)
        plt.waitforbuttonpress()
    else:
        for k in figs:
            plt.figure(k)
            pdf.savefig()

##############################################################################
# Plot du rotor
def rotor():
    plt.figure(1)

    plt.gca().add_patch(Circle((0, 0), r_rotor, fill=True, color=col_rotor_out))
    plt.gca().add_patch(Circle((0, 0), r_rotor, fill=False, color=col_rotor))
    theta = 1
    bx = - r_rotor * np.cos(theta)
    by = - r_rotor * np.sin(theta)
    hx = r_rotor * np.cos(theta)
    hy = r_rotor * np.sin(theta)
    plt.annotate('', xytext=(bx, by), xy=(hx, hy), arrowprops=dict(color=col_rotor, headwidth=10))
    plt.text(bx+hx, by+hy, r'$M$', color=col_rotor, ha='right', va='bottom', fontsize=12)

##############################################################################
# Plot du stator
def stator():
    plt.figure(1)

    theta = np.linspace(0, 2*np.pi, 50)
    xi = (r_rotor+e_entrefer) * np.cos(theta)
    yi = (r_rotor+e_entrefer) * np.sin(theta)
    xe = (r_rotor+e_entrefer+e_stator) * np.cos(theta)
    ye = (r_rotor+e_entrefer+e_stator) * np.sin(theta)

    x = np.ravel(np.append(xi, xe[::-1]))
    y = np.ravel(np.append(yi, ye[::-1]))

    plt.fill(x, y, color=col_stator)
    plt.plot(xi, yi, color=col_stator_out)
    plt.plot(xe, ye, color=col_stator_out)

##############################################################################
# Plot de la spire 1
spires = []
def spire(N):
    global spires
    plt.figure(1)

    for p1,p2,l,a,t in spires:
        p1.remove()
        p2.remove()
        if l is not None:
            l.remove()
        if a is not None:
            a.remove()
        if t is not None:
            t.remove()

    theta0 = np.pi/2

    offset = [np.pi/N * k for k in range(-(N-1)//2, +(N+1)//2)]
    theta = theta0 - np.array(offset)

    r_spire = r_rotor+e_entrefer+e_stator/2

    x = r_spire * np.cos(theta)
    y = r_spire * np.sin(theta)
    p1, = plt.plot(x, y, marker='o', markersize=10, color=col_spire, ls='None')
    p2, = plt.plot(-x, -y, marker='x', markersize=10, color=col_spire, ls='None')

    l = None
    a = None
    t = None
    if N == 1:
        theta = np.linspace(-np.pi/2, +np.pi/2, 50)

        x = (r_spire) * np.cos(theta)
        y = (r_spire) * np.sin(theta)

        l, = plt.plot(x, y, color=col_spire)

        # Flèche sur la spire
        i = len(x)//2
        a = l.axes.annotate('', xytext=(x[i], y[i]), xy=(x[i-1], y[i-1]), arrowprops=dict(color=col_spire, headwidth=10))
        t = plt.text(x[i], y[i+1], r'$i$', color=col_spire, ha='left', va='bottom', fontsize=12)

    spires = []
    spires.append((p1, p2, l, a, t))

##############################################################################
# Plot de l'angle theta
def angle_theta():
    plt.figure(1)

    # axe x
    ylim = plt.ylim()
    plt.plot([0, ylim[1]], [0, 0], color='k', linestyle='dashed')
    plt.text(ylim[1]-0.2, 0.15, r'$x$', color='k', ha='left', va='center', fontsize=12)

    # axe theta
    theta = 0.6
    x = ylim[1] * np.cos(theta)
    y = ylim[1] * np.sin(theta)
    plt.plot([0, x], [0, y], color='k', linestyle='dotted')

    # angle theta
    theta = np.linspace(0, theta, 100)
    x = (r_rotor+e_entrefer/2) * np.cos(theta)
    y = (r_rotor+e_entrefer/2) * np.sin(theta)
    l, = plt.plot(x, y, color='k', linestyle='dotted')
    i = 3*len(x)//4
    plt.text(x[i], y[i+1], r'$\theta$', color='k', ha='right', va='top', fontsize=12)

    #axe y
    ylim = plt.ylim()
    plt.plot([0, 0], [0, ylim[1]], color='k', linestyle='dashed')
    plt.text(0.15, ylim[1]-0.25, r'$y$', color='k', ha='center', va='bottom', fontsize=12)

##############################################################################
# Plot des lignes de champ
def champ():
    plt.figure(1)

    theta = np.linspace(-np.pi/2, np.pi/2, N_champ)[1:-1]
    bx = (r_rotor+champ_offset) * np.cos(theta)
    by = (r_rotor+champ_offset) * np.sin(theta)
    hx = (e_entrefer-2*champ_offset) * np.cos(theta)
    hy = (e_entrefer-2*champ_offset) * np.sin(theta)
    plt.quiver(bx, by, hx, hy, units='xy', scale_units='xy', scale=1, color=col_champ)

    theta = np.linspace(np.pi/2, 3*np.pi/2, N_champ)[1:-1]
    bx = (r_rotor+e_entrefer-champ_offset) * np.cos(theta)
    by = (r_rotor+e_entrefer-champ_offset) * np.sin(theta)
    hx = - (e_entrefer-2*champ_offset) * np.cos(theta)
    hy = - (e_entrefer-2*champ_offset) * np.sin(theta)
    plt.quiver(bx, by, hx, hy, units='xy', scale_units='xy', scale=1, color=col_champ)

    hx = hx[-1]
    hy = hy[-1]
    bx = bx[-1]
    by = by[-1]
    plt.text(bx+hx, by+hy, r'$B$', color=col_champ, ha='left', va='top', fontsize=12)

##############################################################################
# Plot du coutour d'Ampère
def ampere():
    plt.figure(1)

    r_amp_out = r_rotor+e_entrefer+e_stator-inset_ampere
    r_amp_in = r_rotor-inset_ampere
    X = np.cos(np.pi/2 - Theta_ampere) * np.linspace(r_amp_in, r_amp_out, 50)
    Y = np.sin(np.pi/2 - Theta_ampere) * np.linspace(r_amp_in, r_amp_out, 50)
    x = (r_amp_out) * np.cos(np.linspace(np.pi/2 - Theta_ampere, np.pi/2 + Theta_ampere, 50))
    y = (r_amp_out) * np.sin(np.linspace(np.pi/2 - Theta_ampere, np.pi/2 + Theta_ampere, 50))
    X = np.append(X, x)
    Y = np.append(Y, y)
    x = np.cos(np.pi/2 + Theta_ampere) * np.linspace(r_amp_in, r_amp_out, 50)
    y = np.sin(np.pi/2 + Theta_ampere) * np.linspace(r_amp_in, r_amp_out, 50)
    X = np.append(X, x)
    Y = np.append(Y, y)
    x = (r_amp_in) * np.cos(np.linspace(np.pi/2 + Theta_ampere, np.pi/2 - Theta_ampere, 50))
    y = (r_amp_in) * np.sin(np.linspace(np.pi/2 + Theta_ampere, np.pi/2 - Theta_ampere, 50))
    i = len(X) + len(x)//2
    X = np.append(X, x)
    Y = np.append(Y, y)

    l, = plt.plot(X, Y, color=col_ampere)

    plt.text(X[i+7], Y[i+7], r'$\mathcal{L}$', color=col_ampere, ha='right', va='bottom', fontsize=12)
    plt.annotate('', xytext=(X[i], Y[i]), xy=(X[i+1], Y[i+1]), arrowprops=dict(color=col_ampere, headwidth=10))

##############################################################################
# Plot de B=f(theta)
figure2_setup = False
def champ_theta(N):
    plt.figure(2)

    global figure2_setup

    if figure2_setup is False:
        axx = plt.axvline(0, color='grey', lw=1)
        axy = plt.axhline(0, color='grey', lw=1)

        ticks = [n*np.pi/2 for n in range(-2, 8)]
        labels = [r'${}\pi/2$'.format(n) for n in range(-2, 8)]
        plt.xticks(ticks, labels=labels)
        plt.yticks([-1, 0, 1], [r'$-B_0$', r'$0$', r'$B_0$'])

        plt.xlabel(r'$\theta$')
        plt.xlim(-np.pi-0.1, 5*np.pi/2+0.1)

        plt.gca().set_aspect('equal')
    else:
        for l in figure2_setup:
            l.remove()

    def B0(angle):
        B = []
        for a in angle:
            if a < -np.pi/2:
                B = np.append(B, -1)
            elif a < np.pi/2:
                B = np.append(B, 1)
            else:
                B = np.append(B, -1)
        return np.asarray(B)

    angle0 = np.linspace(-np.pi, np.pi, 1000, endpoint=False)
    B = np.zeros_like(angle0)
    for k in range(-(N-1)//2, +(N+1)//2):
        offset = k * np.pi/N
        B = B + B0(angle0 - offset)

    B = B / np.max(B)

    l1, = plt.plot(angle0, B, color='blue')
    l2, = plt.plot(angle0 + 2*np.pi, B, color='blue', linestyle='dotted')
    figure2_setup = (l1, l2)

##############################################################################
rotor()
wait([1])

stator()
wait([1])

spire(1)
wait([1])

angle_theta()
wait([1])

champ()
wait([1])

ampere()
wait([1])

champ_theta(1)
wait([2])

spire(3)
champ_theta(3)
wait([1, 2])

spire(7)
champ_theta(7)
wait([1, 2])

##############################################################################
# Save
plt.figure(1)
if not savefig:
    plt.ioff()
pdf.close()
imgen.done(__file__)
