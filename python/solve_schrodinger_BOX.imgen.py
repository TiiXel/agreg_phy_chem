#!/usr/env python
# -*- coding: utf-8 -*-
import imgen
imgen.setup()

import numpy as np
# Paramètres du problème
mm = 1
args = {'a': 1, 'mm': mm, 'x0': 0}

x_min = -np.pi
x_max = +np.pi
N = 700

# Potentiel
def BOX(X, args):
    a = args['a']
    x0 = args['x0']

    U = np.zeros_like(X)
    for n in range(len(X)):
        x = X[n]
        if (x < -a or x > a):
            U[n] = 100

    return U

import solve_schrodinger
# Résolution
E, Y = solve_schrodinger.solve(x_min, x_max, N, mm, BOX, args)
NN = 8
E = E[:NN]
Y = Y[:NN]

import matplotlib.pyplot as plt
# Affichage
solve_schrodinger.plot(x_min, x_max, N, E, Y, BOX, args)

# Save
imgen.done(__file__)
