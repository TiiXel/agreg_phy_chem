#!/usr/env python3
# -*- coding: utf-8 -*-
import imgen
imgen.setup()

import numpy as np
# Paramètres du problème  [ref: BUP 744]
omega = 1  # omega = sqrt(1/LC)
tau = 0.2  # 1/tau = (Rg + R)/L
Q = -20  # omega/Q = (Rg - R)/L
args = {'omega': omega, 'tau': tau, 'Q': Q}

x_min = -2
x_max = +2
y_min = -2
y_max = +2
Nx = Ny = 20

# Système pour l'équadiff
def d_OH(XV, dt, t, args):
    x = XV[0]
    v = XV[1]
    omega = args['omega']
    Q = args['Q']
    tau = args['tau']

    dxadt = v
    if np.abs(x) <= 1:
        dvadt = - omega/Q * v - omega**2 * x
    else:
        dvadt = - 1/tau * v - omega**2 * x

    return dxadt*dt, dvadt*dt

import portrait_de_phase
import matplotlib.pyplot as plt
# Plot du portrait de phase
ax = portrait_de_phase.plot(x_min, x_max, Nx, y_min, y_max, Ny, d_OH, args)

# Plot de trajectoires de CI
portrait_de_phase.traj(0.05, 0, 0, 300, 10000, d_OH, args, ax)
plt.legend()

# Save
imgen.done(__file__)
