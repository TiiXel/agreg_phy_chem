#!/usr/env python3
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import numpy as np

l = 5e-4
k = 2*np.pi/l  # nombre d'onde
n = 1/2e-3  # traits/mm
e = 1e-3  # largeur des fentes
N = 1  # nombre de traits éclairés

theta = np.linspace(-np.pi/2, +np.pi/2, 1e5)

def calc_I(theta, n, e, N):
    alpha = np.sin(theta) * k/(n*1e3)/2
    beta = e*n*1e3

    if N != 0:
        s = np.sin(beta*alpha)/(beta*alpha) * (np.sin(N*alpha)/np.sin(alpha))
    else:
        s = np.sin(beta*alpha)/(beta*alpha)
    I = s**2 / (np.max(s**2))

    return I

#######
# Plot
fig = plt.figure()
axp = fig.add_subplot(121, projection='polar')
axx = fig.add_subplot(122)

axx.set_xlabel(r'$\theta$')
axx.set_ylabel(r'$I / I_0$')
axp.set_xlabel(r'$\theta$')
axp.set_ylabel(r'$I / I_0$')

axx.set_xlim(-np.pi/4, +np.pi/4)
axp.set_xlim(-np.pi/2, +np.pi/2)
axx.set_xticks([-np.pi/2, -np.pi/4, 0, np.pi/4, np.pi/2])
axp.set_xticks([-np.pi/2, -np.pi/4, 0, np.pi/4, np.pi/2])
axx.set_xticklabels([r'$-\frac{\pi}{2}$',r'$-\frac{\pi}{4}$','0',r'$+\frac{\pi}{4}$',r'$+\frac{\pi}{2}$'])
axp.set_xticklabels([r'$-\frac{\pi}{2}$',r'$-\frac{\pi}{4}$','0',r'$+\frac{\pi}{4}$',r'$+\frac{\pi}{2}$'])

axx.set_ylim(0, 1)

linex, = axx.plot(theta, calc_I(theta, n, e, N))
linep, = axp.plot(theta, calc_I(theta, n, e, N))

def update_object(_):
    global line, ax
    N = int(sl_N.val)
    # Courbes
    linex.set_data(theta, calc_I(theta, n, e, N))
    linep.set_data(theta, calc_I(theta, n, e, N))
    # Sliders
    sl_N.valtext.set_text('{:1.0f}'.format(N))

ax_N = plt.axes([0.1, 0.10, 0.8, 0.03])
sl_N = Slider(ax_N, '$N$', 1, 3000, N)
sl_N.on_changed(update_object)

plt.subplots_adjust(bottom=0.2)

plt.show()
