#!/usr/env python3
# -*- coding: utf-8 -*-
import imgen
imgen.setup()

import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import numpy as np
from labellines import labelLine, labelLines

fig, ax = plt.subplots(2, 1, sharex=True)
ax[1].set_xlabel(r'$x$')
ax[0].set_ylabel(r'$|H|$')
ax[1].set_ylabel(r'arg$H$')
ax[1].yaxis.set_major_formatter(tck.FuncFormatter(lambda v,p: r'{:.0g}$\pi$'.format(v/np.pi) if v != 0 else r'$0$'))
ax[1].yaxis.set_major_locator(tck.MultipleLocator(base=np.pi/4))
ax[1].set_ylim(-np.pi, 0.1)

def FT(x):
    return 1/(1 + 1j*X/q - X**2)

X = np.linspace(0, 2, 3000)
Qplot = [0.1, 0.3, 0.6, 0.8, 1, 1.5, 2]
Qspace = np.linspace(0, 2.5, 3000)

for q in Qplot:
    H = FT(X)
    A = np.absolute(H)
    P = np.angle(H)
    ax[0].plot(X, A, label=r'$Q = {}$'.format(q))
    col = ax[0].get_lines()[-1].get_color()
    ax[1].plot(X, P, color=col)
    imax = np.argmax(A)
    if imax != 0:
        ax[0].plot(X[imax], A[imax], linestyle='None', marker='o', color=col)

Xr = []
Ar = []
for q in Qspace:
    A = np.absolute(FT(Xr))
    imax = np.argmax(A)
    Xr.append(X[imax])
    Ar.append(A[imax])
ax[0].plot(Xr, Ar, linestyle='dashed', color='k')

ax[0].axvline(1, color='k')
ax[1].axvline(1, color='k')
ax[0].legend()

plt.tight_layout()

# Save
imgen.done(__file__)
