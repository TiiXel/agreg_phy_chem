#!/usr/env python3
# -*- coding: utf-8 -*-
import imgen
args = imgen.setup()
savefig = args.save

import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import numpy as np

l = 5e-7
k = 2*np.pi/l  # nombre d'onde
n = 300  # traits/mm
e = 1e-6  # largeur des fentes
N = 10  # nombre de traits éclairés

theta = np.linspace(-np.pi/2, +np.pi/2, 1e5)

def calc_I(theta, n, e, N):
    alpha = np.sin(theta) * k/(n*1e3)/2
    beta = e*n*1e3

    if N != 0:
        s = np.sin(beta*alpha)/(beta*alpha) * (np.sin(N*alpha)/np.sin(alpha))
    else:
        s = np.sin(beta*alpha)/(beta*alpha)
    I = s**2 / (np.max(s**2))

    return I

#######
# Plot
fig, ax = plt.subplots()

def set_orders(ax, n, e, N):
    orders = np.arange(-20, 20, 1)
    orders = orders[np.where(np.abs(orders * l*n*1e3) < 1)]
    posx = np.arcsin(orders * l*n*1e3) + 1e-3  # +1e-3 pour éviter posx=0
    posy = calc_I(posx, n, e, N) + 0.01
    lab = [r'$%s$'%p for p in orders]
    for text in ax.texts:
        text.set_visible(False)
        text.remove()
    for i in range(len(orders)):
         ax.text(posx[i], posy[i], lab[i], horizontalalignment='center')

ax.set_xlabel(r'$\theta$')
ax.set_ylabel(r'$I / I_0$')

set_orders(ax, n, e, N)
ax.set_xlim(-np.pi/2, +np.pi/2)
ax.set_xticks([-np.pi/2, -np.pi/4, 0, np.pi/4, np.pi/2])
ax.set_xticklabels([r'$-\frac{\pi}{2}$',r'$-\frac{\pi}{4}$','0',r'$+\frac{\pi}{4}$',r'$+\frac{\pi}{2}$'])

line, = ax.plot(theta, calc_I(theta, n, e, N))
lline, = ax.plot(theta, calc_I(theta, n, e, 0), linestyle='dashed')

def update_object(_):
    global line, ax
    n = int(sl_n.val)
    e = sl_e.val * 1e-6
    N = int(sl_N.val)
    # Courbes
    line.set_data(theta, calc_I(theta, n, e, N))
    lline.set_data(theta, calc_I(theta, n, e, 0))
    set_orders(ax, n, e, N)
    # Sliders
    sl_n.valtext.set_text('{:1.0f}'.format(n))
    sl_N.valtext.set_text('{:1.0f}'.format(N))

if not savefig:
    ax_n = plt.axes([0.1, 0.20, 0.8, 0.03])
    ax_e = plt.axes([0.1, 0.15, 0.8, 0.03])
    ax_N = plt.axes([0.1, 0.10, 0.8, 0.03])
    sl_n = Slider(ax_n, '$n$ (tr/mm)', 300, 500, n)
    sl_e = Slider(ax_e, '$\epsilon$ (um)', 0.1, 5, e*1e6)
    sl_N = Slider(ax_N, '$N$', 2, 10, N)
    sl_n.on_changed(update_object)
    sl_e.on_changed(update_object)
    sl_N.on_changed(update_object)

    plt.subplots_adjust(bottom=0.35)
# Save
imgen.done(__file__)
