#!/usr/env python
# -*- coding: utf-8 -*-
from dependencies.wavelen2rgb import wavelen2rgb
# http://www.johnny-lin.com/py_refs/wavelen2rgb.html
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import Tkinter as tk
import random
import sys
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
from matplotlib.widgets import Slider
from scipy.constants import *

u = np.arange(-3, 10, 0.01)  # V
l_min = 100
l_max = 800

l_seuil = 650  # nm
def show(l, I, line=None):
    u_seuil = - (l_max - l) * 3./l_max
    if l > l_seuil:
        '''i = 0 au delas de la longueur d'onde seuil'''
        i = [0 for x in u]
    else:
        '''en dessous, i prop à 1/l, I, et suit une loi en 1 - exp(u-u_s)'''
        i = [1./l * I * (1 - np.exp(- (x - u_seuil))) * 10
                if x > u_seuil
                else 0
                for x in u]
    if line is None:
        line, = ax.plot(u, i)
    else:
        line.set_xdata(u)
        line.set_ydata(i)
    c = np.array(wavelen2rgb(l, MaxIntensity = 255)) / 255.
    line.set_color(c)
    ax.set_xlim(-3, 10)
    ax.set_ylim(-1, 3)
    return line

fig, ax = plt.subplots()
fig.subplots_adjust(bottom=0.3)
ax.set_xlabel(u'Tension (V)')
ax.set_ylabel(u'Intensité (A)')
ax.set_title(u'Expérience de Hallwachs')
ax.grid(axis='both')

obj = None
def update_object(_):
    global obj
    l = s_l.val
    I = s_I.val
    if obj is None:
        obj = show(l, I)
    else:
        obj = show(l, I, line=obj)

l0 = show(350, 90)
l1 = show(400, 90)
l2 = show(500, 90)
l3 = show(600, 90)
l4 = show(700, 90)

ax_l = plt.axes([0.15, 0.10, 0.7, 0.03])
ax_I = plt.axes([0.15, 0.15, 0.7, 0.03])
s_l = Slider(ax_l, u'Long onde', l_min, l_max, 700, '%3.0f nm')
s_I = Slider(ax_I, u'Intensité', 0, 100, 90, '%3.0f %%')

s_l.on_changed(update_object)
s_I.on_changed(update_object)

plt.show()
