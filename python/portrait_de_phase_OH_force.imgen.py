#!/usr/env python
# -*- coding: utf-8 -*-
import imgen
imgen.setup()

import numpy as np
# Paramètres du problème
f = 0.3
omega = 2*np.pi*f
Q = 3
gamma = 2
omega_m = 1*np.pi*f
args = {'omega': omega, 'Q': Q, 'gamma': gamma, 'omega_m': omega_m}

x_min = -np.pi
x_max = +np.pi
y_min = -10
y_max = +10
Nx = Ny = 20

# Système pour l'équadiff
def d_OH(XV, dt, t, args):
    x = XV[0]
    v = XV[1]
    omgea = args['omega']
    Q = args['Q']
    gamma = args['gamma']
    omega_m = args['omega_m']

    dxadt = v
    dvadt = - omega/Q * v - omega**2 * x + gamma * np.cos(omega_m*t)

    return dxadt*dt, dvadt*dt

import portrait_de_phase
import matplotlib.pyplot as plt
# Plot du portrait de phase
fig, ax = plt.subplots(1, 2)
ax0 = ax[0]
ax1 = ax[1]
portrait_de_phase.plot(x_min, x_max, Nx, y_min, y_max, Ny, d_OH, args, ax0)
portrait_de_phase.plot(x_min, x_max, Nx, y_min, y_max, Ny, d_OH, args, ax[1])

# Plot de trajectoires de CI
portrait_de_phase.traj(3, 0, 0, 10*Q/f, 10000, d_OH, args, ax0)
portrait_de_phase.traj(0, 3, 0, 10*Q/f, 10000, d_OH, args, ax[1])
ax0.legend()
ax1.legend()

# Save
imgen.done(__file__)
