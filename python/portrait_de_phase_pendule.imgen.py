#!/usr/env python
# -*- coding: utf-8 -*-
import imgen
imgen.setup()

import numpy as np
# Paramètres du problème
f = 0.3
omega = 2*np.pi*f
Q = 10
args = {'omega': omega, 'Q': Q}

x_min = -np.pi
x_max = +np.pi
y_min = -10
y_max = +10
Nx = Ny = 20

# Système pour l'équadiff
def d_OH(XV, dt, t, args):
    x = XV[0]
    v = XV[1]
    omgea = args['omega']
    Q = args['Q']

    dxadt = v
    dvadt = - omega/Q * v - omega**2 * np.sin(x)

    return dxadt*dt, dvadt*dt

import portrait_de_phase
import matplotlib.pyplot as plt
# Plot du portrait de phase
fig, ax = plt.subplots(1, 3)
portrait_de_phase.plot(x_min, x_max, Nx, y_min, y_max, Ny, d_OH, args, ax[0])
portrait_de_phase.plot(x_min, x_max, Nx, y_min, y_max, Ny, d_OH, args, ax[1])
portrait_de_phase.plot(x_min, x_max, Nx, y_min, y_max, Ny, d_OH, args, ax[2])

# Plot de trajectoires de CI
portrait_de_phase.traj(1, 0, 0, 2*Q/f, 10000, d_OH, args, ax[0])
portrait_de_phase.traj(3, 0, 0, 2*Q/f, 10000, d_OH, args, ax[1])
portrait_de_phase.traj(2, 3, 0, 2*Q/f, 10000, d_OH, args, ax[2])
ax[0].legend()
ax[1].legend()
ax[2].legend()

# Save
imgen.done(__file__)
