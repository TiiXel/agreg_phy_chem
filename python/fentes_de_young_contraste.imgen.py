#!/usr/env python3
# -*- coding: utf-8 -*-
##############################################################################
import imgen
imgen.setup()

##############################################################################
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
from dependencies.wavelen2rgb import wavelen2rgb
import colorsys
######
# Paramètres (unité de longueur = mm)
######
l = 589.e-6
l = 600.e-6
D = .5e3
a = 1.
i = l * D / a
rgb = np.asarray(wavelen2rgb(l*1e6, 255)) / 255.
hsv = colorsys.rgb_to_hsv(rgb[0], rgb[1], rgb[2])
hue = hsv[0]
sat = hsv[1]

# Axe des positions sur l'écran (vecteur ligne)
Nx = 1000
x_max = +4*i
x_min = -x_max
X = np.reshape(np.linspace(x_min, x_max, Nx), (1, Nx))
# Axe de contraste (vecteur colonne)
NC = 100
C = np.reshape(np.linspace(0, 1, NC), (NC, 1))

# HSV Value (= intensité) pour chaque point de l'espace (X,Constraste)
VAL = .5*(1 + np.matmul(C, np.cos(2*np.pi*X/i)))  # np.shape(VAL) = (NC, Nx)
# Couleur RGB associée
COL = np.zeros((NC, Nx, 3))
for index, val in np.ndenumerate(VAL):
    COL[index[0]][index[1]] = colorsys.hsv_to_rgb(hue, sat, val)

# Plot
fig, axes = plt.subplots(2, 1)
axes[0].set_title(r"$\lambda = %3.0f$nm, $a = %1.2f$mm, $D = %1.1f$m : $i = %1.2f$mm" % (l*1e6, a, D*1e-3, i))
# Aspect
axes[0].imshow(COL, extent=[x_min, x_max, 0, 1], origin='lower', aspect='auto')
axes[0].set_xlabel(r"Aspect de la figure, position $x$ (mm)")
axes[0].set_ylabel(r"Contraste $C$")
axes[0].set_xlim(x_min, x_max)
axes[0].xaxis.set_major_locator(ticker.MultipleLocator(i))
axes[0].xaxis.set_major_formatter(ticker.FormatStrFormatter(r"%1.2f"))
# Courbes
axes[1].plot(X[0], VAL[int(NC-1)])
axes[1].plot(X[0], VAL[int((NC-1)/2)])
axes[1].plot(X[0], VAL[int((NC-1)/10)])
axes[1].set_xlabel(r"Position sur l'écran $x$ (mm)")
axes[1].set_ylabel(r"Intensité relative $\frac{I(x)}{2 I_0}$")
axes[1].legend([r"%1.1f" % C[int(NC-1)],
    r"%1.1f" % C[int((NC-1)/2)],
    r"%1.1f" % C[int((NC-1)/10)]])
axes[1].set_xlim(x_min, x_max)
axes[1].xaxis.set_major_locator(ticker.MultipleLocator(i))
axes[1].xaxis.set_major_formatter(ticker.FormatStrFormatter(r"%1.2f"))
fig.tight_layout()

##############################################################################
# Save
imgen.done(__file__)
