#!/usr/env python3
# -*- coding: utf-8 -*-
import imgen
imgen.setup()

import matplotlib.pyplot as plt
import numpy as np
from numpy import tanh, cosh, log

fig, ax = plt.subplots(2, 1, sharex=True)

W = np.logspace(0, 10, 100)

mu0 = 1e5
wALI = 100

for B in [0, 1/2, 10, 20, 40]:
    r = 1 + mu0 * B
    G = mu0 / r
    wc = wALI * r

    T = G / (1 + 1j*W / wc)
    Tc = G / (1 + 1j)

    lab = 'B = {:1.1f}'.format(B)

    ax[0].loglog(W, np.abs(T), label=lab)
    col = ax[0].get_lines()[-1].get_color()
    ax[0].loglog([wc, wc], [np.abs(Tc), 0], color=col, linestyle='dashed')
    ax[1].plot(W, np.angle(T), label=lab, color=col)
    ax[1].axvline(x=wc, color=col, linestyle='dashed')

ax[1].set_xlabel(r'$\omega$')
ax[0].set_ylabel(r'$|T|$')
ax[0].legend()
ax[1].set_ylabel(r'arg($T$)')
ax[1].set_yticks([-np.pi/2, -np.pi/4, 0])
ax[1].set_yticklabels([r'$-\frac{\pi}{2}$', r'$-\frac{\pi}{4}$', '0'])

# Save
imgen.done(__file__)

