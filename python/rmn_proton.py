import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.animation import FuncAnimation
from matplotlib.widgets import Button

##############################################################################
# Initialisation de pyplot
##############################################################################
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.view_init(azim=45, elev=27)
ax.set_xlim(-2, 2)
ax.set_ylim(-2, 2)
ax.set_zlim(-2, 2)
ax.set_aspect('equal')

x, y, z = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
ax.plot([0, 1], [0, 0], [0, 0], linestyle='dashed', color='k')
ax.plot([0, 0], [0, 1], [0, 0], linestyle='dashed', color='k')
ax.plot([0, 0], [0, 0], [0, 1], linestyle='dashed', color='k')
u, v, w = 0.5*np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
ax.quiver(x, y, z, u, v, w, color='k')
ax.text(1.8, 0, 0, r'$x$', color='k')
ax.text(0, 1.6, 0, r'$y$', color='k')
ax.text(0, 0, 1.6, r'$z$', color='k')

# Oxy
theta = np.linspace(-1, 3*np.pi/4, 100)
phi = np.linspace(0, np.pi, 100)
x = np.sin(theta)
y = np.cos(theta)
z = np.zeros(len(theta))
ax.plot(x, y, z, color='k')
# Ozx
theta = np.linspace(-1, 3*np.pi/4, 100)
phi = np.linspace(0, np.pi, 100)
x = np.zeros(len(theta))
y = np.sin(theta)
z = np.cos(theta)
ax.plot(x, y, z, color='k')
# Ozy
theta = np.linspace(-1, 3*np.pi/4, 100)
phi = np.linspace(0, np.pi, 100)
x = np.sin(theta)
y = np.zeros(len(theta))
z = np.cos(theta)
ax.plot(x, y, z, color='k')

# Dessin du vecteur moment cinétique
#LO = [np.random.normal(0, 1) for i in range(3)]
LO = np.array([1, 3, 3])
LO = LO / np.sqrt(np.sum(c**2 for c in LO))
LO_hist = np.array([LO])
LO_line = ax.quiver(0, 0, 0, *LO, color='b')
LO_shad = ax.quiver(0, 0, 0, LO[0], LO[1], 0, color='grey')
LO_text = ax.text(*LO, r'$L_O$', color='b')
LO_pers, = ax.plot([], [], [], color='b')

# Dessin du vecteur champ B0
B0_base = np.array([0, 0, 2])
B0 = np.array([0, 0, 1])
B0_line = ax.quiver(*B0_base, *B0, color='r')
B0_text = ax.text(*(B0_base + B0), r'$B_0$', color='red')

# Dessin du vecteur champ B1
B1 = np.array([0, 0, 0])
B1_line = ax.quiver(*B0_base, *B1, color='r')
B1_text = ax.text(*(B0_base + B1), r'$b_1$', color='r')
B1_pers, = ax.plot([], [], [], color='r')

wB1 = 1
B1_cooldown = 0

##############################################################################
# Fonction pour pivoter un vecteur
##############################################################################
def rotate_vec(V, w, axe, hist=None):

    if hist is not None:
        if len(hist) > 200:
            hist = np.delete(hist, 0, axis=0)
        hist = np.append(hist, [V], axis=0)
    else:
        hist = []

    # Le vecteur autour duquel tourner doit être unitaire
    x, y, z = axe / np.sqrt(np.sum(c**2 for c in axe))

    # Angle de rotation
    a = w * dt

    # Matrice de rotation
    c = np.cos(a)
    s = np.sin(a)
    t = 1-c
    R = np.array([
        [t*x*x+c, t*x*y-s*z, t*x*z+s*y],
        [t*x*y+s*z, t*y*y+c, t*y*z-s*x],
        [t*x*z-s*y, t*y*z+s*x, t*z*z+c]])

    # Calcul du nouveau vecteur
    V = np.matmul(V, R)
    return V, hist

dt = 1e-1
##############################################################################
# Fonction pour avancer de dt
##############################################################################
def step():
    #####################################################
    # Mise à jour de B1 (rotation)
    global B1, B1_cooldown
    B1_cooldown -= dt

    if B1_cooldown > 0:
        if np.array_equal(B1, np.array([0, 0, 0])):
            B1 = np.array([0, 1e-1, 0])
        else:
            B1, B1_hist = rotate_vec(B1, wB1, [0, 0, 1])
    else:
        B1_cooldown = 0
        B1 = np.array([0, 0, 0])

    #####################################################
    # Mise à jour de LO (rotation)
    global LO, LO_hist
    # Champ total
    B = B0 + B1

    # Vitesse de rotation
    w = np.sqrt(np.sum(c**2 for c in B)) * np.sqrt(np.sum(c**2 for c in LO))
    LO, LO_hist = rotate_vec(LO, w, B, LO_hist)

    # Mise à jour de LO (relaxation)
    w = (LO[2] - 1) / 100
    v = LO - LO_hist[-1]
    LO, LO_hist = rotate_vec(LO, w, v, LO_hist)

##############################################################################
# Bouttons
##############################################################################
def relax_LO():
    global LO
    LO = np.array([0.001, 0.001, 1])
    LO = LO / np.sqrt(np.sum(c**2 for c in LO))

def activate_b1(cooldown):
    global B1_cooldown
    B1_cooldown = cooldown

relax = Button(plt.axes([0.9, 0, 0.1, 0.1]), 'relax')
relax.on_clicked(lambda x:  relax_LO())

demo = Button(plt.axes([0, 0, 0.1, 0.1]), 'b1, demo')
demo.on_clicked(lambda x, CD=0.26*np.pi/(2*wB1)/dt: activate_b1(CD))

perfect = Button(plt.axes([0.2, 0, 0.1, 0.1]), 'b1, rmn')
perfect.on_clicked(lambda x, CD=np.pi/(2*wB1)/dt: activate_b1(CD))

##############################################################################
# Affichage
##############################################################################
def animate(i):
    fig.suptitle("t = {:2.4f} \n b1: {:2.4f}".format(i*dt, B1_cooldown))
    step()

    global B1_line, B1_shad, B1_text
    B1_line.remove()
    B1_text.remove()
    B1_line = ax.quiver(*B0_base, *(B1*5), color='r')
    B1_text = ax.text(*(B0_base + B1), r'$b_1$', color='r')

    global LO_line, LO_shad, LO_text
    LO_line.remove()
    LO_shad.remove()
    LO_text.remove()
    LO_line = ax.quiver(0, 0, 0, *LO, color='b')
    LO_shad = ax.quiver(0, 0, 0, LO[0], LO[1], 0, color='grey')
    LO_text = ax.text(*LO, r'$L_0$', color='b')
    LO_pers.set_data(LO_hist[:, 0], LO_hist[:, 1])
    LO_pers.set_3d_properties(LO_hist[:, 2])

dt_anim = int(np.round(dt*1e3) + 0.1)
anim = FuncAnimation(fig, animate, interval=1)

plt.show()
