#!/usr/env python
# -*- coding: utf-8 -*-
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import Tkinter as tk
import random
import sys
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
from matplotlib.widgets import Slider
from scipy.constants import *

l = np.arange(100, 5000, 10) * 1e-9  # m
x = l * 1e9  # nm
def show(T, line=None):
    u = 2*pi*h*(c**2) / (l**5 * (np.exp(h*c / (l*k*T)) - 1))  # J/s/m2/m
    if line is None:
        line, = ax.plot(x, u)
    else:
        line.set_xdata(x)
        line.set_ydata(u)
    ax.relim()
    if T < 3500:
        ax.set_xlim(100, 5000)
    else:
        ax.set_xlim(100, 1000)
    ax.autoscale_view()
    return line

fig, ax = plt.subplots()
fig.subplots_adjust(bottom=0.3)
ax.set_xlabel(u'Longueur donde (nm)')
ax.set_ylabel(u'Intensité spectrale (W/m2/m)')
ax.set_title(u'Spectre du corps noir')

# Arc-en-ciel RGB :
# https://scipython.com/book/chapter-7-matplotlib/examples/a-depiction-of-the-electromagnetic-spectrum/
rainbow_rgb = { (400, 440): '#8b00ff', (440, 460): '#4b0082',
                (460, 500): '#0000ff', (500, 570): '#00ff00',
                (570, 590): '#ffff00', (590, 620): '#ff7f00',
                (620, 750): '#ff0000'}
for wv_range, rgb in rainbow_rgb.items():
    ax.axvspan(*wv_range, color=rgb, ec='none', alpha=1)

obj = None
def update_object(_):
    global obj
    T = s_T.val
    if obj is None:
        obj = show(T)
    else:
        obj = show(T, line=obj)

ax_T = plt.axes([0.17, 0.10, 0.7, 0.03])
s_T = Slider(ax_T, u'Température', 30, 10000, 1, '%5.0f K')

s_T.on_changed(update_object)

plt.show()
