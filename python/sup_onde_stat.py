#!/usr/env python3
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

L = 10
x_space = np.linspace(0, L, 1000)

c = 0.1

def mode(N, t=0):
    return np.sin(N * np.pi/L * x_space) * np.sin(N * np.pi/L * t / c) / N

modes = np.asarray([1, 3, 10])

fig, ax = plt.subplots(len(modes)+1, sharex=True)
ax[-1].set_xlabel(r'$x$')
lines = []

onde = np.zeros(len(x_space))

for i in range(len(modes)):
    ax[i].set_ylim(-1, 1)
    ax[i].set_ylabel(r'Mode {}'.format(modes[i]))
    line, = ax[i].plot(x_space, mode(modes[i], 0))
    lines.append(line)
    onde += mode(modes[i], 0)
ax[-1].set_ylim(-1, 1)
ax[-1].set_ylabel(r'Somme')
line, = ax[-1].plot(x_space, onde)
lines.append(line)

dt = 40  # ms
def animate(i):
    t = dt*i*1e-3  # s
    fig.suptitle(r'$t = {:.2f}$ s'.format(t))
    onde = np.zeros(len(x_space))
    for i in range(len(modes)):
        lines[i].set_data(x_space, mode(modes[i], t))
        onde += mode(modes[i], t)
    lines[-1].set_data(x_space, onde)

anim = FuncAnimation(plt.gcf(), animate, interval=dt)

plt.show()
