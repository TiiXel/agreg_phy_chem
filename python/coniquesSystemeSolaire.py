#!/usr/env python3
# -*- coding: utf-8 -*-
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import tkinter as tk
import random
import sys
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
from matplotlib.widgets import Slider

theta = np.arange(0, 2*np.pi, 0.01)
def show(e, p, color=None, line=None, label=None, fill=False):
    """Compute the y(x) trajectory given e and p"""
    r = [r0
            if 0 < r0 and r0 < 10 or e < 1
            else np.inf
            for r0 in p / (1.0 + e * np.cos(theta))]
    x = r * np.cos(theta)
    y = r * np.sin(theta)
    if color is None:
        color = 'black'
    if line is None:
        if not fill:
            line, = ax.plot(x, y, color=color, label=label)
        else:
            line, = ax.fill(x, y, color=color, label=label)
    else:
        if not fill:
            line.set_xdata(x)
            line.set_ydata(y)
        else:
            line.remove()
            line, = ax.fill(x, y, color=color, label=label)
    ax.legend(loc=3)
    ax.relim()
    ax.autoscale_view()
    return line


fig, ax = plt.subplots()
fig.subplots_adjust(bottom=0.3)
ax.set_xlabel('UA')
ax.set_ylabel('UA')
ax.set_title(u'Orbites des planètes internes du système solaire')

sun = show(0.0, 0.0046, color='firebrick', fill=True)

mercury = show(0.205, 0.307, color='gray', label='Mercure, $e=0.205, p=0.307$')
venus   = show(0.007, 0.718, color='yellow', label=u'Vénus, $e=0.007, p=0.718$')
earth   = show(0.017, 0.983, color='blue', label='Terre, $e=0.017, p=0.983$')
mars    = show(0.093, 1.382, color='red', label='Mars, $e=0.093, p=1.382$')
#jupiter = show(0.049, 4.950, color='orange', label='Jupiter, $e=0.049, p=4.950$')
#saturn  = show(0.057, 9.041, color='gold', label='Saturn, $e=0.057, p=9.041$')
#uranus  = show(0.046, 18.33, color='blue', label='Uranus, $e=0.046, p=18.33$')
#neptune = show(0.009, 29.81, color='lightblue', label='Neptune, $e=0.009, p=29.81$')

ax.set_aspect('equal', 'datalim')

obj = None
def update_object(_):
    global obj
    e = s_e.val
    p = s_p.val
    if obj is None:
        obj = show(e, p, color='black')
    else:
        obj = show(e, p, line=obj)

ax_e = plt.axes([0.1, 0.10, 0.8, 0.03])
ax_p = plt.axes([0.1, 0.15, 0.8, 0.03])
s_e = Slider(ax_e, 'e', 0.000, 1.500, 0.01)
s_p = Slider(ax_p, 'p', 0.100, 3.00, 0.01)

s_p.on_changed(update_object)
s_e.on_changed(update_object)

plt.show()
