#!/usr/env python3
# -*- coding: utf-8 -*-
import imgen
imgen.setup()

import matplotlib.pyplot as plt
import numpy as np
from numpy import tanh, cosh, log
from labellines import labelLine, labelLines

fig, ax = plt.subplots(2, sharex=True)

a = 0.01
c = 3e8
wc1 = np.pi * c / a
w = np.linspace(0, 7*wc1, 5000)

for p in range(1, 6):
    y = np.sqrt((w/(p*wc1))**2 - 1)
    kz = (p*wc1) / c * y
    ax[0].plot(w / 1e9, kz)
    ax[1].plot(w / 1e9, w/kz)

ax[1].set_xlabel(r'$\omega$ / GHz')
ax[0].set_ylabel(r'$k_z$ / (1/m)')
ax[1].set_ylabel(r'$v_\phi$ / (m/s)')

# Save
imgen.done(__file__)
