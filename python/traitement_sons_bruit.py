#!/usr/env python3
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import pyaudio
from matplotlib.widgets import Button

buttons = []
def play(S):
    fs = int(1/(S[1, 0] - S[0, 0]))
    try:
        p = pyaudio.PyAudio()
        try:
            stream = p.open(
                format=pyaudio.paFloat32,
                channels=1,
                rate=fs,
                output=True)
            stream.write(S[:, 1].astype(np.float32))
        finally:
            stream.stop_stream()
            stream.close()
    finally:
        p.terminate()

#############################################################################
# Création du signal
plt.figure(0)
#############################################################################
fs0 = 44100  # Freq d'échantillonage
f0 = 440.0  # Freq fondamentale
d0 = 10.0  # Durée du signal
nf0plot = 5  # Durée du signal pour les plots

TF0 = np.array([(f0, 1, 0)])#, (5*f0, 0.3, 0)])
noise = None
def sample_S(fs, d=nf0plot*1/f0, TF=TF0, noise_amp=0):
    global noise
    T = np.arange(0, d, 1/fs)
    noise = noise_amp * (np.random.random(len(T)) - 0.5)
    S = noise
    for f,a,p in TF:
        S += a * np.cos(2*np.pi*f*T + p)
    return np.stack((T, S), axis=-1)
S0 = sample_S(fs0)

#############################################################################
# Acquisition du signal
#############################################################################
noise_amp = [0, 0.3]
for i,na in enumerate(noise_amp):
    plt.figure(i)
    nfs1 = 3
    fs1 = nfs1*f0
    S0n = sample_S(fs0, noise_amp=na)
    S1 = sample_S(fs1, noise_amp=na)
    plt.suptitle('fs = %s*f0 = %s Hz, f0 = %s Hz' % (nfs1, fs1, f0))
    # Plot signal
    plt.subplot(411)
    plt.plot(S0n[:, 0], S0n[:, 1])
    if i != 0:
        plt.plot(S1[:, 0], S1[:, 1], linestyle='dotted', color='r')
        plt.stem(S1[:, 0], S1[:, 1], basefmt='None', markerfmt='.r', linefmt='r')
    # Plot TF
    plt.subplot(412)
    if i != 0:
        TF1A = np.abs(np.fft.rfft(S1[:, 1]))
        TF1A = TF1A / np.max(TF1A)
        TF1P = np.angle(np.fft.rfft(S1[:, 1]))
        TF1F = np.fft.rfftfreq(len(S1[:, 1]), 1/fs1)
        plt.stem(TF1F, TF1A, basefmt='None', markerfmt='r.', linefmt='r')
        TF1 = np.stack((TF1F, TF1A, TF1P), axis=-1)
    else:
        plt.stem(TF0[:, 0], TF0[:, 1], basefmt='None', markerfmt='r.', linefmt='r')
        TF1 = TF0
    fplot = 10
    plt.xlim(0, fplot*f0)
    plt.xticks([i*f0 for i in range(fplot)], [0] + ['%s*f0'%i for i in range(1, fplot)])
    # Plot signal reconstruit
    S2 = sample_S(fs0, TF=TF1, noise_amp=1)
    plt.subplot(413)
    plt.plot(S0[:, 0], S0[:, 1], linestyle='dotted')
    if i != 0:
        plt.plot(S1[:, 0], S1[:, 1], linestyle='dotted')
        plt.plot(S2[:, 0], S2[:, 1], color='r')
    # Plot signal reconstruit après filtrage
    TF3 = [TF1[TF1[:, 1].argmax()]]
    S3 = sample_S(fs0, TF=TF3)
    plt.subplot(414)
    plt.plot(S3[:, 0], S3[:, 1], color='r')
    # Boutons
    buttons.append(Button(plt.axes([0, 0, 0.1, 0.1]), 'Play'))
    buttons[-1].on_clicked(lambda x, TFX=TF1: play(sample_S(fs0, 5.0, TF=TFX)))

plt.show()
