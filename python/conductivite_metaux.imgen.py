#!/usr/env python3
# -*- coding: utf-8 -*-
import imgen
imgen.setup()

import matplotlib.pyplot as plt
import numpy as np
import scipy.integrate
from scipy.optimize import curve_fit

Tspace = np.linspace(40, 900, 300)

def modDrude(T, x, y):
    return x * np.sqrt(T) + y
def modLin(T, x, y):
    return x * T + y

# Resistivités (en 10^-8 Ohm m)
# Handbook
Texp =         np.array([     40,     60,    80,   100,   150,   200,   273,   293,   298,   300,  400,  500,  600,  700,  800,   900])
Rexp_Al = np.array([ 0.0181, 0.0959, 0.254, 0.442, 1.006, 1.587, 2.417, 2.650, 2.709, 2.733, 3.87, 4.99, 6.13, 7.35, 8.70, 10.18])
Rexp_Cu = np.array([ 0.0039, 0.0971, 0.215, 0.348, 0.699, 1.046, 1.543, 1.678, 1.712, 1.725, 2.40, 3.09, 3.79, 4.51, 5.26, 6.04])

for data in [('Al', Rexp_Al), ('Cu', Rexp_Cu)]:
    # Get data and error bars
    material = data[0]
    Rexp = data[1]
    # Plot data
    plt.errorbar(Texp, Rexp, label=material, linestyle='None', marker='+')
    col = plt.gca().get_lines()[-1].get_color()  # Récupère la couleur
    # Plot Drude
    popt, pcov = curve_fit(modDrude, Texp, Rexp)
    Rdrude = modDrude(Tspace, *popt)
    plt.plot(Tspace, Rdrude, color=col, linestyle='dotted')
    # Plot A
    popt, pcov = curve_fit(modLin, Texp, Rexp)
    Rlin = modLin(Tspace, *popt)
    plt.plot(Tspace, Rlin, color=col, linestyle='dashed')

plt.plot([], [], label='Drude', linestyle='dotted', color='gray')
plt.plot([], [], label='Affine', linestyle='dashed', color='gray')

plt.xlabel(r'$T$ / K')
plt.ylabel(r'$\rho$ / $10^{-8} \Omega$ m')
plt.legend()

# Save
imgen.done(__file__)
