#!/usr/env python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt

def plot(x_min, x_max, Nx, y_min, y_max, Ny, D, args, ax=None):
    # Préparation de pyplot
    if ax is None:
        fig, ax = plt.subplots()

    x = np.linspace(x_min, x_max, Nx)
    v = np.linspace(y_min, y_max, Ny)

    X, V = np.meshgrid(x, v)

    DX, DV = np.zeros(X.shape), np.zeros(V.shape)

    Nx, Nv = X.shape

    for i in range(Nx):
        for j in range(Nv):
            x = X[i, j]
            v = V[i, j]
            dx, dv = D([x, v], 1, 0, args)
            DX[i, j] = dx
            DV[i, j] = dv

    # Plot du portrait de phase
    ax.quiver(X, V, DX, DV, color='k', lw=1)
    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'$\dot x$')
    ax.axhline(0, color='k', lw=1)
    ax.axvline(0, color='k', lw=1)

    return ax

def traj(x0, v0, t_min, t_max, N, D, args, ax=None):
    # Préparation de pyplot
    if ax is None:
        fig, ax = plt.subplots()

    Xs = [x0]  # X(t)
    Vs = [v0]  # V(t)
    dt = (t_max-t_min)/N
    for i in range(N):
        dx, dv = D([Xs[-1], Vs[-1]], dt, t_min + dt*i, args)
        Xs.append(Xs[-1] + dx)
        Vs.append(Vs[-1] + dv)

    ax.plot(Xs, Vs, label='[{}, {}]'.format(x0, v0))
    col = ax.get_lines()[-1].get_color()
    ax.plot(Xs[0], Vs[0], color=col, marker='o')

    return ax
