#!/usr/env python3
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

x_min = 0
x_max = 10
x_space = np.linspace(x_min, x_max, 1000)
plt.ylim(-2, 2)
plt.gca().set_aspect('equal')

c = 3

# Forme spatiale du signal en t=0
def St0(x):
    return np.asarray([-np.sin(xx) if xx < 0 else 0. for xx in 2*np.pi*x/x_max])
    return np.exp(-(x)**2)

# Forme spatiale du signal en t
def S(t):
    x = x_space - t*c
    S = St0(x)
    xr = - x_space - t*c + 2*x_max
    S -= St0(xr)
    """
    for n in range(2, 100, 2):
        # Prise en compte des réflexions
        xr = - x_space - t*c + n*x_max
        xrr = x_space - t*c + n*x_max
        S += - St0(xr) + St0(xrr)
    """
    return S

line, = plt.plot(x_space, S(0))

dt = 40  # ms
def animate(i):
    t = dt*i*1e-3  # s
    plt.title(r'$t = {:.2f}$ s'.format(t))
    line.set_data(x_space, S(t))

anim = FuncAnimation(plt.gcf(), animate, interval=dt)

plt.show()
