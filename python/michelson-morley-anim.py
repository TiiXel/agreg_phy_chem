# -*- coding: utf-8 -*-
from dependencies.wavelength2rgb import wavelen2rgb
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
from matplotlib.widgets import Slider
import matplotlib.animation as animation

# Vitesse de la lumière
c = 3e8

# Paramètres de l'écran d'observation
taille_rayon = 50e-2
pas_rayon = 1e-3
N_rayon = int(taille_rayon/pas_rayon)
# Distance écran - mirroirs
D = 1

# Longueur(s) d'onde(s)
longondes = np.linspace(380, 780, 5) * 1e-9

# Longuer des bras
L = 10

# Calculer l'éclairement à l (long onde) et e (lame d'air) donnés
def calcul_eclairement(l, e):
    A = np.full(shape=(N_rayon, 1), fill_value=2.0)
    for r in range(N_rayon):
        # Angle d'incidence sur l'écran
        angle = np.arctan(r*pas_rayon/D)
        # Différence de marche
        delta = 2 * e * np.cos(angle)
        # Déphasage
        phi = 2 * np.pi * delta / l
        # Éclairement
        A[r] = 1 + np.cos(phi)

    return A

def calcul_couleur(e):
    C = np.full(shape=(2, 2*N_rayon, 3), fill_value=[0, 0, 0])
    for longonde in longondes:
        rgb = wavelen2rgb(longonde * 1e9, MaxIntensity = 255./len(longondes))
        A = calcul_eclairement(longonde, e)
        for i in range(N_rayon):
            v = rgb * A[i] + C[0, N_rayon - i]
            C[0, N_rayon - i] = v
            C[0, N_rayon + i] = v
            C[1, N_rayon - i] = v
            C[1, N_rayon + i] = v
    return C

# Calculer la lame d'air équivalente à la vitesse relative à l'ether
def calcul_lame_equiv(v):
    e = L * v**2 / c**2 / 2
    print e
    return e

N = 10
fig = plt.figure()
fig.suptitle(u"Apparence d'un rayon de la figure d'interférences \n en fonction de l'angle entre l'intérféromètre et la vitesse de la Terre")
for n in range(N):
    a = np.linspace(0, np.pi / 2, N)[n]
    v = 30000 * np.sin(a)
    C = calcul_couleur(calcul_lame_equiv(v) + 0.01e-3)
    ax = fig.add_subplot(N, 1, n+1)
    ax.set_yticks([0])
    adeg = a / 2 / np.pi * 360
    ax.set_yticklabels(["%3.0f deg" % adeg])
    ax.imshow(C, extent=(-taille_rayon, taille_rayon, -1e-2, 1e-2))

ax.set_xlabel(u"Position sur l'écran (cm)")

plt.show()
