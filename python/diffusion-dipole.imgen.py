#!/usr/env python
# -*- coding: utf-8 -*-
import imgen
imgen.setup()

import matplotlib.pyplot as plt
import numpy as np

fig, ax = plt.subplots()
l_min = -10
l_max = -6
N = 800
L = np.logspace(l_min, l_max, N)

W = 2 * np.pi * 3e8 / (L*1.0)

w0 = 1.e17  # http://mp3montaignebdx.legtux.org/Cours/Cours_electromagnetisme/OEM_rayonnement.pdf
tau = 1.e-14

P = W**4 / ((w0**2 - W**2)**2 + W**2/tau**2)

ax.loglog(L, P)

ax.set_xlabel(r'$\lambda = \frac{2\pi}{\omega}$')
ax.set_ylabel(r'$\mathcal{P}$')

# Save
imgen.done(__file__)
