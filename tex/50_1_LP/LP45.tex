\chapter{Paramagnétisme, ferromagnétisme~: approximation du champ moyen}

\niveau{L3}

\prereqstart
{Magnétostatique}
{Facteur de \propername{Boltzmann}}
{Moment magnétique, cinétique orbital, et spin de l'électron}
\prereqstop

Dans le cours de magnétostatique,
nous étudions
les champs magnétiques
générés par des courants électriques
et
leurs interactions
avec la matière aimantée.
On associait aux milieux
une quantité vectorielle $\vb{M}$,
l'aimantation,
liée au champ magnétique $\vb{H}$
par $\vb{M} = \chi_m \dotproduct \vb{H}$
sans en expliciter l'origine.
C'est ce que nous allons faire
dans cette leçon.

\section{Approche microscopique du magnétisme}

    \subsection{Moment magnétique atomique \cite{perezelectromag}}

    C'est \propername[Paul]{Langevin} qui,
    en 1905,
    introduit l'hypothèse selon laquelle
    l'aimantation de la matière
    est due à la contribution microscopique
    de chaque atome qui la compose
    mais il n'en propose pas d'origine.
    Le modèle de \propername{Bohr} (1913)
    et la mécanique quantique
    ont fait avancer
    l'idée de \propername{Langevin}.

    On rapelle que
    pour un électron dans atome,
    la projection
    du moment cinétique total $\vb{J}$
    (orbital $\vb{L}$ et spin $\vb{S}$)
    selon un axe quelconque
    (ici $Oz$)
    est
    un multiple
    entier
    ou demi-entier
    de $\hbar$~:
    $J_z = m_j \hbar$.
    Il est lié
    au moment magnétique total $\vb{\mu}$
    par
    le facteur de \propername{Landé} $g$
    et le magnéton de \propername{Bohr}
    $\mu_B = \flatfrac{q\hbar}{2m_e} = \SI{9.27e-24}{\joule\per\tesla}$
    de sorte que
    sa projection soit~:
    \begin{equation*}
        \mu_z = - m_j g \mu_B
    \end{equation*}
    La moment magnétique de l'atome
    correspond à la somme
    des moments magnétiques
    de chaqun de ses électrons,
    il en va de même
    pour la projection.
    En particulier,
    dans une sous-couche électronique complète,
    où tous les électrons sont appareillés,
    en raison du principe d'exclusion de \propername{Pauli},
    la somme
    des moments magnétiques
    est nulle.
    Cela implique que~:
    \begin{itemize}
        \item seuls les électrons de valence
            vont importer dans les calculs,
        \item les éléments
            dont toutes les sous-couches sont pleines
            ne sont pas magnétiques
            (cf \cite[p.94, chap. 3.1.6, table 3.1]{lacheisserie}).
    \end{itemize}
    Pour les espèces chimiques
    moléculaires
    (\ce{O2}, \ce{H2O}, \dots)
    on regarde
    de la même manière
    les nombres d'électrons appareillés ou non.
    Le cas d'étude le plus simple
    est alors celui
    où il n'y a qu'un seul électron
    non appareillé~: $j = \frac{1}{2}, m_j = s = \pm\frac{1}{2}$.

    \subsection{Aimantation résultante}

    Dans ce cadre,
    l'aimantation $\vb{M}$
    d'un échantillon
    est une proprité intrinsèque du milieu
    et s'écrit~:
    \begin{equation*}
        \vb{M}
        = \frac{1}{V} \sum_i{\vb{\mu}_i}
        = \frac{N}{V} \avg{\vb{\mu}}
        = n \avg{\vb{\mu}}
    \end{equation*}

    De la physique statistique,
    il semble raisonable d'avancer
    que pour un milieu isolé de toutes interactions
    mais soumis à l'agitation thermique,
    et dans un modèle sans couplage,
    chaque atome va poséder
    un moment magnétique $\vb{\mu} \neq \vb{0}$
    et pour autant,
    $\vb{M} = \vb{0}$.

\section{Paramagnétisme}

    L'adjectif \emph{paramagnétique}
    désigne
    les milieux
    qui n'ont pas aimantation naturelle,
    mais en acquièrent
    lorsqu'ils sont placés
    dans un champ magnétique.
    L'énergie d'interaction
    entre un moment magnétique $\vb{\mu}$
    et le champ magnétique $\vb{B}$
    est donnée par le produit scalaire~:
    \begin{equation*}
        E_{\pm}
        = - \vb{\mu} \dotproduct \vb{B}
        = s g \mu_B B
        = \mp \tfrac{1}{2} g \mu_B B
        = \mp E_B
    \end{equation*}
    Nous verrons que
    cette énergie
    est à l'origine du paramagnétisme.

    \subsection{Aimantation en présence d'un champ \cite{perezelectromag}}

    Reprenons
    l'énergie potentielle d'interaction
    entre un moment magnétique
    et un champ extérieur
    pour une particule de spin $\pm\tfrac{1}{2}$~:
    \begin{equation*}
        E_{\pm}
        = \mp E_B
    \end{equation*}
    Nous négligeons
    les interactions
    entre les différents électrons
    et entre les différents atomes.
    De bons exemples
    de matériaux correspondant
    à cette description
    sont les complexes du cuivre
    $\ce{Cu^2+(II)}$.
    La configuration électronique
    du cuivre
    étant
    $[\ce{Ar}] 4s^1 3d^{10}$
    (exception à la règle de \propername{Hund}),
    on s'attends en effet
    à un électron non appareillé
    pour l'ion $2+$
    avec la configuration
    $[\ce{Ar}] 4s^0 3d^9$.
    Complexés,
    les ions de cuivre
    sont éloignés les un des autres
    par les ligands
    de sorte que
    les interactions magnétiques entre eux
    soient négligeables.
    \begin{media}
        \url{https://www.adichemistry.com/jee/qb/chemical-bond/1/q1-1.png}
    \end{media}


    Chaque électron,
    associé à un atome
    et à ses nombres quantiques,
    est discernable des autres électrons.
    La distribution de \propername{Maxwell-Boltzmann}
    donne alors
    la probabilité
    qu'un moment magnétique donné
    ait une énergie $E_{\pm}$~:
    \begin{equation*}
        P_{\pm}
        = \frac{e^{-\beta E_{\pm}}}{Z}
        \qq{avec}
        Z
        = e^{-\beta E_{+}} + e^{-\beta E_{-}}
        = 2 \cosh(\beta E_B)
    \end{equation*}
    on en déduit
    la valeur moyenne de $\mu_z$,
    et donc $M = \norm{\vb{M}} = \vb{M} \dotproduct \vu{z}$~:
    \begin{align*}
        M
        = n \sum_{i=\pm} \frac{i g \mu_B}{2} \cdot P_{i}
        = \frac{n g \mu_B}{2}
           \frac{\qty(e^{\beta E_B} - e^{-\beta E_B})}{2 \cosh(\beta E_B)}
        &= \frac{n g \mu_B}{2} \tanh(\beta E_B)
        \\
        &= \frac{n g \mu_B}{2} \tanh(\frac{g \mu_B B}{2 k_B T})
        \\
        &= M_s \tanh(x)
    \end{align*}
    La dépendance
    en $\tfrac{B}{T}$
    nous intéresse~:
    les autres valeurs sont
    soit des constantes fondamentales
    soit liées au milieu.

    Le même calcul,
    avec une description classique
    du moment magnétique
    aurait donné
    un résultat similaire,
    avec la fonction de \propername{Langevin}~:
    $M(x) = M_s \mathcal{L}(x)$~;
    la description classique
    sous estime la valeur de $M$.

    \pyimgen{ferro_para_aimantation}

    \subsubsection{Champ fort, basse température}

    À $x \gg 1 \implies \mu_B B \gg k_B T$
    ($\tanh(x) \approx 1$),
    l'aimantation
    atteint une valeur de saturation~:
    \begin{equation*}
        M(\tfrac{B}{T})
        = M_s
    \end{equation*}
    elle est atteinte
    lorsque
    tous les moments magnétiques atomiques
    sont orientés
    dans le même sens
    que $\vb{B}$
    avec $s = - \tfrac{1}{2}$.
    Cette configuration
    correspond
    à la configuration
    d'énergie potentielle minimale
    des spins
    dans le champ appliqué.

    \subsubsection{Champ faible, haute température}

    À $x \ll 1 \implies \mu_B B \ll k_B T$,
    un développement limité
    ($\tanh(x) \approx x$)
    donne~:
    \begin{equation*}
        M(\tfrac{B}{T})
        = M_s \frac{g mu_B B}{2 k_B T}
        = \frac{n g^2 \mu_B^2}{4 k_B} \frac{B}{T}
    \end{equation*}
    on définit
    la susceptibilité magnétique~:
    \begin{equation*}
        \chi_m
        \defeq \mu_0 \pdv{M(\tfrac{B}{T})}{B}
        = \frac{C}{T}
        \propto \frac{1}{T}
    \end{equation*}
    qui se trouve être porportionelle
    à l'inverse de la température.
    Le résultat de proportionalité
    a été annoncé par \propername[Pierre]{Curie}
    déjà en 1885
    suite à des observations expérimentales
    en magnétostatique.
    La constante $C$
    propre à chaque milieu
    est appelée
    constante de \propername{Curie}.

    L'écriture
    $M(\tfrac{B}{T}) = C \frac{B}{T}$
    rend compte d'une compétition
    entre l'agitation thermique
    et l'alignement magnétique.
    Notons que
    dans le cas des champs forts
    $\chi_m = 0$
    n'a pas d'intérêt.
    La susceptibilité magnétique
    donne,
    dans la matière,
    le lien entre $M$, $H$ et $B$~:
    \begin{equation*}
        M = \chi_m H
        \qquad
        B = \mu_0(H+M) = \mu_0 (1+\chi_m) H
    \end{equation*}

    \subsection{Énergie et entropie \cite{diuphystat}}

    \begin{draft}
        Cette section
        présente peu d'intérêt
        si l'on ne discute pas
        la désaimantation adiabatique.
    \end{draft}

    Puisque l'on prétend voir
    une compétition entre deux paramètres physiques
    vis à vis de l'orientation
    de l'ensemble des spins,
    l'expression de l'entropie du système
    pourra nous renseigner
    plus quantitativement.
    Pour la calculer,
    nous allons utliser~:
    \begin{equation*}
        s = \frac{e - f}{T}
    \end{equation*}


    \paragraph{Énergie volumique $e$}
    L'énergie volumique moyenne
    d'un échantillon
    est donnée par
    (nous avons déjà fait ce calcul
    au préfacteur $-B$ près)~:
    \begin{equation*}
        e
        = \sum_{i=\pm} E_{i} P_{i}
        = \sum_{i=\pm} \frac{-i g \mu_B}{2} B P_{i}
        = - M(\tfrac{B}{T}) B
        = - \frac{n g \mu_B}{2} \tanh(\frac{g \mu_B B}{2 k_B T}) B
    \end{equation*}

    \paragraph{Énergie libre volumique $f$}
    L'énergie libre volumique
    est~:
    \begin{equation*}
        f
        = - k_B T \ln Z
        = - k_B T \ln(2 \cosh(\beta E_B))
        = - k_B T \ln(2 \cosh(\frac{g \mu_B B}{2 k_B T}))
    \end{equation*}

    \paragraph{Entropie volumique}
    D'où
    l'entropie volumique~:
    \begin{equation*}
        s
        = \frac{e - f}{T}
        \implies \frac{s}{k_B}
        = - x \tanh(x) + x \ln(2 \cosh(x))
        = \fof(\tfrac{B}{T})
    \end{equation*}
    qui ne dépend que
    de $\tfrac{B}{T}$.

    \pyimgen{ferro_para_entropie}

    Le graphe
    montre que~:
    \begin{itemize}
        \item à $\tfrac{B}{T} \propto x \gg 1$,
            $s$ tends vers $0$~:
            ce qui est cohérent
            avec l'idée que,
            à température basse
            ou champ fort,
            tous les moment magnétiques
            pointent dans la même direction.
        \item à $\tfrac{B}{T} \propto x \ll 1$,
            $s$ tends vers un maximum~:
            les moments magnétiques
            sont désordonnés.
    \end{itemize}

    \begin{media}
        \software{./python/ferro\_para\_anim.py}
    \end{media}

    \subsection{Valeurs numériques et applications}

    \subsubsection{Sonder les orbitales électroniques}

    Puisque tout notre travail
    repose sur l'hypothèse
    qu'il y a un seul électron non appareillé par atome
    dans le matériaux considéré,
    un écart
    entre le modèle et l'expérience
    pourait indiquer
    que notre hypothèse est mauvaise.

    \paragraph{Dioxygène}
    La structure de \propername{Lewis}
    du dioxygène
    ne prévoit aucun électron non appareillé.
    Pourtant,
    l'expérience montre que
    le dioxygène est paramagnétique.
    (Ce phénomène
    est bien modélisé
    par la théorie des orbitales moléculaires.)
    \begin{media}
        \url{https://www.youtube.com/watch?v=Lt4P6ctf06Q}
    \end{media}

    \paragraph{Lanthanides}
    Pour d'autres ions que le cuivre $\ce{(II)}$,
    on observe un désacord
    entre les constantes de \propername{Curie}
    théoriques et expérimentales,
    qui peut s'expliquer
    par des exceptions à la règle de \propername{Hund}.
    \begin{table}[H]
        \centering
        \begin{tabular}{llS[table-format=1.3]S[table-format=1.2e-1]}
            \toprule
            Ion $\ce{(aq)}$
                & Configuration
                    & {$\flatfrac{C_{\textrm{théo}}}{C_{\textrm{exp}}}$ \cite{gallais}}
                        & {$\chi_m$ \cite{gallais}}
            \\
            \midrule
            \ce{Ce^3+}&$5s^2 5p^6 4f^1$ & 1.058 & 1.23e-6
            \\
            \ce{Pr^3+}&$5s^2 5p^6 4f^2$ & 1.046 & 2.61e-6
            \\
            \ce{Sm^3+}&$5s^2 5p^6 4f^5 \rightarrow 5p^6 4f^7$& 0.321 & 4.79e-7
            \\
            \ce{Eu^3+}&$5s^2 5p^6 4f^6 \rightarrow 5s^1 5p^6 4f^7$& 0 & 2.46e-6
            \\
            \bottomrule
        \end{tabular}
    \end{table}

    \subsubsection{Matériaux étudiés et courants}

    \begin{table}[H]
        \centering
        \begin{tabular}{lS[table-format=2.2e-1]}
            \toprule
            Matériau & {$\chi_m$ \cite{handbook}}
            \\
            \midrule
            \ce{O2(l)} & 3.06e-3
            \\
            \ce{Cu(SO4),5H2O} & 1.69e-4
            \\
            \ce{Al(s)} & 2.07e-5
            \\
            Eau & -9.12e-6
            \\
            \bottomrule
        \end{tabular}
    \end{table}

    Pour l'eau,
    comme pour d'autres substances,
    la valeur de $\chi_m$
    est négative.
    C'est le cas
    de tous les milieux
    dans lesquels
    tous les électrons sont appareillés~:
    on parle de milieux
    \emph{diamagnétiques}.

    \subsubsection{Refroidissement par désaimantation adiabatique}

    \begin{todo}
        Dire des choses dans cette section.
    \end{todo}
    \pyimgen{desaimantation_adiabatique}

\section{Ferromagnétisme}

    L'adjectif \emph{ferromagnétique}
    désigne
    les matériaux
    qui ont une aimantation naturelle.
    Dans le cadre de la description précédente,
    cela revient à dire que,
    en fait,
    $\avg{\vb{\mu}} \neq \vb{0}$
    et donc,
    que l'orientation
    des moments magnétiques atomiques
    n'est \emph{pas complètement}
    aléatoire~:
    le couplage
    n'est pas négligeable
    alors
    comment le modéliser~?

    \subsection{Origine du couplage}

    \subsubsection{L'interaction dipôlaire est trop faible
        \cite{perezelectromag} \cite[III. J. I. 4]{diuphystat}}

    La première hypothèse
    que nous pouvons faire
    est la suivante~:
    «~le couplage est dû
    à l'interaction dipôlaire
    entre les moments magnétiques~».
    Un calcul
    en ordre de grandeur
    permet de la vérifier~:
    soient deux moment dipôlaires atomiques $\vb{\mu}_i$ et $\vb{\mu}_j$
    distants de $r$
    l'énergie d'interaction entre les dipôles
    est~:
    \begin{equation*}
        E_d
        = \frac{\mu_0}{4\pi}
            \frac
            {\vb{\mu}_i\dotproduct\vb{\mu}_j
                - 3 (\vb{\mu}_i\dotproduct\vu{r})(\vb{\mu}_j\dotproduct\vu{r})}
            {r^3}
            \propto \frac{\mu_0}{4\pi} \frac{\mu_B^2}{(\SI{1}{\angstrom})^3}
            \approx \SI{54}{\micro\eV}
            \ll \SI{25}{\milli\eV}
            = k_B T
    \end{equation*}
    Or
    le ferromagnétisme
    est observé à température ambiante.

    \subsubsection{Hamiltonien de \propername{Heisenberg} \cite{diuphystat}}

    Pour modéliser l'interaction,
    \propername{Heisenberg} propose
    une description simple~:
    à chaque couple de moments magnétiques voisins
    il associe une énergie
    qui tend à les aligner~:
    \begin{equation*}
        E_{ij}
        = - J (\vb{\mu}_i \dotproduct \vb{\mu}_j)
    \end{equation*}
    en supposant $J$ suffisament important.
    Ce coefficient est appelé
    \emph{coefficient de couplage}
    il est positif.

    Dans ce modèle modifié,
    l'énergie $E_i$
    d'un moment magnétique $\vb{\mu}_i$
    au coeur du matériaux
    en présence d'un champ $\vb{B}$
    va alors s'écrire~:
    \begin{equation*}
        E_i
        = - \vb{\mu}_i \qty(\vb{B} + J \sum_{j\in v(i)} \vb{\mu}_j)
    \end{equation*}

    La résolution du problème
    se fait alors de la même manière
    que ce que nous avons fait
    pour le paramagnétisme.
    Seulement ici,
    le couplage rend la résolution
    beaucoup plus compliquée\dots
    Il convient alors
    de trouver une approximation
    du problème.

    Ce couplage
    est d'origine quantique et électrostatique.
    Si dans un réseau
    on considère
    deux électons de valence
    associés à
    la fonction d'onde $\Psi(1, 2) = f(\vb{r_1}, \vb{r_2}) g(1, 2)$
    produit de
    $f$ fonction d'onde orbitale
    et
    $g$ fonction d'onde de spin.
    Cette fonction d'onde
    doit être antisymétrique
    ($\Psi(1, 2) = -\Psi(2, 1)$)
    alors,
    si les électrons sont de spin parallèles~:
    $g(1, 2) = g(2, 1) \implies f(\vb{r_1}, \vb{r_2}) = -f(\vb{r_2}, \vb{r_1})$
    la densité de probabilité de présence ($\abs{f}^2$)
    s'annulle
    lorsque les électrons sont au même endroit.
    Au contraire,
    si les électrons sont de spin opposés~:
    $g(1, 2) = -g(2, 1) \implies f(\vb{r_1}, \vb{r_2}) = f(\vb{r_2}, \vb{r_1})$
    la densité de probabilité de présence ($\abs{f}^2$)
    prend une valeur maximale
    lorsque les électrons sont au même endroit~:
    l'énergie d'interaction coulombienne
    entre ces deux électrons (plutôt rapprochés)
    est grande,
    alors qu'elle était faible
    pour les électrons de spins opposés.

    \subsection{Approximation de champ moyen \cite[III.J.2.1]{diuphystat}}

    L'approximation de champ moyen
    consiste à
    faire comme si
    le terme de droite
    n'était pas soumis
    aux fluctuations
    concrètement,
    on le remplace
    par sa valeur moyenne
    avec~:
    \begin{equation*}
        \avg{\sum_{j\in v(i)} \vb{\mu}_j}
        = p \avg{\vb{\mu}}
        = p \frac{\vb{M}}{n}
    \end{equation*}
    alors,
    \begin{equation*}
        E_i
        = - \vb{\mu}_i \qty(\vb{B} + \frac{Jp}{n}\vb{M})
        \defeq - \vb{\mu}_i \vb{B}_\textrm{eff}
    \end{equation*}
    Dans notre modèle
    les moments magnétiques plus proches voisins
    ne sont pas indépendants
    mais l'interaction est ici cachée dans $\vb{M}$.
    Tout se passe comme si
    le milieu était paramagnétique
    avec un champ appliqué $\vb{B}_\textrm{eff}$.
    Nous pouvons alors
    reprendre l'équation du paramagnétisme
    pour l'aimantation~:
    \begin{equation*}
        M(\tfrac{B_\textrm{eff}}{T})
        = M_s \tanh(x_\textrm{eff})
    \end{equation*}
    sans oublier que~:
    \begin{equation*}
        x_\textrm{eff}
        = \fof(M)
        \implies
        M
        = \frac{n}{Jp} \qty(\frac{2 k_B T x_\textrm{eff}}{g \mu_B} - B)
    \end{equation*}
    ce genre d'équations
    dite \emph{d'autocohérence}
    s'étudie graphiquement,
    on cherche les intersections
    entre la droite d'équation
    $M = \fof(x_\textrm{eff})$
    et le graphe de la fonction
    $M = M_s \tanh(x_\textrm{eff})$

    \pyimgen{ferro_para_autocoherence}

    \begin{todo}
        Tracer l'autre branche.
    \end{todo}

    \subsection{Résultats}

    \subsubsection{Température de \propername{Curie}}

    On constate
    que, à $B = 0$
    l'équation d'autocohérence a
    toujours une solution
    en $x_\textrm{eff} = 0$
    et a,
    pour les températures $T < T_C$
    deux solutions supplémentaires.
    Nous pouvons déterminer
    la température critique $T_C$
    en déterminant la pente
	de $M_s \tanh(x_\textrm{eff})$
    à l'origine~:
    l'un des développement limité précédents
    donne~:
    \begin{equation*}
        \eval{\pdv{M}{x_\textrm{eff}}}_{x_\textrm{eff}=0}
        = M_s
        = \frac{n g \mu_B}{2}
    \end{equation*}
    alors,
    \begin{equation*}
        \frac{n}{J p}\frac{2 k_B T_C}{g \mu_B}
        = \frac{n g \mu_B}{2}
        \implies
        T_C
        = \frac{g^2 \mu_B^2 J p}{4 k_B}
    \end{equation*}

    \begin{table}[H]
        \centering
        \begin{tabular}{lS[table-format=5]S[table-format=4]}
            \toprule
            Matériau & {$M_s$ \cite{handbook}} & {$T_C$ \cite{handbook}}
            \\
            \midrule
            \ce{Fe} & 22020 & 1043
            \\
            \ce{Co} & 18170 & 1388
            \\
            \ce{Ni} &  6410 & 627
            \\
            \bottomrule
        \end{tabular}
    \end{table}

    \subsubsection{Aimantation}

    \begin{todo}
        Tracer $M = \fof(B)$,
        constater
        la brisure de symétrie.

        Tracer $f = \fof(M)$
        pour montrer les positions de stabilité.
    \end{todo}

    \subsubsection{\dots}
    \begin{todo}
        Soit projeter des courbes
        des différentes grandeurs physiques,
        soit discuter des limites du modèle,
        soit discuter de la simulation,
        soit\dots
    \end{todo}
    \begin{manip}
        \dots faire la manip~!
    \end{manip}

\plusloin{
    Le paramagnétisme
    étudié ici
    est restreint
    à l'étude de milieux isolants
    en effet,
    dans les conducteurs
    les électrons de conduction
    se comportent différement.
    Le paramagnétisme de \propername{Pauli}
    permet de les étudier
    et tient compte
    en particulier
    de leur caractère indiscernable~:
    plutôt que la statistique
    de \propername{Maxwell-Boltzmann}
    on leur applique
    la statistique
    de \propername{Fermi-Dirac}.

    Le paramagnétisme
    et le ferromagnétisme
    ne constituent pas
    l'ensemble des types de matériaux.
    Nous avons évoqué
    le diamagnétisme
    mais il existe aussi
    l'antiferromagnétisme,
    et le ferrimagnétisme.
}
