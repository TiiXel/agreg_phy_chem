\chapter{Notion de viscosité d'un fluide, écoulement visqueux}

\niveau{PC}

\prereqstart
{Statique des fluides}
{Cinématique des fluides}
{Dérivée particulaire et terme convectif}
\prereqstop

Dans cette leçon
nous étudions
une partie de
la dynamique des fluides.

\section{Introduction~: expérience illustrative \cite{ghp}}

    Considérons un fluide
    initialement au repos
    entre deux cylindres
    que l'on met en mouvement
    à des vitesses de rotations différentes
    à l'instant $t=0$.
    Nous écrivons
    de manière qualitative
    un bilan de forces
    exercées sur une particule fluide
    pour prévoir sa dynamique
    avec la seconde loi de \propername{Newton}.
    Nous connaissons~:
    \begin{itemize}
        \item les forces de pression,
            $\vb{f_p} = - \grad P \dd \tau$
        \item les forces volumiques
            (dont fait partie le poids),
            $\vb{f_v} \dd \tau$
    \end{itemize}
    Une écriture volumique
    du principe fondamental de la dynamique
    est~:
    \begin{equation*}
        \rho \dv{\vb{v}}{t}
        = - \grad P + \vb{f_v}
    \end{equation*}
    \begin{manip}
        Écoulement de \propername{Couette} cylindrique
    \end{manip}
    L'expérience
    met en évidence
    un phénomène
    que nous n'avons pas prévu~:
    le fluide
    a été mis en mouvement
    autour de l'axe vertical,
    alors que nous n'y appliquons
    aucune force volumique.
    Nous pouvons observer
    plusieurs points~:
    \begin{itemize}
        \item le fluide
            a été mis en mouvement
            de proche en proche,
        \item le fluide
            au contact de la parroi mobile
            se déplace à la même vitesse
            que cette dernière.
    \end{itemize}
    Il semble juste
    d'en déduire
    qu'il existe une force tangentielle,
    assimilable à des frottements,
    entre les couches cylindriques de fluide.
    Cette force manque
    dans notre description précédente,
    elle va être caractérisée
    par une propriété
    du fluide
    appellée \emph{viscosité}.

\section{Notion de viscosité \cite{dunodpsi}}

    \subsection{Force de viscosité}

    Nous allons décrire cette viscosité
    avec un modèle d'expérience
    similaire à celle présentée
    mais plus simple à mettre en équation.

    \begin{python}
        \software{./python/ecoulement\_couette.py}
    \end{python}

    \begin{draft}
        L'intéret de l'expérience étudiée ici
        est la géométrie
        qui permet l'utilisation naturelle
        d'un repère cartésien.
        L'intéret du dispositif cylindrique
        est l'abscence de bords.
    \end{draft}

    On considère un fluide
    homogène incompressible
    (en fait un suffit d'un écoulement incompressible, à température fixe)
    maintenu entre
    deux plaques
    horizontales solides planes et parallèles
    perpendiculaires à $\vu{y}$
    séparées d'une distance $a$.
    La plaque du bas
    est fixe
    dans le référentiel d'étude,
    alors que la plaque du haut
    est annimée d'une vitesse $\vb{v} = \vcst$ selon $\vu{x}$~:
    elle va entrainer le fluide.
    On suppose
    le champ de pression uniforme
    et une invariance du problème
    par translation selon $\vu{z}$.
    On étudie le problème
    en régime stationnaire
    établi suite à un temps de régime transitoire.

    Vu les symétries et invariances
    le champ de vitesses
    va s'écrire comme~:
    \begin{equation*}
        \vb{v}(x, y, z)
        = v_x(x, y) \vu{x}
    \end{equation*}

    On considère dans ce fluide
    deux particules (point de vue de \propername{Lagrange})
    situées l'une au dessus de l'autre~;
    notre expérience à montré
    qu'il existait entre ces particules
    une force tangentielle au déplacement.
    On admet (en PC) que
    la force
    exercée par la particule du haut sur la particule du bas
    dite \emph{force de viscosité}
    s'exprime comme~:
    \begin{equation*}
        \dd \vb{F}_{h \rightarrow b}
        = \eta \pdv{v_x}{y} \dd S \vu{x}
        \qq{et évidemment~:}
        \dd \vb{F}_{b \rightarrow h}
        = - \dd \vb{F}_{h \rightarrow b}
        = - \eta \pdv{v_x}{y} \dd S \vu{x}
    \end{equation*}

    Cette expression est empirique mais
    on peut réfléchire à sa signification~:
    \begin{itemize}
        \item la force augmente
            si la différence de vitesse
            entre les deux particules
            est plus grande
        \item la force augmente
            pour des particules
            de surface plus importante
        \item si la différence de vitesse selon $\vu{y}$
            change de sens
            la force change de sens
        \item si les deux particules
            ont la même vitesse
            la force est nulle
        \item le coefficient $\eta$
            est un coefficient de proportionnalité
            entre la force
            les deux autres paramètres
    \end{itemize}
    Ce coefficient $\eta$
    est caractéristique du fluide
    et appelée \emph{viscosité dynamique},
    son unité
    est le pascal seconde (symbole \si{\pascal\second})
    historiquement appele poiseuille.
    On peut donner
    des ordres de grandeur pour $\eta$,
    mais ce coefficient
    dépend de la pression et de la température.
    \begin{table}[H]
        \centering
        \begin{tabular}{rS}
            \toprule
            Fluide
            & {Viscosité dynamique $\eta$ (\si{\pascal\second})}
            \\
            \midrule
            Air
            & 1.8e-5
            \\
            Eau
            & 1.0e-3
            \\
            Glycérine
            & 0.80
            \\
            \bottomrule
        \end{tabular}
    \end{table}

    \begin{media}
        Graphe $\eta_{\textrm{eau}}(T)$~:
        \url{http://gpip.cnam.fr/ressources-pedagogiques-ouvertes/hydraulique/res/viscositeEau_vs_T.png}
    \end{media}

    \subsection{Équivalent volumique de la force de viscosité}

    On connait maintenant
    la force
    exercée par une particule
    sur une autre particule.
    Dans le fluide
    sous cette descrption en géométrie cartésienne,
    chaque particule de fluide
    est en contact
    avec 6 autres particules,
    cherchons alors
    à réexprimer le bilan des forces de viscosité
    exercées sur celle du centre.

    Si l'on considère
    une particule fluide
    en forme de pavé droit
    comprise dans un volume
    $\dd x \dd y \dd z$,
    elle est soumise aux forces de viscosité
    de la part de ses voisines
    en $y$ et $y + \dd y$.
    Ces forces s'expriment~:
    \begin{equation*}
        \dd \vb{F}(y)
        = - \eta \pdv{v_x}{y}(y) \dd x \dd z \vu{x}
        \qq{et}
        \dd \vb{F}(y + \dd y)
        = \eta \pdv{v_x}{y}(y + \dd y) \dd x \dd z \vu{x}
    \end{equation*}
    leur somme donne~:
    \begin{equation*}
        \dd \vb{F}(y) + \dd \vb{F}(y + \dd y)
        = \eta \pdv[2]{v_x}{y} \dd y \dd x \dd z \vu{x}
        = \eta \pdv[2]{v_x}{y} \dd \tau \vu{x}
        \defeq \dd \vb{f} \dd \tau
    \end{equation*}
    pour les écoulement
    à 3 dimensions,
    on généralise
    cette expression
    de la force volumique de viscosité
    avec le laplacien~:
    \begin{equation*}
        \dd \vb{f}
        = \eta \laplacian\vb{v}
    \end{equation*}

    \begin{todo}
        En écrivant
        $\rho \Dv*{\vb{v}}{t} = \eta \laplacian \vb{v}$
        on trouve l'équation
        de diffusion
        de quantité de mouvement~:
        $\pdv*{v}{t} = \flatfrac{\eta}{\rho} \laplacian \vb{v}$.
    \end{todo}

\section{Dynamique des fluides}

    \subsection{Équation de \propername{Navier-Stokes}}

    L'équation de \propername{Navier-Stokes}
    résulte du principe fondamental de la dynamique
    appliqué à la particule fluide,
    pour un fluide newtonnien
    en écoulement incompressible~;
    on l'admet en classe de PC~:
    \begin{equation*}
        \rho \qty(\pdv{\vb{v}}{t}
            + \underbrace{(\vb{v}\grad)\vb{v}}_{\textrm{conv}})
        = - \grad{P}
            + \underbrace{\eta \laplacian \vb{v}}_{\textrm{diff}} + \vb{f}_v
    \end{equation*}

    \begin{todo}
        Lire \cite[chap. 15, 2.2, p. 400]{dunodpsi}
        pour la siginification
        des deux termes mis en évidence ici.
    \end{todo}

    Les deux termes
    convectif et diffusif
    qui apparaissent dans l'équation
    la rendent difficile à résoudre
    en effet,
    le premier la rend dépendante de $v^2$
    et
    le second la rend dépendante d'une dérivée seconde.

    Cette équation
    n'admet pas de solutions analytique
    (problème du milénaire).
    Pour poursuivre notre étude
    de la dynamique des fluides,
    nous allons chercher
    à mieux la comprendre
    pour pouvoir négliger
    l'un ou l'autre de ces termes.

    \subsection{Caractérisation des écoulements}

    On décide
    de caractésier l'écoulement
    par le rapport,
    appelé \emph{nombre de \propername{Reynolds}}~:
    \begin{equation*}
        \mathrm{R_e}
        = \frac{\textrm{conv}}{\textrm{diff}}
        = \frac{\norm{\rho (\vb{v}\grad)\vb{v}}}{\norm{\eta\laplacian\vb{v}}}
    \end{equation*}
    on peut estimer
    en ordres de grandeur
    que~:
    \begin{equation*}
        \rho (\vb{v}\grad)\vb{v}
        \propto \frac{\rho U^2}{L}
        \qq{et}
        \eta\laplacian\vb{v}
        \propto \eta \frac{U}{L^2}
    \end{equation*}
    d'où
    pour le nombre de \propername{Reynolds}~:
    \begin{equation*}
        \mathrm{R_e}
        = \frac{\rho U L}{\eta}
    \end{equation*}

    Si $\mathrm{R_e} \ll 1$
    le terme convectif est dominant,
    alors que
    dans le cas contraire
    le terme diffusif est dominant.
    Cette caractérisation
    peut sembler arbitraire
    mais on remarque expérimentalement
    que les écoulements
    à bas $\mathrm{R_e}$ ($< 2000$)
    sont toujours laminaires,
    alors que les écoulements
    à haut $\mathrm{R_e}$ ($> 2000$)
    sont toujours turbulents.

    \begin{todo}
        Donner des ordres de grandeur
        et parler de possibles transitions
        entre les régimes
        \cite[chap. 15, 3.4, p. 405]{dunodpsi}.
    \end{todo}

    \begin{todo}
        Montrer des images
        d'écoulements
        laminaire et turbulents.
    \end{todo}

    Dans le cadre de cette leçon
    qui porte sur la viscosité
    nous allons continuer
    avec des écoulements pour lesquels
    le nombre de \propername{Reynolds}
    est bas.

\section{Écoulement de \propername{Poiseuille} \cite{dunodpsi}}

    Il s'agit d'un écoulement
    qui présente une importance particulière
    en ingénieurie
    puisqu'il décrit
    l'écoulement de fluides
    dans des conduites.

    \subsection{Profil de vitesse}

    \subsection{Perte de charge, chute de pression}

    \begin{todo}
        Particulièrement en avant
        dans le BO de PSI.
    \end{todo}

\plusloin{
    Dans la suite ce cette leçon
    nous pouvons étudier
    d'autres géométries d'écoulements.
    En particulier,
    les écoulement qui se font
    \emph{autour} d'un obstacle.
    On pourra alors parler de
    la force de trainée,
    qui est
    la résultate des forces de viscosité
    exercées par le fluide
    sur l'obstacle.
    La chute d'une bille
    dans un milieux visqueux
    est un bel exemple
    qui permet
    de mesurer la viscosité du fluide.

    Suite à cette leçon
    nous pourrons introduire
    la notion d'écoulement parfait
    pour lequel
    les phénomènes
    assosiés à la viscosité
    sont négligeables.
    On introduita alors
    la notion de \emph{couche limite}
    zone dans laquelle la viscosité
    n'est justement pas négligeable.
    L'équation d'\propername{Euler}
    qui décrit les écoulement parfaits
    permettra d'arriver
    aux équations de \propername{Bernoulli}
    qui permettent
    d'expliquer le fonctionnement de dispositifs
    comme le débimètre de \propername{Venturi},
    ou le tube de \propername{Pitot}.
}
