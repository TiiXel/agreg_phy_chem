\chapter{Propagation guidée des ondes}

\section{Intérêt du guidage}

\section{Notions pour le guidage
    \cite{taillet, thibiergeondes, feynmanem2, wikiversityoemguidees}}

    \subsection{Refléxion d'une onde sur une surface}

    \begin{pydo}
        Montrer
        que
        lors de la refléxion d'une onde incidente
        sur un plan
        un front d'onde résultant
        se propage parallèlement au plan.
    \end{pydo}

    \subsection{Onde guidée}

    Une onde se propage
    dans la direction $\vu{z}$
    dans un région de l'espace
    infinie,
    d'axe $\vu{z}$
    délimitée par des surfaces réfléchissantes.

    \subsection{Équation de \propername{Helmoltz}}

    Les champs peuvent s'écrire
    dans le cas général~:
    \begin{align*}
        \vb{\mathcal{E}}(x, y, z, t)
        &= \vb{E}(x, y) e^{i(\omega t - k_z z)}
        \\
        \vb{\mathcal{B}}(x, y, z, t)
        &= \vb{B}(x, y) e^{i(\omega t - k_z z)}
    \end{align*}

    L'équation de propagation
    pour $\vb{E}$
    est celle de \propername{D'Alembert}~:
    \begin{equation*}
        \laplacian\vb{\mathcal{E}}
        - \frac{1}{c^2} \pdv[2]{\vb{\mathcal{E}}}{t}
        = \vb{0}
        \implies
        \laplacian\vb{\mathcal{E}}
        + \frac{\omega^2}{c^2} \vb{\mathcal{E}}
        = \vb{0}
    \end{equation*}
    or les dérivées spatialles donnent~:
    \begin{equation*}
        \laplacian\vb{\mathcal{E}}
        = \laplacian\vb{E} \cdot e^{i(\omega t - k_z z)}
            - k_z^2 \vb{\mathcal{E}}
    \end{equation*}
    donc l'équation de propagation devient~:
    \begin{equation*}
        \laplacian\vb{E} + \qty(\frac{\omega^2}{c^2} - k_z^2) \vb{E}
        = \vb{0}
    \end{equation*}
    c'est l'équation de \propername{Helmoltz}.
    Pour la propagation
    du champ magnétique
    on obtient
    la même équation.

    \subsection{Solutions transverses}

    Dans le cas général
    on peut décomposer
    $\vb{E}$ et $\vb{B}$
    en une composante tangentielle
    et une composante longitudinale
    vis-à-vis de la propagation~:
    \begin{equation*}
        \vb{E}
        = \vb{E}_t(x,y) + E_z(x,y) \vu{z}
        \qq{et}
        \vb{B}
        = \vb{B}_t(x,y) + B_z(x,y) \vu{z}
    \end{equation*}
    On appelle
    \emph{transverse électriques}
    les solutions pour lesquelles
    la composante $E_z$ est nulle,
    et
    \emph{transverse magnétique}
    les solutions pour lesquelles
    la composante $B_z$ est nulle.
    Ces solutions
    sont plus simples à étudier
    que les solutions générales.

\section{Guide d'onde rectangulaire \cite{garingoem}}

    \subsection{Description}

    On étudie un guide d'onde
    illimité dans la direction $\vu{z}$
    dont
    la section droite est rectangulaire
    ($0 < x < a$ et $0 < y < b$)
    et délimitée par des surfaces
    parfaitement conductrices.
    On assimile
    le milieu intérieur
    au vide.

    On commence l'étude
    avec une onde
    qui vérifie $\vb{E} = E_0(x,y) \vu{y}$
    qui en plus d'être transverse électrique
    s'accorde bien avec la symétrie du problème.

    \subsection{Conditions aux limites pour les champs}

    Les conditions aux limites
    pour le champ électromagnétique
    sont associées
    aux relations de passage
    aux surfaces~:
    \begin{align*}
        \delta \vb{\mathcal{E}}
        &= \frac{\sigma}{\epsilon_0} \vb{n}
        \\
        \delta \vb{\mathcal{B}}
        &= \mu_0 \vb{j_s} \crossproduct \vb{n}
    \end{align*}
    Pour le champ électrique considéré ici
    les surfaces des conducteurs imposent donc~:
    \begin{equation*}
        E_0(x = 0, y) = 0 = E_0(x = a, y)
    \end{equation*}

    \subsection{Forme des champs en solution TE}

    \subsubsection{Champ $\vb{\mathcal{E}}$}

    Comme
    $\div\vb{\mathcal{E}} = 0$
    on va pouvoir simplifier
    l'équation de propagation~:
    \begin{equation*}
        \div\vb{\mathcal{E}}
        = e^{i(\omega t - k_z z)} \cdot \div\vb{E}
            + \vb{E} \dotproduct \grad(e^{i(\omega t - k_z z)})
    \end{equation*}
    le gradient
    dans le deuxième terme
    est parallèle à $\vu{z}$
    donc il reste~:
    \begin{equation*}
        e^{i(\omega t - k_z z)} \cdot \div\vb{E}
        = 0
        \implies
        \div\vb{E}
        = 0
        \implies
        \pdv{E_0(x, y)}{y}
        = 0
    \end{equation*}
    on trouve
    que $E_0$
    ne dépend en fait pas de $y$.
    Alors l'équation de propagation
    est~:
    \begin{equation*}
        \pdv[2]{E_0}{x} + \qty(\frac{\omega^2}{c^2} - k_z^2) E_0
        = \vb{0}
    \end{equation*}

    Cette équation différentielle
    est connue,
    ses solutions
    sont de différents types,
    en posant
    $k^2 \defeq \flatfrac{\omega^2}{c^2} - k_z^2$
    les solutions sont~:
    \begin{align*}
        \frac{\omega^2}{c^2} - k_z^2 < 0
        &\implies E_0(x) = C e^{-k x}
        \\
        \frac{\omega^2}{c^2} - k_z^2 > 0
        &\implies E_0(x) = A \cos(k x) + B \sin(k x)
    \end{align*}
    Pour ne garder
    que des solutions
    compatibles avec les conditions aux limites,
    on ne retient que
    le cas oscillant
    où
    $k_z^2 < \flatfrac{\omega^2}{c^2}$,
    et on doit poser $A = 0$
    avec $ka = p\pi, p \in \mathbb{N}$.
    Finalement,
    le champ électrique $\vb{\mathcal{E}}$
    présent dans le guide
    va pouvoir se décomposer
    sur la base des solutions
    qui s'écrivent~:
    \begin{equation*}
        \vb{\mathcal{E}}_p(x, y, z, t)
        = E_{p,0} \sin(\frac{p \pi x}{a}) e^{i(\omega t - k_z z)} \vu{y}
    \end{equation*}
    ces solutions
    constituent l'ensemble des \emph{modes} TE
    pour une onde de pulsation $\omega$.
    L'onde est
    progressive selon $\vu{z}$
    et
    stationnaire selon $\vu{x}$~;
    dans les plans
    où $z = \cst$
    l'amplitude varie.

    \subsubsection{Champ $\vb{\mathcal{B}}$}

    L'équation de \propername{Helmoltz}
    étant valable pour $\vb{B}$
    et les conditions aux limites étant connues
    on pourrait procéder de la même manière
    pour déterminer
    la forme de $\vb{B}_p$.
    Mais l'équation de \propername{Maxwell-Faraday}
    permet d'arriver au résultat
    plus rapidement~:
    \begin{align*}
        \pdv{\vb{\mathcal{B}}_p}{t}
        &= - \curl\vb{\mathcal{E}}_p
        \\
        &= \qty(- \pdv{z} \vu{x} + \pdv{x} \vu{z}) \mathcal{E}_y
    \end{align*}
    après dérivations
    puis intégration
    on trouve~:
    \begin{equation*}
        \vb{\mathcal{B}}_p
        = - E_{p,0} \qty(
            \frac{k_z}{\omega} \sin(\frac{p\pi x}{a}) \vu{x}
            + i \frac{p\pi}{\omega a} \cos(\frac{p\pi x}{a}) \vu{z}
        ) e^{i(\omega t - k_z z)}
    \end{equation*}

    \subsubsection{Structure de l'onde électromagnétique}

    Contraîrement à ce que l'on trouve
    pour la propagation libre
    des ondes électromagnétiques,
    l'onde obtenue ici
    n'est pas plane (on en a déjà discuté)
    mais en plus
    n'est pas transverse
    (on n'a pas $\vb{E} \perp \vb{B} \perp \vb{k}$)
    puisque la partie magnétique
    porte une composante selon $\vu{z}$.
    Le vecteur de \propername{Poynting} lui,
    est bien dirigé selon $\vu{z}$.

    La structure de l'onde
    ne dépend pas que du milieu,
    mais aussi des conditions aux limites.

    \begin{todo}
        Vérifier dans quelle mesure
        la forme des champs
        en solution TM
        est semblable
        (la relation de dispersion
        est la même).
    \end{todo}

    \subsection{Dispersion}

    On a trouvé
    lors de l'exploitation des conditions aux limites
    la relation de dispersion~:
    \begin{equation*}
        k_z^2
        = \frac{\omega^2}{c^2} - k^2
        = \frac{\omega^2}{c^2} - \qty(\frac{p\pi}{a})^2
    \end{equation*}
    le guide d'onde
    est donc dispersif
    même si l'onde
    se propage dans le vide à l'intérieur.
    Pour garder $k_z$ réel
    on a une pulsation de coupure pour chaque mode~:
    \begin{equation*}
        \omega_{c,p}
        = \frac{p\pi c}{a}
    \end{equation*}
    les $k_z$ s'écrivent alors~:
    \begin{equation*}
        k_z^2
        = \frac{\omega^2 - \omega_{c,p}^2}{c^2}
    \end{equation*}

    \pyimgen{guide_donde_dispersion}
    \pyimgen{guide_donde_paquet_donde}

    \subsection{Modèles équivalents}

    \subsubsection{Courants et charges en surface}

    \begin{draft}
        On ne pourra pas
        trouver un modèle similaire
        au câble coaxial
        puisqu'ici
        il n'y a qu'un seul conducteur.
    \end{draft}

    \subsubsection{Solutions en ondes planes}

    \begin{draft}
        Rayons lumineux~?
    \end{draft}

