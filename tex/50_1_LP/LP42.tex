\chapter{Fusion, fission}

\niveau{L3}

\prereqstart
{Atomistique}
{Classification périodique}
{Machines thermiques}
\prereqstop

Le graphe
sur lequel on représente
l'abondance des éléments chimique dans l'univers
en fonction de leur numéro atomique,
présente quelques particularités.

\begin{media}
    \url{https://commons.wikimedia.org/wiki/File:SolarSystemAbundances.png?uselang=fr}
\end{media}

Dans cette leçon
nous allons discuter
des noyaux atomiques,
nous proposerons
un modèle semi-empirique
pour les décrire
et expliquer la tendance du graphe.

Nous parlerons de radioactivité,
découverte par \propername[Henri]{Becquerel} en 1896.

\section{Le noyau atomique \cite{ngo}}

    \subsection{Densité et interactions internes}

    \propername{Rutherford} a montré en 1932
    que les atomes d'or
    étaient majoritairement vides,
    composés d'électrons
    qui circulent autour d'un noyau
    dont il put estimer la taille.
    Connaissant
    la taille du noyau
    et sa masse,
    on peut en calculer
    la densité~:

    \begin{table}[H]
        \centering
        \begin{tabular}{
                l
                    S[table-format=3.3]
                        S[table-format=3.0]
                            S[table-format=2.1e2]}
            \toprule
            \ce{^197_79Au}
                & {Rayon (\si{\pico\meter})}
                    & {Masse (\si{\amu})}
                        & {Densité (\si{\kilo\gram\per\meter\cubed})} \\
            \midrule
            Atome
                & 135
                    & 196
                        & 31e3 \\
            Noyau
                & 0.027
                    & 196
                        & 3.9e15 \\
            \bottomrule
        \end{tabular}
    \end{table}

    On constate
    que la densité nucléaire
    est bien supérieure
    à celle de la matière.
    On sait par ailleurs
    que les noyaux
    sont composés
    de protons,
    de charge $+e$.
    La force de \propername{Coulomb}
    s'y exerce,
    et doit être compensée
    par une autre
    pour assurer la stabilité
    des noyaux~:
    c'est l'interaction forte
    qui joue ce rôle,
    en exercant des forces attractives
    entre les nucléons.

    \begin{table}[H]
        \centering
        \begin{tabular}{lSSr}
            \toprule
            Interaction
                & {Portée (\si{\meter})}
                    & {Intensité (relative)}
                        & Au sein du noyau \\
            \midrule
            Gravitationelle
                & $\infty$
                    & e-26
                        & Négligeable \\
            Électrostatique
                & $\infty$
                    & e-3
                        & Entre les protons \\
            Forte
                & e-15
                    & 1
                        & Entre les nucléons \\
            Faible
                & e-18
                    & e-7
                        & Entre particules élémentaires \\
            \bottomrule
        \end{tabular}
    \end{table}

    On constate d'ailleurs,
    expérimentalement,
    une corrélation entre
    le nombre de protons
    et le nombre de neutrons.

    \begin{media}
        Demie-vie
        des nucléides observés
        (age de l'univers \SI{13.8e9}{\year})~: \\
        \url{https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Isotopes_and_half-life.svg/2000px-Isotopes_and_half-life.svg.png}
    \end{media}

    \begin{media}
        Simulation d'une population de particules
        dont la demie-vie est de \SI{10}{\second}~:
        \url{https://meaningof42.github.io/radioactive/main.html}
    \end{media}

    \subsection{Composition et masse}

    \begin{draft}
        Dans la suite de la leçon,
        on jongle entre les unités
        en suivant l'équivalence~:
        $\SI{1}{\amu} = \num{931.5}\flatfrac{\si{\mega\eV}}{c^2}$.
        Certains ouvragent
        changent de système d'unité
        et posent $c^2 = 1$
        de sorte à pouvoir écrire que
        $\SI{1}{\amu} = \SI{931.5}{\mega\eV}$,
        ce qu'on évitera ici.
        On utilisera en revanche
        l'unité d'énergie $\si{\amu}c^2$.
    \end{draft}

    Une dernière chose à découvrir
    concernant les noyaux~:

    \begin{table}[H]
        \centering
        \begin{tabular}{cS[table-format=3.4]}
            \toprule
            Particule & {Masse (\si{\amu})} \\
            \midrule
            Proton    & 1.0073 \\
            Neutron   & 1.0087 \\
            \bottomrule
        \end{tabular}
    \end{table}
    \begin{table}[H]
        \centering
        \begin{tabular}{cS[table-format=3.4]SSS}
            \toprule
            Particule
                & {Masse}
                    & \multicolumn{2}{c}{Défaut de masse}
                        & {Énergie de liaison par nucléons} \\
                & {(\si{\amu})}
                    & {(\si{\amu})}
                        & {($\flatfrac{\si{\mega\eV}}{c^2}$)}
                            & {($\si{u}c^2$)} \\
            \midrule
            Noyau d'or \ce{^197_79Au}
                & 196.9272
                    & {\multirow{2}{*}{\num{1.6761}}}
                        & {\multirow{2}{*}{\num{1561}}}
                            & {\multirow{2}{*}{\num{7.92}}} \\
            \ce{79 p + 118 n}
                & 198.6033
                    &
                        &
                            & \\
            \midrule
            Noyau de deutérium \ce{^2_1H}
                & 2.0136
                    & {\multirow{2}{*}{\num{0.0024}}}
                        & {\multirow{2}{*}{\num{2.24}}}
                            & {\multirow{2}{*}{\num{1.12}}} \\
            \ce{p + n}
                & 2.0160
                    &
                        &
                            & \\
            \midrule
            Noyau de fer \ce{^56_26Fe}
                & 55.9216
                    & {\multirow{2}{*}{\num{0.5292}}}
                        & {\multirow{2}{*}{\num{492.9}}}
                            & {\multirow{2}{*}{\num{8.80}}} \\
            \ce{p + n}
                & 56.4508
                    &
                        &
                            & \\
            \bottomrule
        \end{tabular}
    \end{table}

    La somme
    de la masse
    des particules
    qui composent un noyau
    n'est pas égale
    à la masse du noyau.
    Il existe
    ce que l'on va appeler
    un défaut de masse
    (ou excès, selon ce que l'on regarde)~:
    \begin{equation*}
        \Delta m
        = \qty[Z m_p + (A-Z) m_n] - m(A, Z)
        > 0
    \end{equation*}
    Les nucléons
    qui composent un noyau stable
    sont liés entre eux
    par des interactions attractives
    d'énergie $E_l < 0$.
    Lorsque l'on veut dissocier
    un noyau stable
    en nucléons isolés,
    ont doit apporter l'énergie $B = -E_l$.
    Par la relation d'\propername{Einstein}
    $E = m c^2$,
    on mesure
    dans le système de nucléons isolés,
    un excès de masse
    $\Delta m = \flatfrac{B}{c^2}$.

    Pour caractériser
    la stabilité des nucléons
    dans un noyau,
    il est plus pertinant
    de discuter de
    l'énergie de liaisons \emph{par nucléons},
    puisque pour deux noyaux
    de nombre de masses
    $A_1$ et $A_2 > A_1$
    de même énergie de liaison $B_1 = B_2$,
    l'énergie de liaison par nucléons
    $\frac{B_1}{A_1} > \frac{B_2}{A_2}$
    nous renseignera
    sur l'énergie moyenne
    de chaque nucléons
    au sein du noyau.

    En revenant
    sur les exemples précédents,
    on pourra dire que
    le noyau de fer
    est plus lié que le noyau d'or
    qui
    est plus lié que le noyau de deutérium.

    \begin{media}
        Énergie de liaison par nucléons pour les isotopes les plus courants~:\\
        \url{https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Binding_energy_curve_-_common_isotopes_FR.svg/690px-Binding_energy_curve_-_common_isotopes_FR.svg.png}
    \end{media}

    Rapidement,
    calculons la masse
    portée par l'énergie de liaison
    de l'électron
    dans l'\emph{atome} d'hydrogène~:
    $B = \SI{13.6}{\eV}$,
    donc $\Delta m = \SI{1.5e-8}{\amu}$.
    Elle est négligeable.

    \subsection{Modèle de la goutte liquide}

    La valeur
    de l'énergie de liaison
    est prévisible
    par un modèle semi-empirique,
    dit \emph{de la goutte liquide}
    car nous allons modéliser
    le noyau
    comme un volume (une goutte)
    rempli de particules (les nucléons)
    parmis lesquelles
    celles en surface
    apportent une énergie négative.

    La formule
    de \propername{Bethe-Weizsäcker} (1936)
    s'énnonce~:
    \begin{equation*}
        B(A, Z)
        = \underbrace{E_v}_{\propto A}
        - \underbrace{E_c}_{\propto \frac{e^2 Z(Z-1)}{R}}
        - \underbrace{E_s}_{\propto 4\pi R^2}
        - \underbrace{E_a}_{\propto \frac{(A-2Z)^2}{A}}
    \end{equation*}
    Les quatre termes d'énergie
    sont~:
    \begin{itemize}
        \item l'énergie dûe à l'interaction forte ($v$ pour volume)
        \item l'énergie électrostatique ($c$ pour \propername{Coulomb})
        \item l'énergie de surface
        \item l'énergie d'antisymétrie
                (lié au principe d'exclusion de \propername{Pauli})
    \end{itemize}
    \begin{todo}
        Une biblio
        qui explique
        le terme d'antisymétrie~?
    \end{todo}

    La densité nucléaire
    discutée plus haut
    et donnée pour l'atome d'or
    (\SI{3.9e15}{\kilo\gram\per\meter\cubed})
    est en fait
    constante
    pour tous les noyaux,
    on pourra écrire~:
    \begin{equation*}
        \rho = \frac{4}{3} \pi R^3
        \qq{et}
        \rho \propto A^3
        \qq{d'où}
        R \propto A^{1/3}
    \end{equation*}
    Expérimentalement,
    $R = \num{1.2}\,A^{\flatfrac{1}{3}}\;\si{\femto\meter}$.
    On a~:
    \begin{equation*}
        B(A, Z)
        = a_v A
        - a_c \frac{Z(Z-1)}{A^{\flatfrac{1}{3}}}
        - a_s A^{\flatfrac{2}{3}}
        - a_a \frac{(A-2Z)^2}{A}
    \end{equation*}
    avec les valeurs numériques
    tabulées pour les coefficients~:
    \begin{align*}
        a_v &\approx \SI{16}{\mega\eV}
            & a_c &\approx \SI{0.7}{\mega\eV} \\
        a_s &\approx \SI{17}{\mega\eV}
            & a_a &\approx \SI{23}{\mega\eV} \\
    \end{align*}

    \subsection{Stabilité}

    Le modèle
    permet alors
    de prévoir l'isotope le plus stable
    d'un élément de la classification périodique.
    Une recherche
    des valeurs qui vérifient
    $\eval{\pdv{B}{A}}_Z = 0$
    donnera
    l'équation de la
    \emph{vallée de stabilité}
    présentée précédement.

    Sous cette compréhension
    de l'énergie de liaison,
    la courbe $B(A, Z)$
    présente le fer \ce{^56Fe}
    comme extrémum
    le plus stable.

    Les systèmes physiques
    gouvernés par des énergies potentielles
    évoluent de sorte à minimiser ces dernières,
    on comprend
    que certains processus
    analogues à des réactions chimiques
    peuvent être thermodynamiquement favorables,
    lorsque l'énergie $B$ augmente.

\section{Fission}

    \subsection{Fission spontannée}

    Nous étudions
    le processus~:
    \begin{equation*}
        \ce{^{238}U -> ^{145}La + ^{90}Br + 3n}
    \end{equation*}
    on constate que
    le nombre de masses
    et le nombre de charges
    est bien conservé.

    \begin{table}[H]
        \centering
        \begin{tabular}{cS[table-format=3.0]S[table-format=3.0]}
            \toprule
            Particule
                & {Énergie de liaison par nucléons (\si{\mega\eV})}
                    &  \\
            \midrule
            \ce{^238_98U}  & 1801 & 1801 \\
            \midrule
            \ce{^145_57La} & 1199 & \\
            \ce{^90_35Br}  & 763  & 1962 \\
            \ce{^1_0n}       & 0    & \\
            \bottomrule
        \end{tabular}
    \end{table}

    \subsection{Fission induite}

    \subsection{Réacteurs à fission}

\section{Fusion}

    \subsection{Fusion stellaire, l'origine des éléments}

    \subsection{Réacteurs à fusion}
