\chapter{Machines thermiques réelles}

    Une machine thermique
    est un système qui permet de convertir de l'énergie
    entre les formes thermique et mécanique.
    Dans une machine thermique,
    un fluide parcourt un chemin cyclique
    au cours duquel il subit
    plusieurs transformations thermodynamiques.
    Ces transformations ont lieux
    dans différentes parties de la machine,
    il peut s'agir
    d'échanger de l'énergie thermique
    avec une source chaude ou froide
    ou de l'énergie mécanique
    avec une turbine ou une pompe.

    \begin{draft}
        Faire attention
        à la différence entre
        \emph{efficacité}
        et \emph{rendement}.
    \end{draft}

  \section{Situation}

    On étudie une machine thermique cyclique ditherme,
    dont le fluide échange
    l'énergie thermique massique $q_c$ avec une source chaude,
    l'énergie thermique massique $q_f$ avec une source froide,
    le travail massique $w$ avec un système extérieur.
    Suite à l'écriture des premier et second principe
    sur un cycle,
    l'inégalité de \propername{Clausius} donne~:
    \begin{equation*}
        \frac{q_c}{T_c} + \frac{q_f}{T_f} \leq 0
    \end{equation*}

    Le diagramme de \propername{Raveau}
    permet de distinguer
    quatre classes de machines thermiques,
    d'utilité différente~:
    \begin{media}
        \url{https://fr.wikipedia.org/wiki/Fichier:Diagramme_de_Raveau.png}
    \end{media}
    \begin{itemize}
        \item {\romannumeral 1}~:
            $q_c > 0$, $q_f < 0$, $q_c + q_f = w < 0$
            $\rightarrow$ moteur
        \item {\romannumeral 2}~:
            $q_c > 0$, $q_f < 0$, $q_c + q_f = w > 0$
            $\rightarrow$ système de refroidissement
        \item {\romannumeral 3}~:
            $q_c < 0$, $q_f < 0$, $q_c + q_f = w > 0$
            $\rightarrow$ sans intéret
        \item {\romannumeral 4}~:
            $q_c < 0$, $q_f > 0$, $q_c + q_f = w > 0$
            $\rightarrow$ réfrigérateur
    \end{itemize}

    Nous allons nous intéresser
    à deux d'entre eux~:
    \begin{itemize}
        \item Les moteurs
            sont des machines
            qui produisent du travail utilisable ($w < 0$).
        \item Les réfrigérateurs (ou pompe à chaleur)
            sont des machines
            qui transfèrent de l'énergie thermique
            d'une source froide ($q_f > 0$)
            vers une source chaude ($q_c < 0$),
            pour refroidir la source froide
            ou réchauffer la source chaude.
    \end{itemize}

  \section{Moteur~: cycle de \propername{Rankine}}

    L'étude d'un cycle sans combustion
    se prête bien à une première découverte
    des machines thermiques réelles,
    puisqu'il n'y a pas de difficulté
    concernant la modélisation
    des sources chaude et froide.

    Le cycle de \propername{Rankine}
    est utilisé dans le circuit d'eau secondaire
    des centrales nucléaires
    pour produire l'énergie électrique.

    \subsection{Cycle de fonctionnement}

    Le cycle se compose de quatre transformations~:
    \begin{itemize}
        \item 1~:
            L'eau qui sort du condenseur
            est liquide, sous la pression $P_1$
            saturante à la température $T_2$.
        \item $1 \rightarrow 2$~:
            Dans la pompe,
            l'eau liquide subit
            une petite augmentation de pression adiabatique réversible,
            sa température
            et sa pression
            ne varient quasiment pas.
        \item $2 \rightarrow 3$~:
            L'eau passe dans la chaudière,
            sa température augmente
            jusqu'à sa température d'ébullition sous $P_2 = P_1$,
            puis elle s'évapore complètement
            de manière isobare.
        \item $3 \rightarrow 4$~:
            Dans la turbine,
            la vapeur d'eau subit
            une détente adiabatique réversible
            jusqu'à $P_1$ et $T_1$,
            une partie de l'eau se liquéfie.
        \item $4 \rightarrow 1$~:
            Dans le condenseur,
            la vapeur d'eau restante se liquéfie
            de manière isobare
            sous la pression $P_1$.
    \end{itemize}

    \begin{figure}[H]
    \begin{tikzpicture}
        \node[draw=black] (pompe) at (0, 2) {Pompe};
        \node (pompe-in) at (-2, 2) {$W_{p} > 0$};
        \draw[->] (pompe-in) -- (pompe);

        \node (s1) at (0, 0) {\shortstack{Liquide \\$P_1 = P_{sat}(T_1)$}};

        \node[draw=black] (chaudiere) at (4, 4) {Chaudière};
        \node (chaudiere-in) at (4, 5) {$Q_c > 0$};
        \draw[->] (chaudiere-in) -- (chaudiere);
        \node (chaudiere-tr) at (4, 3) {\shortstack{Chauffage isobare\\puis vaporisation}};

        \node (s2) at (0, 4) {\shortstack{Liquide \\$P_2 > P_1$}};

        \node[draw=black] (turbine) at (8, 2) {Turbine};
        \node (turbine-out) at (10, 2) {$W_t < 0$};
        \node (machine-out) at (12, 2) {$W < 0$};
        \draw[->] (turbine) -- (turbine-out) -- (machine-out);

        \node (s3) at (8, 4) {\shortstack{Vapeur \\$P_3 = P_2 = P_{sat}(T_3)$}};

        \node[draw=black] (condenseur) at (4, 0) {Condenseur};
        \node (condenseur-out) at (4, -1) {$Q_f < 0$};
        \draw[->] (condenseur) -- (condenseur-out);
        \node (consenseur-tr) at (4, 1) {\shortstack{Liquéfaction}};

        \node (s4) at (8, 0) {\shortstack{Diphasé \\$P_4 = P_1$\\ $T_4 = T_1$}};

        \draw (s1) -- (pompe) -- (s2) -- (chaudiere) -- (s3) -- (turbine) -- (s4) -- (condenseur) -- (s1);

        \draw[->] (turbine-out) -- (10, -2) -- (-2, -2) -- (pompe-in);
    \end{tikzpicture}
    \end{figure}

    On s'intéresse à décrire ce cycle
    dans un diagramme $(T, s)$
    puis dans un diagramme $(P, v)$.
    Ils nous permettront de confronter ce cycle
    au cycle de \propername{Carnot}.

    \subsubsection{Diagramme entropique}

    Dans le diagramme $(T, s)$
    les isothermes sont des droites horizontales
    les isenthalpiques sont des droites verticales.
    Puisque pour une transformation isobare
    $\dd s = \frac{c_P}{T}\dd T$,
    alors~:
    \begin{equation*}
        s(T)
        = \int{\frac{c_P}{T}\dd T}
        = c_p \ln(\frac{T}{T_0})
        \qq{d'où}
        T \propto \exp(\frac{s}{c_P})
    \end{equation*}
    les isobares sont des exponentielles.
    Connaissant les températures
    $T_1 = \SI{30}{\degreeCelsius}$
    et $T_2 = \SI{300}{\degreeCelsius}$,
    on peut tracer le cycle~:
    \begin{media}
        \software{./documents/rankine\_eau.svg}
    \end{media}

    \subsubsection{Diagramme de \propername{Clapeyron}}

    \subsection{Efficacité}

    \subsubsection{Efficacité de \propername{Carnot}}

    Pour le circuite secondaire d'une centrale nucléaire~:
    \begin{equation*}
        \eta_C
        = \abs{\frac{W}{Q_c}}
        = 1 - \frac{T_f}{T_c}
        = 1 - \frac{\SI{30}{\degreeCelsius}}{\SI{300}{\degreeCelsius}}
        = 0.47
    \end{equation*}

    \subsubsection{Efficacité réelle}

    \begin{equation*}
        \eta
        = \abs{\frac{w}{q_c}}
        = \abs{\frac{w_t - w_p}{q_c}}
    \end{equation*}

    Du premier principe
    on obtient~:
    \begin{align*}
        w_p &= \Delta_{1 \rightarrow 2} h = h_2 - h_1 \approx 0\\
        q_c &= \Delta_{2 \rightarrow 3} h = h_3 - h_2 \approx h_3 - h_1\\
        w_t &= \Delta_{3 \rightarrow 4} h = h_4 - h_3
    \end{align*}

    Pour un circuit à eau,
    il est facile
    de trouver des valeurs tabulées
    d'enthalpie massique
    à pression et température données,
    on peut d'ailleurs lire des valeurs approcheés
    sur le diagramme entropique présenté.
    Le tableau suivant
    regroupe les valeurs utiles
    issues de \cite{tablethermoeau}.

    \begin{table}[H]
        \centering
        \begin{tabular}{SSSSS}
            \toprule
            {Température (\si{\degreeCelsius})}
                & {Pression (\si{\kilo\pascal})}
                    & \multicolumn{3}{c}{
                        {Enthalpie (\si{\kilo\joule\per\kilo\gram})}} \\
            & & {Liquide sat.} & {Évap.} & {Vapeur sat.} \\
            \midrule
            30  & 4.2 &  125.8 & 2430.5 & 2556.3 \\
            300 & 8581 & 1344.0 & 1404.9 & 2749.0 \\
            \bottomrule
        \end{tabular}
    \end{table}

    On lit~:
    $h_1 = \SI{125.8}{\kilo\joule\per\kilo\gram}$,
    $h_3 = \SI{2749.0}{\kilo\joule\per\kilo\gram}$.
    La détermination de $h_4$
    nécessite un calcul suplémentaire,
    puisque l'eau est dans un état biphasé
    en ce point.
    Sur le diagramme, on peut lire
    $x_4 = \num{0.65}$,
    donc~:
    \begin{equation*}
        h_4
        = x_4 h_{liq} + (1 - x_4) h_{vap}
        = \SI{976.5}{\kilo\joule\per\kilo\gram}
    \end{equation*}
    Finalement,
    \begin{equation*}
        \eta
        = \num{0.37}
        \qq{et}
        r
        = \frac{\eta}{\eta_C}
        = \num{0.88}
    \end{equation*}

  \section{Le réfrigérateur \cite{dunodpcsi}}

    \subsection{Cycle de fonctionnement}

    \begin{todo}
        Faire le schéma technique avec les pièces,
        se référer au schéma fait
        lors de la présentation de cette leçon,
        qui était très bien.
    \end{todo}

    Le fluide \emph{frigorigène}
    suit un circuit comportant~:
    \begin{itemize}
        \item Un compresseur
            dans lequel
            il reçoit du travail
            sans échange thermique.
        \item Un condenseur
            dans lequel
            il est en contact avec la source chaude
            ($T_c = \SI{20}{\degreeCelsius}$).
        \item Un détendeur
            dans lequel
            il ne reçoit ni travail
            ni énergie thermique.
        \item Un évaportaeur
            dans lequel
            il est en contact avec la source froide
            ($T_f = \SI{-5}{\degreeCelsius}$).
    \end{itemize}
    \begin{pydo}
        Trouver un fluide réel,
        faire le cycle dans plusieur diagrammes,
        peut être dans python.
    \end{pydo}

    \subsection{Efficacité de \propername{Carnot}}

    \begin{todo}
        Établir
        et discuter le sens physique de
        $\eta_C = \frac{T_f}{T_c - T_f}$,
        ($\eta$ diminue avec $T_f$,
        et $T_f \rightarrow T_c \implies \eta \rightarrow \inf$).
        Puis donner un odre de grandeur
        à \SI{4}{\kelvin}
        et \SI{4}{\degreeCelsius}.
    \end{todo}

    \subsection{Efficacité réelle}

    Le diagramme enthalpique $(T, H)$
    permet de la lire facilement.
