\chapter{Effet tunnel}

\niveau{L3}

\prereqstart
{Notion de fonction d'onde}
{Équation de \propername{Schrödinger}}
{États liés et quantification}
{Densité d'états en physique statistique}
\prereqstop

Dans cette leçon
qui fait suite
à celle sur la quantification
(dans laquelle nous avons décrit
une particulle
confinée dans un potentiel),
nous étudions
le cas d'une particule
qui va pouvoir évoluer
semble-t-il
sur un demi-espace.

\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \begin{axis}[
            height = 5 cm,
            width = 8cm,
            axis lines = left,
            ylabel = {$V(x)$},
            xtick={-1},
            ytick={0},
        ]
        \addplot[
            domain=0:4,
            samples=100,
        ] {x*sin(deg(x))};
        \end{axis}
    \end{tikzpicture}
\end{figure}

\section{Barrière de potentiel rectangulaire \cite{dufour}}

    \subsection{Description}

    Comme pour l'étude
    des potentiels confinants,
    nous commencons
    par un cas qui semble simple.
    \begin{figure}[H]
        \centering
        \begin{tikzpicture}
            \begin{axis}[
                height = 5 cm,
                width = 8cm,
                axis lines = left,
                ylabel = {$V(x)$},
                xmin=-1, xmax=2,
                ymin=-1, ymax=6,
                xtick={-2},
                ytick={-2},
                extra x ticks={0,1},
                extra x tick labels={$0$, $a$},
                extra y ticks={0,5},
                extra y tick labels={$0$, $V_0$},
            ]
            \addplot[mark=+] coordinates {
                (-1, 0)
                (0, 0)
                (0, 5)
                (1, 5)
                (1, 0)
                (2, 0)
            };
            \end{axis}
        \end{tikzpicture}
    \end{figure}

    Une particule
    de masse $m$
    arrive de la gauche ($x < 0$, $x$ croissant),
    avec une énergie $E$
    comprise entre $0$ et $V_0$.
    La mécanique classique
    prévoit une réflexion
    de la particule
    sur la barrière.

    \subsection{Équation de \propername{Schrödinger}}

    De l'équation de \propername{Schrödinger}
    à une dimension
    pour un potentiel indépendant du temps~:
    \begin{equation*}
        i \hbar \pdv{t} \Psi(t, x)
        = \qty(- \frac{\hbar^2}{2 m} \laplacian + V(x) )\Psi(t, x)
    \end{equation*}
    nous rappelons
    une classe de solutions
    issue de la séparation des variables~:
    \begin{equation*}
        \Psi(t, x)
        = f(t) \cdot \psi(x)
    \end{equation*}
    les deux fonctions vérifiant alors~:
    \begin{gather*}
        f(t)
        = e^{-i \frac{E}{\hbar}t}
        \qq{et}
        \qty(- \frac{\hbar^2}{2m} \dv[2]{x} + V(x)) \psi(x)
        = E \psi(x)
    \end{gather*}

    \subsubsection{Solutions des équations stationnaires}

    \paragraph{Dans les régions 1 ($x < 0$) et 2 ($x > a$)}
    où $V(x) = 0$
    on écrit~:
    \begin{equation*}
        - \frac{\hbar^2}{2m} \dv[2]{\psi(x)}{x}
        = E \psi(x)
        \implies
        \dv[2]{\psi(x)}{x} = -k^2 \psi(x)
        \qq{avec}
        k
        = \frac{\sqrt{2 m E}}{\hbar}
    \end{equation*}
    alors,
    la solution générale
    dans chaque région
    est~:
    \begin{align*}
        \psi_1(x)
        &= A_1 e^{ikx} + B_1 e^{-ikx}
        \\
        \psi_2(x)
        &= A_2 e^{ikx} + B_2 e^{-ikx}
    \end{align*}

    \paragraph{Dans la région 0 ($0 < x < a$)}
    où $V(x) = V_0$
    on écrit~:
    \begin{equation*}
        - \frac{\hbar^2}{2m} \dv[2]{\psi(x)}{x}
        = (E - V_0) \psi(x)
        \implies
        \dv[2]{\psi(x)}{x} = -\kappa^2 \psi(x)
        \qq{avec}
        \kappa^2
        = \frac{- 2 m \abs{E - V_0}}{\hbar^2}
    \end{equation*}
    cette fois la solution générale est~:
    \begin{equation*}
        \psi_0(x)
        = A_0 e^{\kappa x} + B_0 e^{-\kappa x}
    \end{equation*}

    \subsubsection{Fonctions d'ondes}

    En écrivant les fonctions d'ondes
    qui correspondent~:
    \begin{align*}
        \Psi_1(t, x)
        &= A_1 e^{i(kx - \frac{E}{\hbar} t)}
            + B_1 e^{-i(kx + \frac{E}{\hbar} t)}
        \\
        \Psi_0(t, x)
        &= A_0 e^{\kappa x - i \frac{E}{\hbar} t}
            + B_0 e^{- \kappa x - i \frac{E}{\hbar} t}
        \\
        \Psi_2(t, x)
        &= A_2 e^{i(kx - \frac{E}{\hbar} t)}
            + B_2 e^{-i(kx + \frac{E}{\hbar} t)}
    \end{align*}
    nous trouvons
    dans les régions 1 et 2
    des équations d'ondes
    qui sont
    combinaisons linéaires
    d'ondes planes progressives (OPP).
    Les OPP associées
    aux coefficients $A_i$ ($B_i$)
    correspondent à une propagation
    dans le sens $+x$ ($-x$)~:
    \begin{itemize}
        \item l'OPP associée au coefficient
            $A_1$ correspond à la particule incidente,
        \item l'OPP associée au coefficient $B_1$
            correspond à la particule réflechie,
        \item l'OPP associée au coefficient $A_2$
            correspond à la particule transmise,
        \item l'OPP associée au coefficient $B_2$
            correspond à la particule
            qui aurait été réfléchie
            loin à l'infini.
    \end{itemize}
    Dans notre modèle simple,
    le seul «~obstacle~»
    est la barrière
    située entre $x=0$ et $x=a$~:
    l'OPP associée à $B_2$
    n'a donc pas lieu d'être,
    nous allons imposer $B_2 = 0$.

    \paragraph{Sur la solution globale}
    On remarquera
    qu'elle n'est pas de carré sommable.
    La particule se trouvera donc
    en réalité
    dans une combinaison linéaire
    (une superposition quantique)
    de ces états
    (que l'on appelle un \emph{paquet d'ondes}).
    Notons
    que le spectre accessible
    est continu,
    il n'y a plus de quantification.

    \subsection{Réflexion et transmission}

    \begin{todo}
        Voir si
        l'introduction/utilisation du courant de probabilités
        est nécessaire ou non.
        Il donne de toute façon
        $R = \abs{B_1}^2$
        et
        $T = \abs{A_2}^2$,
        comme nous l'interprétons
        déjà.
        Si oui,
        l'ajouter aux prérequis.
    \end{todo}

    \begin{todo}
        Donner le mode de calcul
        pour
        $R = \abs{A_2}^2$
        et
        $T = \abs{B_1}^2$
        en fonction
        de $E$
        (et de $A_1$ comme paramètre).
        Mais ne pas le faire
        car c'est
        très long
        et les expressions sont moches
        et vont prendre du temps à écrire.

        Remarquer que $R+T=1$.
    \end{todo}

    \begin{pydo}
        Tracer les graphes
        des fonctions
        $R(E)$
        et $T(E)$
        en faisant apparaître
        la valeur de $V_0$~;
        puis animer $\Psi(t, x)$.
    \end{pydo}

    \subsection{Présence «~dans~» la barrière}

    \begin{draft}
        Remarquons que
        puisque $R+T=1$,
        la probabilité de présence
        de la particule
        entre $x=0$ et $x=a$
        est nulle.

        Cependant,
        si l'on y place un détecteur,
        on va détecter la particule
        (car on ajoute un trou~!).
    \end{draft}

    \section{Ordres de grandeurs dans des potentiels réels}

    \subsection{Microscope à effet tunnel}

    L'effet tunnel
    présente un intéret technologique\dots

    \subsection{Électrons dans les transistors}

    Mais il pose aussi des problèmes\dots

    \subsection{Particule $\alpha$ dans les noyaux lourds}

    S'il semble associé
    à des conditions bien spécifiques,
    n'oublions pas
    qu'il s'agit d'abord
    d'un phénomène naturel\dots

\plusloin{
    On pourra montrer
    qu'une particule d'énergie
    $E > V_0$
    n'est pas indifférente
    à la présence de la barrière.
    Ensuite,
    on pourra discuter
    la modélisation de potentiels réels
    selon l'approximation WKB \cite{dufour}.
}
