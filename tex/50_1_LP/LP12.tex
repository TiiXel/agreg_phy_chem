\chapter{Premier principe de la thermodynamique}

    La thermodynamique est une science qui a été introduite
    lors de la révolution industrielle.
    Elle traîte
    des échanges d'énergies
    et de l'évolution des systèmes d'un état d'équilibre vers un autre
    en généralisant le théorème de l'énergie cinétique.
    Le premier principe de la thermodynamique
    justifie l'impossibilité de créer des machines à mouvement perpétuel,
    recherchées à cette période de l'histoire des sciences.

    \begin{todo}
        Voir si la première partie
        ne doit pas aller en prérequis.
    \end{todo}

  \section{Énergie interne d'un système thermodynamique}

    \subsection{Énergie totale d'un système \cite{dunodpsi}}

    Un système physique (quel qu'il soit) contient de l'énergie,
    qui peut se manifester sous différentes formes.
    \begin{equation*}
        E_{totale}
        = E_{c,\,macro} + E_{pot,\,ext} + U_{\acute{e}nergie\,interne}
    \end{equation*}
    Le terme d'énergie interne contient~:
    \begin{itemize}
      \item l'énergie cinétique
          de toutes les particules qui composent le système
          par rapport au centre de masse du système
      \item les énergies potentielles d'interactions
          entre toutes les particules
      \item \dots
    \end{itemize}

    Pour calculer
    $E_{c,\,macro}$
    et $E_{pot,\,macro,\,ext}$
    on divise le système en éléments de volumes mésoscopique
    et on somme les énergie de ces éléments.

    Il est beaucoup trop difficile d'évaluer $U$
    en considérant chacune des $10^{23}$ particules
    qui composent le système,
    mais on sait qu'elle peut se calculer
    à partir des variables d'état.

    Cela dit,
    il est parfois tout aussi compliqué
    de déterminer la forme fonctionnelle de la fonction d'état
    $U = \fof(P, V, T, N, \dots)$.
    Toutefois, une étude du système
    permet facilement de déterminer les variations de son énergie
    lorsqu'il subit une transformation.

    \subsection{Variation d'énergie interne d'un système thermodynamique}

    Puisqu'il y a plusieurs conrtibutions
    à l'énergie d'un système thermodynamique,
    il y a plusieurs manières de la faire varier.
    Les énergies cinétique
    macroscopique et potentielle exterieure
    s'expriment comme en mécanique classique
    et varient selon les mêmes procédés.

    L'énergie interne $U(P, V, T, N)$
    peut varier de différentes manières,
    lorsque ses variables changent de valeurs.

    Exemples~:
    \begin{itemize}
      \item On chauffe un récipient fermé indéformable rempli d'eau~:
          on apporte de l'énergie thermique
          et la température augmente.
      \item On compresse un volume d'air
          maintenu à température constante
          dans un piston~:
          on apporte du travail (énergie mécanique),
          la pression augmente.
    \end{itemize}

    Dans ces deux cas,
    l'augmentation de $T$ ou $P$
    va se traduire par une augmentation de l'énergie interne du système.
    On notera les échanges d'énergie thermique $Q$, et $W_{nc}$
    le travail des forces non conservaitves
    exercées par l'extérieur sur le système.

    Ces deux grandeurs sont algébriques
    car le système peut
    recevoir
    ou donner
    de l'énergie thermique et du travail.
    La convention est alors de prendre
    des valeurs positives
    quand le système reçoit de l'énergie,
    et négatives
    quand il en cède.

    \subsection{Capacité thermique à volume constant}

    Pour un système fermé
    qui subit une transformation à volume constant,
    on définit sa capacité thermique à volume constant~:
    \begin{equation*}
        C_V
        = \eval{\pdv{U}{T}}_{V, n}
        = \left\{\begin{array}{ll}
            \flatfrac{3}{2} n R & \qq{pour un gaz parfait monoatomique}\\
            \flatfrac{5}{2} n R & \qq{pour un gaz parfait diatomique}\\
            m c_V & \qq{pour les phases condensées}
        \end{array}\right.
    \end{equation*}
    \begin{todo}
        Donner des valeurs,
        en particulier pour l'eau.
        Dire qu'on a parfois
        la valeur molaire ou massique
        tabulée.
    \end{todo}

  \section{Le premier principe de la thermodynamique}

    \subsection{Énoncé}

    Le premier principe de la thermodynamique énonce
    que pour un système fermé,
    l'énergie interne
    (notée $U$)
    est extensive
    et est une fonction d'état,
    telle que au cours d'une transformations
    pendant laquelle il reçoit algébriquement
    une énergie thermique $Q$
    et un travail $W$~:
    \begin{equation*}
        \Delta{E_{tot}}
        = \Delta{E_{c,\,macro}} + \Delta{E_{pot,\,ext}} + \Delta{U}
        = W_{nc} + Q
    \end{equation*}
    Il s'agit d'un principe de conservation de l'énergie.
    Comme fonction d'état,
    $U$ ne dépend
    que de l'état (d'équilibre)
    dans lequel se trouve le système.

    \subsection{Termes d'échange de travail: $W_{nc}$}

    Sources de travail~:
    \begin{itemize}
        \item Forces de pression
        \item Travail d'une pompe
        \item Force électrochimique entre les molécules du système
    \end{itemize}

    Dans le cas des forces de pression (que l'on rencontrera souvent),
    la  force de pression exercée par le milieu extérieur
    sur le système, qui voit son volume varier
    de $V_i$ à $V_f$,
    fourni entre un travail~:
    \begin{equation*}
        W_p = - \int_{V_i}^{V_f} P_{ext} \cdot \dd{V}
    \end{equation*}
    où $P_{ext}$
    est la pression dans le milieu extérieur
    au voisinage du système.
    Si cette pression est constante alors~:
    \begin{equation*}
        W_p = - P_{ext} \Delta{V}
    \end{equation*}
    En particulier,
    si le système est à l'équilibre mécanique avec l'extérieur
    pendant toute la transformation ($P = P_{ext}$)
    alors~:
    $W_p = -P\Delta{V}$
    puis on peut écrire~:
    \begin{equation*}
        \Delta(U + PV) = W_{\cancel{p}} + Q
    \end{equation*}

    \subsection{Exemple~: échauffement d'un gaz par compression}

    \citep[chap 24, 1.5 c)]{dunodpcsi}

  \section{Une autre fonction d'état~: l'enthalpie}

    Nous avons déjà établi,
    pour une transformation isobare~:
    $\Delta(U + PV) = W_{\cancel{p}} + Q$.
    On constate que
    $U+PV$ est une fonction d'état
    (elle ne dépend que de variables d'état
    et est extensive \cite{gie}).
    On appelle cette grandeur enthalpie~:
    $H = U + PV$,
    et on peut réécrire le premier principe comme~:
    \begin{equation*}
        \Delta{E_{c,\,macro}} + \Delta{E_{pot,\,ext}} + \Delta{H}
        = W_{\cancel{p}} + Q
    \end{equation*}
    Notons que
    dans le cas particulier des gaz parfaits,
    puisque $PV = nRT$,
    alors $H = U + nRT$.

    \subsection{Capacité thermique à pression constante}

    De manière similaire à l'énergie interne,
    on nomme la dérivée partielle de $H$ par rapport à $T$,
    \emph{capacité thermique à pression constante}~:
    \begin{equation*}
        C_P
        = \eval{\pdv{H}{T}}_P
        = \eval{\pdv{U}{T}}_P + \eval{\pdv{(PV)}{T}}_P
        = \left\{\begin{array}{ll}
            \flatfrac{5}{2} n R & \qq{pour un GP monoatomique}\\
            \flatfrac{7}{2} n R & \qq{pour un GP diatomique}\\
            m c_P \approx m c_V & \qq{pour les phases condensées}
        \end{array}\right.
    \end{equation*}

    \subsection{Intéret}

    L'intéret est qu'il n'y a pas besoin de calculer $W_p$,
    qui est déjà compté dans le terme $\Delta H$.
    Si l'on chauffe un volume de gaz à pression constante,
    il va s'étendre et obéir au premier principe.
    Connaissant $Q$ (l'énergie thermique apportée au gaz)
    et $\Delta V$,
    on peut calculer $\Delta T$.
    Mais si l'on connaît $\Delta H$,
    alors on peut connaître $\Delta T$
    sans connaître la variation de volume.

    \subsection{Exemple d'application}

    \begin{todo}
        Ajouter une application de \cite{dunodpcsi},
        ou alors la détente de \propername{Joules-Kelvin} (\cite{gie})
        avec le premier principe industriel
        qui permet d'aller plus loins.
    \end{todo}
