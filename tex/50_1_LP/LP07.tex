\chapter{Dynamique relativiste}

\section{Outils de la dynamique relativiste}

    \subsection{Quadrivecteur énergie-impulsion}

    On aura vu,
    en cinématique relativiste,
    qu'un évènement
    doit être repéré par
    un quadrivecteur position
    $\vq{X} = (ct, x, y, z)$
    et que la vitesse d'un point
    dans un référentiel $\fref{R}$
    doit s'exprimer comme
    $\vq{V} = \dv{\vq{X}}{\tau}$
    avec $\tau$
    le temps propre
    perçu par ce point
    dans son référentiel $\fref{R_0}$.

    L'outil de base de la dynamique newtonienne
    est le principe fondamental de la dynamique
    \begin{equation*}
        \dv{\vb{p}}{t} = \vb{F}
    \end{equation*}
    si l'on veut étudier des mouvements
    en mécanique relativiste,
    il va être nécessaire d'adapter le principe
    pour le rendre invariant
    lors d'un changement de référentiel
    selon les transformations de \propername{Lorentz},
    de manière à vérifier
    le principe de relativité.
    La mécanique classique repose également sur
    la conservation de l'énergie pour laquelle on peut écrire~:
    \begin{equation*}
        \dd E
        = \vb{F} \vb{v} \dd t
    \end{equation*}

    Nous allons utiliser
    le formalisme des quadrivecteurs,
    et sommes soumis à quelques contraintes~:
    \begin{itemize}
        \item À basse vitesse,
            nous devons retrouver la mécanique classique.
        \item La nouvelle équation
            doit être invariante par transformations de \propername{Lorentz}.
        \item Les grandeurs conservées
            en mécanique classique ($p$ et $E$)
            doivent être conservées.
    \end{itemize}
    Nous partons des relations précédentes~:
    \begin{equation*}
        \dv{\vb{p}}{t}
        = \vb{F}
        \qq{et}
        \dd E
        = \vb{F} \vb{v} \dd t
    \end{equation*}
    qui ensemble donnent~:
    \begin{equation*}
        \dd E
        = \dv{\vb{p}}{t} \vb{v} \dd t
        \implies
        \frac{\dd E}{c} c \dd t - \dd \vb{p} \dd \vb{r}
        = 0
    \end{equation*}
    on reconnaît ici
    la norme dans l'espace de \propername{Minkowski}
    d'un quadrivecteur s'écrivant~:
    \begin{equation*}
        \vq{P} = (\flatfrac{E}{c}, \vb{p})
    \end{equation*}
    Nous l'appelleront
    quadrivecteur énergie-impulsion.
    La norme au carré du quadrivecteur s'écrit et vaut~:
    \begin{equation*}
        \norm{\vq{P}}^2
        = m^2 c^2
        = \frac{E^2}{c^2} - \vb{p}\dotproduct\vb{p}
    \end{equation*}
    on remarque que,
    étant donné le caractère d'invariant relativiste de la norme,
    la masse $m$
    est elle aussi, un invariant relativiste.
    En outre,
    on tire une expression pour l'énergie~:
    \begin{equation*}
        E^2
        = m^2 c^4 + \vb{p}^2 c^2
    \end{equation*}

    Enfin,
    la question des particules sans masse se pose~:
    \begin{equation*}
        \vq{P_{\mathrm{photon}}}
        = (\flatfrac{E}{c}, \vb{p})
        \qq{d'où}
        \qty(\frac{E}{c})^2 - \vb{p}\dotproduct\vb{p}
        = 0
        \implies
        E = c p
    \end{equation*}

    Remarquons que~:
    \begin{itemize}
        \item Dans la limite de la mécanique classique,
            pour $v \ll c$,
            la partie spatiale de $\vq{P}$
            correspond à l'impulsion de la mécanique de \propername{Newton}
            $\vb{p} = m\vb{v}$.
        \item Pour une particule au repos,
            on écrit $E = m c^2$
            autrement dit,
            même au repos,
            la particule porte une énergie.
    \end{itemize}

    \subsection{Conservations et d'invariances en dynamique relativiste}

    La norme d'un quadrivecteur
    est un invariant relativiste.
    Par ailleurs,
    la conservation de l'énergie
    et la conservation de la quantité de mouvement,
    vont être utile dans plusieurs problèmes.
    Il va se trouver que
    beaucoup de problèmes peuvent déjà être résolus
    avec ces deux seules propriétés.

\section{Utilisation de la dynamique relativiste}

    \subsection{Effet \propername{Compton} (1927)}

    L'expérience de \propername{Compton}
    consiste envoyer un photon (en rayons X)
    sur un électron
    dans un collisioneur,
    et d'observer ce qui en ressort.
    Un détecteur de photons
    et un détecteur d'électrons
    permettent de réaliser l'expérience.
    \begin{todo}
        Faire un schéma~!
    \end{todo}

    Puisque les composantes du quadrivecteur sont conservées,
    écrivons les
    (après avoir multiplité par $c$)~:
    \begin{equation*}
        \left\{
        \begin{array}{lclclcl}
            \hbar \omega &+& m c^2
            &=& \hbar \omega' &+& E' \\
            \hbar \omega &+& 0
            &=& \hbar \omega' \cos\theta &+& p' c \cos\phi \\
            0 &+& 0
            &=& \hbar \omega' \sin\theta &-& p' c \sin\phi \\
            0 &+& 0
            &=& 0 &+& 0 \\
        \end{array}
        \right.
    \end{equation*}
    On en déduit~:
    \begin{equation*}
        (p'c)^2
        = (\hbar\omega - \hbar\omega'\cos\theta)^2 + (\hbar\omega'\sin\theta)^2
    \end{equation*}
    et avec $E'^2 = m^2 c^4 + (p'c)^2$,
    les calculs vont donner, avec $\omega = \flatfrac{2 \pi c}{\lambda}$~:
    \begin{equation*}
        \lambda' - \lambda
        = \frac{\hbar}{mc} (1 - \cos\theta)
    \end{equation*}
    Ce qui est vérifié expérimentalement par \propername{Compton}
    pour toutes les valeurs de $\theta$.
    Lors du choc
    le photon cède une partie de son énergie à l'électron.

    \subsection{Topographie par émission de positrons (PET-Scan)}

    Un pan de la médecine nucléaire
    repose sur la collision
    entre une particule (électron)
    et son antiparticule (positron).

    On injecte au patient
    un traceur radioactif.
    Il s'agit d'une molécule
    dont le premier rôle va être de se fixer
    sur les tissus que l'on souhaite imager~:
    les cellules cancéreuses par exemple
    consomment beaucoup de glucose,
    alors une molécule
    qui en est biologiquement proche
    sera captée par ces cellules
    (et va s'y accumuler car n'est pas consommable).
    Ensuite,
    l'imagerie repose
    sur la détection à distance de la position des traceurs
    grace à leur radioactivité.
    Le fluorodesoxyglucose marqué au fluor 18
    est radioactif de type $\beta^+$,
    lorsqu'il se désintègre,
    le fluor donne de l'oxygène 18,
    un positron,
    et un neutrino électronique
    ($p^+ \rightarrow n + e^+ + \nu_e$).

    Lorsqu'il rencontre un électron
    des tissus organiques avoisinants (distance de l'odre du milimètre),
    le positron va s'annihiler avec l'électron
    et produidre deux photons gamma,
    capables de traverser le corps humain,
    qui partent dans la même direction
    mais le sens opposé.
    Les deux photons,
    ensemble,
    permettent,
    par mesure du temps mis à rejoindre les détecteurs,
    de déterminer précisément la position de la désintégration
    et donc,
    de la cellule cancéreuse.

    \begin{todo}
        Raconter plus de choses,
        ordre de grandeur comparé aux autres méthodes,
        autre désintégration du fluor 18,
        quarks,
        \dots
    \end{todo}

    \section{Collision inélastique~: limite GZK}
