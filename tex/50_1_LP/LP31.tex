\chapter{Présentation de l'optique géométrique à l'aide du principe de
\propername{Fermat}}

\niveau{L1}

\prereqstart
{Lois de \propername{Snell-Descartes} (2nde)}
\prereqstop

Le 25 mars 2019 à 19h12
une photo du coucher de Soleil
a été prise à Berck-Plage.

\begin{media}
    \url{https://www.infoclimat.fr/photolive-photos-meteo-241327-coucher-de-soleil.html#photo6}
\end{media}

Le logiciel de planétarium \software{Stellarium}
permet de simuler cette situation
en précisant
les coordonnées GPS et l'heure.

\begin{media}
    \software{./documents/coucher\_sol\_stellarium\_noatmo.png}
\end{media}

Il y a un problème~:
d'après le logiciel
et donc,
d'après l'équation horaire
pourtant très précises
de la trajectoire du Soleil,
ce dernier
se trouvait à la position et à l'heure indiquée
sous l'horizon.
Nous ne voulons pas
incriminer le photographe,
et nous allons chercher une explication
à ce phénomène.

L'optique géométrique
du lycée
nous apprend
que la lumière se propage en ligne droite
dans les milieux homogène,
et les lois de \propername{Snell-Descartes}
pour la réflexion et la réfraction
permettent de calculer
l'angle de déviation
des rayons lumineux
au passage d'un dioptre,
un dioptre étant
la surface qui sépare
deux milieux d'indices de réfraction différents.
L'atmosphère
est un milieux inhomogène
que l'on peut modéliser
comme une infinité de dioptres~:
cela va expliquer
le problème que nous étudions,
mais
il sera difficile
d'appliquer les lois de \propername{Snell-Descartes}.

\begin{manip}
    Envoyer
    le rayon d'un laser
    dans une longue cuve d'eau
    ($\approx \SI{40}{\centi\metre}$)
    saturée en sucre
    et ayant reposée
    de sorte à créer
    un gradient d'indice.
\end{manip}

Dans cette leçon
nous allons découvrir
un principe physique
qui sert aujourd'hui
de fondement à l'optique géométrique.
Le principe de \propername{Fermat},
postulé en 1657
permet d'unifier
les lois expérimentales de \propername{Snell-Descartes} (984 puis 1621)
sous une seule expression mathématique,
et permet d'étudier
des phénomènes plus compliqués.

On se place
dans un cadre d'optique géométrique
sans phénomènes
de diffraction
ou d'interférences.
On se place aussi
dans le cadre d'indices de réfraction réels~:
pas d'atténuation,
pas d'anisotropie.

\section{Le principe de \propername{Fermat}}

    \subsection{Chemin optique}

    Pour la lumière
    qui se déplace en ligne droite
    sur une distance $l$
    dans un milieu d'indice $n$,
    on appelle chemin optique
    la grandeur
    $\mathcal{L} = n l$.
    On généralise
    cette définition
    à une trajectoires courbe $\mathcal{C}$ de $A$ à $B$,
    paramétrée par un déplacement élémentaire $\dd s$
    dans des milieux d'indices variables $n(P)$
    par l'intégrale curviligne~:
    \begin{equation*}
        \mathcal{L}
        = \int_\mathcal{C} n(P) \dd s
    \end{equation*}

    \begin{figure}[H]
        \centering
        \begin{tikzpicture}
            \coordinate (A) at (0, 0);
            \coordinate (B) at (3, 1);

            \draw (A) node[anchor=north]{$A$};
            \draw (B) node[anchor=south]{$B$};

            \draw (A) to[out=+120,in=-40] node[below]{$\mathcal{C}$} (B);
        \end{tikzpicture}
    \end{figure}

    On rappelle
    que l'indice optique
    donne le rapport entre
    la célérité de la lumière dans le vide
    et la célérité dans le milieux
    de sorte que~:
    $v = \flatfrac{c}{n} = \dv*{s}{t}$
    avec $c \defeq \SI{299792458}{\meter\per\second}$.
    Ainsi,
    le chemin optique s'exprime~:
    \begin{equation*}
        \mathcal{L}
        = \int_\mathcal{C} n(P) \frac{c}{n(P)} \dd t
        = c \int_\mathcal{C} \dd t
        = c \Delta t
    \end{equation*}
    on comprend
    que le chemin optique
    correspond à la distance
    qu'aurait dû parcourir
    la lumière si elle était dans le vide
    pour que la durée du parcourt
    soit la même que celle
    du parcourt effectivement suivit dans la matière.

    \subsection{Énoncé du principe}

    «~La lumière se propage
    d'un point à un autre
    sur des trajectoires telles
    que le chemin optique
    soit extrêmal.~»

    Précisions le terme \emph{extrêmal}.
    On considère
    le chemin $\mathcal{C}$
    entre $A$ et $B$
    et
    le chemin $\mathcal{C'}$
    qui est une variation infinitésimale
    de $\mathcal{C}$,
    mais avec $A$ et $B$ inchangés.

    \begin{figure}[H]
        \centering
        \begin{tikzpicture}
            \coordinate (A) at (0, 0);
            \coordinate (B) at (3, 1);

            \draw (A) node[anchor=north]{$A$};
            \draw (B) node[anchor=south]{$B$};

            \draw (A) to[out=+120,in=-40] node[below]{$\mathcal{C}$} (B);
            \draw[dashed] (A) to[out=+120,in=-40,distance=2cm] node[above]{$\mathcal{C'}$} (B);
        \end{tikzpicture}
    \end{figure}

    Le chemin optique
    $\mathcal{L}$
    et le chemin optique $\mathcal{L'}$
    sont différents,
    mais
    cette différence est nulle au premier ordre
    si $\mathcal{C}$
    est le chemin effectivement suivi par la lumière.

\section{Conséquences et applications~: retrouvons l'optique géométrique}

    \subsection{Propagation en ligne droite dans les milieux homogènes}

    On considère
    un milieu d'indice $n$,
    le chemin optique
    entre deux points $A$ et $B$
    est~:
    \begin{equation*}
        \mathcal{L}
        = \int_\mathcal{C} n \dd s
        = n \int_\mathcal{C} \dd s
    \end{equation*}
    il s'agit donc d'extrêmaliser
    la distance géométrique
    entre $A$ et $B$,
    ce qui s'obtient avec la ligne droite
    (la distance est minimale,
    mais n'as pas de borne supérieure
    et n'est donc jamais maximale).

    \subsection{Principe du retour inverse de la lumière}

    On considère un chemin $\mathcal{C}$ de $A$ à $B$
    effectivement suivi par la lumière,
    de sorte que
    $\mathcal{L} = \int_\mathcal{C} n \dd s$
    soit extrêmal.
    Le calcul de
    $\mathcal{L'} = \int_\mathcal{C} n \dd s'$
    en parcourant $\mathcal{C}$
    dans le sens inverse (de $B$ à $A$)
    va nécessairement donner
    $\mathcal{L} = \mathcal{L'}$
    donc ce dernier
    est lui aussi
    extrêmal.

    \subsection{Lois de \propername{Snell-Descartes}}

    \subsubsection{Préambule}

	On considère
    deux points
    $A$ et $B$.
    La distance géométrique
    $AB = \va{AB}\dotproduct\va{u}$
    peut se différencier
    pour donner~:
    \begin{align*}
        \dd AB
        &= \dd\va{AB}\dotproduct\va{u} + \va{AB}\dotproduct\dd\va{u}
        \\
        &= (\va{\dd B} - \va{\dd A})\dotproduct\va{u} + \va{0}
    \end{align*}

    On considère maintenant
    deux milieux $1$ et $2$
    d'indices $n_1$ et $n_2$
    séparés par un dioptre $\mathcal{D}$.
    Dans $1$ se trouve le point $A$
    et dans $2$ se trouve le point $B$.
    On cherche le point $I$
    par lequelle passe la lumière
    qui va de $A$ à $B$.

    \begin{figure}[H]
        \centering
        \begin{tikzpicture}
            \draw (-2, 1) -- (4, 1);
            \node at (-1, 1.5) {$n_1$};
            \node at (-1, 0.5) {$n_2$};
            \node[anchor=east] at (-2, 1) {$\mathcal{D}$};

            \coordinate (A) at (0, 2);
            \coordinate (I) at (1.3, 1);
            \coordinate (I') at (0.6, 1);
            \coordinate (B) at (2, 0);

            \draw (A) node[anchor=south east]{$A$};
            \draw (I) node[anchor=south]{$I$};
            \draw (I') node[anchor=north]{$I'$};
            \draw (B) node[anchor=north west]{$B$};

            \draw (A) -- (I) -- (B);
            \draw[dashed] (A) -- (I') -- (B);
        \end{tikzpicture}
    \end{figure}

    Le chemin optique
    entre $A$ et $B$
    et sa différentielle
    s'écrivent~:
    \begin{gather*}
        \mathcal{L}
        = n_1 AI + n_2 IB
        \\
        \dd\mathcal{L}
        = n_1 (\va{\dd I} - \va{\dd A}) \dotproduct \va{u_1}
            + n_2 (\va{\dd B} - \va{\dd I}) \dotproduct \va{u_2}
        = \va{\dd I} (n_1 \va{u_1} - n_2 \va{u_2})
    \end{gather*}
    la dernière égalité
    découlant du fait
    que l'on s'intéresse
    à des points $A$ et $B$ fixés.
    Ainsi,
    le chemin effectivement suivi
    étant celui qui extrêmalise $\mathcal{L}$,
    on écrit~:
    \begin{gather*}
        \dd\mathcal{L}
        = \va{\dd I} (n_1 \va{u_1} - n_2 \va{u_2})
        = 0
        \implies
        (n_1 \va{u_1} - n_2 \va{u_2})
        \perp
        \va{\dd I}
        \in
        \mathcal{D}
    \end{gather*}
    on trouve que
    la quantité vectorielle
    $n_1 \va{u_1} - n_2 \va{u_2}$
    doit être perpendiculaire au dioptre.
    On notera
    $(n_1 \va{u_1} - n_2 \va{u_2}) \parallel \va{N}$
    la normale au dioptre.

    \subsubsection{Réflexion}

    Dans le cas de la réflextion,
    $n_2 = n_1 = n$
    alors~:
    \begin{alignat*}{2}
        n \va{u_1} - n \va{u_2}
        = a \va{N}
        &\implies&
        \va{u_2}
        &= - \frac{a}{n} \va{N} + \va{u_1}
        \\
        &\implies&
        \va{N} \crossproduct \va{u_2}
        &= \va{N} \crossproduct(- \frac{a}{n} \va{N} + \va{u_1})
        \\
        &\implies&
        \sin(\va{N}, \va{u_2})
        &= \sin(\va{N}, \va{u_1})
        \\
        &\implies&
        i_2
        &= -i_1
    \end{alignat*}

    \subsubsection{Réfraction}

    \begin{draft}
        Même principe.
    \end{draft}

\section{Conséquences et applications~: retour au coucher de Soleil
    \cite{femtophysique}}

    \subsection{L'équation du rayon lumineux}

    Dans un milieu d'indice variable
    (suivant l'altitude $y$),
    le chemin optique s'écrit~:
    \begin{equation*}
        \mathcal{L}
        = \int_\mathcal{C} n(y) \dd s
    \end{equation*}
    avec~:
    \begin{equation*}
        \dd s
        = \sqrt{{\dd x}^2 + {\dd y}^2}
        = \dd x \sqrt{1 + \qty(\dv{y}{x})^2}
        = \dd x \sqrt{1 + y'^2}
    \end{equation*}
    d'où~:
    \begin{equation*}
        \mathcal{L}
        = \int_\mathcal{C} n(y) \sqrt{1 + y'^2} \dd x
        = \int_\mathcal{C} f(x, y(x), y'(x)) \dd x
    \end{equation*}

    Pour résoudre le problème
    d'extrémalisation,
    on peut utiliser
    l'équation d'\propername{Euler-Lagrange}
    qui y est équivalente.

    \begin{draft}
        On considère
        un chemin $\mathcal{C} = y(x)$
        et une variation infinitésimale $\mathcal{C'} = y(x) + \epsilon g(x)$
        qu'on associe
        aux chemis optiques $\mathcal{L}$
        et $\mathcal{L'}[y] = \mathcal{L}[y + \epsilon g]$
        de sorte que~:
        \begin{align*}
            \mathcal{L}[n+\epsilon]
            &= \int_A^B f[y + \epsilon g, y' + \epsilon g'] \dd x
            \\
            &= \int_A^B \qty(f[y, y']
                + \pdv{f}{y} \epsilon g(x)
                + \pdv{f}{y'} \epsilon g'(x)) \dd x
            \\
            &= f[y, y']
                + \epsilon \int_A^B \pdv{f}{y} g(x) \dd x
                + \epsilon \int_A^B \pdv{f}{y'} g'(x) \dd x
        \end{align*}
        À l'aide de l'intégration par partie~:
        \begin{equation*}
            \int_A^B \pdv{f}{y'} g'(x) \dd x
            = \qty[\pdv{f}{y'} g(x)]_A^B
                - \int_A^B \dv{t}(\pdv{f{y'}}) g(x) \dd x
        \end{equation*}
        où le premier terme est nul
        car $g(x_A) = 0 = g(x_B)$,
        on obtient~:
        \begin{equation*}
            \mathcal{L}[n+\epsilon]
            = f[y, y']
                + \epsilon \int_A^B \pdv{f}{y} g(x) \dd x
                - \int_A^B \dv{t}(\pdv{f{y'}}) g(x) \dd x
        \end{equation*}
        donc~:
        \begin{equation*}
            \dd \mathcal{L}
            = \epsilon\int_A^B \qty(\pdv{f}{y} - \dv{x}(\pdv{f}{y'})) g(x)\dd x
        \end{equation*}
        Pour les solutions
        $f[y]$ extrêmales,
        $\dd \mathcal{L} = 0$
        pour $g(x)$ arbitraire
        donc~:
        \begin{equation*}
            \pdv{f}{y} - \dv{x}(\pdv{f}{y'}) = 0
        \end{equation*}
        c'est l'équation
        d'\propername{Euler-Lagrange}.
    \end{draft}

    Adapté au problème,
    elle donne~:
    \begin{align*}
        \pdv{f}{y}
        &= \dv{x}(\pdv{f}{y'})
        \\
        \implies
        \pdv{n}{y} \sqrt{1 + y'^2}
        &= \dv{x}(\frac{ny'}{\sqrt{1 + y'^2}})
        \\
        \implies
        \pdv{n}{y} \sqrt{1 + y'^2}
        &= \dv{s}(ny')
        \\
        \implies
        \pdv{n}{y} \sqrt{1 + y'^2}
        &= \dv{s}(n\dv{y}{x})
        \\
        \implies
        \pdv{n}{y}
        &= \dv{s}(n\dv{y}{s})
    \end{align*}
    et de la même manière
    (en reprenant le calcul avec $x(y)$)~:
    \begin{align*}
        \pdv{n}{x}
        = \dv{s}(n\dv{x}{s})
    \end{align*}
    finalement (à 3D)~:
    \begin{equation*}
        \grad(n)
        = \dv{s}(n \qty[\dv{x}{s}\va{u_x}+\dv{y}{s}\va{u_y}+\dv{z}{s}\va{u_z}])
        = \dv{s}(n \va{u})
    \end{equation*}

    \subsection{Rayon de courbure du rayon lumieux}

    On retrouve de suite
    que dans un milieu homogène
    les rayons se propagent en ligne droite~:
    $n = \cst \implies \grad(n) = 0 \implies n\va{u} = \vcst$.

    Pour un milieu non homogène,
    on peut développer~:
    \begin{equation*}
        \grad(n)
        = \dv{s}(n \va{u})
        = n \dv{\va{u}}{s} + \va{u} \dv{n}{s}
    \end{equation*}
    Le second terme
    qui est colinéaire à $\va{u}$
    correspond à la propagation
    dans le sens du rayon,
    alors que
    le premier terme
    colinéaire à $\dd\va{u}$
    correspond à la déviation
    dans la direction perpendiculaire.
    On définit le rayon de courbure
    comme~:
    \begin{equation*}
        \frac{1}{R}
        \defeq \dv{\va{u}}{s} \dotproduct \va{u_\perp}
    \end{equation*}
    donc en projetant~:
    \begin{equation*}
        \grad(n) \dotproduct \va{u_\perp}
        = \frac{n}{R}
        \implies
        \frac{1}{R}
        = \frac{\grad(n) \dotproduct \va{u_\perp}}{n}
    \end{equation*}
    On trouve que
    le rayon
    est toujours courbé
    vers les régions de fort indice,
    donc dans le sens du gradient~;
    il est d'autant plus courbé
    que le gradient d'indice est grand.

    \paragraph{Remarque sur les fibres optiques}
    Dans les fibres optiques
    le cœur à toujours un indice plus grand.
    Pour les fibres à saut d'incide
    le gradient est infini (discontinuité)
    d'où le «~rebond~» (rayon de courbure nul).

    \subsection{Indice optique dans l'atmosphère}

    \begin{todo}
        Trouver une description
        pour l'atmosphère~:
        on trouvera que
        le gradient d'incide est dirigé vers le sol
        pour expliquer le phénomène
        au coucher de Soleil.

        Le calcul explique égallement
        la courbure des rayons dans la manip.
        Ainsi que
        les mirages sur la route en été
        où cette fois
        le gradient d'indice
        est dirigé vers le ciel.
    \end{todo}
