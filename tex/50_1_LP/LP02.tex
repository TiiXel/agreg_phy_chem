\chapter{Gravitation}

    \propername{Copernic} et les anciens
    étudiaient le mouvements des astres dans le ciel,
    en essayant de déterminer leurs positions au fil des nuits.
    L'hypothèse d'un univers géocentrique de \propername{Ptolémée}
    sera contre-dite dans une publication de \propername{Copernic}
    qui en 1543,
    après 36 ans d'observations,
    avance un modèle héliocentrique
    plus simple
    du système solaire.
    L'Église ne s'opposera à ces écrits qu'en 1616
    lorsque \propername{Galilée} les confirmes par l'expérience.
    Il se base
    sur les observations de \propername[Tycho]{Brahé}
    (lui même adepte du modèle géocentrique)
    qui le premier a l'idée de
    sceller les débat
    par une rigoureuse prise de mesures.

  \section{État des lieux en 1642 \cite{feynmanm1} \cite{perezmecanique}}

    \begin{todo}
        Préparer un document à projeter pour aller vite.
    \end{todo}

    \subsection{Lois de \propername{Képler}}

    \propername{Képler},
    cherchant lui aussi à comprendre le système solaire,
    s'appuie sur les idées de \propername{Copernic}
    et les mesures de \propername{Brahé}.
    Il ne cherche pas à vérifier
    que les trajectoires correspondent à des cercles,
    mais cherche à déterminer directement
    la forme de leur orbite.

    Il en conclut que
    chaque planète se déplace autour du Soleil
    selon une trajectoire élliptique.

    Il observe aussi
    que les planètes ne tournent pas autour du Soleil à une vitesse constante,
    mais se déplacent plus rapidement
    lorsqu'elles sont plus proches de l'étoile,
    de sorte que le rayon vecteur
    liant le Soleil à la planète
    balaye une surface égale pendant des durées égales.
    On dira qu'elles ont
    une \emph{vitesse aréolaire}\label{lp2-vitesseareolaire} constante~:
    la surface balayée par unité de temps est constante.

    Enfin, il remarque
    que le temps nécessaire à une planète
    pour faire le tour de sa trajectoire
    est proportionnel
    à la puissance $\frac{3}{2}$
    du demi-grand axe de cette dernière.

    \subsection{Dynamique de \propername{Galilée} \cite{feynmanm1}}

    À la même période, \propername{Galilée}
    qui étudie le mouvement
    énoncait le principe d'inertie
    selon lequel
    un corps préserve
    son état de repos ou de mouvement
    tant qu'il n'est pas contraint,
    par quelque chose qui le touche ou le perturbe autrement,
    à changer cet état.

  \section{Contribution de \propername{Newton} (1687)}

    \subsection{Dynamique de \propername{Newton} \cite{feynmanm1}}

    \propername{Newton}
    formalise les idées de \propername{Galilée},
    il définit
    les notions de froce
    et d'accélération
    puis exprime mathématiquement
    les trois lois qui portent son nom.

    \subsection{La loi de gravitation universelle \cite{feynmanm1}}

    Il peut conclure
    que la force exercée par le Soleil
    sur une planète doit nécessairement
    être orientée de cette dernière vers ce premier.

    Le principe d'action-réaction
    combiné au principe d'inertie
    permet d'avancer
    que la masse du Soleil et la masse de la planète planète
    doivent jouer un rôle symétrique
    dans l'expression de la force d'attraction.
    Par ailleurs,
    les calculs de la vitesse de chute de la Lune vers la Terre,
    puis de celle de la Terre et de Jupiter vers le Soleil,
    permettent d'établir
    une relation de la force à la distance
    en $\frac{1}{r^2}$.
    L'idée est répetée sur les satellites connus de Jupiter,
    affirmant un début de caractère universel à la loi ainsi énoncée.

  \section{Dynamique des astres}

    On cherche dans cette partie
    à décrire plus complètement
    les mouvements d'un système à deux corps
    liés par la gravitation.
    On suppose
    la loi de gravitation de \propername{Newton}
    et on va montrer
    qu'elle est mathématiquement conforme
    aux lois de \propername{Képler}.

    Considérons un objet $P$
    de masse $m$
    et vitesse $\vb{v}$
    parcourant une trajectoire autour d'un astre attracteur
    de centre $O$
    et de masse $m_\odot$,
    les deux étants reliés par un vecteur $\vb{r}$
    orienté vers l'objet.

    \begin{equation*}
        \vb{F} = - G \frac{m m_\odot}{r^2} \vu{r}
    \end{equation*}

    \subsection{Loi des aires \cite{perezmecanique}}

    D'une part,
    $\vb{F}$ est une force centrale,
    le calcul de son moment en $O$
    donne le vecteur nul,
    ce qui entraine
    $\frac{\dd{\vb{L}_O}}{\dd t} = \vb{0}$.
    Le moment cinétique $P$
    calculé en $O$ est donc constant et vaut~:
    \begin{equation*}
        \vb{L}_O
        = \vb{r} \cross m \vb{v}
        = \vcst
    \end{equation*}
    Vu cette expression,
    $\vb{r}$
    et $\vb{v}$
    doivent être contenus
    dans un plan orthogonal à $\vb{L}_O$,
    la trajectoire de $P$
    est donc contenue
    dans le plan perpendiculaire à $\vb{L}_O$
    passant par $O$.
    On choisit un repère cylindrique $O(r, \theta, z)$
    tel que $Oz$ soit parallèle à $\vb{L}_O$,
    ce qui permet d'exprimer le vecteur moment cinétique
    comme $\vb{L}_O = L_z \vu{z}$
    avec $L_z = \cst$.
    Calculons~:
    \begin{equation*}
        \vb{L}_O
        = L_z \vu{z}
        = r \vu{r} \cross m \dt{\vb{r}}
        = r \vu{r} \cross m \qty(\dt{r} \vu{r} + r \dt{\theta} \vu{\theta})
        = m r^2 \dt{\theta} \vu{z}
    \end{equation*}
    D'où $L_z = m r^2 \dt{\theta}$.

    D'autre part,
    la vitesse aréolaire $\mathcal{V}_a$
    définie \cref{lp2-vitesseareolaire}
    s'exprime~:
    \begin{equation*}
        \mathcal{V}_a
        = \frac{\dd{\mathcal{A}}}{\dd t}
        = \frac{1}{\dd t} \qty(\frac{r \times r \dd{\theta}}{2})
        = \frac{L_z}{2m}
        = \frac{C}{2}
    \end{equation*}
    D'où finalement,
    $\mathcal{V}_a = \cst$.

    \subsection{Trajectoire et période de révolution \cite{bfr1}}

    Nous avons déjà exprimé les vecteurs $\vb{r}$ et $\vb{v}$
    dans le repère cylindrique.
    Le vecteur accélération $\vb{a}$ peut s'exprimer
    soit par la dérivée temporelle de $\vb{v}$,
    soit par le principe fondamental de la dynamique~:
    \begin{equation*}
        \vb{a}
        = - G \frac{m_\odot}{r^2} \vu{r}
        = (\dtt{r}-r\dt{\theta}^2)\vu{r}
            + \cancel{(2\dt{r}\dt{\theta}+r\dtt{\theta})\vu{\theta}}
    \end{equation*}
    Par ailleurs nous avions
    $\dt{\theta} = \flatfrac{C}{r^2}$~;
    et puisque~:
    \begin{equation*}
        \dt{r}
        = \dv{r}{t}
        = \dv{r}{\theta} \dv{\theta}{t}
        = \dv{r}{\theta} \frac{C}{r^2}
    \end{equation*}
    alors ces égalités nous permettent de réexprimer
    $\vb{a}(r, \dtt{r}, \dt{\theta})$
    comme~:
    \begin{align*}
        \vb{a}
        = \qty(\dv{t} \dv{r}{t} - r \qty(\dv{\theta}{t})^2) \vu{r}
        = \qty(\dv{t} \qty(\frac{C}{r^2} \dv{r}{\theta})
            - r \frac{C^2}{r^4}) \vu{r}
        = \qty(\dv{\theta} \qty(\frac{C}{r^2} \dv{r}{\theta}) \dv{\theta}{t}
            - \frac{C^2}{r^3}) \vu{r}\\
        = \frac{C^2}{r^2} \qty(-\dv[2]{\theta}(\frac{1}{r}) - \frac{1}{r}) \vu{r}
        = \vb{a}(\flatfrac{1}{r})
    \end{align*}
    Nous avons maintenant deux expressions pour $\vb{a}$,
    toutes deux exprimées comme fonctions de
    $\flatfrac{1}{r}$.
    C'est l'intéret de ce calcul
    qui nous permet d'égaliser $\norm{\vb{a}}$
    pour écrire une équation différentielle
    de $\frac{1}{r}$ en $\theta$~:
    \begin{equation*}
        \frac{C^2}{r^2} \qty(\dv[2]{\theta}(\frac{1}{r}) + \frac{1}{r})
        = G \frac{m_\odot}{r^2}
        \implies
        \dv[2]{\theta}(\frac{1}{r}) + \frac{1}{r} - G \frac{m_\odot}{C^2}
        = 0
        \end{equation*}
    La résolution de cette équation différentielle donne~:
    \begin{equation*}
        \frac{1}{r} = G \frac{m_\odot}{C^2} + A \cos(\theta - \theta_0)
    \end{equation*}
    égalité que l'on peut écrire,
    en choisisant la constante d'intégration $\theta_0$ nulle~:
    \begin{equation*}
        r
        = \frac{1}{G \flatfrac{m_\odot}{C^2} + A \cos\theta}
        = \frac
            {\flatfrac{C^2}{G m_\odot}}
            {1 + \flatfrac{A C^2}{G m_\odot} \cos\theta}
        = \frac{p}{1 + e \cos\theta}
    \end{equation*}
    que l'on reconnait comme
    équation d'une cônique d'exentricité $e$ et de paramètre $p$.

    \begin{python}
        \software{python/coniquesSystemeSolaire.py}
    \end{python}

    Notre objet $P$ suit donc autour du Soleil une trajectoire
    qui correspond à une cônique,
    comme l'annonçait la troisième loi (empirique) de \propername{Képler}
    pour le cas particulier des planètes.

    \begin{table}[H]
        \begin{tabular}{rSSSS}
            \toprule
            Astre
                & {Aphélie (\si{\astronomicalunit})}
                    & {Périhélie (\si{\astronomicalunit})}
                        & {Exentricité}
                            & {Période orbitale (\si{\year})}\\
            \midrule
            Terre
                & 1.02
                    & 0.98
                        & 0.0137
                            & 1,00\\
            Mars
                & 1.66
                    & 1.38
                        & 0.0934
                            & 1.88\\
            Neptune
                & 30.44
                    & 29.77
                        & 0.00858
                            & 164.80\\
            Swift-Tuttle
                & 51.23
                    & 0.96
                        & 0.9362
                            & 133.28\\
            Hale-Bopp
                & 370.8
                    & 0.91
                        & 0.995
                            & 4000\\
            Oumuamua
                &
                    & 0.25
                        & 1.197
                            &\\
            \bottomrule
        \end{tabular}
    \end{table}

    Le cas des orbites fermées permet de définir une période de révolution $T$,
    que l'on peut exprimer simplement
    à partir de l'aire de la surface délimitée par la trajectoire
    et la vitesse aréolaire~:
    \begin{equation*}
        T^2
        = \qty(\frac{S}{\mathcal{V}_a})^2
        = \qty(\frac{\pi a b}{\flatfrac{C}{2}})^2
        = \frac{\pi^2 a^3 p}{\qty(\flatfrac{C}{2})^2}
        = \frac{4 \pi^2 a^3}{G m_\odot}
    \end{equation*}
    d'où~:
    \begin{equation*}
        \frac{T^2}{a^3}
        = \frac{4 \pi^2}{G m_\odot}
        = \cst
    \end{equation*}
    l'on retrouve la troisième loi de \propername{Képler}
    puisque la constante ne dépend que de la masse de l'attracteur.

    \subsection{Énergie mécanique \cite{perezmecanique}}

    L'énergie mécanique de $P$ s'exprime~:
    \begin{equation*}
        \mathcal{E}_m
        = \mathcal{E}_c(\dt{r}) + \mathcal{E}_p(r)
        = \frac{1}{2} m \qty(\dt{r}^2 + r^2 \dt{\theta}^2) + \mathcal{E}_p(r)
        = \frac{1}{2} m \dt{r}^2 + \frac{L_z^2}{2mr^2} + \mathcal{E}_p(r)
    \end{equation*}
    On constate que deux des termes ne dépendent que $r$.
    Nous les regroupons en un terme d'énergie potentielle effective~:
    \begin{equation*}
        \mathcal{E}_m
        = \frac{1}{2} m \dt{r}^2 + \mathcal{E}_{p,e}(r)
    \end{equation*}
    dont, nous allons le voir, la valeur,
    comparée à celle de l'énergie mécanique,
    renseigne elle aussi sur la trajectoire de $P$.

    \begin{pydo}
        Tracer la courbe
        et y associer les côniques.
    \end{pydo}

\plusloin{
    La gravitation de \propername{Newton}
    constitue un modèle satisfaisant
    d'observations astronomiques et quotidiennes,
    mais certains faits expérimentaux y échappent~:
    \begin{itemize}
        \item D'une part elle a entrainé les astronomes,
            qui observaient des déviations dans l'orbite d'Uranus,
            à émettre l'hypothèse qu'une huitième planète
            (aujourd'hui Neptune)
            pourrait faire partie du système solaire,
            prédire sa position,
            et l'observer (1864).
        \item D'autre part
            elle n'a jamais su expliquer
            la précession de l'orbite de Mercure,
            mesurée dès 1859.
    \end{itemize}
    C'est \propername{Einstein}
    qui en 1915 propose une nouvelle théorie de la gravitation,
    résolvant le problème de l'orbite de Mercure.
    Sa théorie de la relativité générale
    englobe bien la théorie de \propername{Newton},
    et propose l'existence de phénomènes nouveaux
    qui sont aujourd'hui mesurés.
    Mais ce modèle
    pose quant à lui des problèmes plus fondamentaux\dots
}
