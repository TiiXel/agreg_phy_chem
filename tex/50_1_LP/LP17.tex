\chapter{Rayonement d'équilibre thermique, corps noir}

  \section{Rayonement et bilan radiatif \cite{planetteterre}}

    \subsection{Définition des flux}

    \subsubsection{Émission}

    \begin{manip}
        Fil chauffé au rouge avec un courant électrique.
    \end{manip}

    Tout corps convertit
    une partie de son énergie interne
    en rayonnement thermique.
    Il emmet par sa surface
    par unité de temps
    une certaine quantité d'énergie
    que l'on décrit mathématiquement comme
    un flux
    noté $\phi_e$
    homogène à une puissance.

    \subsubsection{Absorbtion}

    Lorsqu'un corps
    reçoit de l'énergie
    via un flux incident sur sa surface,
    noté $\phi_i$,
    une partie peut être transformée
    en énergie interne.
    On définit un flux absorbé
    $\phi_a$.
    La fraction du flux incident
    qui va être absorbée
    dépend de manière générale
    de la longueur d'onde.

    \subsubsection{Réflexion, diffusion}

    La partie du flux incident
    qui n'est pas absorbée
    peut être
    réfléchie (renvoyée dans une direction particulière),
    ou diffusée (renvoyée dans toutes les directions).
    On note la somme de ces deux termes
    $\phi_r$.
    Le rayonnement réfléchit ou diffusé
    l'est à la même longueur d'onde
    que le rayonnement incident,
    mais là encore
    la fraction de flux qui est réfléchie ou diffusée
    est fonction de la longueur d'onde.

    \subsubsection{Transparence, opacité, exemples}

    Enfin,
    la partie du flux incident
    qui n'est ni absorbée ni réfléchie
    se comporte de manière indiférente
    à la présence du corps~:
    elle le traverse.
    De manière catégorique
    on caractérisera ce type de milieux
    de transparents
    et les autres
    d'opaques,
    pour différents domaines de longueurs d'onde.

    Le verre, par exemple,
    est un milieu transparent
    pour les longueurs d'ondes du visible
    mais il ne l'est pas
    pour les longueurs d'ondes de l'infrarouge.

    Une salade nous apparait verte
    car elle ne diffuse que les longueurs d'ondes du vert
    et absorbe les autres longueurs d'ondes visibles.

    \begin{todo}
        Faire un schéma avec des grosses flèches,
        pour la salade et le verre.
    \end{todo}

    Dans la suite,
    nous allons nous restreindre
    aux corps opaques
    sur tout le spectre.

    \subsection{Relations entre les flux}

    \subsubsection{Flux incident}

    Le flux incident est
    soit réfléchit,
    soit diffusé,
    soit absorbé~:
    \begin{equation*}
        \phi_i
        = \phi_r + \phi_a
    \end{equation*}

    \subsubsection{Flux partant}

    Le flux partant correspond
    à la somme du flux émis et du flux réfléchit~:
    \begin{equation*}
        \phi_p
        = \phi_e + \phi_r
    \end{equation*}

    \subsection{Équilibre radiatif}

    On dit qu'un corps opaque est
    en équilibre radiatif avec le rayonnement
    s'il n'emmaganisne pas et ne perds pas
    d'énergie via le rayonnement,
    ce qui revient à~:
    \begin{equation*}
        \phi_p
        = \phi_i
        \implies
        \phi_e
        = \phi_a
    \end{equation*}

    Notons que
    un corps en équilibre radiatif
    peut recevoir de l'énergie
    via un autre processus~:
    l'équilibre radiatif
    n'implique pas l'équilibre thermodynamique.

\section{Le problème du corps noir}

    \subsection{Problématique}

    \begin{manip}
        Avec un spectro,
        montrer le spectre du fil chauffé,
        puis du Soleil,
        puis d'une lampe à filament.
    \end{manip}

    Le spectrophotomètre
    permet de mesurer
    une fraction du flux émis par le corps chauffé.
    L'appareil mesure
    une énergie reçue (sur le capteur)
    pendant une durée d'intégration $\delta t$
    pour chaque domaine de longueur d'onde de largeur $\delta \lambda$.
    Sur le spectre on lit
    en ordonnée
    cette énergie
    par unité de surface
    par unité de temps
    par domaine de longueur d'onde.
    On peut définir plusieurs grandeurs
    toutes liées par des facteurs
    géométrique
    de surfaces
    ou d'angles
    (luminance, exitance, densité d'énergie),
    mais ce qui nous intéresse
    c'est la dépendance de ces grandeurs
    à la longueur d'onde.

    Le spectre d'émission
    des corps chauds
    semble être approximable par
    une loi universelle fonction de la température,
    puisque la forme de la courbe
    est identique pour deux objets différents
    mais à la même température.

    Vers la fin du \century{19}
    les physiciens cherchèrent une modélisation qui permettrait
    de décrire ce phénomène
    et de fournir une expression mathématique du spectre.
    Pour s'affranchir de complications,
    il semble judicieux de considérer en premier lieux
    des objets dont le rayonnement partant
    est indépendant de leur environnement,
    autrement dit des objets tels que
    $\phi_p = \phi_e$
    soit
    $\phi_r = 0$
    et donc $\phi_i = \phi_a$.
    Ces objets absorbent
    tout le rayonnement incident,
    on les appelle \emph{corps noirs}
    mais attention~:
    il n'apparaissent pas noirs,
    puisqu'ils émettent un rayonnement dans le visible~;
    leur appelation est due
    à leur spectre d'absorbtion.

    Nous décrirons le spectre
    en terme d'énergie rayonnée
    par unité de temps
    par unité de surface
    par domaine de longueur d'onde,
    grandeur notée~:
    $F_e(\lambda, T)$
    exprimée en
    $\si{\watt\per\meter\squared\per\meter}$
    telle que~:
    $\int_0^\infty F_e(\lambda, T) \dd\lambda = \flatfrac{\phi_e(T)}{S}$.

    \subsection{La loi de \propername{Wien}}

    Dès 1869,
    \propername{Wien} propose une relation empirique
    résultat d'un ajustement de la courbe mesurée~:
    \begin{equation*}
        F_e(\lambda, T)
        = \frac{\SI{3.7e-16}{\watt\metre\squared}}{\lambda^5}
            \frac{1}{e^{\frac{\SI{0.014}{\meter\kelvin}}{\lambda T}}}
    \end{equation*}
    mais le graph de cette fonction
    présente un petit écart à l'expérience
    pour les grandes longueurs d'ondes.
    Surtout,
    il s'agit d'une relation empirique
    à laquelle les physiciens voulaient apporter
    une explication physique.

    \subsection{Le modèle de \propername{Rayleigh} \cite{dufour}}

    En 1900,
    \propername{Rayleigh} propose un modèle
    issu de la physique statistique
    et qui semble correct à l'ensemble des physiciens.
    Il annonce~:
    \begin{equation*}
        F_e(\lambda, T)
        \propto \frac{c}{\lambda^4}
            \frac{\int_0^\infty E e^{\flatfrac{-E}{k_B T}} \dd E}
            {\int_0^\infty e^{\flatfrac{-E}{k_B T}} \dd E}
        = \frac{c}{\lambda^4} k_B T
    \end{equation*}
    Mais le spectre d'émission donné par ce modèle
    est très différent du spectre expérimental
    en particulier,
    il est monotone
    et présente une divergence aux courtes longueurs d'ondes.

    \begin{draft}
    \subsubsection{Explication}

    Considérons une cavité
    cubique d'arrête $a$,
    dans laquelle existe un champ électromagnétique
    que l'on peut décomposer comme somme d'ondes stationnaires.
    Ces ondes existent à condition que
    la longueur $a$ soit
    un multiple entier de demi longueur d'onde,
    de sorte que~:
    \begin{equation*}
        \sin(\frac{a}{\lambda})
        = 0
        \implies
        \frac{a}{\lambda}
        = n \pi, n \in \mathbb{Z}
        \implies
        k
        \defeq \frac{2\pi}{\lambda}
        = \frac{n \pi}{a}
    \end{equation*}
    Si l'on se ramène aux trois dimensions,
    on pourra écrire~:
    \begin{align*}
        k_x &= \frac{n_x \pi}{a} \\
        k_y &= \frac{n_y \pi}{a} \\
        k_z &= \frac{n_z \pi}{a}
    \end{align*}
    d'où le vecteur d'onde~:
    \begin{equation*}
        \vb{k}^2
        = k_x^2 + k_y^2 + k_z^2
        = \qty(\frac{\pi}{a})^2 (n_x^2 + n_y^2 + n_z^2)
        \defeq \qty(\frac{2 \pi}{\lambda})^2
    \end{equation*}

    Les vecteurs d'ondes
    décrits par les nombres
    $\{n_x, n_y, n_z\}$
    tels que la somme de leurs carrés
    prend la même valeur
    (par exemple~:
    $\{1, 1, 4\}$
    et
    $\{3, 3, 0\}$)
    correspondent à des ondes
    de même longueurs d'ondes,
    mais de dirrection différente.
    Cependant,
    les vecteurs d'ondes
    décrits par les nombres
    $\{n_x, n_y, n_z\}$
    et les nombres
    $\{-n_x, n_y, n_z\}$
    décrivent les mêmes ondes stationnaires
    (il en va de même pour les autre composantes)~:
    il faut donc restreindre
    les valeurs prises par les $n_i$
    à l'ensemble $\mathbb{N}$.

    Dans l'espace des états d'ondes stationnaires,
    divisé en volumes $\qty(\frac{\pi}{a})^3$,
    le huitième positif du cadrant
    de la couche sphérique
    délimitée par les boules de rayons
    $k$ et $k + \dd k$
    a un volume~:
    \begin{equation*}
        \frac{1}{8} 4 \pi k^2 \dd k
        = \frac{1}{2} \pi k^2 \dd k
    \end{equation*}
    en tenant compte
    des deux polarisaitons possibles
    pour chaque vecteur d'onde
    elle contient $\dd N_k$ états d'ondes stationnaires~:
    \begin{equation*}
        \dd N_k
        = 2 \frac{\flatfrac{\pi k^2 \dd k}{2}}{\qty(\flatfrac{\pi}{a})^3}
        = \frac{a^3 k^2 \dd k}{\pi^2}
    \end{equation*}
    nombre que l'on peut exprimer
    en fonction de la longueur d'onde
    associée à $k$~:
    ($k = \frac{2\pi}{\lambda}$
    $\implies \dd k = \frac{2\pi}{\lambda^2}\dd\lambda$)
    \begin{equation*}
        \dd N_\lambda
        = \frac{8 a^3 \pi \dd \lambda}{\lambda^4}
    \end{equation*}
    On définit la densité d'états
    par unité de volume,
    $\rho(\lambda)$,
    comme~:
    \begin{equation*}
        \rho(\lambda) \dd \lambda
        \defeq \frac{\dd N_\lambda}{a^3}
        = \frac{8 \pi}{\lambda^4} \dd \lambda
    \end{equation*}

    On s'intéresse maintenant à
    la densité d'énergie
    associée aux ondes stationnaires
    de longueurs d'ondes comprises entre
    $\lambda$ et $\lambda + \dd \lambda$~:
    elle s'écrit comme
    $\rho(\lambda)$
    multiplié par l'énergie moyenne des états
    $\avg{\epsilon(\lambda, T)}$~:
    \begin{equation*}
        u(\lambda, T)
        = \rho(\lambda) \avg{\epsilon(\lambda, T)}
    \end{equation*}
    avec~:
    \begin{equation*}
        \avg{\epsilon(\lambda, T)}
        = \int_0^\infty\epsilon p(\epsilon) \dd \epsilon
    \end{equation*}
    où $p(\epsilon)\dd\epsilon$
    est la probabilité pour que
    l'énergie d'un état
    se situe entre
    $\epsilon$ et $\epsilon + \dd \epsilon$.

    En faisant l'hypothèse
    que les ondes stationnaires sont répartites en énergie
    selon une statistique
    de \propername{Maxwell-Boltzmann},
    on aura~:
    \begin{equation*}
        p(\epsilon)
        = \frac{e^{\frac{-\epsilon}{k_B T}}}
            {\int_0^\infty e^{\frac{-\epsilon}{k_B T}} \dd \epsilon}
    \end{equation*}
    d'où, après calcul~:
    \begin{equation*}
        \avg{\epsilon(\lambda, T)}
        = k_B T
    \end{equation*}
    finalement~:
    \begin{equation*}
        u(\lambda, T)
        = \frac{8 \pi}{\lambda^4} k_B T
    \end{equation*}

    Alors,
    l'énergie rayonnée par les états
    de longueurs d'ondes comprises entre
    $\lambda$ et $\lambda + \dd \lambda$
    est~:
    \begin{equation*}
        u(\lambda, T) \dd \lambda
        = \frac{8 \pi}{\lambda^4} k_B T \dd \lambda
    \end{equation*}
    elle est émise
    à la vitesse $c$
    de sorte que le spectre associé
    soit~:
    \begin{equation*}
        F_e(\lambda, T)
        = c u(\lambda, T)
        = \frac{8 \pi c}{\lambda^4} k_B T
    \end{equation*}

    \end{draft}

    \subsection{La solution de \propername{Planck} \cite{dufour}}

    \propername{Planck},
    qui cherchait une solution au problème
    se rendit compte que s'il remplaçait
    les intégrales $\int_0^\infty \dd E$
    par des sommes discrètes $\sum_{n=0}^\infty n \epsilon$
    alors le spectre devenait~:
    \begin{equation*}
        F_e(\lambda, T)
        \propto\frac{hc^2}{\lambda^5}\frac{1}{e^{\frac{hc}{\lambda k_B T}} - 1}
    \end{equation*}
    à condition de poser
    $\epsilon = h \flatfrac{c}{\lambda}$.
    Cette substitution,
    qui constitue en fait
    une hypothèse de quantification du rayonnement,
    n'avait aucun fondement théoriques,
    \propername{Planck} lui même
    n'était alors pas convaincu par le modèle
    qui impliquait que les transferts d'énergie par rayonnement
    se faisaient de manière discrète
    comme par un échange de particules.
    Cette description donna naissance à
    la mécanique quantique
    et au photon~: objet d'une autre leçon.

    \subsubsection{La loi de \propername{Stefan-Boltzmann}}

    \subsubsection{La loi du déplacement de \propername{Wien}}

  \section{Applications}

    \subsection{Soleil - Terre}

    Température d'équilibre, effet de serre.
