\chapter{Rétroaction et oscillations}

\begin{manip}
    Préparer
    un ALI en montage
    amplificateur non inverseur
    avec
    $R_1 = R_2 = \SI{1}{\kilo\ohm}$.
\end{manip}

\section{Rétroaction}

    \subsection{Schémas blocs et définitions}

    \subsubsection{Schéma bloc}

    On modélise
    un morceau de système technique (électronique, mécanique)
    par un ensemble de blocs
    à une entrée et une sortie.
    \begin{figure}[H]
        \centering
        \begin{tikzpicture}
            \node (e) at (-2, 0) {};
            \node[draw=black] (T) at (0, 0) {$T$};
            \node (s) at (2, 0) {};

            \draw[->] (e) --node[anchor=south]{$e$} (T);
            \draw[->] (T) --node[anchor=south]{$s$} (s);
        \end{tikzpicture}
    \end{figure}
    Plutôt que d'exprimer ces grandeurs
    dans le domaine temporel (variable $t$)
    on les exprimes
    dans le domaine de \propername{Laplace} (variable $p$).
    La transformation associée
    est étudiée
    dans le cours de sciences de l'ingénieur.
    En électronique,
    l'entrée $e(p)$ et la sortie $s(p)$
    sont des tensions.

    \subsubsection{Fonction de transfert}

    On appelle
    \emph{fonction de transfert}
    le rapport~:
    \begin{equation*}
        T(p)
        \defeq \frac{s(p)}{e(p)}
    \end{equation*}

    \subsubsection{Système bouclé}

    On dit
    qu'un système est \emph{bouclé}
    lorsque le schéma bloc
    se présente sous cette forme~:
    \begin{figure}[H]
        \centering
        \begin{tikzpicture}
            \node (e) at (-2, 0) {};
            \node[draw=black, circle] (comp) at (0, 0) {$\;\;$};
            \node (comp+) at (-0.4, 0.2) {$^+$};
            \node (comp-) at (-0.2, -0.4) {$^-$};
            \node[draw=black] (A) at (1.5, 0) {$A$};
            \node[draw=black] (B) at (1.5, -1) {$B$};
            \node (s) at (5, 0) {};

            \draw[->] (e) --node[anchor=south]{$e$} (comp);
            \draw[->] (comp) --node[anchor=south]{$\epsilon$} (A);
            \draw[->] (A) -- (3, 0) -- (3, -1) -- (B);
            \draw[->] (B) -- (0, -1) --node[anchor=east]{$r$} (comp);
            \draw[->] (3, 0) --node[anchor=south]{$s$}(s);
        \end{tikzpicture}
    \end{figure}

    Le bloc $A$
    est appelé \emph{chaine directe},
    et le bloc $B$
    est appelé \emph{chaine de retour}.
    L'élément supplémentaire
    est un \emph{comparateur}~: $\epsilon = e - r$.
    On peut exprimer
    la fonction de transfert
    de l'ensemble~:
    \begin{equation*}
        s
        = A \epsilon
        \qq{et}
        \epsilon
        = e - B s
        \implies
        T
        = \frac{s}{e}
        = \frac{A}{1 + A B}
    \end{equation*}

    \subsection{Exemple avec l'ALI non inverseur}

    \subsubsection{Fonctions de transfert}

    L'ALI combine à la fois
    le comparateur
    et
    le bloc de la chaîne directe.
    On peut réaliser le montage non inverseur
    et dans ce cas,
    le pont diviseur de tension
    constitue la chaine de retour
    du système bouclé.

    \paragraph{La chaine directe}
    intégrée à l'ALI
    se comporte comme un filtre passe bas
    dont l'équation différentielle
    est~:
    \begin{equation*}
        \dv{s}{t} + \omega_0 s(t)
        = \mu_0 \omega_0 e(t)
    \end{equation*}
    La transformée de \propername{Laplace}
    pour les dérivées donne~:
    $y'(t) \rightarrow p y(p) - y'(0)$,
    alors
    l'équation différentielle devient~:
    \begin{equation*}
        p s(p) + \omega_0 s(p)
        = \mu_0 \omega_0 e(p)
        \implies
        T(p)
        = \frac{\mu_0}{1 + \flatfrac{p}{\omega_0}}
    \end{equation*}

    Pour les ALI ordinaires
    $\mu_0 = \num{e5}$
    et
    $\omega_0 = \SI{100}{\radian\per\second}$.

    \paragraph{La chaîne de retour}
    correspond à un amplificateur proportionnel
    dont la fonction de transfert
    va s'écrire~:
    \begin{equation*}
        B(p)
        = \frac{R_1}{R_1 + R_2}
        = B
    \end{equation*}

    \paragraph{La fonction de transfert de l'ensemble}
    va alors s'écrire~:
    \begin{equation*}
        T(p)
        = \frac{A(p)}{1 + A(p) B(p)}
        = \frac
            {\frac{\mu_0}{1 + \flatfrac{p}{\omega_0}}}
            {1 + \frac{\mu_0}{1 + \flatfrac{p}{\omega_0}} B}
        = \frac
            {\frac{\mu_0}{1 + \mu_0 B}}
            {1 + \flatfrac{p(1 + \mu_0 B)}{\omega_0}}
        = \frac{\mu}{1 + \flatfrac{p}{\omega_c}}
    \end{equation*}
    le calcul étant mené
    jusqu'à la forme dite
    \emph{canonique}.

    \subsubsection{Gain et bande passante}

    Le gain $\mu_0$
    et la pulsation de coupure $\omega_0$
    de l'ALI seul
    ne s'appliquent plus
    lorsqu'il est bouclé~:
    les quantités
    sont modifiées par
    $\mu = \flatfrac{\mu_0}{(1 + \mu_0 B)}$
    et $\omega_c = \omega_0 (1 + \mu_0 B)$.
    On peut cependant calculer~:
    \begin{equation*}
        \mu \omega_c
        = \mu_0 \omega_0
    \end{equation*}
    et constater
    que le produit
    est indépendant du bouclage $B$.
    En choisissant
    par exemple
    $R_1 = R_2 = \SI{1}{\kilo\ohm}$,
    on peut calculer
    $\mu = 2$
    et
    $\omega_c = \SI{5e6}{\radian\per\second}$.
    Le bouclage permet
    d'élargir la bande passante
    en diminuant le gain.

    \pyimgen{bouclage_ali}

\section{Stabilité des systèmes bouclés}

    On retourne
    vers l'équation différentielle
    du filtre passe bas~:
    \begin{align*}
        T(p)
        = \frac{s(p)}{e(p)}
        = \frac{\mu}{1 + \flatfrac{p}{\omega_c}}
        &\implies
        \dv{s}{t} + \omega_c s(t)
        = \mu \omega_c e(t)
        \\
        &\implies
        \dv{s}{t} + \omega_0 (1 + \mu_0 B) s(t)
        = \mu_0 \omega_0 e(t)
        \\
        &\implies
        s(t)
        = s_0 e^{-t \omega_0 (1 + \mu_0 B)} + \mu e(t)
    \end{align*}

    On dit que
    le système est stable
    si
    quand $e(t)$ tends vers une constante
    alors $s(t)$ tends aussi vers une constante.
    Il faut donc
    que le terme en exponentielle
    converge vers une valeur nulle,
    c'est-à-dire
    qu'il faut~:
    \begin{equation*}
        - \omega_0 (1 + \mu_0 B)
        < 0
    \end{equation*}
    Dans le cas
    de l'amplificateur non inverseur,
    comme
    le gain $\mu_0$ est fixé est positif
    et $B > 0$,
    alors nécessairement
    le système est stable.

    Si l'on avait branché
    la commande $e$
    sur l'entrée inverseuse
    et la rétroaction
    sur l'entrée non inverseuse,
    la fonction de transfert générale
    serait modifiée en~:
    \begin{equation*}
        T = \frac{-A}{1 - AB}
    \end{equation*}
    et donc,
    l'équation différentielle~:
    \begin{equation*}
        \dv{s}{t} + \omega_0 (1 - \mu_0 B) s(t)
        = - \mu_0 \omega_0 e(t)
    \end{equation*}
    donnerait les solutions~:
    \begin{equation*}
        s(t)
        = s_0 e^{-t \omega_0 (1 - \mu_0 B)} - \mu e(t)
    \end{equation*}
    la stabilité est possible
    pour~:
    \begin{equation*}
        - \omega_0 (1 - \mu_0 B)
        < 0
        \implies
        \mu_0 B < 1
    \end{equation*}
    en partique
    cela correspond à
    $B < \num{e-5}$
    (soit $R_1 = \SI{1}{\ohm}$ et $R_2 > \SI{100}{\kilo\ohm}$).
    Dans le cas limite,
    la pulsation de coupure
    est~:
    $\omega_c = \SI{200}{\radian\per\second}$
    et le gain
    $\mu = \num{50000}$.
    Le problème est alors
    de ne pas saturer l'ALI.
    Il faut
    $e < \frac{\SI{15}{\volt}}{\num{50000}} = \SI{0.3}{\milli\volt}$.

\section{Oscillation des systèmes bouclés}

    \subsection{Condition d'oscillation}

    \subsection{Oscillateur à pont de \propername{Wien}}

    \begin{manip}
        Faire la manip.
    \end{manip}

    \subsection{Étude des oscillations}
