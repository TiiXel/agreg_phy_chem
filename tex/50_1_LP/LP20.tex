\chapter{Conversion de puissance électromécanique}

\niveau{PSI}

\pyimgen{machine_synchrone}
\begin{python}
    Document interactif
    accompagnant cette leçon~:
    \software{./machine\_synchrone.imgen.py}
\end{python}

\section{Structure de la machine synchrone}

    \subsection{Le rotor}

    Le rotor
    est un aimant permanant
    ou un circuit électrique
    (une spire)
    qui a le même comportement,
    on lui associe
    un moment magnétique $\vb{M}_r$.
    Le rotor
    est un élement passif
    dans le moteur synchrone~:
    il est mis en rotation
    par le stator.

    \subsection{Le stator}

    Le stator
    va générer
    un champ magnétique
    dont l'objectif
    est de faire pivoter
    le rotor.
    On comprend que
    ce champ magnétique
    doit varier dans le temps
    pour que le rotor
    tourne sans s'arrêter~:
    le stator
    est constitué
    de bobinages
    que nous allons essayer de construire.

    \begin{manip}
        Montrer avec une boussolle
        qu'un champ magnétique fixe
        ne permet pas
        de faire un moteur.
        Montrer
        qu'avec un champ glissant
        on obtient un moteur.
    \end{manip}

    \subsection{L'entrefer}

    L'entrefer
    correspond
    à la couche d'air
    qui sépare
    le stator du rotor.
    Dans cette première étude
    de la machine synchrone
    nous considérons
    un cas idéal où
    l'entrefer a une épaisseur constante.

    On l'appelle
    \emph{entrefer}
    car
    le rotor et le stator
    sont fabriqués
    dans un matériau magnétique
    (typiquement un alliage de fer)
    de perméabilité magnétique $\mu$
    que l'on suppose infinie,
    dans cette situation
    l'électromagnétisme nous apprend
    que le champ magnétique
    qui sort des morceaux métalliques
    est perpendiculaire à leurs surfaces.
    Dans l'entrefer
    le champ magnétique
    est donc radial.

\section{Le champ glissant généré par le stator}

    \subsection{Stator a une spire}

    On essaye de voir
    le mouvement qu'aurait un rotor
    associé à un stator constitué d'une spire.
    On construit le stator
    de sorte que
    seule les deux branches
    parallèles à l'axe de symétrie
    contribuent à la production
    du champ magnétique,
    c'est à dire que l'on suppose
    la longueur des spires
    grandes devant
    la longueur du rotor.

    \subsubsection{Invariances et symétries}

    Le problème
    est considéré invariant
    par translation selon l'axe $z$.
    Le problème
    n'est \emph{pas} invariant
    par translation selon $\vu{r}$,
    ni par rotation selon $\vu{\theta}$~:
    on considèrera
    a priori
    que le champ magnétique
    dépend de ces deux dernières coordonnées.
    Cependant,
    on fera l'approximation
    que l'entrefer
    est suffisament mince
    pour que l'on puisse négliger
    les variations du champ
    lorsqu'on s'y déplace radialement.

    La distribution de courant
    étant symétrique
    par rapport au plan $Oyz$
    (on ignore le fil qui contourne du haut vers le bas),
    on a pour $\vb{B}$
    dans l'entrefer~:
    \begin{equation*}
        \vb{B}\qty(\frac{\pi}{2} - \Theta)
        = -\vb{B}\qty(\frac{\pi}{2} + \Theta)
    \end{equation*}

    La distribution de courant
    étant antisymétrique
    par rapport au plan $Oxz$
    le champ magnétique
    est symétrique par rapport à ce plan
    donc~:
    \begin{equation*}
        \vb{B}\qty(- \Theta)
        = \vb{B}\qty(\Theta)
    \end{equation*}

    \subsubsection{Champ magnétique créé par l'un des fils}

    On peut appliquer
    le théorème d'\propername{Ampère}
    sur le contour $\mathcal{L}$,
    qui entoure l'un des fils
    et
    qui traverse radialement l'entrefer
    aux deux angles suivant~:
    \begin{equation*}
        \theta
        = \frac{\pi}{2} - \Theta
        \in \qty[-\frac{\pi}{2}, \frac{\pi}{2}]
        \qq{et}
        \theta' = \frac{\pi}{2} + \Theta
        \in \qty[\frac{\pi}{2}, \frac{3\pi}{2}]
    \end{equation*}

    Cependant,
    comme les parties métalliques
    qui constituent le rotor et le stator
    ont une perméabilité magnétique $\mu = \infty$,
    l'excitation magnétique $\vb{H}$
    y est nulle~:
    $\vb{H} = \vb{0}$,
    donc
    on ne s'intéresse pas vraiment
    au chemin qui prend le countour
    hors de l'entrefer.

    Appliquons alors
    le théorème~:
    \begin{equation*}
        i
        = \oint_{\mathcal{L}} \vb{H} \dd \vb{l}
        = \int_{\mathcal{L}_m} \vb{H} \dd \vb{l}
            + \int_{\mathcal{L}_e} \vb{H} \dd \vb{l}
    \end{equation*}
    Comme $\vb{H}$ est radial dans l'entrefer~:
    \begin{align*}
        i
        = \int\displaylimits_{\mathcal{L}_e(\theta)} \vb{H} \vu{r} \dd l
            + \int\displaylimits_{\mathcal{L}_e(\theta')} \vb{H} (-\vu{r}) \dd l
        &= \int\displaylimits_{\mathcal{L}_e(\theta)} H \dd l
            + \int\displaylimits_{\mathcal{L}_e(\theta')} - H \dd l
        \\
        &= e \qty\Big(H(\theta) - H(\theta'))
        = 2 e H(\theta)
        = -2e H(\theta')
    \end{align*}
    Enfin,
    comme l'aimantaion est nulle dans l'entrefer,
    $H(\theta) = \flatfrac{B(\theta)}{\mu_0}$
    donc~:
    \begin{equation*}
        \left\{\begin{array}{ll}
                B(\theta)
                = +\displaystyle \frac{\mu_0 i}{2e}
                &\qq{pour}
                \displaystyle \theta \in \qty[-\frac{\pi}{2}, \frac{\pi}{2}]
                \\
                \displaystyle
                B(\theta)
                = \displaystyle - \frac{\mu_0 i}{2e}
                &\qq{pour}
                \displaystyle \theta \in \qty[\frac{\pi}{2}, \frac{3\pi}{2}]
        \end{array}\right.
    \end{equation*}

    \subsubsection{Champ créé par les deux fils}

    Par principe de superposition,
    on obtient rapidement que~:
    \begin{equation*}
        \left\{\begin{array}{ll}
                B(\theta)
                = +\displaystyle \frac{\mu_0 i}{e}
                = B_0
                &\qq{pour}
                \displaystyle \theta \in \qty[-\frac{\pi}{2}, \frac{\pi}{2}]
                \\
                \displaystyle
                B(\theta)
                = \displaystyle - \frac{\mu_0 i}{e}
                = - B_0
                &\qq{pour}
                \displaystyle \theta \in \qty[\frac{\pi}{2}, \frac{3\pi}{2}]
        \end{array}\right.
    \end{equation*}

    \subsection{Stator a plusieur spires}

    Puisqu'il n'y a pas
    de gradient de champ magnétique
    (hormis les deux discontinuités),
    cette spire
    ne permet pas
    de modifier l'orientation du rotor.
    Pour créer
    un gradient de champ magnétique
    centré en $\theta = 0$,
    nous allons disposer
    un ensemble de spires
    régulièrement sur la circonférence du stator,
    ce qui permet d'obtenir
    un champ $B = \fof(\theta)$ triangulaire.
    En pratique
    les spires sont plutôt disposées
    de sorte à créer
    un champ sinusoïdal
    qui s'écrit alors~:
    \begin{equation*}
        B(\theta)
        = \frac{2 \mu_0 N i}{\pi e} \cos(\theta) \vu{r}
        \propto i \cos(\theta) \vu{r}
    \end{equation*}
    on note que
    ce champ
    est bien dirigé selon $-\vu{r}$
    pour les valeurs de $\theta$
    qui correspondent
    à celles que nous avons déjà déterminées.

    Ce champ
    est toujours statique
    et ne permet donc pas
    de créer un moteur
    à proprement parler.

    \subsection{Stator a plusieur phases}

    On décide
    d'alimenter
    l'ensemble des spires
    étudiées jusqu'ici
    avec le même courant alternatif
    $i_1 = I \cos(\omega t)$.
    Dans ce cas,
    le champ magnétique
    dans l'entrefer
    s'écrit~:
    \begin{equation*}
        B(\theta, t)
        = k I \cos(\omega t) \cos(\theta) \vu{r}
        = k I \cos(\omega t) \cos(\theta) \vu{r}
    \end{equation*}
    il a la forme
    d'une onde sationnaire.
    Ce champ
    varie dans le temps
    mais la position de son maximum
    correspond toujours
    à $\theta = 0$,
    nous n'avons donc pas avancé.

    Cependant,
    de la trigonométrie
    (ou de LP24)
    on se rappelle que
    à partir de deux ondes stationnaires
    judicieusement déphasées,
    on peut former
    une onde progressive~:
    \begin{equation*}
        A_1
        = \cos(\omega t) \cos(\theta)
        \qq{et}
        A_2
        = \sin(\omega t) \sin(\theta)
        \implies
        A_1 + A_2
        = \cos(\omega t - \theta)
    \end{equation*}
    On peut donc
    en superposant
    les champs créés~:
    \begin{itemize}
        \item par les spires
            étudiées jusqu'ici
            et alimentées
            avec $i_1$~;
        \item par des spires
            décalées spatialement de $-\frac{\pi}{2}$
            et alimentées
            par un courant
            en quadrature avance
            par rapport à $i_1$, soit
            $i_2 = I \cos(\omega t - \frac{\pi}{2}) = I \sin(\omega t)$.
    \end{itemize}
    produire un champ glissant
    dont l'expression est~:
    \begin{equation*}
        B(\theta, t)
        = B_0 \cos(\omega t - \theta)
    \end{equation*}
    ce champ
    s'écrit comme une onde progressive,
    il se déplace
    à vitesse angulaire
    $\omega$
    dans le sens trigonométrique
    autour de l'axe $Oz$.

\section{Le champ du rotor}

    \begin{draft}
        On lui associe
        un moment magnétique\dots~?
        Ou alors
        un champ sinusoïdal aussi\dots~?
    \end{draft}

\section{Couple moteur}

    \begin{draft}
        Attention,
        au niveau PSI
        on utilise
        pour calculer le couple
        l'expression fournie~:
        $\vb{\Gamma} = \pdv*{E_\textrm{mag}}{\theta_r} \vu{z}$.

        Dans l'approximation des régimes quasi stationnaires,
        l'énergie volumique s'écrit~:
        $e_\textrm{mag} = \flatfrac{B^2}{2\mu_0\mu_r}$.
        Elle est alors
        nulle dans les parties métalliques
        et constante dans l'entrefer.
        On calcule
        $E$ à partir de $e$
        en intégrant sur ce volume,
        en tenant compte
        du champ du stator
        \emph{et}
        du champ du rotor~:
        $B = B + B_r$.
        On trouve
        un terme de couplage
        de sorte que l'énergie magnétique
        s'écrive~:
        $E \propto \cst + \cos(\omega t - \theta_r)$.

        Avec $\theta_r$ l'angle qui repère le rotor
        et $\Omega_r$ la vitesse angulaire du rotor,
        on trouve~:
        $\Gamma \propto \sin((\omega - \Omega_r) t + \alpha)$

        Pour que ce couple
        ne soit pas nul en moyenne,
        il faut $\Omega = \omega$.
        C'est la \emph{condition de synchronisme}.

        En tracant $\Gamma = \fof(\alpha)$
        on voit que
        il peut y avoir un décrochage,
        et que
        la machine peut fonctionner en alternateur.
        On voit aussi
        que le démarrage
        n'est pas possible
        sans aide.
    \end{draft}
