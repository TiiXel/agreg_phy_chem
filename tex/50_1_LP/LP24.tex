\chapter{Ondes progressives, ondes stationnaires}

Dans cette leçon
sur les ondes
on s'intéresse
à deux types particuliers
de solutions à l'équation de \propername{D'Alembert}.
Ces solutions
ont déjà été rencontrées
dans divers problèmes
et introduites
par des considérations phénoménologiques.

L'objectif de cette leçon
est de revenir sur l'équation de \propername{D'Alembert}
et
sur les considérations physiques
qui mênent à l'écriture
d'ondes progressives
ou
d'ondes stationnaires
et nous pourrons étudier
les différences qui existent entre les deux.

\begin{todo}
    Énnoncer les hypothèses
    faites dans cette leçon~:
    dispersion, absorbtion, linéarité, \dots
\end{todo}

\begin{draft}
    On fait le choix
    de mener les calculs
    sur la corde
    plutôt que le câble
    car l'étude de la corde
    ne nécessite l'introduction
    que d'un seul paramètre~:
    la déformation $y$~;
    l'étude du câble
    nécessite l'introduction
    de $u$ et $i$.
\end{draft}

\section{Équation de \propername{D'Alembert}}

    \subsection{Rappels, notations}

    \begin{manip}
        Montrer
        la corde de \propername{Melde}
        avec des conditions aux limites
        qui ne donnent pas lieu
        à une réflexion.

        Montrer aussi
        le câble coaxial
        dans les mêmes conditions.
    \end{manip}

    L'équation de \propername{D'Alembert}
    décrit la propagation des ondes,
    c'est une équation
    aux dérivées partielles
    dont
    la dérivée seconde
    par rapport au temps
    illustre
    le caractère réversible.
    Concernant
    les ondes
    sur la corde
    on trouve~:
    \begin{equation*}
        \pdv[2]{y}{x} - \frac{1}{c^2}\pdv[2]{y}{t}
        = 0
    \end{equation*}
    avec
    $c = \sqrt{\tfrac{T_0}{\mu}}$
    la célérité de l'onde.

    \subsection{Solution générale}

    On montre mathématiquement
    que
    les solutions générales
    de cette équation
    sont de la forme~:
    \begin{equation*}
        y(x, t)
        = f(t - \tfrac{x}{c}) + g(t + \tfrac{x}{c})
    \end{equation*}
    où $f$ et $g$
    sont deux fonctions
    deux fois dérivables
    par rapport au temps et à l'espace.
    Les solutions progressives
    et les solutions stationnaires
    sont donc
    des cas particuliers
    de combinaisons lin'éaires
    de cette forme
    de solution générale.

\section{Solutions progressives}

    \subsection{Ondes progressives}

    Si l'on considère
    le cas où
    $g(t + \frac{x}{c}) = 0$,
    et
    $f(t - \frac{x}{c})$ est quelconque,
    puis que l'on trace
    des graphes
    de la fonction
    $y(x; t_i)$
    pour différents instants $t_i$,
    on constate que
    cette solution
    correspond à
    la propagation
    d'un \emph{paquet d'ondes}
    dans le sens des $x$ croissants.

    Concernant le paquet d'ondes
    sa forme
    est donnée par~:
    \begin{itemize}
        \item $y(x, t = 0) = y_{t0}(x)$
            donne
            l'amplitude du signal
            en fonction de $x$
            à l'instant $t = 0$,
            cela correspond
            à prendre une photo
            de la corde
            à un instant donné~;
        \item $y(x = 0, t) = y_{x0}(t)$
            donne
            l'amplitude du signal
            au cours du temps
            en $x = 0$,
            cela correspond
            à filmer
            la hauteur de la corde
            à un point donné.
    \end{itemize}

    \begin{manip}
        Avec deux oscilloscopes
        on peut regarder $y_x(t)$
        le long du câble coaxial.
    \end{manip}

    \begin{python}
        On peut utiliser
        \software{./python/prop\_onde\_cl.py}
        pour illustrer
        la propagation d'un paquet
        et montrer $y_t(x)$.
    \end{python}

    Évidemment,
    $g$ va correspondre
    à la propagation
    dans les sens des $x$ décroissants.
    Ce type de solutions
    nécessite donc
    de faire l'hypothèse
    que le signal
    ne se déplace que dans une direction~:
    il ne doit
    pas y avoir
    de réflexions
    ou d'autres sources.

    \subsection{Ondes progressives harmoniques}

    La linéarité
    de l'équation de \propername{D'Alembert}
    permet de décomposer
    une des solutions
    pour l'écrire
    comme une somme d'autres solutions.

    En physique
    il est très utile
    de décomposer les phénomènes ondulatoires
    en solutions harmoniques.
    Concernant
    les solutions progressives
    de l'équation de \propername{D'Alembert},
    on peut vérifier que~:
    \begin{equation*}
        f_i(t - \tfrac{x}{c})
        = y_0 \cos(\omega_i (t - \tfrac{x}{c}) + \phi_i)
    \end{equation*}
    correspond bien
    à une solution.
    Et
    les travaux de \propername{Fourier}
    nous permettent d'affirmer que
    toute fonction
    peut s'écrire
    comme une somme de $f_i$.
    Le paquet d'onde
    dans l'illustration précédente
    est formé
    à partir de~:
    \begin{equation*}
        y(x, t)
        = y_0 \int e^{- \frac{(\omega - \omega_0)^2}{2 \Delta_\omega^2}}
            \cos(\omega (t - \tfrac{x}{c}))
            \dd \omega
    \end{equation*}

    \begin{manip}
        Le paquet d'onde
        dans l'expérience du câble coaxial
        est formé à partir de\dots
    \end{manip}

    \subsection{Propagation de l'énergie}

    On définit les ondes
    comme les phénomènes
    qui propagent de l'énergie
    sans transport de matière.
    Il est alors important
    de regarder comment se propage cette énergie.

    Sur l'exemple de la corde
    l'énergie cinétique linéique
    et
    l'énergie potentielle linéique
    sont données par~:
    \begin{equation*}
        e_c
        = \frac{\mu}{2} \qty(\pdv{y}{t})^2
        \qq{et}
        e_p
        = \frac{T_0}{2} \qty(\pdv{y}{x})^2
    \end{equation*}
    donc l'énergie linéique
    est~:
    \begin{equation*}
        e
        = \frac{\mu}{2} \qty(\pdv{y}{t})^2 + \frac{T_0}{2} \qty(\pdv{y}{x})^2
    \end{equation*}
    La conservation de l'énergie
    assure que~:
    \begin{equation*}
        \pdv{e}{t} + \div(\vb{\Pi})
        = 0
    \end{equation*}
    où
    $\vb{\Pi}$ correspond
    au vecteur densité de courant d'énergie.
    On peut calculer
    à une dimension~:
    $\div{\vb{\Pi}} = \pdv*{\Pi}{x}$~:
    \begin{align*}
        - \pdv{\Pi}{x}
        = \pdv{e}{t}
        &= \mu \pdv[2]{y}{t} \pdv{y}{t} + T_0 \pdv{y}{t}{x} \pdv{y}{x}
        \\
        &= T_0 \pdv[2]{y}{x} \pdv{y}{t} + T_0 \pdv{y}{t}{x} \pdv{y}{x}
    \end{align*}
    par propriété inverse
    sur la dérivée du produit~:
    \begin{align*}
        - \pdv{\Pi}{x}
        &= T_0 \qty(\pdv[2]{y}{x} \pdv{y}{t} + \pdv{y}{t}{x} \pdv{y}{x})
        \\
        &= \pdv{x}(T_0 \pdv{y}{x} \pdv{y}{t})
    \end{align*}
    en intégrant ce résultat~:
    \begin{equation*}
        \Pi
        = -T_0 \pdv{y}{x} \pdv{y}{t} + \Pi_0
    \end{equation*}

    On choisira
    la constante d'intégration
    comme étant
    le vecteur nul.
    Pour une onde progressive harmonique~:
    \begin{equation*}
        y(x, t)
        = y_0 \cos(\omega (t - \tfrac{x}{c}))
    \end{equation*}
    on obtient~:
    \begin{equation*}
        \Pi
        = - T_0 \frac{y_0^2 \omega^2}{c} \sin[2](\omega (t - \tfrac{x}{c}))
        = \fof(t - \tfrac{x}{c})
    \end{equation*}
    la densité d'énergie
    se propage donc
    à la vitesse $c$
    dans la même direction
    que l'onde.

    \begin{draft}
        Pour le câble coaxial~:
        $e_C = \frac{\Gamma}{2} u^2$
        et
        $e_L = \frac{\Lambda}{2} i^2$.
        \begin{equation*}
            \pdv{e}{t}
            = \pdv{t}(\frac{\Gamma}{2} u^2 + \frac{\Lambda}{2} i^2)
            = \Gamma u \pdv{u}{t} + \Lambda i \pdv{i}{t}
        \end{equation*}

        or,
        les équations différentielles couplées
        sont~:
        \begin{align*}
            \pdv{u}{x} &= - \Lambda \pdv{i}{t}
            \\
            \pdv{i}{x} &= - \Gamma \pdv{u}{t}
        \end{align*}
        alors~:
        \begin{equation*}
            \pdv{e}{t}
            = u \pdv{i}{x} + i \pdv{u}{x}
            = \pdv{x}(ui)
            \implies
            \Pi = ui
        \end{equation*}
        à nouveau,
        pour une onde progressive harmonique~:
        \begin{equation*}
            \Pi
            \propto \sin[2](\omega (t - \tfrac{x}{c}))
            = \fof(t - \tfrac{x}{c})
        \end{equation*}
    \end{draft}

\section{Réflexion d'ondes progressives \cite{perezmecanique}}

    \subsection{Forme mathématique, condtions aux limites}

    \subsubsection{Forme mathématique de l'onde réfléchie}

    Pour étudier les ondes progressives
    nous avons tout à l'heure
    dû faire l'hypothèses
    que les ondes
    ne se propagaient
    que dans une direction
    $(f \neq 0, g = 0)$~;
    donc qu'il n'y avait
    ni réflexion
    ni autre sources.

    Ces hypothèses
    nous placaient
    dans un cadre mathématique simple
    avec une seule fonction
    à étudier.
    Dans cette deuxième partie
    on propose
    une hypothèse différente~:
    nous admettons que
    la réflexion en $x = L$ est parfaite
    de sorte que
    l'onde qui se propage dans un sens
    prend la même forme
    que celle qui se propage dans l'autre sens
    mais avec un signe opposé~:
    \begin{equation*}
        g(t + \tfrac{L}{c})
        = - f(t - \tfrac{L}{c})
    \end{equation*}

    On impose donc
    un lien entre $g$ et $f$
    en $L$ quelque soit $t$.
    Pour connaître
    l'amplitude de l'onde réfléchie $g$
    en $x \neq L$ à l'instant $t$,
    on doit chercher
    à quel instant $t'$
    elle a été générée en $x' = L$~:
    \begin{align*}
        g(t + \tfrac{x}{c}) &= - f(t' - \tfrac{x'}{c})
        \\
        t + \tfrac{x}{c} &= t' - \tfrac{L}{c}
        \\
        t + \tfrac{x + L}{c} &= t'
        \\
        \implies
        g(t + \tfrac{x}{c})
        &= - f(t + \tfrac{x + L}{c} - \tfrac{x'}{c})
        \\
        \implies
        g(t + \tfrac{x}{c})
        &= - f(t + \tfrac{x - 2L}{c})
    \end{align*}

    \subsubsection{Onde résultante}

    L'amplitude
    effectivement mesurée
    en un point de la corde
    à un instant donné
    est~:
    \begin{equation*}
        y(x, t)
        = f(t - \tfrac{x}{c}) - f(t + \tfrac{x - 2L}{c})
    \end{equation*}

    En particulier,
    au point $x = L$
    où se fait la réflexion
    on a $y(L, t) = 0$.

    \subsection{Ondes harmoniques}

    Pour une onde incidente harmonique~:
    \begin{align*}
        y(x, t)
        &= y_0 [
            \cos(\omega (t - \tfrac{x}{c}))
            - \cos(\omega (t + \tfrac{x - 2L}{c}))
            ]
        \\
        &= 2 y_0 \cos(\omega t - \tfrac{L}{c})
            \cos(\tfrac{\omega}{c} x + \tfrac{L}{c})
    \end{align*}

\section{Ondes stationnaires \cite{dunodpcsi}}

    \subsection{Caractéristiques}

    En considérant
    deux ondes progressives harmoniques
    $f$ et $g$
    de même pulsation, vitesse, amplitude
    mais de direction de propagation opposées,
    on trouve que
    l'onde résultante
    prend la forme~:
    \begin{equation*}
        y(x, t)
        = 2 y_0 \cos(\omega t + \phi) \cos(\tfrac{\omega}{c} x + \psi)
    \end{equation*}

    L'amplitude $y(x, t)$
    s'écrit comme le produit
    d'une fonction périodique du temps
    et
    d'une fonction périodique de l'espace.
    Contrairement
    aux ondes progressives $f$ et $g$
    dont l'amplitude
    dépend de $t - \tfrac{x}{c}$,
    les variables
    de temps et d'espace
    sont maintenant séparées.

    \subsection{Noeuds et ventres}

    Le facteur $\cos(\tfrac{\omega}{c} x + \phi)$
    s'annule pour des positions précises,
    que l'on appelle \emph{noeuds}
    et prend sa valeur maximale
    pour des positions
    que l'on appelle \emph{ventres}.
    Le facteur $\cos(\omega t + \psi)$
    fait varier
    l'amplitude des ventres
    au cours du temps.
    Pour alléger les expressions,
    on prend $\phi = 0 = \psi$.

    Les ventres sont situés
    aux positions $x$
    telles que~:
    \begin{equation*}
        \cos(\tfrac{\omega}{c} x)
        = 1
        \implies
        \tfrac{\omega}{c} x
        = n \pi
        \implies
        x
        = \tfrac{c n \pi}{\omega}
        = \tfrac{n \lambda}{2}
    \end{equation*}
    et les noeuds~:
    \begin{equation*}
        \cos(\tfrac{\omega}{c} x)
        = 0
        \implies
        \tfrac{\omega}{c} x
        = \qty(n + \tfrac{1}{2}) \pi
        \implies
        x
        = \tfrac{c n \pi}{\omega} + \tfrac{c \pi}{2 \omega}
        = \tfrac{n \lambda}{2} + \tfrac{\lambda}{4}
    \end{equation*}

    La distance
    entre deux noeuds
    ou
    entre deux ventre
    consécutifs
    est $\frac{\lambda}{2}$,
    et
    la distance
    entre un noeud et un ventre
    consécutifs
    est $\frac{\lambda}{4}$.

    \subsection{Aspect énergétique}

    Montrer que l'énergie
    ne se propage pas~:
    $\avg{\vb{\vb{\Pi}}} = \vb{0}$.
