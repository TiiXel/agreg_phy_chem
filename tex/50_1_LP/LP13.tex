\chapter{Évolution et condition d'équilibre d'un système thermodynamique
    fermé}

  \section{Le potentiel}

    \subsection{Une notion de mécanique \cite{diu}}

    Les systèmes mécaniques
    initialement maintenus dans un état donné
    évoluent lorsqu'une contrainte est relachée
    vers un autre état
    de sorte que leur énergie potentielle diminue.
    Ils atteignent un équilibre
    lorsque leur énergie potentielle atteint un extrémum.
    L'équilibre peut être
    stable,
    métastable,
    ou instable
    selon qu'il s'agisse
    d'un minimum global,
    d'un minimum local,
    ou d'un maximum.

    La conservation de l'énergie
    assure que cette énergie potentielle
    a été dissipée par le système.

    Un système masse-ressort
    initialement contraint dans une position étendue
    et baignant dans une atmosphère
    va osciller
    dès lors que l'on relache la contrainte,
    convertissant énergie cinétique et énergie potentielle,
    avant de le rejoindre une position équilibre
    où l'énergie potentielle est minimale.

    \subsection{Une généralisation en thermodynamique \cite{diu} \cite{gie}}

    Les systèmes thermodynamique
    sont soumis aux deux premiers principes~:
    \begin{align*}
        &\Delta U
        = W + Q\\
        &\Delta S
        = S_{\mathrm{cr}} + S_{\mathrm{ech}} \qq{avec} S_{\mathrm{cr}} \geq 0
    \end{align*}

    Un système fermé et isolé
    initialement contraint dans un état $(I)$
    poura évoluer vers un état final $(F)$
    par création d'entropie,
    de sorte que l'inégalité
    $\Delta S \geq 0$
    soit vérifiée.

    Cette affirmation laisse entendre que
    l'on peut définir une fonction $-S$
    appelée \emph{négentropie}
    de sorte que l'évolution du sytème fermé et isolé
    soit contrainte par
    sa diminution~:
    $\Delta(-S) \leq 0$.
    La néguentropie joue alors un rôle similaire à
    l'énergie potentielle des systèmes mécaniques
    et on la qualifiera de
    potentiel thermodynamique.

    Le système atteint un état d'équilibre stable
    lorsque sa néguentropie est globalement minimale.
    Les équilibres peuvent être métastables
    en cas de minimum local,
    mais la thermodynamique
    ne prévoit pas d'équilibre instable
    car les fluctuations
    présentes à l'échelle microscopiques
    feraient spontanément sortir le système
    de cette situation.

    Comme indiqué,
    la néguentropie ne constitue un potentiel thermodynamique
    que pour les systèmes fermés et isolés.
    Mais nous allons voir
    que pour chaque système
    il est possible de trouver
    une fonction
    agissant comme potentiel.

  \section{Système fermé et indéformable, en contact avec un thermostat}

    \subsection{Le potentiel \cite{gie}}

    Considérons un système
    fermé,
    indéformable,
    en contact avec un thermostat à la température $T_0$
    qui est la seule source d'échange thermique.
    Le système contraint dans un état initial $(I)$
    est relaché
    et évolue vers un état final $(F)$.

    Les variables ajustables du système
    sont notées $Y$ (variable conjuguée $y$)
    et la température du système $T$ (variable conjuguée $S$)
    en fait partie.
    \begin{todo}
        Trouver un exemple de variables internes ajustables
        autre que la température
        dans ce type de transformation.
    \end{todo}

    Les premier et second principes peuvent s'écrire~:
    \begin{gather*}
        \Delta U
        = U_F - U_I
        = \cancel{W} + Q\\
        \Delta S
        \geq \flatfrac{Q}{T_0}
    \end{gather*}
    D'où~:
    $\Delta U \leq T_0 \Delta S$

    On définit alors
    $F^\ast \defeq U - T_0 S$
    de sorte que $F^\ast$ agisse en potentiel thermodynamique
    pour le système~:
    \begin{equation*}
        \Delta F^\ast
        = \Delta(U - T_0 S)
        \leq 0
    \end{equation*}
    Remarquons que $F^\ast$ dépend
    à la fois de l'état du système (via $U$ et $S$)
    et de la température du thermostat ($T_0$)
    qui est un paramètre extérieur,
    pas une variable interne du système.
    Il ne s'agit donc pas d'une fonction d'état.

    \subsection{Condition d'équilibre \cite{diu} \cite{gie}}

    Le système atteindra un état d'équilibre
    lorsque la différentielle $\dd F^\ast$
    de $F^\ast$
    est nulle~:
    \begin{align*}
        \dd F^\ast
            &= \dd U - T_0 \dd S
                = 0 \\
        \dd U
            &= T \dd S - P \cancel{\dd V} + Y \dd y
                \qq{(identité thermodynamique)} \\
        \implies
        \dd F^\ast
            &= 0
                = (T - T_0) \dd S + Y \dd y
    \end{align*}
    Cette dernière égalité donne la condition d'équilibre~:
    $T = T_0$ et $Y = 0$.
    \begin{todo}
        Stabilité~?
    \end{todo}
    \begin{todo}
        Traiter un exemple où $Y$ apparait clairement.
    \end{todo}

    \subsection{Cas particulier d'un équilibre thermique avec le thermostat
        dans l'état initial et l'état final \cite{gie}}

    Dans le cas particulier
    où les états $(I)$ et $(F)$
    sont des états d'équilibres thermiques avec le thermostat,
    alors les températures $T_I$ et $T_F$ des deux états vérifient~:
    $T_I = T_0 = T_F$.
    Dans ce cas,
    la variation entre $(I)$ et $(F)$
    de l'énergie libre du système $F = U - TS$
    vérifie~:
    \begin{equation*}
        \Delta F
        = \Delta F^\ast
        = \Delta(U-TS)
    \end{equation*}
    Cette fois,
    le potentiel thermodynamique
    dont le rôle est \emph{aussi} joué par la fonction d'état $F$
    ne dépend que de variables internes.

  \section{Système fermé déformable, en contact avec un thermostat et
    une atmosphère}

    \subsection{La fonction $F^\ast$ \cite{gie}}

    Dans cette situation,
    le système reçoit algébriquement un travail $W$
    et le premier principe s'écrit~:
    \begin{equation*}
        \Delta U
        = W + Q
    \end{equation*}
    D'où~:
    $\Delta U \leq T_0 \Delta S + W$.
    Alors~:
    \begin{equation*}
        \Delta F^\ast
        \leq W
    \end{equation*}

    Si le système reçoit du travail ($W > 0$),
    alors $F^\ast$ peut augmenter~:
    il ne s'agit pas d'un potentiel thermodynamique
    pour les systèmes déformables.

    Toutefois, si le système cède du travail ($W < 0$),
    et que le système est destiné à le récupérer,
    le travail \emph{maximum récupérable} est~:
    \begin{equation*}
        0
        < \abs{W}
        \leq -\Delta F^\ast
        = F^\ast_I - F^\ast_F
    \end{equation*}
    La fonction $F^\ast$ diminue au cours de la transformation,
    et sa diminution correspond au travail maximum
    que l'on peut récupérer.
    Avec un système adapté,
    on le récupère entièrement
    dans le cas d'une transformation réversible,
    et on n'en récupère qu'ne partie
    dans le cas d'une transformation irréversible

    $F^\ast$ reste intéressante pour ces systèmes,
    même s'il ne s'agit pas d'un potentiel.

    \paragraph{Le cas de la machine cyclique monotherme}

    Puisque sur un cycle,
    $\Delta F^\ast = 0$ et $\Delta F^\ast \leq W$,
    alors forcément $W \geq 0$ pour ce cycle.
    Ce qui rend compte du problème
    théorique lié à ces machines
    longtemps cherchées par les inventeurs.

    \begin{todo}
        Préparer un document à projeter
        pour toute la suite
        de cette partie,
        puisque c'est très similaire
        à la précédente.
    \end{todo}

    \subsection{Le potentiel}

    Le système est semblable au précédent mais
    déformable
    et soumis à une pression $P_0$ par l'atmosphère
    qui est la seule source de travail.

    Le travail des forces de pression
    s'écrit $W = - P_0 \Delta V$,
    ce qui permet de réexprimer
    l'inégalité sur $\Delta F^\ast$~:
    \begin{align*}
        &\Delta F^\ast
        \leq - P_0 \Delta V \\
        \implies
        &\Delta(F^\ast + P_0 \Delta V)
        \leq 0
    \end{align*}
    On définit alors
    $G^\ast \defeq U - T_0 S + P_0 V$
    de sorte que
    $G^\ast$
    agisse en potentiel thermodynamique pour le système~:
    \begin{equation}
        \Delta G^\ast
        = \Delta(U - T_0 S + P_0 V)
        = \Delta(F^\ast + P_0 V)
        \leq 0
    \end{equation}
    Là encore il faut remarquer que $G^\ast$
    n'est pas une fonction d'état du système
    puisque la fonction dépend de $T_0$ et $P_0$.

    \subsection{Condition d'équilibre}

    Le système atteindra un état d'équilibre
    lorsque la différentielle $\dd G^\ast$
    de $G^\ast$
    est nulle~:
    \begin{align*}
        \dd G^\ast
            &= \dd U - T_0 \dd S + P_0 \dd V
                = 0 \\
        \dd U
            &= T \dd S - P \dd V + Y \dd y \\
        \implies
        \dd G^\ast
            &= 0
                = (T - T_0) \dd S + (P_0 - P) \dd V + Y \dd y
    \end{align*}
    Cette dernière égalité donne la condition d'équilibre~:
    $T = T_0$ et $P = P_0$ et $Y = 0$.

    \subsection{Cas particulier d'un équilibre thermique avec le thermostat
        et mécanique avec l'atmosphère dans l'état initial et l'état final}

    Cette fois,
    le potentiel thermodynamique
    dont le rôle est \emph{aussi} joué par la fonction d'état
    énergie libre $G$
    ne dépend que de variables internes.

    \begin{todo}
        Lire \cite[p 190, chap. 5, III.D.1]{diu} sur le choix du potentiel,
        et se préparer à discuter
        la notion de variables naturelles.
    \end{todo}

    \section{Un exemple~: la pile \propername{Daniell}}

    \begin{manip}
        Montrer la pile~!
    \end{manip}

    \begin{todo}
        Faire vérifier tout ça
        avant de raconter n'importe quoi\dots
    \end{todo}

    On considère un système fermé, constitué~:
    \begin{itemize}
        \item de cuivre solide \ce{Cu(s)}
        \item de zinc solide \ce{Zn(s)}
        \item d'ions cuivre en solution aqueuse \ce{Cu^2+(aq)}
        \item d'ions zinc en solution aqueuse \ce{Zn^2+(aq)}
    \end{itemize}
    où la réaction chimique
    $\ce{Cu^2+(aq) + Zn(s) = Zn^2+(aq) + Cu(s)}$
    est susceptible de se produire.

    Les variations d'énergie interne de ce système s'écrivent~:
    \begin{gather*}
        \dd U
        = T \dd S - P \dd V
        + E \dd q
        + \mu_{\ce{Cu}} \dd n_{\ce{Cu}}
        + \mu_{\ce{Cu^2+}} \dd n_{\ce{Cu^2+}}
        + \mu_{\ce{Zn}} \dd n_{\ce{Zn}}
        + \mu_{\ce{Zn^2+}} \dd n_{\ce{Zn^2+}}\\
        \implies\\
        \dd G^\ast
        = (T-T_0) \dd S - (P-P_0) \dd V
        + E \dd q
        + \mu_{\ce{Cu}} \dd n_{\ce{Cu}}
        + \mu_{\ce{Cu^2+}} \dd n_{\ce{Cu^2+}}
        + \mu_{\ce{Zn}} \dd n_{\ce{Zn}}
        + \mu_{\ce{Zn^2+}} \dd n_{\ce{Zn^2+}}
    \end{gather*}
    où $E$ est le potentiel entre les électrodes
    et $q$ la charge électrique relative entre les électrodes.
    \begin{draft}
        La variable conjugée à $q$ est $E$ car~:
        puissance électrique $P = \dv{U}{t} = E i = E\dv{q}{t}$
        donc $\dd U = E \dd q$.
    \end{draft}
    avec les relations sur les quantités de matière et la charge~:
    \begin{align*}
        \dd\xi
        &=\dd n_{\ce{Cu}}
        = -\dd n_{\ce{Cu^2+}}\\
        &= \dd n_{\ce{Zn}}
        =- \dd n_{\ce{Zn^2+}}\\
        &= \flatfrac{\dd q}{2 \mathcal{F}}
    \end{align*}
    d'où~:
    \begin{equation*}
        \dd G^\ast
        = (T-T_0) \dd S - (P-P_0) \dd V
        + (2E\mathcal{F}
        + \mu_{\ce{Cu}} - \mu_{\ce{Cu^2+}} - \mu_{\ce{Zn}} + \mu_{\ce{Zn^2+}})
        \dd \xi
    \end{equation*}
    à l'équilibre on aura,
    d'après la condition démontrée plus haut~:
    \begin{gather*}
        T = T_0\\
        P = P_0\\
        (2E\mathcal{F}
        + \mu_{\ce{Cu}} - \mu_{\ce{Cu^2+}} - \mu_{\ce{Zn}} + \mu_{\ce{Zn^2+}})
        = 0\\
        \implies
        E = - \frac{\mu_{\ce{Cu}} - \mu_{\ce{Cu^2+}}
            - \mu_{\ce{Zn}} + \mu_{\ce{Zn^2+}}}
            {2\mathcal{F}}
    \end{gather*}
    L'application numérique dans l'état standard
    ($P_0 = P^\std$, $T_0 = T^\std$, $c = c^\std$)
    donne
    $E = \SI{1.1}{\volt}$
    avec~:
    \begin{table}[H]
        \centering
        \begin{tabular}{lS}
            \toprule
            Espèce
                & {Potentiel standard (\si{\kilo\joule\per\mol})
                \cite{wiberg2011chemische}} \\
            \midrule
            \ce{Cu(s)} & 0.00 \\
            \ce{Zn(s)} & 0.00 \\
            \ce{Cu^2+(aq)} & 65.52 \\
            \ce{Zn^2+(aq)} & -147.03 \\
            \bottomrule
        \end{tabular}
    \end{table}
