\chapter{Caractère non galiléen du référentiel terrestre}

    L'objectif ce cette leçon est d'étudier
    l'application du principe fondamental de la dynamique
    à tous les réferentiels,
    et d'en tirer des résultats comparables aux observations.

    Nous aurons déjà vu
    que deux nouveaux termes s'ajoutent à l'accélération,
    et servent à transposer l'expression d'un référentiel galiléen
    vers un référentiel non galiléen,
    tenant compte des effets inertiels.

    Dans la leçon nous considèreront les référentiels~:
    \begin{itemize}
        \item géocentrique $\fref{T}$,
            supposé galiléen et repéré par
            $T(x_\fref{T}, y_\fref{T}, z_\fref{T})$
            de sorte que l'axe
            $Tz_\fref{T}$
            soit l'axe Sud-Nord~;
        \item du laboratoire,
            à la latitude $\lambda$,
            repéré par
            $O(x, y, z)$
            de sorte que~:
            \begin{itemize}
                \item $Oz$ corresponde à la verticale locale,
                \item $Oy$ soit dirigé vers le Nord,
                \item $Ox$ soit dirigé vers l'Est,
                \item $\norm{\va{TO}} = R_T$ soit le rayon de la Terre,
            \end{itemize}
    \end{itemize}

    Le vecteur $\vb{\Omega}$
    est le vecteur rotation de la Terre autour de $Tz_\fref{T}$.

    L'accélération d'inertie d'entrainement d'un point $A$
    dans le laboratoire est~:
    \begin{equation*}
        \vb{a_e}_{A/\fref{L}}
        = \vb{a}_{O/\fref{T}}
            + \dv{\vb{\Omega}}{t} \cross \va{OA}
            + \vb{\Omega} \cross (\vb{\Omega} \cross \va{OA})
    \end{equation*}
    et l'accélération d'inertie de \propername{Coriolis}
    s'écrit~:
    \begin{equation*}
        \vb{a_c}_{A/\fref{L}}
        = 2 \vb{\Omega} \cross \vb{v}_{A/\fref{L}}
    \end{equation*}
    avec des notations usuelles.

  \section{Effets de l'accélération d'entraînement}

    \subsection{Poids, champ de pesanteur, verticale}

    Qu'est ce que, expérimentalement, le poids~?
    Une réponse possible est la suivante~:
    le poids d'un corps est expérimentalement
    la force qu'il faut compenser
    pour garder ce corps immobile
    dans le référentiel géocentrique.

    Prenons l'exemple d'une masse $m$
    immobile dans $\fref{L}$.
    La terre y exerce une force via son champ gravitationnel $\vb{\fref{G}}$,
    et son support y applique une force $\vb{F_r}$,
    de sorte que le principe fondamental de la dynamique dans $\fref{L}$
    s'écrive~:
    \begin{equation*}
        m\qty(\vb{a_e} + \vb{a_c} + \vb{0})
        = \vb{F_r} + m\vb{\fref{G}}(R_T)
    \end{equation*}
    avec~:
    \begin{align*}
        \vb{a_e}
        = - \Omega^2 R_T \cos(\lambda)
            \qty(\cos(\theta)\vu{x_\fref{T}} + \sin(\theta)\vu{y_\fref{T}})
        &= - \Omega^2 R_T
            \qty(\vu{z} - \sin(\lambda)\vu{z_\fref{T}})\\
        &= - \Omega^2 R_T
            \qty(\cos[2](\lambda)\vu{z} - \cos(\lambda)\sin(\lambda)\vu{y})
    \end{align*}
    ainsi que~:
    \begin{equation*}
        \vb{a_c}
        = \vb{0}
    \end{equation*}
    d'où~:
    \begin{equation*}
        \vb{F_r}
        = m\qty
            (\qty(- \Omega^2 R_T \cos[2](\lambda) + \frac{G M_T}{R_T^2})\vu{z}
            - \Omega^2 R_T \cos(\lambda)\sin(\lambda)\vu{y})
        = - \vb{P}
        = - m \vb{g}
    \end{equation*}
    définissant ainsi le poids $\vb{P}$
    et le champ de pesanteur $\vb{g}$.

    Alors la verticale étant définie comme la normale au sol
    (vecteur $\vu{z}$)
    on constate
    que le champ de pensanteur $\vb{g}$
    n'y est pas nécessairement parallèle.
    L'angle $\alpha$ entre les deux varie selon la latitude~:
    \begin{equation*}
        \alpha
        = \left\{\begin{array}{ll}
            \ang{0;0;0} & \mbox{à l'équateur ($\lambda = \SI{0}{\degree}$)}\\
            \ang{0;5;55} & \mbox{à Paris ($\lambda = \SI{48}{\degree}$)}\\
            \ang{0;0;0} & \mbox{aux pôles ($\lambda = \SI{90}{\degree}$)}\\
        \end{array}\right.
    \end{equation*}
    Notons qu'à l'équateur
    les deux directions sont confondues et~:
    \begin{equation*}
        \frac{\flatfrac{G M_T}{R_T^2}}{\Omega^2 R_T} = 290 = 17^2
    \end{equation*}
    autrement dit,
    si la Terre tournait sur elle même 17 fois plus rapidement
    ($\Omega \rightarrow 17 \Omega$),
    le champ de pesanteur serait nul à l'équateur.

  \section{Effet de l'accélération de \propername{Coriolis}}

    \subsection{Pendule de \propername{Foucault} (1851) \cite{perezmecanique}}

    \begin{todo}
        Citer la feuille volante du correcteur.
    \end{todo}

    \begin{manip}
        Montrer le pendule
        que l'on aura lancé en début de leçon.
    \end{manip}
    \begin{python}
        Montrer
        le script
        \software{./python/pendule\_de\_foucault.py}
        qui simule le pendule du Panthéon
        que l'on aura lancé en début de leçon.
    \end{python}

    \begin{draft}
        Décrire rapidement l'expérience
        au Pôle-Nord
        puis avec les mains à une autre latitude.
    \end{draft}

    On considère maintenant un pendule
    de période $\tau$
    qui oscille sur l'axe Sud-Nord local,
    de masse $m$ au point $A$,
    de sommet $S$,
    et de sorte que $A$ coïncide avec $O$ au repos,
    le principe fondamental de la dynamique s'écrit
    dans le référentiel du laboratoire~:
    \begin{equation*}
        m\qty(\vb{a_c} + \vb{a}_{A/\fref{L}})
        = f_t\va{AS} + m\vb{g}
    \end{equation*}
    \begin{equation*}
        \vb{a_c}
        = 2 \vb{\Omega} \cross \vb{v}_{A/\fref{L}}
    \end{equation*}
    on s'intéresse ici au mouvement dans le plan horizontal,
    décrit quand $\vb{a_c}$ n'est pas présent par~:
    \begin{equation*}
        \left\{\begin{array}{ll}
            x(t) &= 0\\
            y(t) &= - Y \cos(\omega t)
        \end{array}\right.
    \end{equation*}
    d'où
    $\dt{y} = - Y \omega \sin(\omega t)$ (avec $y(0) = -Y$).
    Alors pour le premier aller ($t \leq \frac{\tau}{2}$)~:
    \begin{equation*}
        \vb{a_c}
        = 2 \vb{\Omega} \cross \vb{v}_{A/\fref{L}}
        = + 2 \Omega \sin(\lambda) \dt{y} \vu{x}
    \end{equation*}
    qui est la seule contribution d'accélération selon $\vu{x}$.
    Donc~:
    \begin{align*}
        \norm{\vb{a_c}}
        = \dv{\dt{x}}{t}
        \implies
        \dt{x}
        &= - 2 \Omega \sin(\lambda) Y \cos(\omega t) + \cst\\
        &= 2 \Omega \sin(\lambda) Y (1 - \cos(\omega t))
    \end{align*}
    en prenant $\dt{x}(0) = 0$ pour l'intégration.
    Enfin, toujours pour $t \leq
    \frac{\tau}{2}$,
    \begin{equation*}
        x(t) = 2 \Omega \sin(\lambda) Y (t - \frac{\sin(\omega t)}{\omega})
    \end{equation*}
    Au bout du premier aller,
    calculons~:
    \begin{equation*}
        x\qty(\flatfrac{\tau}{2})
        = 2 \Omega Y \sin(\lambda) \frac{\tau}{2}
        = \delta_0
    \end{equation*}
    La circonférence du cercle
    qui joint les différents points de rebroussement
    (dans l'hypothèse d'un pendule sans amortissement)
    est $C = 2 \pi Y$.
    On exprime alors~:
    \begin{equation*}
        \delta_0
        = \frac{\Omega}{2\pi} C \tau \sin(\lambda)
        = \frac{\tau}{\tau_{\mathrm{Terre}}} C \sin(\lambda)
    \end{equation*}
    L'application numérique
    pour le pendule de \propername{Foucault} au panthéon
    (pendule haut de \SI{67}{\meter},
    $\lambda = \SI{48}{\degree}$
    et $Y = \SI{15}{\meter}$)
    donne~:
    $\tau = \SI{16}{\second}$
    puis
    $\delta_0 = \SI{1.2}{\centi\meter}$.
    Le phénomène était donc facilement observable sur ce pendule.
    Notons que le pendule fait un tour complet après
    $n = \flatfrac{C}{\delta_0} \approx 7600$ demi-oscillations
    en $n \tau = \frac{\tau_{\mathrm{Terre}}}{\sin(\lambda)} = \SI{1.4}{\day}$
    (résultat qui est indépendant des paramètres du pendule~!).

    \subsection{Déviation vers l'Est (1903) \cite{perezmecanique}}

    Lors de la chute d'une bille $B$ sur Terre depuis une altitude $h$,
    étudiée dans le référentiel $\fref{L}$,
    le principe fondamental de la dynamique s'écrit~:
    \begin{equation*}
        m\qty(2 \vb{\Omega} \cross \vb{v}_{B/\fref{L}} + \vb{a}_{B/\fref{L}})
        = m\vb{g}
    \end{equation*}
    en prenant
    $\vb{v}_{B/\fref{L}} = t\vb{g}$ et $\vb{g} = g\vu{z}$
    il vient~:
    \begin{equation*}
        \vb{a}_{B/\fref{L}}
        = g\vu{z} - 2 \vb{\Omega} \cross \qty(tg\vu{z})
        = g \qty(\vu{z} - 2 \Omega t \cos(\lambda) \vu{x})
    \end{equation*}
    Le calcul indique que la bille va avoir un mouvement
    dirigé suivant $\vu{x}$ (l'Est).
    L'intégration de $\vb{a}_{B/\fref{L}} \dotproduct \vu{x}$
    donne~:
    \begin{equation*}
        x(t)
        = \frac{g t^3 \Omega \cos(\lambda)}{3}
    \end{equation*}
    puis en introduisant l'altitude de départ $h$
    calculée pour une chute qui a duré $\Delta{t}$,
    $h=\flatfrac{1}{2}g\Delta{t}^2$~:
    \begin{equation*}
        x_h
        = \qty(\frac{8 h^3}{9g})^{1/2} \Omega \cos(\lambda)
    \end{equation*}
    on peut calculer la distance horizontale
    parcourue par la bille pendant sa chute.

    \propername{Flammarion} est le premier à avoir réussi l'expérience,
    depuis la coupole du Panthéon ($h = \SI{68}{\meter}$)
    il a mesuré
    $x = \SI{7.6}{\milli\meter}$
    pour une valeur attendue de
    $x=\SI{8.1}{\milli\meter}$.

    \subsection{Quelque autres phénomènes}

    \begin{itemize}
        \item Les cyclones s'enroulent tous dans le même sens
            expliqué par les effets de \propername{Coriolis},
            en considérant les vents
            qui convergent vers une zone de dépression.
        \item Les tireurs d'élite corrigent la déviation vers l'Est
            due a \propername{Coriolis}
            quand ils tirent sur de longue distances
            (\SI{3.5}{\kilo\meter}).
        \item Les rails de chemin de fer
            sont plus abimé côté Est
            (quelque soit la direction des trains qui les empruntes).
    \end{itemize}

  \section{Marées océaniques \cite{perezmecanique}}

    \begin{draft}
        Dire qu'on pourait en parler
        mais ne pas traiter cette partie.
    \end{draft}

\plusloin{
    Dans cette leçon nous considérons le référentiel $\fref{T}$ géocentrique
    comme Galiléen
    mais bien sûr,
    il ne l'est pas vraiment.

    \begin{todo}
        Trouver quoi dire de plus sur cette ouverture dangereuse.
    \end{todo}
}
