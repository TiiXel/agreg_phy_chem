\chapter{Interférométrie à division d'amplitude}

Dans la leçon précédente,
nous avons étudié
les conditions d'obtention d'interférences.
En particulier,
nous avons vu que
les problèmes
de cohérence saptiale
posaient problème vis-à-vis
de la luminosité des franges,
dans les expérience
de division du front d'onde
(fentes de \propername{Young} entre autres).

Dans cette leçon
nous présentrons une solution
alternative
à la division du front d'onde
dite
division d'amplitude~:
l'idée est
de faire interférer chaque rayon
avec lui même.

\begin{draft}
    Il sera nécessaire
    d'avoir en tête
    des dispositifs
    à division du front d'onde
    et
    à division d'amplitude
    moins scolaires
    que
    les fentes de \propername{Young}
    ou
    l'interféromètre de \propername{Michelson}
    et le \propername{Fabry-Perrot}.
\end{draft}

\section{Théorème de non brouillage (théorème de localisation)}

Le théorème de localisation
va permettre de
mathématiquement
prévoir les zones de l'espace
où la figure d'interférences
ne sera pas brouillée,
pour une source d'extension donnée.

    \subsection{Système optique général \cite{thibiergelocalisation}}

    On considère
    un système optique
    tout à fait général.
    Une source étendue
    est utilisée
    pour l'éclairer,
    de cette source
    on isole deux points
    $S$ et $S'$.

    \begin{figure}[H]
        \centering
        \begin{tikzpicture}[use optics]
            % S, S' et M
            \node[label=north:{$S$}] (S) at (0, 0) {};
            \draw[fill] (S) circle[radius=.05cm];
            \draw[color=red] (S.center) circle[radius=.3];

            \node[label=south:{$S'$}] at (-0.1, -0.2) (S') {};
            \draw[fill] (S') circle[radius=.05cm];
            \draw[color=blue] (S'.center) circle[radius=.3];

            \node[label=east:{$M$}] at (12, 1) (M) {};
            \draw[fill] (M) circle[radius=.05cm];

            % Système optique (paraboles, translatées et tournées)
            \node[thick optics element, object height=3cm, object aspect ratio=.5] at (5, 0) (sys) {};

            % Rayons jusqu'à sys
            \node[label=north east:{$I_1$}] (I1) at (sys.west |- 0, 0.8) {};
            \node[label=south east:{$I_1'$}] (I1') at (sys.west |- 0, 0.7) {};
            \node[label=north east:{$I_2'$}] (I2') at (sys.west |- 0, -1.0) {};
            \node[label=south east:{$I_2$}] (I2) at (sys.west |- 0, -1.1) {};

            \draw[color=red] (S.center) -- (I1.center);
            \draw[color=red] (S.center) -- (I2.center);
            \draw[color=blue] (S'.center) -- (I1'.center);
            \draw[color=blue] (S'.center) -- (I2'.center);

            % Rayons après sys
            \node[label=north west:{$J_1$}] (J1) at (sys.east |- 0, 0.8) {};
            \node[label=south west:{$J_1'$}] (J1') at (sys.east |- 0, 0.7) {};
            \node[label=north west:{$J_2'$}] (J2') at (sys.east |- 0, -1.0) {};
            \node[label=south west:{$J_2$}] (J2) at (sys.east |- 0, -1.1) {};

            \draw[color=red] (J1.center) -- (M.center);
            \draw[color=red] (J2.center) -- (M.center);
            \draw[color=blue] (J1'.center) -- (M.center);
            \draw[color=blue] (J2'.center) -- (M.center);
        \end{tikzpicture}
    \end{figure}

    On observe
    les interférences
    au point $M$
    de l'autre côté du système.

    Pour les rayons
    issus de $S$ (resp $S'$),
    la différence de marche
    au point $M$
    est $\delta$ (resp $\delta'$)~:
    \begin{align*}
        \delta
        = (SM)_1 - (SM)_2
        &= (SI_1J_1M) - (SI_2J_2M)
        \\
        \delta'
        = (S'M)_1 - (S'M)_2
        &= (S'I_1'J_1'M) - (S'I_2'J_2'M)
    \end{align*}


    L'abscende de brouillage
    au point $M$
    se traduit par~:
    $\delta' - \delta = 0$.
    Calculons~:
    \begin{equation*}
        \delta' - \delta
        = \underbrace{\qty[(S'I_1'J_1'M) - (SI_1J_1M)]}_{\delta_1}
        - \underbrace{\qty[(S'I_2'J_2'M) - (SI_2J_2M)]}_{\delta_2}
    \end{equation*}

    Nous allons nous contenter
    d'une égalité au premier ordre,
    en acceptant
    une légère différence~:
    celà revient à dire
    qu'un léger brouillage
    est toléré.
    On calcul alors
    $\delta_1$
    et par similarité,
    nous déterminerons $\delta_2$~:
    \begin{align*}
        \delta_1
        &= (S'I_1') - (SI_1) + \overbrace{(I_1'J_1'M) - (I_1J_1M)}^{\delta_d}
        \\
        &= n[\va{u_1'}\dotproduct\va{S'I_1'} - \va{u_1}\dotproduct\va{SI_1}]
            + \delta_d
        \\
        &\approx n\qty[\dd(\va{u_1}\dotproduct\va{SI_1})] + \delta_d
        \\
        &= n\qty[\va{u_1}\dotproduct\dd\va{SI_1}
                + \dd\va{u_1}\dotproduct\va{SI_1}]
            + \delta_d
    \end{align*}
    $\delta_d$
    correspond à la différence de marche
    dans la partie droite du schéma,
    après l'arrivée
    dans l'interféromètre.
    En outre,
    on remarque que~:
    $\va{u_1} \parallel \va{SI_1}$
    par construction,
    et que~:
    $\va{u_1} \perp \dd\va{u_1}$
    par définition,
    d'où~:
    \begin{align*}
        \delta_1
        &= n\qty[\va{u_1}\dotproduct\dd\va{SI_1}] + \delta_d
    \end{align*}
    on doit exprimer~:
    \begin{align*}
        \dd\va{SI_1}
        &= \va{S'I_1'} - \va{SI_1}
        \\
        &= \va{S'O} - \va{SO} + \va{OI_1'} - \va{OI_1}
        \\
        &= \va{S'S} + \va{OI_1'} - \va{OI_1}
    \end{align*}
    alors,
    en reprenant l'expression pour $\delta_d$,
    on peut exprimer $\delta_1$ comme~:
    \begin{align*}
        \delta_1
        &= n\va{u_1}\dotproduct\va{S'S}
        + n\va{u_1}\dotproduct\va{OI_1'}
        - n\va{u_1}\dotproduct\va{OI_1}
        + \delta_d
        \\
        &= n\va{u_1}\dotproduct\va{S'S}
        + \qty[n\va{u_1}\dotproduct\va{OI_1'} + (I_1'J_1'M)]
        - \qty[n\va{u_1}\dotproduct\va{OI_1} + (I_1J_1M)]
        \\
        &= n\va{u_1}\dotproduct\va{S'S}
        + (OI_1'J_1'M)
        - (OI_1J_1M)
    \end{align*}

    Les deux derniers termes,
    correspondent
    à deux chemins optiques
    de rayons lumineux
    partant d'un point $O$
    et arrivant au point $M$.
    Par le principe de \propername{Fermat}
    on déduit l'égalité de ces termes.

    \begin{draft}
        La substitution
        $n\va{u_1}\dotproduct\va{OI_1} = (OI_1)$
        n'est valable
        que dans le cadre
        du développement limité~:
        les quatre vecteurs
        (primés et non primés)
        doivent être parallèles
        deux à deux,
        mas les rayons
        doivent passer par le point $O$.)
    \end{draft}

    Finalement,
    \begin{equation*}
        \delta_1
        = n\va{u_1}\dotproduct\va{S'S}
        \qq{on déduit}
        \delta_2
        = n\va{u_2}\dotproduct\va{S'S}
    \end{equation*}
    d'où~:
    \begin{equation*}
        \delta' - \delta
        \approx n \qty(\va{u_1} - \va{u_2}) \dotproduct \va{S'S}
    \end{equation*}
    Nous disions que,
    pour qu'il n'y ai pas brouillage
    il fallait
    $\delta' - \delta \approx 0$
    ce qui,
    d'après notre calcul
    revient à~:
    \begin{equation*}
        \qty(\va{u_1} - \va{u_2}) \dotproduct \va{S'S}
        \approx 0
    \end{equation*}

    Nous avons deux options,
    selon la direction des rayons incidents~:
    \begin{itemize}
        \item $\va{u_1} \neq \va{u_2}$,
            dans ce cas,
            $\delta'-\delta= 0 \implies \va{S'S} \perp (\va{u_1} - \va{u_2})$~:
            pour qu'il n'y ait pas de brouillage,
            la source
            doit être étendue
            uniquement dans un plan perpendiculaire
            au plan de propagation des rayons
            se dirigeant vers le système optique.
        \item $\va{u_1} = \va{u_2}$,
            dans ce cas,
            $\delta'-\delta= 0 \forall \va{S'S}$~:
            l'absence de brouillage
            n'impose aucune contrainte sur la source,
            mais impose que l'interféromètre
            ne fasse se rejoindre en $M$
            que des rayons émergeants
            issus de rayons incidents
            parallèles entre eux~:
            il ne peut pas s'agir
            d'un interféromètre à division du front d'onde.
    \end{itemize}

    \subsection{Deux mots sur les fentes de \propername{Young}}

    \begin{figure}[H]
        \centering
        \begin{tikzpicture}[use optics]
            % S
            \node[label=west:{$S$,$S'$}] (S) at ( 0, 0) {};
            \draw[fill] (S) circle[radius=.05cm];
            \draw[color=red] (S.center) circle[radius=.3];

            \node[label=west:{$T$,$T'$}] (T) at ( 0,-.5) {};
            \draw[fill] (T) circle[radius=.05cm];
            \draw[color=green] (T.center) circle[radius=.3];

            % Système optique (paraboles, translatées et tournées)
            \node[double slit, object height=6cm, slit separation=4cm] at (5, 0) (sys) {};

            % Écran
            \node[screen, object height=6cm] at (10, 0) (Ea) {};
            \node[screen, object height=6cm] at (12, 0) (Eb) {};

            % M
            \node (Maa) at (10, 1) {};

            \def\toVerticalProjection#1#2#3{
                let \p{1} = #1,
                \p{2} = #2,
                \p{3} = #3
                in -- (\x{3},
                    {\y{1} + (\y{2}-\y{1})/(\x{2}-\x{1}) * (\x{3}-\x{1})}
                )}

            % Rayons
            \draw[color=red] (S.center) -- (sys.slit 1 center);
            \draw[color=red] (sys.slit 1 center) -- (Maa.center)
                \toVerticalProjection{(sys.slit 1 center)}{(Maa.center)}{(Eb)};

            \draw[color=blue] (S.center) -- (sys.slit 2 center);
            \draw[color=blue] (sys.slit 2 center) -- (Maa.center);

            \draw[color=blue] (sys.slit 2 center)
                \toVerticalProjection{(sys.slit 1 center)}{(Maa.center)}{(Eb)};

            \draw[color=green] (T.center) -- (sys.slit 1 center);
            \draw[color=green] (T.center) -- (sys.slit 2 center);
        \end{tikzpicture}
    \end{figure}

    Pour une source
    étendue dans l'axe perpendiculaire au schéma ($SS'$ ou $TT'$),
    en n'importe quel point de l'espace,
    les rayons arrivants
    sont issus de rayons incidents
    qui vérifient
    $\va{u_1} - \va{u_2} \perp \va{SS'}$.
    Il n'y a du brouillage
    nulle part~:
    les interférences
    ne sont pas localisées
    (dans le cadre
    de la validité
    du développement limité).

    Pour une source
    étendue dans le plan du schéma ($ST$),
    on ne vérifie pas
    les conditions de non brouillage
    puisque $\va{ST} \perp (\va{u_1} - \va{u_2})$.

    \subsection{Interférences par division d'amplitude}

    L'autre condition
    que l'on peut vérifier
    ($\va{u_1} = \va{u_2}$)
    correspond à
    ce qui était annoncé
    dans l'introduction,
    c'est à dire
    l'intérférences d'un rayon
    «~avec lui même~»,
    comme illustré sur ce schéma~:

    \begin{figure}[H]
        \centering
        \begin{tikzpicture}[use optics]
            % S
            \node[label=west:{$S$}] (S) at ( 0, -1.0) {};
            \draw[fill] (S) circle[radius=.05cm];
            \draw[color=red] (S.center) circle[radius=.3];

            \node[label=west:{$S'$}] (S') at (-.3, -2.0) {};
            \draw[fill] (S') circle[radius=.05cm];
            \draw[color=blue] (S'.center) circle[radius=.3];

            % Système optique
            \node[mirror, object height=4cm] at (3, 0) (M1) {};
            \node[mirror, object height=4cm] at (5, 0) (M2) {};

            % Écran
            \node at (1, 0) (Ea) {};

            \def\mirrorRay#1#2#3#4{
                % 1 = source, 2 = direction, 3 = plan mirroir, 4 = plan arrivée
                let \p{1} = #1, \p{2} = #2, \p{3} = #3, \p{4} = #4
                in
                (\x{1}, \y{1})
                -- (\x{3}, {\y{1} + (\y{2}-\y{1})/(\x{2}-\x{1})*(\x{3}-\x{1})})
                -- (\x{4}, {2*(\y{1} + (\y{2}-\y{1})/(\x{2}-\x{1})*(\x{3}-\x{1}))-\y{1}})
            }

            % Rayons
            \node at ($ (S) + (1, .3) $) (Su1) {};
            \draw[color=red] \mirrorRay{(S.center)}{(Su1.center)}{(M1.center)}{(Ea.center)};
            \draw[color=red] \mirrorRay{(S.center)}{(Su1.center)}{(M2.center)}{(Ea.center)};

            \node at ($ (S') + (1, .3) $) (S'u1) {};
            \draw[color=blue] \mirrorRay{(S'.center)}{(S'u1.center)}{(M1.center)}{(Ea.center)};
            \draw[color=blue] \mirrorRay{(S'.center)}{(S'u1.center)}{(M2.center)}{(Ea.center)};
        \end{tikzpicture}
    \end{figure}
    Le système optique,
    que l'on appelle
    \emph{lame d'air},
    est constitué
    de deux mirroirs semi-réfléchissants.
    On constate que
    les rayons incidents
    parallèles
    aux vecteur directeur
    $\va{u_1}$
    sont tous renvoyés
    dans la même direction
    et cela,
    peu importe
    leur point source.
    Ce sont aussi
    les seuls
    à repartir
    dans cette direction~:
    ils interfèrent
    à l'infini
    et sans brouillage,
    sans condition
    sur l'extension spatiale de la source.
    On pourra les faire interférer
    sur un écran
    par projection
    avec une lentille convergente.

    Ce type de système
    est dit
    \emph{à division d'amplitude}
    car chaque rayon
    est divisé
    avant d'être conduit
    à interférer avec lui même.

    On rapelle que
    l'extension spatiale de la source
    permet l'obtention
    de figures d'interférences
    plus lumineuses.
    En contre-partie,
    nous sommes soumis
    à la localisation des interférences~:
    elles ne sont observables
    que dans
    des régions finies de l'espace.

    Le système optique
    présenté ici,
    et appellé
    \emph{lame d'air}
    peut être généralisé et constitué
    de deux interfaces
    séparants trois
    milieux de propagation de la lumière
    (plutôt que trois zones d'air).
    On parlera
    dans le cas général
    de \emph{lame mince}~:
    les interférences
    sur lames minces
    sont celles qui
    donnent leur couleur
    aux ailes de papillons,
    et
    aux taches d'huile ou de savon.

    Le système optique
    présenté ici,
    n'a pas été complètement étudié,
    en effet
    les mirroirs semi-réfléchissants
    donnent lieux
    à des réfléxions multiples
    qui risquent de
    compliquer l'étude.
    On pourrait aussi
    essayer d'observer des interférences
    dans une situation
    où les deux moirroirs
    ne sont pas parallèles.
    Nous allons étudier ceci,
    dans le cas plus simple
    d'interférences à deux ondes,
    grace à un système
    analogue à celui là,
    mais qui évite
    les réfléxions multiples.

\section{Interférences à deux ondes
    avec l'interféromètre de \propername{Michelson}}

    L'interféromètre de \propername{Michelson}
    est une solution technique
    similaire à celle déjà présentée
    qui permet d'aisément
    faire apparaître
    une figure d'interférences
    à deux ondes
    issue d'une division d'amplitude
    des rayosn incidents.

    \subsection{Constitution}

    \begin{media}
        Pour les schémas
        et les calculs~:
        \url{http://www.f-legrand.fr/scidoc/docmml/sciphys/optique/michelson2/michelson2.html}
    \end{media}

    \begin{draft}
        On se place
        à $n=1$.

        Construire
        les miroirs équivalents~:
        tous les rayons incidents
        (peu importe l'angle d'incidence)
        produisent deux rayons émergents parallèles
        donc
        la figure d'interférence
        est localisée à l'infini.
    \end{draft}


    \subsection{Configuration en lame d'air}

    \begin{todo}
        Rédiger cette section.
    \end{todo}

    \subsubsection{Figure d'interférences}

    \paragraph{Différence de marche}
    La différence de marche $\delta(\theta)$
    qui existe entre
    deux rayons qui se rencontrent sur l'écran
    car ils étaient parallèles
    en sortie de l'interféromètre
    (cela car
    ils sont issus du même rayon incident
    lui même incident
    avec un angle $\theta$),
    est~:
    \begin{align*}
        \delta(\theta)
        &= (SIM) - (SIJKM)
        \\
        &= (SI) - (SIJK)
        \qq{(\propername{Malus} et ret. inv.)}
        \\
        &= (IJK)
        \\
        &= IJ + JK
    \end{align*}
    Et
    par trigonométrie,
    on détermine
    les relations~:
    \begin{gather*}
        \cos(KJI)
        = \cos(2\theta)
        = \frac{KJ}{IJ}
        \implies
        JK = IJ \cos(2 \theta)
        \\
        \cos(\theta)
        = \frac{e}{IJ}
        \implies
        IJ \cos(\theta) = e
    \end{gather*}
    donc
    finalement~:
    \begin{equation*}
        \delta(\theta)
        = IJ (1 + \cos(2\theta))
        = IJ (2 \cos[2](\theta))
        = 2 e \cos\theta
    \end{equation*}

    La figure d'interférences
    va être à géométrie circulaire~:
    on aura
    des anneaux brillants
    et des anneaux sombres.
    Comme la différence de marche
    ne dépend pas de la position de la source
    mais seulement
    de l'angle d'incidence du rayons incident,
    les interférences
    ne sont pas brouillées
    même pour une source étendue~:
    c'est bien
    ce que l'on cherchait à obtenir.

    \paragraph{Interférences au centre}
    Les interférences
    formées au centre de l'écran
    ($\delta(\theta = 0) = 2 e$)
    sont constructives
    ou destructives
    si~:
    \begin{gather*}
        2 e = k \lambda, k \in \mathbb{Z}
        \\
        \qq{ou}
        2 e = (k + \frac{1}{2}) \lambda
    \end{gather*}
    Mais dans le cas général
    pour une épaisseur $e$ quelconque,
    les interférence
    formées au centre de l'écran
    sont quelconques
    et on note~:
    \begin{equation*}
        2 e
        = (k + \epsilon) \lambda
    \end{equation*}

    \paragraph{Anneaux}
    Le premier anneau lumineux
    se forme à un angle $\theta_1$
    pour lequel
    la différence de marche
    est~:
    $2 e \cos\theta_1 < 2 e$.
    Comme
    la différence de marche
    associée à l'angle $\theta_1$
    est inférieure
    à la différence de marche
    associée au centre de l'écran,
    l'ordre d'interférences
    va y être
    le premier entier
    inférieur à $k + \epsilon$,
    c'est à dire~: $k$.
    Donc~:
    \begin{equation*}
        \delta_1
        = 2 e \cos\theta_1
        = k \lambda
    \end{equation*}
    En continuant ce raisonnement
    on trouve que
    le $n$ième anneau lumineux
    se forme à l'angle
    $\theta_n$
    tel que
    \begin{equation*}
        \delta_n
        = 2 e \cos\theta_n
        = (k + 1 - n) \lambda
    \end{equation*}

    On peut aussi calculer
    le rayon des anneaux lumineux
    formés sur l'écran
    à travers la lentille mince convergente $L$
    de distance focale $f'$~:
    \begin{equation*}
        r_n
        = f' \sin(\theta_n)
        \approx f' \theta_n
    \end{equation*}

    La relation
    $2 e \cos\theta_n = (k + 1 - n) \lambda$
    permet d'exprimer l'angle~:
    \begin{equation*}
        1 - \frac{\theta_n^2}{2}
        \approx \cos\theta_n
        = \frac{(k+1-n)\lambda}{2e}
        \implies
        \theta_n
        \approx \sqrt{2 - \frac{(k+1-n)\lambda}{e}}
    \end{equation*}
    d'où~:
    \begin{align*}
        r_n^2
        &= f'^2 \qty(2 - \frac{(k+1-n)\lambda}{e})
        \\
        &= f'^2 \qty(2 - \frac{k+1}{e} \lambda) + \frac{f' \lambda}{e} n
    \end{align*}

    On peut tracer une droite
    $r_n = a_e n + b_e$
    et une autre droite
    $r_n' = a_{e'} n + b_{e'}$
    à épaisseur différente $e'$.
    Cela permettra
    par un calcul habile
    de déterminer $\lambda$ ou $e$.
    \begin{draft}
        On aura
        $a_e = \frac{f'^2\lambda}{e}$
        et la même relation
        pour $e'$,
        d'où~:
        \begin{equation*}
            a_e - a_{e'}
            = \frac{f'^2\lambda}{e} - \frac{f'^2\lambda}{e'}
            = f'^2 \lambda \qty(\frac{e' - e}{e e'})
        \end{equation*}
        et par ailleurs~:
        \begin{equation*}
            a_e \cdot a_{e'}
            = \frac{(f'^2\lambda)^2}{ee'}
        \end{equation*}

        Donc~:
        \begin{equation*}
            a_e - a_{e'}
            = \frac{a_e a_{e'}}{f'^2 \lambda} (e' - e)
        \end{equation*}
        puis~:
        \begin{equation*}
            \lambda
            = \frac{a_e a_{e'} (e' - e)}{f'^2 (a_e - a_{e'})}
            \qq{ainsi que}
            e' - e
            = \frac{f'^2 (a_e - a_{e'}) \lambda}{a_e a_{e'}}
        \end{equation*}
        d'où l'utilisation
        possible des anneaux
        en spectroscopie
        ou
        pour mesurer
        des variations de longueurs.
    \end{draft}

    \begin{draft}
        Une autre utilisation possible
        pour déterminer cette fois
        l'écart entre deux raies
        de longueurs d'ondes très proches
        est d'exploiter
        le brouillage qui apparaît
        lors d'anticoïncidendes.
    \end{draft}

    \subsection{Configuration en coin d'air}

    \subsubsection{Différence de marche}

    \subsubsection{Figure d'interférences}

    \subsection{Applications de l'interféromètre de \propername{Michelson}}

    \subsubsection{Application d'origine}

    \subsubsection{Spectroscopie par transformée de \propername{Fourier}}

    \subsubsection{Mesure d'épaisseur}

    \subsubsection{Tomographie optique cohérente}

    \subsubsection{Détection des ondes gravitationnelles}

\section{Interférences à $N$ ondes
    avec l'interféromètre de \propername{Fabry-Perrot}}
