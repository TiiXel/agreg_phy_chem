\chapter{Microscopies optiques}

\begin{manip}
    Pendant toute la leçon,
    avoir un microscope de labo
    posé sur la table.
    Il servira
    à montrer les différents éléments
    et à avoir une idée de valeurs
    pour les ordres de grandeurs.
\end{manip}

\begin{manip}
    Préparer
    la modélisation du microscope
    sur un banc optique,
    avec un œil
    facile à enlever.
\end{manip}

\begin{media}
    Les illustrations
    de cette leçon
    ont été regroupées
    dans le document~:\\
    \software{./documents/LP32.pdf}
\end{media}


\section{Étude géométrique}

    \subsection{Limites de l'œil humain}

    On modélise l'œil emmétrope
    comme une lentille mince
    de distance focale variable
    (cornée et cristallin),
    un diaphragme
    (iris),
    et un écran
    (rétine).
    La distance
    entre la lentille
    et l'écran
    est fixe.

    L'œil émmétrope
    peut voir nettement
    des objets situés
    entre le ponctum remotum (à l'infini)
    et le ponctum proximum (à $d_{pp} \approx \SI{25}{\centi\metre}$),
    grâce à la factulté d'accommodation
    du cristallin
    (changement de distance focale).

    Le détail le plus petit
    observable au ponctum proximum
    a une taille
    de l'ordre du dixième de millimètre.
    La grandeur plus appropriée
    pour exprimer
    la taille des objets que l'on observe
    est leur
    \emph{diamètre angulaire},
    on peut distinguer des détails
    dès lors que leur diamètre angulaire
    est supérieur à \ang{0;1;}.

    \begin{draft}
        Cette limite
        correspond à la fois
        à la taille caractéristique
        des battonnets sur la rétine
        et la taille
        de la tâche d'\propername{Airy} formée sur la rétine
        lors de la diffraction par l'iris.
        La nature fait bien les choses.
    \end{draft}

    Pour ordre de grandeur~:
    \begin{table}[H]
        \centering
        \begin{tabular}{crrr}
            \toprule
            Objet
            & Diamètre
            & Distance
            & Diamètre angulaire
            \\
            \midrule
            Orange
            & \SI{8}{\centi\metre}
            & \SI{25}{\centi\metre}
            & \ang{18;1;}
            \\
            Orange
            & \SI{8}{\centi\metre}
            & \SI{8.6}{\metre}
            & \ang{;32;}
            \\
            Soleil
            & \SI{1.4e6}{\kilo\metre}
            & \SI{150e6}{\kilo\metre}
            & \ang{;32;}
            \\
            Lune
            & \SI{3.4e3}{\kilo\metre}
            & \SI{380e3}{\kilo\metre}
            & \ang{;31;}
            \\
            Cheveu
            & \SI{0.09}{\milli\metre}
            & \SI{25}{\centi\metre}
            & \ang{;1.2;}
            \\
            Cellule
            & \SI{20}{\micro\metre}
            & \SI{25}{\centi\metre}
            & \ang{;0.3;}
            \\
            Gravure IBM
            & \SI{10}{\nano\metre}
            & \SI{25}{\centi\metre}
            & \ang{;0.00014;}
            \\
            \bottomrule
        \end{tabular}
    \end{table}

    Avec une loupe,
    on peut améliorer les choses
    d'un facteur 10 environ.
    Un microscope
    offre de meilleurs performances,
    nous allons le voir.

    \subsection{Modélisation du microscope \cite{agrega2015}}

    Sur le microscope
    posé sur la paillasse,
    on observe la présence
    de différents éléments,
    certains sont réglable
    d'autres non.
    Le schéma \cite[Figure 1]{agrega2015}
    présente ces élements.
    L'oculaire
    et l'objectif
    sont les éléments optiques
    qui nous intéressent le plus ici,
    nous allons les modéliser
    chacun par une lentille mince convergente.

    \begin{draft}
        En réalité,
        ils sont constitués
        de plusieurs lentilles
        qui servent à corriger
        les différentes abbérations
        introduites.
    \end{draft}

    L'objectif $L_1$
    est situé près de l'objet $AB$ étudié
    et en forme une image $A_1B_1$.

    On désire
    pour pouvoir utiliser le microscope
    sans que l'œil ne doive accommoder.
    En conséquence,
    l'oculaire $L_2$
    qui est situé près de l'œil
    et forme une image de $A_1B_1$,
    doit être disposé
    disposé de sorte que
    son point focal objet $F_2$ et le point $A_1$
    soient confondus,
    pour que l'image de l'objet
    se trouve à l'infini.

    La distance $\Delta = F'_1F_2 = \SI{16}{\centi\metre}$
    est fixée par la construction du microscope
    (c'est la longueur du tube).
    Les objectifs
    et les oculaires
    peuvent être changés
    mais sont fixés au bâti de l'appareil
    par leurs extrémités $F'_1$ et $F_2$.

    Pour que l'image $A_1$ de $A$
    se trouve au point $F_2$
    (observation sans accommoder)
    l'objet doit se trouver
    dans une position particulière~:
    c'est à celà
    que servent la vis de réglage
    de la plateforme
    que l'on ajuste
    pendant le processus de mise au point.
    \begin{manip}
        Le montrer
        sur le banc optique.
    \end{manip}
    \begin{draft}
        Pour que
        $F'_1A_1 = \Delta$
        il faut~:
        $F_1A = \flatfrac{{f'_1}^2}{\Delta}$.
    \end{draft}

    \subsection{Caractéristiques du microscope \cite{agrega2015}}

    \subsubsection{Grossissement commercial de l'oculaire~: $G_{oc}$}

    Le grossissement commercial de l'oculaire
    correspond au rapport $\flatfrac{\alpha'}{\alpha}$ des angles
    sous lequel apparaît un objet $A_1B_1$ lorsqu'observé~:
    \begin{itemize}
        \item à travers l'oculaire,
            placé au foyer objet ($\alpha'$)
        \item à l'œil nu,
            placé au ponctum proximum ($\alpha$)
    \end{itemize}

    On peut exprimer~:
    \begin{gather*}
        \alpha'
        = \frac{A_1B_1}{f'_2}
        \qq{et}
        \alpha
        = \frac{A_1B_1}{d_{pp}}
        \implies
        G_{oc}
        = \frac{\alpha'}{\alpha}
        = \frac{d_{pp}}{f'_2}
    \end{gather*}

    Pour un oculaire
    de grandissement commercial $\times 10$,
    la distance focale $f'_2$
    de la lentille mince correspondante
    est $f'_2 = \SI{2.5}{\centi\metre}$.

    \subsubsection{Grandissement de l'objectif~: $\gamma_{obj}$}

    Le grandissement de l'objectif
    correspond au rapport $\flatfrac{A_1B_1}{AB}$ des tailles~:
    \begin{itemize}
        \item de l'image de l'objet,
            formée par l'objectif,
            à la distance $\Delta$ de $F'_1$ ($A_1B_1$)
        \item de l'objet ($AB$)
    \end{itemize}

    On peut exprimer~:
    \begin{equation*}
        \gamma_{obj}
        = \frac{\Delta}{f'_1}
    \end{equation*}

    Pour un objectif de grandissement $\times 20$,
    la distance focale $f'_1$
    de la lentille mince correspondate
    est $f'_1 = \SI{8}{\milli\metre}$.

    \subsubsection{Grossissement commercial du microscope~: $G$}

    Le grossissement commercial du microscope
    correspond au rapport $\flatfrac{\alpha'}{\beta}$ des angles
    sous lequel apparaît l'objet lorsqu'observé~:
    \begin{itemize}
        \item à travers le microscpe ($\alpha'$)
        \item à l'œil nu,
            placé au ponctum proximum ($\beta$)
    \end{itemize}

    On peut exprimer~:
    \begin{equation*}
        G
        = \frac{\alpha'}{\beta}
        = \gamma_{obj} \cdot G_{oc}
    \end{equation*}

    Avec les valeurs numériques précédentes
    on trouve
    $G = 200$.

    Le grossissement du microscope
    mesure aussi
    le rapport entre
    la dimension de l'image sur la rétine
    lors de l'observation à travers le microscope
    et
    lors de l'observation à l'œil nu.

    \subsubsection{Puissance du microscope~: $P$}

    La puissance du microscope
    est le rapport entre
    l'angle sous lequel on voit l'objet à travers le microscope
    et la taille de l'objet~:
    \begin{equation*}
        P
        = \frac{\alpha'}{AB}
        = \frac{\Delta}{f'_1f'_2}
    \end{equation*}

    \subsubsection{Profondeur de champ}

    On a vu
    que l'objet $AB$
    pouvait être placé
    de sorte que son image
    à traver le microscope
    soit renvoyée à l'infini.
    L'objet étant placé de cette manière
    l'utilisation du microscope
    peut être faite
    sans que l'œil ne doive accommoder.
    Cependant,
    dans une situation où
    l'échantillon étudié
    n'est pas plan
    alors il n'est pas possible
    de placer tous les points de sa surface
    à la bonne position.
    Dans ce cas,
    certains points de l'objet
    verront leur image
    être envoyée à distance finie de l'œil.
    Si l'image
    est envoyée à plus de $d_{pp} = \SI{25}{\centi\metre}$
    de l'observateur,
    alors il pourra observer
    toujours sans accommoder.

    On appelle profondeur de champ
    la distance sur l'axe optique
    entre les deux positions extrêmes de $A$,
    compatibles avec une vision sans accommodation.

    \subsubsection{Diaphragmes}

    Le modèle des lentilles minces
    fait l'hypothèse de lentilles infinies
    mais en réalité
    les éléments optiques sont de diamètres finis.
    Pour tenir compte
    de cet effet
    dans notre modèle
    on peut ajouter
    deux diaphragmes.

    \paragraph{Le diaphragme d'ouverture}
    modélise le diamètre fini
    du tube de l'objectif.
    Plus ce diaphragme est grand,
    plus l'image est lumineuse sur l'écran.

    \paragraph{Le diaphragme de champ}
    modélise le diamètre fini
    du tube de l'oculaire.
    Plus ce diaphragme est grand
    plus le champ visuel
    est large.

    \begin{draft}
        En pratique
        on préfère toujours
        un champ visuel
        le plus large possible,
        le diaphragme de champ
        permet toutefois
        de ne sélectionner
        que la partie de l'image
        qui n'a pas subie trop d'abbérations
        et
        qui n'est pas hors de la profondeur de champ,
        pour que l'observation
        reste agréable.
        Dans tous les cas,
        le champ visuel est limité
        par l'œil.
    \end{draft}

    \begin{manip}
        Illuster l'effet des diaphragmes
        sur le banc optique.
    \end{manip}

\section{Étude ondulatoire}

    La tâche d'\propername{Airy}
    est limitante.

\section{Améliorations}

    \subsection{Microscopie confocale}

    \subsection{PALM}
