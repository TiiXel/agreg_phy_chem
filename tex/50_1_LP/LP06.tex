\chapter{Cinématique relativiste}

  \section{Origines de la relativité restreinte}

    \subsection{Référentiel galiléen, principe de relativité}

    Dès 1687,
    le principe fondamental de la dynamique
    énnoncé par \propername{Newton}
    permet de distinguer deux types de référentiels.
    Dans les référentiels galiléens
    le principe s'exprime sous sa forme la plus simple,
    alors que dans les autres
    il nécessite des termes correctifs.
    Son expression mathématique,
    couplée aux transformations de \propername{Galilée},
    permet d'affirmer que
    si un référentiel est galiléen,
    le principe fondamental de la dynamique
    s'appplique dans
    tous les référentiels
    en translation rectiligne uniforme
    par rapport au premier.
    Autrement dit,
    les lois de la mécanique
    sont invariantes
    entre deux référentiels galiléens.

    Cette dernière affirmation
    constitue le \emph{principe de relativité}.
    Un autre principe,
    ou plutôt une hypothèse,
    si naturelle qu'on ne la précise jamais en mécanique,
    est la suivante~:
    la durée perçue
    entre deux évènement,
    par deux observateurs
    dans deux référentiels différents,
    est la même.

    \subsection{La question de la propagation de la lumière}

    La propagation de la lumière
    à travers de nombreux millieux
    et nottament à travers le millieux interstéllaire,
    posait un problème conceptuel
    aux physicien jusqu'au \century{17}.
    Pour harmoniser ce problème
    avec la propagation des ondes mécaniques
    l'idée d'un milieu de propagation
    appelé \emph{ether luminifère}
    fut avancée.
    Ce milieu doit répondre
    à de nombreuses propriétés
    que nous n'étudions pas ici
    hormis celle
    de son mouvement par rapport à la matière~:
    l'éther est-il entrainé ou non
    par la matière autour de laquelle il se trouve~?

    Considérons
    une lampe
    (un haut-parleur)
    au milieu d'un wagon de train,
    un observateur $O_t$ dans le train,
    et un observateur $O_g$ sur le quai de la gare.
    À l'instant $t = 0$
    la lampe
    (le haut-parleur)
    emet un signal vers l'avant et vers l'arrière,
    et le train démarre.
    Peu de temps après,
    ce signal arrive
    sur les murs du wagon.

    \subsubsection{Ether entrainé par le train}

    L'observateur $O_t$
    va constater que les deux signaux
    arrivent en même temps
    après une durée mesurable.
    Puisque la durée perçue
    entre deux évènement
    par deux observateurs
    dans deux référentiels différents
    est la même,
    l'observateur $O_g$
    doit lui aussi
    constater que les deux signaux
    arrivent en même temps.
    Mais comme dans son référentiel
    les murs du train se déplacent,
    le signal allant vers l'avant
    aura parcouru une distance plus longue
    que celui allant vers l'arrière.

    Dans le cas du signal sonore,
    nous expliquons ce phénomène simplement~:
    la vitesse du son n'est pas la même
    dans les deux référentiels dans les deux directions,
    elle est liée à la vitesse de l'air
    qui se déplace avec le wagon.

    Dans le cas du signal lumineux,
    nous pouvons avancer des hypothèses~:
    oit la vitesse de la lumière
    est elle aussi liée au train,
    soit elle est infinie.

    La première hypothèse
    ne correspond pas
    à l'observation dite
    d'\emph{abérration des étoiles}.
    \begin{media}
        \software{./documents/stellarAberrationVersusTheDraggedAether.gif}
    \end{media}

    La seconde hypothèse
    ne correspond pas aux expérience
    qui mettent en évidence
    une vitesse de la lumière finie
    (\propername{Rømer} la mesure en 1675).

    \subsubsection{Ether immobile par rapport à la gare}

    Une autre explication
    au phénomène lumineux
    est qu'en fait,
    les signaux lumineux n'arrivent pas en même temps
    sur les murs du train
    ni pour $O_t$ ni pour $O_g$.
    La vitesse de la lumière
    pourrait être liée à la gare.
    Pourquoi la gare~?
    Cette question restera sans réponses
    car nous allons généraliser directement,
    en disant plutôt
    qu'il existe un référentiel
    dans lequel la vitesse de la lumière
    est la même dans toutes les directions,
    c'est le référentiel de l'éther.

    Nous entrons en contradiction
    avec le principe de relativité
    qui ne distingue pas de référentiel galiléen particulier,
    mais tennons une hypothèse réfutable.
    Le Soleil et les autres étoiles
    sont manifestement immobiles entre eux,
    et l'abérration des étoiles indique
    que la Terre se déplace par rapport à l'éther.
    Il nous faut trouver une expérience
    qui permette de vérifier
    si la vitesse de la lumière
    mesurée sur Terre
    diffère entre
    la direction colinéaire à la vitesse de la Terre,
    et la direction qui y est orthogonale.

    \subsection{L'éxpérience de \propername{Michelson-Morley} \cite{feynmanm1}}

    \begin{media}
        \url{http://www.feynmanlectures.caltech.edu/img/FLP\_I/f15-02/f15-02\_tc\_big.svgz}
    \end{media}
    Si l'appareil est au repos dans l'éther,
    les temps mis par la lumière
    pour parcourir les chemins
    $(BCB)$ et $(BEB)$
    doivent être égaux.
    Mais si l'appareil est en mouvement dans l'éther
    à la vitesse $u$ vers la droite,
    les temps mis
    pour parcourir les chemins
    $(BC'B')$ et $(BE'E)$
    seront différents
    et nous observerons sur l'écran des interférences.
    (Les temps nécessaire à parcourir
    $(AB)$ et $(BD)$ ou $(BF)$
    sont identiques
    quelque soit l'état de mouvement.)

    Soit $t_{BE'}$
    le temps mis par la lumière
    pour aller de $B$ au miroir $E$.
    Pendant que la lumière parcourt ce trajet,
    l'interféromètre se déplace dans l'éther
    d'une distance $u t_{BE'}$
    de sorte que le mirroir $E$ se trouve en $E'$,
    la lumière parcourt donc la distance
    \begin{equation*}
        L + u t_{BE'}
        = c t_{BE'}
        \implies
        t_{BE'} = \frac{L}{c - u}
    \end{equation*}
    Pendant le chemin du retour $(E'B')$
    l'interféroèmtre avance encore
    de sorte que la lumière doit parcourir une distance
    \begin{equation*}
        L - u t_{E'B'}
        = c t_{E'B'}
        \implies
        t_{E'B'} = \frac{L}{c + u}
    \end{equation*}
    Finalement,
    \begin{equation*}
        t_{BE'B'} = \frac{\flatfrac{2L}{c}}{1 - \flatfrac{u^2}{c^2}}
    \end{equation*}

    Concernant le trajet de la lumière
    sur l'axe vertical du schéma,
    la question de pose
    de savoir si la lumière va «~rater~» ou non
    le centre du mirroir $C$.
    Nous savons que dans un référentiel galiléen
    la lumière ne le raterait pas,
    le principe de relativité
    nous permet donc de conclure
    que dans le référentiel de la Terre
    en mouvement rectiligne uniforme dans l'éther,
    la lumière ne rate pas non plus le centre du mirroir.
    La lumière va parcourir un trajet oblique sur le schéma.

    Soit alors $t_{BC'}$
    le temps mis par la lumière
    pour aller de $B$ au miroir $C$,
    comme avant,
    pendant que la lumière parcourt ce trajet
    le miroir $C$ avance jusqu'en $C'$
    (distance $u t_{BC'}$).
    Nous avons un triangle rectangle
    où le théorème de \propername{Pythoagore} s'applique~:
    \begin{equation*}
        (c t_{BC'})^2
        = L^2 + (u t_{BC'})^2
        \implies
        t_{BC'} = \frac{L}{\sqrt{c^2 - u^2}}
    \end{equation*}
    La distance $C'B'$ est la même,
    alors $t_{C'B'} = t_{BC'}$ et~:
    \begin{equation*}
        t_{BC'B'}
        = \frac{\flatfrac{2L}{c}}{\sqrt{1 - \flatfrac{u^2}{c^2}}}
    \end{equation*}

    Cette expérience réalisée
    par \propername{Michelson} et \propername{Morley}
    doit donc permettre
    de mettre en évidence
    le mouvement de la Terre
    à travers l'éther
    via le déphasage des ondes lumineuses
    introduit par la différence de marche~:
    \begin{equation*}
        \delta
        = c \Delta t
        = c (t_{BE'B'} - t_{BC'B'})
        \approx \frac{Lu^2}{c^2}
    \end{equation*}

    Les deux physiciens
    réalisèrent l'expérience
    deux fois
    (1881 et 1889)
    avec des interféromètres
    dont les bras mesuraient
    $L_1 = \SI{1}{\meter}$,
    ($L_2 = \SI{10}{\meter}$),
    ils s'attendaient donc à
    une différence de marche de
    $\delta_1 \approx \SI{10}{\nano\meter}$
    ($\delta_2 \approx \SI{100}{\nano\meter}$)
    soit un déphasage~:
    \begin{equation*}
        \Delta \phi_1
        = 2 \pi \frac{\delta_1}{\lambda}
        \approx \SI{0.12}{\radian}
        \qq{et}
        \Delta \phi_2
        \approx \SI{1.2}{\radian}
    \end{equation*}
    Ces déphasages
    doivent être observables.
    \begin{python}
        \software{./python/michelson-morley-anim.py}
    \end{python}
    Mais \propername{Michelson} et \propername{Morley}
    n'observèrent pas ce qu'ils attendaient~:
    l'expérience n'indiquait
    aucune interférence,
    aucune différence de temps,
    et la vitesse de la Terre au travers de l'éther
    ne put être mise en évidence.

    \subsection{La force de \propername{Lorentz}
        et les équations de \propername{Maxwell}}

    Deux autres problèmes du même type existait.
    D'une part,
    les équations de \propername{Maxwell} s'expriment~:
    \begin{equation*}
        \qty(\frac{1}{c^2}\pdv[2]{t} - \grad^2) \vb{E} = \vb{0}
        \qq{et}
        \qty(\frac{1}{c^2}\pdv[2]{t} - \grad^2) \vb{B} = \vb{0}
    \end{equation*}
    La vitesse de la lumière
    apparaît clairement dans ces équations,
    sans qu'a aucun moment
    la question du référentiel d'étude ne soit soulevée.

    D'autre part,
    la force de \propername{Lorentz}
    s'exprime,
    dans le principe fondamental de la dynamique~:
    \begin{equation*}
        \vb{F}
        = m \vb{a}
        = q \qty(\vb{E} + \vb{v} \cross \vb{B})
    \end{equation*}
    or dans cette expression,
    l'accélération $\vb{a}$
    ne dépend pas du référentiel galiléen choisit
    alors que la vitesse $\vb{v}$ en dépend.

    C'est \propername{Lorentz}
    qui en cherchant à résoudre ces deux problèmes,
    fondra les bases mathématiques
    sur lesquelles la théorie d'\propername{Einstein}
    va reposer.

    \subsection{\propername{Einstein} pose
        les principes de la relativité restreinte}

    Les idées en physiques
    étaient coincées,
    l'hypothèse de l'éther était manifestement
    en désacord avec les expériences.
    La vitesse de la lumière
    semblait ne pas dépendre du référentiel d'observation,
    ce qui entre en contradiction avec
    le principe de relativité
    et les transformations de galilée.

    Justement, \propername{Lorentz}
    qui cherchait à résoudre
    les problèmes discutés plus haut,
    calcula quel types de transformations
    étaient nécessaire
    à garder
    les équations de \propername{Maxwell}
    et la force de \propername{Lorentz}
    invariantes par changement de référentiel.
    Ces transformations,
    que nous allons étudier maintenant,
    eurent du mal à être acceptées
    en raison des paradoxes qu'elles soulèvent.
    Mais \propername{Einsetin} eu l'odace
    de les accepter malgré tout.

    Il posa des nouveaux principes,
    les principes de la relativité restreinte~:
    \begin{itemize}
        \item La vitesse de la lumière,
            dans le vide,
            est la même
            dans tous les référentiels galiléens.
        \item Toutes les lois de la physique
            sont les mêmes
            dans tous les référentiels galiléens.
    \end{itemize}
    L'acceptation de ces principes
    est équivalente à
    l'acceptation des
    transformations de \propername{Lorentz}.

  \section{Les transformation de Lorentz}

    \subsection{Contraintes imposées par les deux postulats
    \cite{perezrelativite}}

    Objectif de rentre invariantes les équations de Maxwell. Distance parcourue
    par un rayon dans deux référentiens donnent les transformation de Lorentz.

    \subsection{Conséquences \cite{perezrelativite}}


    Perte de la simultanéité ; invariance de l'intervalle (definition, genre,
    cône de lumière), dilatation du temps, contraction des longueurs : calcul
    du temps de vie des muons.
