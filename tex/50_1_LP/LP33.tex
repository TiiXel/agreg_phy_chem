\chapter{Interférences à deux ondes en optique}

\niveau{L2}

\prereqstart
{Optique géométrique}
{Diffraction (lycée)}
{Interférences (lycée)}
{Ondes électromagnétiques}
\prereqstop

L'optique géométrique est
un très bon outil pour étudier
la propagation de rayons lumineux,
elle permet d'expliquer
les phénomènes observés
lorsque la lumière traverse
des dioptres,
et la formation d'images.
Cependant,
elle ne permet pas
d'expliquer tous les phénomènes.

En classe de terminale
on découvre les phénomènes d'interférences~:
l'addition de deux ondes lumineuses
ne donne pas nécessairement
plus de luminosité~;
on apprend que
pour pouvoir observer des interférences
les sources doivent avoir même longueur d'onde.

\begin{manip}
    Montrer la supperposition de lumière
    avec de la lumière blanche
    restreinte à une fente
    et filtrée~:
    \begin{itemize}
        \item pour deux lampes et des filtres différents,
        \item pour deux lampes et les mêmes filtres,
        \item pour des fentes de \propername{Young}.
    \end{itemize}
\end{manip}

Cette expérience annonce
les connaissances de terminale
incomplètes.
Dans cette leçon,
nous utiliserons l'électromagnétisme
comme modèle ondulatoire de la lumière
pour mieux comprendre
pourquoi
ce que l'on apprend au lycée
est vrai,
et pourquoi
ça n'est pas suffisant.

\begin{draft}
    Il sera nécessaire
    d'avoir en tête
    des dispositifs
    à division du front d'onde
    et
    à division d'amplitude
    moins scolaires
    que
    les fentes de \propername{Young}
    ou
    l'interféromètre de \propername{Michelson}
    et le \propername{Fabry-Perrot}.
\end{draft}

\section{Quelque mots sur les détecteurs en optique}

    Avant toute chose,
    nous devons savoir
    ce que l'on mesure réellement
    lors d'expériences en optique~:
    ce que l'on perçoit,
    l'intensité lumineuse,
    comment est elle liée
    aux quantités de
    l'électromagnétisme~?

    Les photorécepteurs mesurent
    une quantité d'énergie,
    or l'énergie
    transportée par les ondes électromagnétique
    est liée
    au vecteur de \propername{Poynting}.
    Pour des ondes monochromatiques,
    la norme de ce vecteur
    est proportionnelle à
    la norme du champ électrique
    au carré~:
    $\norm{\vb{E}}^2$.

    Les ondes lumineuses
    oscillent à des fréquences
    de l'ordre de $\SI{e15}{\hertz}$,
    donc des périodes
    de l'odre de $\SI{e-15}{\second}$.
    Les capteurs eux,
    ont des temps de réponses
    bien plus long
    que la période des phénomènes qu'ils mesurent~:

    \begin{table}[H]
        \centering
        \begin{tabular}{cS[table-format=1e2]}
            \toprule
            Capteur & {Temps de réponse (\si{\second})} \\
            \midrule
            Œil & e-1 \\
            Photorésistance & e-3 \\
            Phototransistor & e-6 \\
            Photodiode & e-9 \\
            \bottomrule
        \end{tabular}
    \end{table}

    Il resulte de tout cela,
    que la quantité $I(M, t)$ mesurée
    par le capteur en $M$ à $t$
    sera proportionelle à
    l'énergie reçue
    pendant un certain temps
    dit \emph{temps d'intégration}.
    On écrira~:
    \begin{equation*}
        I(M, t)
        \propto
        \frac{1}{\tau} \int_{t-\tau}^t \vb{E}^2(M, t') \dd t'
    \end{equation*}

    Pour les temps de réponses indiqués,
    l'intégrale se rapproche
    de la valeur moyenne
    de sorte que l'on écrira~:
    \begin{equation*}
        I(M, t)
        \approx
        \avg{\vb{E}^2(M, t)}
        = I(M)
    \end{equation*}

    \begin{draft}
        On pourra se préparer
        à dire des choses
        sur l'expériende de \propername{Winer}.
    \end{draft}

\section{Supperposition de deux ondes monochromatiques
    émises par des sources ponctuelles}

    \subsection{Terme d'interférences}

    On considère
    deux sources ponctuelles
    $S_1$ et $S_2$
    qui émettent
    des ondes lumineuses
    monochromatiques
    de pulsations $\omega_1$ et $\omega_2$,
    (nombres d'ondes $k_1$ et $k_2$).
    Nous observons ces sources
    au point $M$,
    dans une région de l'espace
    où nous pouvons considérer
    les ondes reçues
    comme planes.
    On peut alors donner
    une expression pour
    le champ électrique
    et l'intensité lumineuse mesurée
    en $M$,
    en discriminant les sources~:
    \begin{gather*}
        \vb{E}_i(M, t)
        = \vb{E}_{0i} \cos(\omega_i t - \phi_i(M))
        \\
        \phi_i(M)
        = k_i (S_iM) + \phi_i
        \\
        I_i(M)
        = \avg{\vb{E}_{0i}^2 \cos[2](\omega_i t - \phi_i(M))}
        = \frac{\vb{E}_{0i}^2}{2}
        = I_i
    \end{gather*}

    Par addition,
    on obtient
    le champ électrique résultant~:
    \begin{equation*}
        \vb{E}(M, t)
        = \vb{E}_{01} \cos(\omega_1 t - \phi_1(M))
        + \vb{E}_{02} \cos(\omega_2 t - \phi_2(M))
    \end{equation*}

    La quantité
    mesurée ou percue
    est~:
    \begin{align*}
        I(M)
        = I_1 + I_2
        + 2 \vb{E}_{01} \vb{E}_{02}
            \avg{\cos(\omega_1 t - \phi_1(M))\cos(\omega_2 t - \phi_2(M))}
    \end{align*}

    On constate que
    l'intensité lumineuse en $M$
    n'est pas égale
    à la somme des intensités individuelles
    de chaque source~;
    le terme supplémentaire
    est celui qui donne naissance
    aux interférences.
    Selon les propriétés
    des ondes émises par chaque source
    il sera plus ou moins important.

    \subsection{Conditions d'interférences}

    \subsubsection{Polarisation}

    Si les source
    emmetent des ondes électromagnétiques
    de sorte que
    $\vb{E}_1 \perp \vb{E}_2$
    alors,
    le produit scalaire est nul,
    le terme d'interférences disparait.

    Dans la suite,
    nous ne considèrerons que
    des sources de même polarisation.

    \subsubsection{Pulsation}

    \paragraph{Sources de pulsations différentes}
    $\omega_1 \neq \omega_2$,
    alors
    la valeur moyenne
    du produit de cosinus
    va être nulle~:
    \begin{equation*}
        I(M)
        = I_1 + I_2
    \end{equation*}
    Dans cette situation,
    on remarque que l'on a additionné
    les valeurs
    qui auraient été mesurées
    individuellement pour chaque source.
    C'est bien que que l'on à observé
    dans l'expérience d'introduction.

    \paragraph{Sources de même pulsations}
    $\omega_1 = \omega_2 = \omega$, ($k_1 = k_2 = k$)~:
    \begin{align*}
        I(M)
        &= I_1 + I_2
        + \sqrt{I_1 I_2} \avg{
            \cos(2 \omega t - (\phi_1(M) + \phi_2(M)))
            + \cos(\phi_1(M) - \phi_2(M))
            }
        \\
        &= I_1 + I_2 + \sqrt{I_1 I_2} \avg{\cos(\phi_1(M) - \phi_2(M))}
    \end{align*}

    Ici
    le terme d'interférence n'est pas nul,
    on voit qu'il dépend
    du retard existant
    entre la phase des ondes
    quand elles arrivent en $M$,
    ce \emph{déphasage}
    est lié à la
    \emph{différence de marche} $\delta$~:
    \begin{equation*}
        \Delta\phi(M)
        \defeq \phi_1(M) - \phi_2(M)
        = k (\underbrace{(S_1M) - (S_2M)}_{\delta(M)})
            + \underbrace{\phi_1 - \phi_2}_{\Delta\phi}
    \end{equation*}

    C'est ce terme
    qui donne lieu aux interférences.
    Jusque là,
    les connaissances du lycée
    sont bien valides
    mais rappelons que,
    dans l'expérience d'introduction
    nous n'observions pas toujours
    les interférences.

    Dans la suite,
    nous n'étudierons que
    des sources de même pulsations.

    \paragraph{Retour sur l'expérience}

    \begin{manip}
        Ajouter des polariseurs.
    \end{manip}

    Concernant notre expérience,
    on pourrait penser
    que nos sources n'avaient pas
    la même polarisation
    mais,
    l'ajout de polariseurs
    ne change pas le résultat.
    Il y a donc quelque chose d'autre\dots

    \subsubsection{Cohérence temporelle}

    Regardons à nouveau
    le terme d'interférences~:
    \begin{equation*}
        I(M)
        = I_1 + I_2 + \sqrt{I_1 I_2} \avg{\cos(k \delta(M) + \Delta\phi)}
    \end{equation*}
    quand nous n'observons pas d'interférences,
    c'est que ce terme est nul.
    En un point $M$ de l'écran,
    $k \delta(M)$
    est constant~;
    mais
    que peut-on dire
    de $\Delta\phi$~?
    Il s'agit
    du déphasage entre les ondes
    au moment de leur émission
    par leur source.

    L'émission de lumière
    est un processus
    discret
    et aléatoire~:
    \begin{itemize}
        \item dans le modèle corpusculaire,
            la désexcitation d'un atome
            entraîne l'émission d'un photon,
        \item dans le modèle ondulatoire,
            la désexcitation d'un atome
            entraîne l'émission d'un $\emph{train d'onde}$~:
            il s'agit
            d'un signal sinusoïdal
            de fréquence $\omega$
            mais restreint dans le temps.
    \end{itemize}

    \paragraph{Dans le cas de plusieur sources primaires}
    Le déphasage
    entre deux sources indépendantes
    est aléatoire
    à chaque instant,
    on parle de sources
    non cohérentes.
    L'argument
    du cosinus
    dans le terme d'interférences
    prend donc des valeurs
    aléatoires à chaque instant~:
    la valeur moyenne
    du cosinus
    est donc nulle.

    Cela explique
    notre expérience.
    Dans le cas des deux sources indépendantes,
    on ne peut pas observer d'interférences.
    Alors que
    dans le cas des fentes de \propername{Young},
    l'onde qui arrive
    à chaque moment
    sur les deux fentes
    est issue du même train d'onde d'émission,
    les ondes émises
    par ces sources secondaires
    sont donc cohérentes.

    \paragraph{Dans le cas d'une seule source primaire}
    Considérons
    deux trains d'ondes
    qui arrivent en $M$
    à un instant donné,
    suivant deux chemins optiques différents.
    \begin{itemize}
        \item Si
            l'extension spatiale
            de ces trains d'ondes
            est suppérieure à la différence de marche,
            alors
            ces trains d'ondes
            ont été émis
            au même moment
            par la source
            avec la même phase
            $\phi_1 = \phi_2 \implies \Delta\phi = 0$.
            On observe donc des interférences.
        \item Au contraire, si
            l'extension spatiale
            de ces trains d'ondes
            est inférieure à la différence de marche,
            alors c'est que
            ces trains d'ondes
            ont été émis
            à des instants différents
            par la source~:
            ils ont alors chacun
            une phase $\phi_i$ aléatoire.
            Pour ces deux trains d'ondes particuliers
            le déphasage $\delta\Phi$
            prend une valeur bien spécifique (mais alatoire),
            qui donne lieu à des interférences.
            En conséquence,
            pour l'ensemble
            des paires de trains d'ondes
            reçues
            pendant la période d'intégration du capteur,
            le très grand nombre
            de $\Delta\phi$
            aléatoires
            va donner lieu
            à un brouillage
            des interférences.
    \end{itemize}

    \begin{pydo}
        Faire une animation
        des trains d'ondes.
    \end{pydo}

    \begin{todo}
        La discussion
        sur le cas
        de plusieur sources primaires
        à-t-elle sa place
        ici~?
    \end{todo}

    Les interférences lumineuses
    ne sont donc pas exactement
    faciles à obtenir.
    Pour les faire apparaître
    et les exploiter,
    nous avons besoin
    de solutions techniques
    particulières
    que l'on appelle
    interféromètres.
    Nous allons étudier
    un peu plus en détails
    les fentes de \propername{Young}
    et la figure d'interférences
    qu'elles permettent d'obtenir.

\section{Un interféromètre~: les fentes de \propername{Young}}

    Il s'agit donc
    d'une solution technique
    qui permet d'aisément
    faire apparaître
    une figure d'interférences.
    Le système
    nous place dans des conditions
    «~idéales~» où~:
    \begin{align*}
        I(M)
        &= I_1 + I_2 + \sqrt{I_1 I_2} \avg{\cos(k\delta(M) + \Delta\phi)}
        \\
        &= 2 I_0 + 2 I_0 \cos(k\delta(M))
        \\
        &= 2 I_0 (1 + \cos(k\delta(M)))
    \end{align*}
    les sources secondaires
    sont cohérentes
    et ont la même intensité.

    \subsection{Figure d'interférence}

    \subsubsection{Allure}

    La valeur de
    $\delta(M)$
    se trouve facilement
    sur le schéma suivant~:
    \begin{todo}
        Faire le schéma.
    \end{todo}

    La trigonométrie
    donne~:
    \begin{equation*}
        \delta(M)
        \approx \frac{a x(M)}{D}
    \end{equation*}

    \begin{todo}
        Tracer l'allure.
    \end{todo}

    \subsubsection{Interfrange}

    On appelle interfrange
    l'espace qui sépare
    deux maximas
    (ou minimas)
    d'intensité lumineuse.
    Les maximas sont obtenus pour
    les différences de marche particulières $\delta_p$~:
    \begin{equation*}
        k \delta_p
        = \frac{2\pi}{\lambda} \frac{ax_p}{D}
        = 2 p \pi, p \in \mathbb{N}
        \implies
        x_p
        = p \frac{\lambda D}{a}
    \end{equation*}

    La différence
    $x_{p+1} - x_p$
    donne l'interfrange
    $i = \frac{\lambda D}{a}$.

    \subsubsection{Contraste}

    Le contraste
    (ou la visibilité)
    est une grandeur
    sans dimension
    qui donne une indication
    sur la différence d'intensité lumineuse
    ente les maximas
    et les minimas.
    On définit~:
    \begin{equation*}
        C
        = \frac{I_{max} - I_{min}}{I_{max} + I_{min}}
    \end{equation*}

    Pour les fentes de \propername{Young}
    le contraste vaut $\num{1}$.
    En retournant momentanément
    au cas le plus général
    de deux sources monochromatique indépendantes,
    le contraste s'exprime~:
    \begin{equation*}
        C = 2 \frac{\vb{E_{01}E_{02}}}{I_1 + I_2}
    \end{equation*}

    \pyimgen{fentes_de_young_contraste}

    \subsection{Une nouvelle mise en défaut~: cohérence spatiale}

    \begin{manip}
    \end{manip}

    \begin{todo}
        Faire le calcul
        pour une fente réelle
        qui éclaire les fentes de \propername{Young}.
        Parler de brouillage.
    \end{todo}

\plusloin{
    Dans cette leçon,
    nous avons complété
    les connaissances aquises au lycée
    concernant les conditions d'obtentions
    de figures d'interférences.

    La dernière section
    mettait en évidence
    le problème de cohérence spatiale
    associé aux sources étendues.
    L'utilisation des fentes de \propername{Young}
    restreint l'expérimentateur
    à des sources fines
    et par conséquent
    peu lumineuses,
    il résulte bien sûr
    que la figure d'interférence,
    même si elle présente un bon contrase,
    sera peu brillante.

    Dans la prochaine leçon
    nous étudierons
    de manière plus formelle
    la cohérence spatiale
    pour chercher à réaliser
    un interféromètre
    qui permet l'utilisation
    de sources étendues.
}
