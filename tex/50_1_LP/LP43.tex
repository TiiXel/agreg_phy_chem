\chapter{Évolution temporelle d'un système quantique à deux niveaux}

\niveau{L3}

\prereqstart
{Équation de \propername{Schrödinger}}
{Fonction d'onde}
{Notation de \propername{Dirac}}
{Confinement, quantification}
{États stationnaires}
{Effet tunnel}
\prereqstop

Suite à l'étude
des potentiels confinants
et de l'effet tunnel,
qui étaient des premiers exemples
d'exercices de mécanique quantique.
Dans ces situations,
on résolvait l'équation de \propername{Schrödinger} indépendante du temps
($i \hbar \pdv{\Psi}{t} = H \Psi \rightarrow H\Psi = E\Psi$)
et l'on obtenait des états stationnaires.

On s'intéresse dans cette leçon
à des états quantiques
qui sont superposition
d'états stationnaires d'énergies différentes~:
ces états vont manifester
une évolution temporelle non triviale.

\section{Le double puits de potentiel~: système à deux niveaux
    \cite{basdevant, courseradoublepuit, mp3montaigne}}

    \begin{draft}
        L'étude objet de cette leçon
        peut très bien se faire
        de manière abstraite
        en définissant des états
        $\ket{1}$ et $\ket{2}$
        et une combinaison linéaire
        $\ket{\Psi} = a\ket{1} + b\ket{2}$,
        mais\dots
    \end{draft}

    Les systèmes type «~double puits~»
    sont des systèmes qui peuvent fonctionner
    comme bonnes approximations
    de systèmes à deux niveaux.
    L'intérêt de les étudier dans cette leçon
    est de permettre une représentation visuelle
    pour laquelle
    on apprécie
    le système préparé en superposition d'états.

    \subsection{Exemples et modèle}

    Le problème du «~double puits~»
    fait référence aux situations
    dans lequelles on trouve
    deux puits de potentiels
    suffisament proches
    pour qu'il soit possible
    de passer de l'un à l'autre
    par effet tunnel.
    Dans les molécules chimiques
    les distances interatomiques
    sont de l'ordre de l'angström
    et les énergie de l'ordre de l'électronvolt
    ce qui rend l'électron susceptible à l'effet tunnel.

    On considère l'ion $\ce{H2+}$,
    constituté d'un électron et deux protons.
    Le potentiel auquel est soumis l'électron
    correspond à l'ajout
    des potentiels de chaque proton.

    Autre système du même type,
    la molécule d'ammoniac
    de géométrie pyramide trigonale
    est formée d'un atome d'azote
    et de trois noyaux d'hydrogène
    qui forment un plan.
    On peut décrire
    des vibrations de la molécule
    comme le déplacement de ce plan
    par rapport à l'atome d'azote.

    \begin{todo}
        Faire les dessins \cite{basdevant}.
    \end{todo}

    On s'intéresse donc
    au double puits
    dont la barrière,
    de hauteur $V_0$ et largeur $\Delta$,
    sépare les deux puits de largeur $a$.

    \subsection{Fonction d'onde}

    On étudie la situation
    où l'énergie $E$
    du système (la particule, qui est l'électron ou les trois hydrogènes),
    est inférieure à $V_0$.
    Comme d'habitude
    pour ce genre de problèmes
    on divise l'espace
    en régions que l'on nommera $G$, $M$ et $D$
    et on définit~:
    \begin{equation*}
        k = \sqrt{\frac{2mE}{\hbar^2}}
        \qq{et}
        \kappa = \sqrt{\frac{2m(E-V_0)}{\hbar^2}}
    \end{equation*}
    où $m$ est la masse du système.
    On impose
    les conditions aux limites nulles,
    $\phi(x = \pm (a + \flatfrac{\Delta}{2})) = 0$.

    En résolvant l'équation de \propername{Schrödinger} stationnaires
    on trouve deux classes de fonctions d'ondes propres~:
    des solutions symétriques $\phi_s$
    et des solutions antisymétrique $\phi_a$.

    \begin{equation*}
        \phi_s(x)
        = \left\{\begin{array}{ll}
                A \sin(k (a + \frac{\Delta}{2} + x)) &\qq{dans} G \\
                B \cosh(k (a + \frac{\Delta}{2} + x)) &\qq{dans} M \\
                A \sin(k (a + \frac{\Delta}{2} + x)) &\qq{dans} D
        \end{array}\right.
    \end{equation*}
    \begin{equation*}
        \phi_a(x)
        = \left\{\begin{array}{ll}
                -A \sin(k (a + \frac{\Delta}{2} + x)) &\qq{dans} G \\
                B \sinh(k (a + \frac{\Delta}{2} + x)) &\qq{dans} M \\
                A \sin(k (a + \frac{\Delta}{2} + x)) &\qq{dans} D
        \end{array}\right.
    \end{equation*}

    \pyimgen{solve_schrodinger_AMMO}

    On constate que
    dans les états stationnaires,
    la particule
    à la même probabilité
    d'être mesurée
    à gauche ou à droite.
    On remarque aussi
    que~:
    $\psi_s + \psi_a \defeq \psi_g$
    correspond à un état
    où la particule est à gauche,
    alors que
    $\psi_s - \psi_a \defeq \psi_d$
    correspond à un état
    où la particule est à droite.
    Ces deux états,
    $\psi_g$ et $\psi_d$
    semblent plus «~raisonables~»
    alors qu'ils ne sont pas
    des états propres.
    Rappelons que
    comme l'équation de \propername{Schrödinger} est linéaire
    alors toute combinaison linéaire
    de solutions
    est une solution~:
    on peut décrire le système
    aussi bien avec un couple
    qu'avec l'autre.

    Pour ne pas perdre de généralités
    en le décrivant avec $\psi_g$ et $\psi_d$
    on doit toutefois s'assurer
    que ces deux états
    forment une base
    si possible orthonormée,
    comme $\psi_s$ et $\psi_a$ le font.
    Les deux calculs
    $\bra{\psi_d}\ket{\psi_d} = \int \psi_d \psi_d^*$
    et
    $\bra{\psi_g}\ket{\psi_g} = \int \psi_g \psi_q^*$
    donnent tous les deux $2$.
    Le calcul
    $\bra{\psi_g}\ket{\psi_d} = \int \psi_g \psi_d^*$
    donne $0$.
    Les deux fonctions d'onde
    sont orthogonales
    mais pas normées~:
    on pourrait plutôt choisir
    $\psi_g \rightarrow \frac{1}{\sqrt{2}}\psi_g$
    et
    $\psi_d \rightarrow \frac{1}{\sqrt{2}}\psi_d$,
    mais cela
    à très peu d'importance pour la suite.

    \subsection{Niveaux d'énergie}

    La continuité de la fonction d'onde
    permet de calculer les valeurs
    des énergies
    $E_s$ et $E_a$
    associées aux deux états stationnaires,
    et sous certaines approximations \cite{basdevant}
    on trouve des expressions littérales.

    \subsection{Évolution temporelle}

\section{La molécule d'ammoniac dans un champ électrique}

    \section{Couplage avec le champ}

    \section{Hamiltonien et niveaux d'énergie}
