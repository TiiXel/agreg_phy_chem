\chapter{Bilans thermiques~: flux conductif, convectif et radiatif}

\niveau{Cette leçon
se situe à la limite extérieure
du programme de seconde année de CPGE,
puisque les phénomènes d'échange radiatifs
vont être étudiés et pris en compte.}

\prereqstart
{Conduction, convection, rayonnement}
{Loi de \propername{Fourier}}
{Équation de la diffusion thermique}
\prereqstop

L'objet de cette leçon
est d'étudier les phénomènes de transferts thermiques
en proposant une unification de la modélisation,
qui permettra de résoudre un problème
qui peut sembler complexe à première vue~:
on étudira les économies en énergie
réalisables grâce à l'installation d'un double vitrage
en tenant compte
des trois modes d'échanges.

\begin{todo}
    L'application est au cœur de la leçon~!

    Reprendre cette leçon
    en axant la discussion
    sur une optique d'économie d'énergie,
    on peut commencer
    en parlant du double vitrage.
    Bien penser à discuter
    les expressions des résistances~:
    comment bien isoler~?
\end{todo}
\begin{todo}
    Ajouter des ordres de grandeur
    et penser à discuter
    des unités~:
    du flux,
    des coefficients,
    des constantes,
    etc.
\end{todo}
\begin{draft}
    Avoir en tête
    des ordres de grandeur
    pour les conductivités
    des gaz, des liquides, des solides.
\end{draft}
\begin{draft}
    Connaître la constante de \propername{Stefan-Boltzmann}.
\end{draft}
\begin{draft}
    Savoir d'où vient
    le coefficient de convection $h$~:
    lié à la conduction dans la couche limite,
    dépend de $\lambda$ du fluide (mais pas de la parroie).
    En ordre de grandeur $\delta = \flatfrac{h}{\lambda}$.
\end{draft}

\section{Notion de résistance thermique~: étude de la conduction}

    \subsection{Vecteur densité de courant thermique}

    L'équation de la diffusion thermique
    à une dimension
    \begin{equation*}
        \pdv{T}{t} - \frac{\lambda}{\rho c} \pdv[2]{T}{x}
        = 0
    \end{equation*}
    se retrouve à partir
    d'un bilan d'énergie thermique
    sur une portion infinitésimale
    d'un corps homogène de section droite $S$ dans lequel
    il n'y a aucune source d'énergie,
    la température de dépend
    que de $x$ et de $t$
    et duquel les parois parallèles à l'axe $x$
    sont isolées thermiquement.
    Le vecteur densité de courant thermique
    $\vb{J}(x, t)$
    associé est lié à la température
    par la loi de \propername{Fourier}~:
    \begin{equation*}
        \vb{J}(x, t)
        = - \lambda \grad{T(x, t)}
    \end{equation*}

    Si l'on se place en régime stationnaire,
    les températures en $x=0$ et $x=L$
    etant fixées à $T_0$ et $T_L$,
    on obtient rapidement~:
    \begin{equation*}
        \pdv[2]{T}{x}
        = 0
        \implies
        T(x)
        = \frac{T_L - T_0}{L} x + T_0
        \implies
        J
        \defeq \norm{\vb{J}}
        = - \lambda \frac{T_L - T_0}{L}
    \end{equation*}

    On peut alors exprimer
    le flux thermique au travers de la section $S$~:
    \begin{equation*}
        \phi
        = J S
        = - \lambda \frac{T_L - T_0}{L} S
        = - \frac{\lambda S}{L} \Delta T
    \end{equation*}

    \subsection{Résistance thermique}

    En avancant une analogie
    avec l'électrocinétique,
    dans laquelle
    la différence de température
    jour un rôle de différence de potentiel
    et le flux thermique
    un rôle de courant,
    on peut définir une résistance thermique~:
    \begin{equation*}
        R_{th}
        \;\suchas\;
        \Delta T
        = R_{th} \phi
        \implies
        R_{th}
        = \frac{L}{\lambda S}
    \end{equation*}
    L'expression de cette résistance
    suit complètement l'analogie puisque
    pour un corps
    de conductivité électrique $\sigma$,
    de longueur $L$
    et de section $S$,
    traversé par un courant électrique $i$
    dans l'axe de la longueur,
    la résistance électrique s'exprime
    $R_{el}
    = \frac{L}{\sigma S}$.

    \subsection{Association de résitances}

    \subsubsection{Rappels d'électrocinétique}

    En électrocinétique on associe
    des résistances en série
    par sommation directe~:
    \begin{equation*}
        R_{eq} = R_1 + R_2
    \end{equation*}
    et des résistances en parallèle
    par sommation des inverses~:
    \begin{equation*}
        R_{eq} = \qty(\frac{1}{R_1} + \frac{1}{R_2})^{-1}
    \end{equation*}

    Revenons sur les définitions de
    \emph{série} et \emph{parallèle}.
    On dit que deux branches d'un circuit
    sont en \emph{série}
    lorsqu'elles sont
    traversées par le même courant.
    On dit que deux branches d'un circuit
    sont en \emph{parallèle}
    lorsqu'elles sont
    soumises à la même différence de potentiels.

    \subsubsection{En thermodynamique~: série}

    On associe deux corps
    de conductivité thermique $\lambda_1$ et $\lambda_2$~;
    le premier remplit l'espace entre $x=0$ et $x=L_1$
    et le second l'espace entre $x=L_1$ et $x=L_1+L_2$.
    En régime permanent
    on note $T_0$, $T_1$, $T_2$
    les températures en $x=0$, $x=L_1$, $x=L_1+L_2$.

    Ces deux corps
    sont traversés par le même flux thermique
    $\phi_1 = \phi = \phi_2$,
    avec~:
    \begin{equation*}
        \left\{\begin{array}{cc}
            \phi_1 &= - \lambda_1 S \frac{T_1 - T_0}{L_1}
            \\
            \phi_2 &= - \lambda_2 S \frac{T_2 - T_1}{L_2}
        \end{array}\right.
        \implies
        T_2 - T_0
        = - (\frac{L_1}{\lambda_1 S} + \frac{L_2}{\lambda_2 S}) \phi
    \end{equation*}
    On va donc pouvoir exprimer
    une résistance équivalente de l'ensemble
    par la relation~:
    \begin{equation*}
        R_{eq}
        = \frac{L_1}{\lambda_1 S} + \frac{L_2}{\lambda_2 S}
    \end{equation*}

    \subsubsection{En thermodynamique~: parallèle}

    On associe maintenant les deux corps
    de sorte que les deux soient exposés
    à la même différence de température,
    et traversés par des flux thermiques
    qui sont différents.
    Le premier corps
    de section droite $S_1$
    remplit une partie de l'espace
    entre $x=0$ et $x=L_1$
    et l'autre,
    de section droite $S_2$
    une partie de l'espace
    entre $x=0$ et $x=L_2$.
    La température en $x=0$
    est notée $T_0$,
    et la température en $x=L_1$
    égale à la température en $x=L_2$
    est notée $T_1$.
    Le flux total
    au travers de l'association
    est maintenant
    $\phi = \phi_1 + \phi_2$
    avec~:
    \begin{equation*}
        \left\{\begin{array}{cc}
            \phi_1 &= - \lambda_1 S \frac{T_1 - T_0}{L_1}
            \\
            \phi_2 &= - \lambda_2 S \frac{T_1 - T_0}{L_2}
        \end{array}\right.
    \end{equation*}
    d'où~:
    \begin{equation*}
        \phi
        = - \qty(\frac{\lambda_1 S_1}{L_1}+\frac{\lambda_2 S_2}{L_2}) (T_1-T_0)
    \end{equation*}
    soit~:
    \begin{equation*}
        (T_1 - T_0)
        = - \frac{1}{\qty(\frac{\lambda_1 S_1}{L_1}+\frac{\lambda_2 S_2}{L_2})}
            \phi
    \end{equation*}
    On va donc pouvoir exprimer
    une résistance équivalent de l'ensemble
    par la relation~:
    \begin{equation*}
        R_{eq}
        = \qty(\frac{\lambda_1 S_1}{L_1} + \frac{\lambda_2 S_2}{L_2})^{-1}
    \end{equation*}

\section{Convection et rayonnement}

    Comme annoncé en introduction,
    l'objectif de cette leçon
    est d'étudier les trois phénomènes de transferts thermiques
    en proposant une unification de la modélisation.
    C'est à travers cette notion
    de résistance thermique
    que nous allons simplifier les descriptions.

    Les phénomènes
    de convection
    et de rayonnement
    permettent aussi de définir
    une forme de résistance thermique.
    Un corps qui échange
    avec son environement
    de l'énergie
    à travers sa surface
    par convection
    et par rayonnement
    voit ses surfaces
    soumises à une différence de température $\Delta T$
    traversées par un flux d'échange convectif $\phi_c$
    et un flux d'échange radiatif $\phi_R$.
    Si l'on arrive à introduire
    pour ces modes de transferts
    des grandeurs analogues à la résistance,
    alors on pourra dire
    que les échanges d'un corps avec son environement
    se font avec une résistance thermique
    équivalente à l'association en parallèle
    des deux résistances.

    \subsection{Convection}

    La loi phénoménologique de \propername{Newton}
    (dont l'expression n'est pas à connaître en CPGE)
    décrit les échanges thermiques pour
    les interfaces solide-fluide.
    Si l'on considère
    une surface $S$
    à la température $T$
    en contact avec de l'air à la température $T_a$,
    le vecteur densité de courant thermique
    sortant algébriquement de la surface
    lié à la convection
    va s'écrire~:
    \begin{equation*}
        \vb{J_c}
        = h_c (T - T_a) \vu{x}
    \end{equation*}
    où
    $h_c$ est le coefficiant de convection
    qui dépent des milieux en contact.

    Ici,
    on peut définir une résistance thermique~:
    \begin{equation*}
        R_c
        = \frac{1}{h_c S}
    \end{equation*}

    \subsection{Rayonnement}

    La loi de \propername{Stefan}
    donne l'énergie rayonnée
    par unité de surface
    par unité de temps
    pour un corps noir à la température $T$~:
    \begin{equation*}
        J_R = \sigma T^4
    \end{equation*}
    en fonction de
    la constante universelle de \propername{Stefan-Boltzmann}~:
    \begin{equation*}
        \sigma
        = \frac{2\pi^5k_B^4}{15 c^2 h^3}
        \approx \SI{5.67e-8}{\watt\per\meter\squared\per\kelvin\tothe{4}}
    \end{equation*}

    On rapelle que le corps noir
    est un modèle théorique
    qui décrit de manière approximative
    le spectre continu d'émission de corps réels.

    À la surface d'un corps
    de température $T$
    en contact avec un environement
    de température $T_e$,
    la densité de courant thermique
    sortant algébriquement de la surface de contact
    va donc s'écrire~:
    \begin{equation*}
        J_R
        = \sigma (T^4 - T_e^4)
    \end{equation*}

    On pourra,
    pour un corps de surace $S$,
    définir le flux thermique par rayonnement~:
    \begin{equation*}
        \phi_R
        = J_R S
        = \sigma S (T^4 - T_e^4)
    \end{equation*}
    L'abscence de dépendance en
    $T - T_e$
    empêche de définir une résistance thermique
    par les mêmes considération que précédement.
    Mais
    pour des écarts de températures très faibles
    ($T = T_e + \Delta T, \Delta T \ll T_e$)
    un développement limité permettra d'écrire~:
    \begin{equation*}
        \phi_R
        \approx \sigma S T_e^3 \Delta T
    \end{equation*}
    On peut maintenant exprimer~:
    \begin{equation*}
        R_R
        = \frac{1}{\sigma S T_e^3}
        \approx \frac{1}{\sigma S T^3}
    \end{equation*}

\section{Le double vitrage \cite{ccp2003mpphi1}}

    On se propose d'estimer
    les pertes thermiques
    d'un local
    maintenu à la température $T_{int}$
    dans une atmosphère de température $T_{ext}$.
    On suppose que la paroi
    qui sépare l'intérieur de l'extérieur
    est constituée
    d'un mur en béton
    et d'une vitre,
    d'abord en simple vitrage
    puis en double vitrage.

    On considèrera
    que la paroi en contact avec l'extérieur
    échange de l'énergie thermique par
    convection avec l'air
    et par rayonnement avec l'atmosphère~;
    et que la paroi en contact avec l'intérieur
    échange de l'énergie thermique par
    convection avec l'air
    et par rayonnement avec l'intérieur du local.

    Dans le cas du double vitrage
    on pourra négliger les phenomènes de convection
    pour l'air emprisoné entre les vitres,
    car l'épaisseur de la couche d'air est réduite
    à une épaisseur inférieur à celle de la couche limite
    d'adhérence de l'air sur le verre.

\plusloin{
    Cette leçon se concentre
    sur des échanges thermiques
    entre milieux unidimensionels et thermostatés,
    mais l'on pourrait
    (dans le cadre du programme de PSI)
    d'une part généraliser les problèmes à trois dimensions
    et d'autre part s'intéresser
    à des bilans d'énergie thermique
    dans des situations où~:
    \begin{itemize}
        \item les conditions aux limites sont périodiques
            (ondes thermiques)
        \item il y a des sources d'énergie thermique
            (effet \propername{Joule})
    \end{itemize}

    En sortant à nouveau du programme de PSI,
    on pourait
    établir une expression pour le coefficient de convection $h_c$ \cite{gie},
    ou justifier l'origine de la loi de \propername{Stefan}.
}
