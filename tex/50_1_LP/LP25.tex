\chapter{Ondes acoustiques}

\niveau{Cette leçon
se situe à la limite extérieure
du programme de PSI,
puisqu'on démontre les expression
de $\vb{\Pi}$ et $e$.}

\prereqstart
{Theromdynamique}
{Mécanique des fluides (description eulerienne)}
{Notions d'électromagnétisme (\propername{Poynting}, OPPH)}
{Équation de \propername{D'Alembert}}
\prereqstop

\begin{draft}
    On se restreint
    aux ondes sonores
    dans les fluides
    et d'amplitude
    associées
    à la prerception humaine.
    Donc
    pas les ondes dans les solides,
    ni les gros boum.
\end{draft}

\begin{todo}
    Parler plus précisément
    des émetteurs
    et des capteurs
    (façon \cite{dunodpsi},
    ou LP33)~?
\end{todo}

\section{Équation de propagation des ondes acoustiques \cite{dunodpsi}}

    \subsection{Grandeurs dynamiques}

    Concernant
    les ondes acoustiques
    la grandeur qui se propage est,
    essentiellement,
    une variation locale de pression.
    Les sons
    sont produits par
    une compression locale du fluide,
    qui va à son tour
    comprimer le fluide avoisinant,
    et ainsi de suite.

    \begin{pydo}
        Faire une petite animation.
    \end{pydo}

    Une variation de pression
    est associée
    à une variation de masse volumique~;
    le fluide étant mis
    en mouvement
    nous pourons
    considérer
    une variation locale
    de vitesse.
    Concrètement,
    en se placant
    dans le référentiel
    dans lequel le fluide
    est
    à l'équilibre
    en absence d'onde sonore,
    nous décrivons le fluide
    par~:
    \begin{alignat*}{3}
        p(M, t)
        &= p_0 &+ p_1(M, t)
        \\
        \mu(M, t)
        &= \mu_0 &+ \mu_1(M, t)
        \\
        \vb{v}(M, t)
        &= \vb{0} &+ \vb{v}_1(M, t)
    \end{alignat*}
    Les grandeurs
    indicées $0$
    sont
    celles d'équilibre
    alors que
    celles
    indicées $1$
    sont
    celles dues à l'onde.

    Nous avons pensé judicieux
    de décrire le fluide
    avec trois grandeurs.
    Nous devons alors trouver
    trois équations
    pour résoudre le problème.

    \subsection{Équations de la dynamique des fluide}

    \subsubsection{Équation d'\propername{Euler}}

    Nous allons supposer
    un fluide parfait
    dans lequel
    l'écoulement est
    isentropique
    (pas de viscosité,
    ni de transfert thermique~:
    les variations
    sont suffisament rapides
    pour que ces effets
    soient négligeables).
    Cette hyposthèse
    sera justifiée à posteriori.

    On étudie
    une particule fluide
    soumise
    à l'action de la pesanteur,
    à la poussée d'\propername{Archimède},
    et aux forces de pression horizontales.
    Pour simplifier
    notre approche
    nous allons négliger
    les effets de pesanteur
    sur le fluide parfait
    (propagation horizontale
    des ondes).

    L'équation d'\propername{Euler}
    s'écrit~:
    \begin{equation*}
        \dv{\mu\vb{v}}{t}
        = \pdv{\mu\vb{v}}{t} + \qty(\vb{v} \dotproduct \grad) (\mu\vb{v})
        = - \grad p
    \end{equation*}

    \subsubsection{Équation de continuité}

    La conservation
    de la masse
    va s'écrire~:
    \begin{equation*}
        \pdv{\mu}{t} + \div(\mu \vb{v})
        = 0
    \end{equation*}

    \subsubsection{Équation d'état}

    On rappelle
    qu'une équation d'état
    est une équation
    qui lie
    les variables d'état
    à l'équilibre du système.
    Nous souhaitons ici
    une équation liant
    $\mu$ à $p$,
    toujours dans le cadre
    de petites variations
    nous pourrons supposer
    un lien de proportionalité
    donné par
    le coefficient de compressibilité isentropique~:
    \begin{equation*}
        \chi_s
        = \frac{1}{\mu} \eval{{\pdv{\mu}{p}}}_s
        \implies
        \chi_s
        \approx \frac{1}{\mu} \frac{\mu_1}{p_1}
        \implies
        \mu_1
        \approx \chi_s (\mu_0 + \mu_1) p_1
        \approx \chi_s \mu_0 p_1
    \end{equation*}

    \subsection{Linéarisation des équations}

    Les variations relatives
    des grandeurs $\mu$, $p$, et $v$
    sont suffisament faibles
    de sorte que l'on écrive~:
    $\mu_1 \ll \mu_0$,
    et $p_1 \ll p_0$.
    Cette hyposthèse
    sera justifiée à posteriori.
    Le développement
    de ces grandeurs
    dans les équations précédentes
    donne~:
    \begin{gather*}
        \pdv{\mu_0 \vb{v}_1}{t}
        + \underbrace{\cancel{
            \pdv{\mu_1 \vb{v}_1}{t}
            + \qty(\vb{v} \dotproduct \grad) (\mu\vb{v})
        }}_{\textrm{ordre 2}}
        =
        - \underbrace{\cancel{\grad p_0}}_{\vb{0}}
        - \grad p_1
        \\
        \underbrace{\cancel{\pdv{\mu_0}{t}}}_{0}
        + \pdv{\mu_1}{t}
        + \div(\mu_0 \vb{v}_1)
        + \underbrace{\cancel{\div(\mu_1 \vb{v}_1)}}_{\textrm{ordre 2}}
        = 0
    \end{gather*}
    dans le cadre
    d'un développement
    au premier ordre
    nous gardons donc~:
    \begin{equation*}
        \mu_0 \pdv{\vb{v}_1}{t}
        = - \grad p_1
        \qq{et}
        \pdv{\mu_1}{t} + \mu_0 \div \vb{v}_1
        = 0
        \qq{et}
        \mu_1
        = \chi_s \mu_0 p_1
    \end{equation*}

    \subsection{Bilan des hypothèses~: approximation acoustique}

    Nous avons été amenés
    à faire quelque hypothèses
    qui ensemble
    portent le nom
    d'approximation acoustique~:
    \begin{itemize}
        \item Le fluide
            dans lequel l'onde se propage
            est
            un fluide parfait.
        \item On néglige
            les effets de pensanteur
            sur le fluide.
        \item Les variations relatives
            de $\mu$ et $p$
            sont petites.
    \end{itemize}

    \subsection{Équation de \propername{D'Alembert}}

    Notre système d'équations
    mène à~:
    \begin{equation*}
        \left.\begin{array}{r}
            \left\{\begin{array}{c}
                    \displaystyle
                    \pdv{\mu_1}{t} + \mu_0 \div \vb{v}_1
                    = 0
                    \\
                    \displaystyle
                    \mu_1
                    = \chi_s \mu_0 p_1
            \end{array}\right.
            \implies
            \displaystyle
            \chi_s\mu_0\pdv{p_1}{t} + \mu_0 \div \vb{v}_1
            = 0
            \\
            \displaystyle
            \mu_0 \pdv{\vb{v}_1}{t}
            = - \grad p_1
        \end{array}\right\}
        \implies
        \begin{array}{l}
            \displaystyle
            \mu_0 \chi_s \pdv[2]{p_1}{t} - \laplacian p_1
            = 0
            \\
            \qq{ou}
            \\
            \displaystyle
            \mu_0 \chi_s \pdv[2]{\vb{v}_1}{t} - \laplacian \vb{v}_1
            = 0
            \\
            \qq{ou}
            \\
            \displaystyle
            \mu_0 \chi_s \pdv[2]{\mu_1}{t} - \laplacian \mu_1
            = 0
        \end{array}
    \end{equation*}
    On reconnait
    une équation de \propername{D'Alembert}
    pour des ondes sonores
    qui se propagent
    à la célérité
    $c = \frac{1}{\sqrt{\mu_0 \chi_s}}$,
    et l'on justifie ici
    que les ondes sonores
    sont souvent qualifiées de
    «~ondes de pression~»
    ou
    «~ondes de densité~».

\section{Célérité}

    \begin{todo}
        Il y a certainement
        des choses à dire sur
        la dispersion.
        En particulier concernant
        les exemples avec
        l'hélium et l'argon.
    \end{todo}

    \subsection{Gaz parfaits}

    Dans l'étude des gaz parfaits,
    l'hypothèse isentropique
    doit rappeler la loi de \propername{Laplace}~:
    $pV^\gamma = \cst$.
    De cette loi
    on déduit directement
    $p\mu^{-\gamma} = \cst$
    puis,
    par dérivation logarithmique~:
    \begin{equation*}
        \frac{\dd p}{p}
        = \gamma \frac{\dd \mu}{\mu}
        \implies
        \frac{p_1}{p_0}
        = \gamma \frac{\mu_1}{\mu_0}
    \end{equation*}
    finalement,
    avec $T_0$ la température
    et $M$ la masse molaire du gaz,
    la loi des gaz parfait
    et l'une des relations précédentes
    permettent d'obtenir~:
    \begin{equation*}
        \chi_s
        = \frac{1}{\gamma p_0}
        \implies
        c
        = \sqrt{\frac{\gamma p_0}{\mu_0}}
        = \sqrt{\frac{\gamma R T_0}{M}}
    \end{equation*}

    \begin{table}[H]
        \centering
        \begin{tabular}{cSSSSc}
            \toprule
            Gaz
                & {$M$ (\si{\gram\per\mol})}
                    & {$T_0$ (\si{\degreeCelsius})}
                        & {$\gamma$ \cite{lange}}
                            & {$c$ (\si{\meter\per\second}) \cite{handbook}}
            \\
            \midrule
            hélium
                & 4.00
                    & 20
                        & 1.660
                            & 264
            \\
            air sec
                & 28.97
                    & 0
                        & 1.403
                            & 332
            \\
            air sec
                & 28.97
                    & 20
                        & 1.400
                            & 343
            \\
            air saturé en eau
                &
                    & 20
                        & 1.400
                            & 345
            \\
            dioxygène
                & 31.99
                    & 20
                        & 1.400
                            & 326
            \\
            argon
                & 39.95
                    & 20
                        & 1.670
                            & 323
            \\
            \bottomrule
        \end{tabular}
    \end{table}
    On constate
    qu'à $\gamma$ constant,
    la célérité
    est plus faible
    dans les gaz plus «~lourds~».
    En particulier
    humidifier l'air
    apporte de l'eau,
    ce qui \emph{diminue} $M$
    ($M_{\ce{H2O}} = \SI{18}{\gram\per\mol}$),
    donc
    augmente $c$.

    \subsection{Liquides}

    Pour les liquides,
    l'ordre de grandeur de $c$
    est différent~:
    \begin{table}[H]
        \centering
        \begin{tabular}{cSSSScl}
            \toprule
            Fluide
                & {$\mu_0$ (\si{\kilogram\per\meter\cubed})}
                    & {$\chi_s$ (\si{\per\pascal})}
                        & {$c$ (\si{\meter\per\second}) \cite{handbook}}
            \\
            \midrule
            eau
                & 1000
                    & 5e-10
                        & 1410
            \\
            eau de mer
                &
                    &
                        & 1449
            \\
            acétone
                &
                    &
                        & 1203
            \\
            \bottomrule
        \end{tabular}
    \end{table}

    \subsection{Solides}

    En dehors du cadre de notre étude,
    on peut donner quelque valeurs~:
    \begin{draft}
        Dans les solides,
        les ondes acoustiques
        peuvent être
        longitudinales
        mais aussi transversalles,
        les deux types d'ondes
        ne se propagent pas
        avec la même célérité.
        On donne ici
        les célérités
        longitudinales.
    \end{draft}
    \begin{table}[H]
        \centering
        \begin{tabular}{cS}
            \toprule
            Solide
                & {$c$ (\si{\meter\per\second}) \cite{handbook}}
            \\
            \midrule
            aluminium
                & 6420
            \\
            cuivre
                & 5010
            \\
            acier ($\SI{1}{\percent}\ce{C}$)
                & 5940
            \\
            verre (pyrex)
                & 5640
            \\
            caoutchouc
                & 1550
            \\
            polystyrène
                & 2350
            \\
            \bottomrule
        \end{tabular}
    \end{table}
    Ces valeurs donnent
    une idée de l'ordre de grandeur,
    mais sont variables.

\section{Aspect énergétiques}

    \subsection{Densité d'énergie et flux d'énergie}

    Les ondes
    transportent de l'énergie.
    On note $\vb{\Pi}$
    et l'on appelle
    vecteur de \propername{Poynting},
    le vecteur
    associé
    aux flux d'énergie
    porté par l'onde.
    Sa norme
    est homogène
    à des $\si{\watt\per\meter\squared}$
    et sa direction
    est celle
    de propagation de l'énergie.

    Dans le cas des ondes acoustiques
    les forces de pressions
    qui s'exercent
    entre particules fluides voisines
    sont associées
    à une puissance surfacique
    $v p_1$
    de sorte que,
    finalement,
    $\vb{\Pi} = \vb{v} p_1$.
    L'équation de continuité
    pour la densité d'énergie
    doit s'écrire~:
    \begin{alignat*}{2}
        \pdv{e}{t}
        = - \div \vb{\Pi}
        &= p_1 \div \vb{v}_1
            &&+ \vb{v}_1 \grad p_1
        \\
        &= p_1 \qty(- \chi_s \pdv{p_1}{t})
            &&+ \vb{v}_1 \qty(- \mu_0 \pdv{\vb{v}_1}{t})
    \end{alignat*}
    d'où~:
    \begin{equation*}
        \div \vb{\Pi}
        = - \pdv{t}(\frac{\chi_s p_1^2}{2} + \frac{\mu_0 v_1^2}{2})
    \end{equation*}

    On reconnait
    la densité d'énergie cinétique,
    et on déduit l'expression
    de la densité d'énergie potentielle~:
    \begin{equation*}
        e_c
        = \frac{1}{2} \mu_0 v_1^2
        \qq{et}
        e_p
        = \frac{1}{2} \chi_s p_1^2
    \end{equation*}

    \subsection{Niveau sonore}

    \begin{draft}
        Les résultats
        de cette section
        sont valables pour les OPPH,
        dont on discutera juste après
        s'il reste du temps.
        On arrive
        à l'expression suivante
        simplement
        si l'on exprime $v$ et $p$
        pour les OPPH.
        Comme c'est un petit calcul
        simple à faire
        et dont on a l'habitude
        avec l'électromagnétisme
        je ne le traite pas tout de suite
        dans le cadre de cette présentation,
        même si
        il apporte d'autres informations importantes.
    \end{draft}

    La valeur moyenne
    du vecteur de \propername{Poynting}
    donne l'intensité sonore~:
    \begin{equation*}
        I
        \defeq \avg{\norm{\vb{\Pi}}}
        = \frac{1}{2} \mu_0 c v_{10}^2
    \end{equation*}
    c'est à cette grandeur
    que l'oreille humaine est sensible,
    mais la perception
    est logarithmique
    de sorte que l'on définisse
    le \emph{niveau sonore}
    en décibel,
    relatif
    au seuil d'audibilité de l'oreille humaine $I_0$~:
    \begin{equation*}
        I_{\si{\decibel}}
        = 10 \log(\frac{I}{I_0})
        \qq{avec}
        I_0
        = \SI{e-12}{\watt\per\meter\squared}
    \end{equation*}

    \begin{table}[H]
        \centering
        \begin{tabular}{cSSSSS}
            \toprule
            {}
                & {$I$ (\si{\watt\per\meter\squared})}
                    & {$I_{\si{\decibel}}$ (\si{\decibel})}
                        & {$p_1$ (\si{\pascal})}
                            & {$\mu_1$ (\si{\kilo\gram\per\meter\cubed})}
                                & {$v_1$ (\si{\meter\per\second})}
            \\
            \midrule
            Seuil auditif (\SI{2}{\kilo\hertz})
                & e-12
                    & 0
                        & e-5
                            & e-10
                                & e-7
            \\
            \midrule
            Chuchottement
                & e-11
                    & 10
                        & e-4
                            & e-9
                                & e-7
            \\
            Campagne
                & e-9
                    & 30
                        & e-3
                            & e-8
                                & e-6
            \\
            Avenue
                & e-4
                    & 80
                        & e-1
                            & e-6
                                & e-3
            \\
            Marteau piqueur
                & e-2
                    & 100
                        & {1}
                            & e-5
                                & e-3
            \\
            Seuil de douleur
                & {1}
                    & 120
                        & e1
                            & e-4
                                & e-2
            \\
            \midrule
            Ambiant ($p_0$, $\mu_0$)
                &
                    &
                        & {$p_0 \approx \num{e5}$}
                            & {$\mu_0 \approx \num{1}$}
                                &
            \\
            \bottomrule
        \end{tabular}
    \end{table}

    \subsection{Justification des hypothèses avancées}

    \begin{draft}
        Il faut absolument traiter cette partie.
    \end{draft}

    \subsubsection{Surpression, surdensité, vitesse}

    Le tableau précédent
    dans lequel des ordres de grandeur
    sont présentés
    permet de conclure
    que les hypothèses
    $p_1 \ll p_0$
    et
    $\mu_1 \ll \mu_0$
    sont validées.

    \subsubsection{Abscence de transfert thermique}

    Pour une onde
    de pulsation $f$,
    le transfert thermique
    est lié aux variations de température
    associées aux variations de pression.
    \begin{itemize}
        \item La diffusion thermique
            est caractérisée par
            le coefficient de diffusion $D$
            et se fait
            sur des distances
            de l'ordre de
            $\sqrt{\flatfrac{D}{f}}$.
        \item Les zones
            de surperession et souspression
            sont séparées
            de $\flatfrac{\lambda}{2} = \flatfrac{c}{2f}$.
    \end{itemize}
    Le transfert thermique
    par diffusion
    est négligeable
    si~:
    \begin{equation*}
        \sqrt{\flatfrac{D}{f}}
        \ll
        \frac{c}{2f}
        \implies
        f
        \ll
        \frac{c^2}{4D}
    \end{equation*}
    On peut faire
    quelque applications numériques,
    et l'on remarque que
    le domaine audible
    (jusqu'à \SI{20e3}{\hertz})
    vérifie largement l'inégalité.
    \begin{table}[H]
        \centering
        \begin{tabular}{lSSS}
            \toprule
            Fluide
                & {$c$ (\si{\meter\per\second})}
                    & {$D$ (\si{\meter\squared\per\second})}
                        & {$\flatfrac{c^2}{4D}$ (\si{\hertz})}
            \\
            \midrule
            Air
                & 340
                    & 2e-5
                        & 1e10
            \\
            Eau
                & 1500
                    & 1e-7
                        & 5e12
            \\
            \bottomrule
        \end{tabular}
    \end{table}

\section{Propagation des ondes planes \cite{dunodpsi}}

    \subsection{Relation de dispersion}

    En représentation harmonique
    l'équation de \propername{D'Alembert} donne,
    avec~:
    \begin{equation*}
        \z{p_1}
        = p_{10} e^{i (\omega t - \vb{k} \vb{r})}
        \qquad
        \z{\vb{v}_1}
        = \vb{v}_{10} e^{i (\omega t - \vb{k} \vb{r})}
        \qquad
        \z{\mu_1}
        = \mu_{10} e^{i (\omega t - \vb{k} \vb{r})}
    \end{equation*}
    pour la convention de signe,
    puis~:
    \begin{equation*}
        \omega^2 \z{p_1} - \frac{\vb{k}^2}{c^2} \z{p_1}
        = 0
    \end{equation*}
    d'où
    la relation de dispersion~:
    \begin{equation*}
        c = \pm \frac{\omega}{k}
    \end{equation*}
    le signe est à choisir
    selon la direction de propagation.
    La propagation
    est ici
    non dispersive,
    ce résultat
    est conséquence
    de notre hypothèse sur
    la linéarité de l'équation d'état.

    \subsection{Caractère longitudinal}

    L'équation d'\propername{Euler}
    au premier ordre
    devient~:
    \begin{equation*}
        \mu_0 \omega \z{\vb{v}_1}
        = \vb{k} \z{p_1}
    \end{equation*}
    les vecteurs
    $\vb{v}_1$
    (qui représente le déplacement des particules fluide)
    et $\vb{k}$
    (vecteur d'onde)
    sont colinéaires~:
    les ondes acoustiques
    dans les fluides
    sont longitudinales.

    \subsection{Énergie cinétique et potentielle}

    Nous avons établi
    la relation
    $\mu_0 \omega v_1 = k p_1$
    qui donne
    un lien
    entre la surpression
    et la vitesse des particules~:
    \begin{equation*}
        v_1
        = \frac{k}{\omega \mu_0} p_1
        = \frac{1}{c \mu_0} p_1
        = \sqrt{\frac{\chi_s}{\mu_0}} p_1
    \end{equation*}
    de cela
    on déduit~:
    \begin{equation*}
        e_c
        = \frac{1}{2} \chi_s p_1^2
        = \frac{1}{2} \mu_0 v_1^2
        = e_p
        = \frac{e}{2}
    \end{equation*}
    Pour les OPPH
    l'énergie est équitablement répartie
    entre l'énergie potentielle
    et l'énergie cinétique.
    En valeur moyenne
    pour la densité d'énergie
    on a localement~:
    \begin{equation*}
        \avg{e}
        = \avg{\mu_0 v_1^2}
        = \frac{1}{2} \mu_0 v_{10}^2
        = \frac{1}{2} \chi_s p_{10}^2
    \end{equation*}

    \subsection{Impédances, dioptres}

    \begin{draft}
        Normalement
        on a pas le temps d'arriver là\dots
    \end{draft}
