\chapter{Phénomènes interfaciaux impliquant des fluides}

\niveau{L3}

\prereqstart
{Énergie interne}
{Potentiels thermodynamiques}
\prereqstop

\begin{manip}
    Trombonne dans l'eau~:
    existance d'une force à la surface.

    Cadre avec film de savon~:
    diminution des surfaces.

    Deux gouttes d'eau sur deux surfaces~:
    elles n'ont pas la même forme.
\end{manip}

Dans cette leçon,
nous étudions les \emph{interfaces}~:
il s'agit de régions de l'espace
où plusieurs phases sont en contact.
Par un modèle simple
nous expliquerons
les phénomènes physiques
mis en jeu dans les manips précédentes.
Autre chose à noter,
en lien avec ce sujet~:
en chimie
on lit les volumes
«~au bas du ménisque~»,
mais pourquoi
le liquide monte-t-il aux parroies~?

\section{La tension superficielle}

    \subsection{Origine physique}

    \subsubsection{Description énergétique \cite{dimeglio} \cite{degennes}}

    On considère
    un liquide sur réseau,
    constitué de $N_x \cdot N_y \cdot N_z$ molécules
    de taille $a$~;
    entre chaque paire de molécules voisines
    existe
    une énergie liante $- \epsilon < 0$.

    Pour calculer l'énergie du liquide,
    on le découpe en tranches
    de $N_y \cdot N_z$ molécules.
    L'une de ces tranches
    interagit avec ses deux voisines
    par une énergie $- \epsilon N_y N_z$,
    puisqu'il y a $(N_x - 1)$ paires de tranches
    alors l'ensemble de ces tranches
    implique une énegrie d'interaction
    $- \epsilon (N_x - 1) N_y N_z$.
    En découpant de manière similaire
    le liquide
    selon les deux autres axes,
    on calcule une énergie d'interaction~:
    \begin{align*}
        E
        &= - \epsilon ((N_x-1) N_y N_z + (N_y-1) N_x N_z + (N_z-1) N_x N_y) \\
        &= - \epsilon (3 N_x N_y N_z - N_y N_z - N_x N_z - N_x N_y)
    \end{align*}
    En notant $V = a^3 N_x N_y N_x$ le volume du liquide,
    et $\Sigma = 2 a^2 (N_x N_y + N_y N_z + N_x N_z)$ sa surface,
    on obtient~:
    \begin{equation*}
        E
        = - \epsilon \qty(\frac{3 V}{a^3} - \frac{\Sigma}{2a^2})
        = - \frac{3 \epsilon}{a^3} V + \frac{\epsilon}{2a^2} \Sigma
    \end{equation*}
    on constate que
    $\pdv{E}{\Sigma} > 0$~:
    les molécules
    à la surface
    manquent
    d'énergie liante
    par rapport
    à celles du volume.
    Autrement dit,
    augmenter la surface du liquide
    coûte de l'énergie.
    En consquence,
    un système physique
    isolé d'autres interactions
    et étant stable
    dans les états d'énergies minimales,
    va minimiser sa surface.
    \begin{todo}
        Revenir sur les expériences introductives~:
        trombonne,
        cadre.
    \end{todo}
    Notons que
    pour un volume donné
    la sphère
    est la forme géométrique
    qui minimise la surface~:
    les gouttes d'eau
    isolées ou pseudo-isolées
    (par exemple dans l'ISS)
    adoptent une forme sphérique.

    L'énergie de tension superficielle
    $\gamma = \frac{\epsilon}{2 a^2} > 0$
    dépend des interactions inter-moléculaires~:

    \begin{table}[H]
        \centering
        \begin{tabular}{rSlS}
            \toprule
            Liaison
                & {$\epsilon (\si{\eV})$}
                    & Liquide
                        & {$\gamma (\si{\milli\joule\per\meter\squared})$} \\
            \midrule
            \propername{Van Der Waals}
                & 0.02
                    & Huile
                        & 20 \\
            Hydrogène
                & 0.5
                    & Eau
                        & 72 \\
            Métalique
                & 1
                    & Mercure
                        & 500 \\
            \bottomrule
        \end{tabular}
    \end{table}

    Cette énergie
    contribue à l'énergie interne
    des systèmes thermodynamiques.
    Elle est souvent négligée
    car le terme de volume domine,
    mais dans certains systèmes
    sa contribution est importante.

    \subsubsection{Description dynamique \cite{degennes}}

    On sait maintenant
    qu'il faut apporter de l'énergie
    pour ajouter des molécules à la surface.
    Cela revient
    à fournir un travail,
    donc une force.
    Pour augmenter la surface
    de $\dd \Sigma$,
    on doit fournir le travail
    $\delta W = \gamma \dd \Sigma$.
    Considérons un exemple simple~:
    \begin{todo}
        Baguette de verre \cite{degennes},
        film de savon~:
        $\delta W = F \dd x = 2 \gamma l \dd x = 2 \gamma \dd \Sigma$.
    \end{todo}

    \subsection{Loi de Laplace \cite{diu}}

    Comme mentionné plus haut,
    la l'énergie superficielle
    contribue à l'énergie interne
    des systèmes thermodynamiques.

    Considérons alors
    une goutte d'eau
    sphérique
    de rayon $R$
    (volume $V$, surface $\Sigma$)
    évoluant
    dans une atmosphère
    à la pression $P_0$
    thermostatée
    à la température $T_0$.
    La différentielle
    de l'énergie interne $U$
    s'écrit~:
    \begin{equation*}
        \dd U
        = \gamma \dd \Sigma - P \dd V + T \dd S
    \end{equation*}
    On montre que
    l'enthalpie libre externe~:
    \begin{equation*}
        G_0
        = U + P_0 V - T_0 S
    \end{equation*}
    agit comme potentiel thermodynamique
    pour l'étude de ce système.
    Sa différentielle s'exprime~:
    \begin{align*}
        \dd G_0
        &= \gamma \dd \Sigma - (P - P_0) \dd V + (T - T_0) \dd S \\
        &= \gamma 2 \pi R \dd R - (P - P_0) 4 \pi R^2 \dd R + (T - T_0) \dd S\\
        &= 2 \pi \qty[\gamma - 2 (P - P_0) R] R \dd R + (T - T_0) \dd S
    \end{align*}
    Alors à l'état d'équilibre $\dd G_0 = 0$ donc~:
    \begin{equation*}
        \eval{\pdv{G_0}{S}}_R = 0
        \qq{et}
        \eval{\pdv{G_0}{R}}_S = 0
    \end{equation*}
    donc, $T = T_0$
    et~:
    \begin{equation*}
        P - P_0
        = \frac{2\gamma}{R}
        > 0
    \end{equation*}

    La pression
    dans la goutte
    est supérieure
    à la pression ambiante,
    mais diminue
    lorsque $R$ augmente.

    \begin{manip}
        Bulles de savon reliées,
        penser à faire la différence
        entre bulle et goutte.
    \end{manip}

\section{Mouillage}

    \subsection{Types de mouillage}

    Total, partiel, nul. Angle de description.

    \subsection{Loi de Young-Dupré}


    Bilan des forces \cite{degennes} sans définir S. Comparer les gamma et
    discuter de l'angle. Ne pas oublier la réaction du support.

  \section{Capilarité}

    \subsection{Longueur capilaire \cite{agregchimieb2013}}

    Nous avons ici
    discuté de gouttes sphériques,
    en avancant que
    la sphère était la forme la plus stable.
    Pourant,
    nous observons que
    toutes les gouttes
    ne sont pas sphériques.
    Notre modèle
    présentait la tension superficielle
    comme seule énergie potentielle,
    mais sur Terre
    la gravité joue un rôle important,
    voyons
    sur quel ordre de grandeur
    elle est en effet négligeable.

    On considère
    une goutte sphérique
    de masse volumique uniforme,
    baignant dans
    l'atmosphère
    de masse volumique uniforme
    dans laquelle la pression
    obéit à la loi de l'hydrostatique.
    \begin{todo}
        Faire le schéma de \cite[]{}.
    \end{todo}


    \subsection{Forme des gouttes}



    \subsection{Méniques}



    \subsection{Loi de Jurin}
