\chapter{Lois de conservation en dynamique}

    \propername{Aristote} pensait que
    pour qu'il y ait un movement,
    il fallait des forces.
    \propername{Newton} à corrigé cela
    en disant que
    les forces sont là
    pour modifier le mouvement.
    L'erreur d'\propername{Aristote}
    semble naturelle
    dans la mesure où,
    sur Terre,
    des forces «~invisibles~» dissipatives
    stoppent systématiquement
    les objets qui se déplacent.

  \section{Lois de conservation}

    On considère une particule
    en $M$
    de quantité de mouvement $\vb{p}$
    sur lesquels s'appliquent des forces
    de résultante $\vb{F}$
    évoluant dans un référentiel galiléen
    dans lequel on place un repère fixe,
    d'origine $O$.
    On définit le rayon vecteur~:
    $r = \va{OM}$.

    \subsection{Quantité de mouvement}

    La seconde loi de \propername{Newton} énnonce que~:
    \begin{equation*}
        \dv{\vb{p}}{t} = \vb{F}
    \end{equation*}

    Si le vecteur $\vb{F}$ est nul,
    alors la quantité de mouvement $\vb{p}$
    est constante,
    on dit qu'elle est conservée.
    $\vb{F}$ peut être nul
    si aucune force ne s'exerce sur le système,
    ou si les forces qui s'y exercent se compensent.
    Autrement,
    pour $\vb{F} \neq \vb{0}$,
    la seconde loi de \propername{Newton}
    est en accord avec cette observation~:
    les forces
    modifient
    le mouvement.

    \subsection{Moment cinétique}

    \begin{manip}
        Sur coussin d'air barycentre et angle avec deux mobiles liés.
    \end{manip}

    Le moment cinétique s'écrit~:
    \begin{equation*}
        \vb{L}_O \defeq \vb{r} \cross \vb{p}
    \end{equation*}
    En dérivant cette écriture par rapport au temps
    on obtient~:
    \begin{equation*}
        \dv{\vb{L_O}}{t} = \vb{r} \cross \vb{F}
    \end{equation*}

    Le moment cinétique
    est conservé
    si le produit vectoriel
    du membre de droite
    en nul,
    c'est à dire si~:
    \begin{equation*}
        \left\{\vb{F} = \vb{0} \qq{ou} \vb{r} \parallel \vb{F}\right\}
        \iff \mathcal{M}_O(\vb{F}) = \vb{0}
    \end{equation*}

    \subsection{Énergie mécanique}

    On considère
    le vecteur $\vb{r}(t)$
    et le vecteur $\vb{r}(t + \dd t)$,
    dont la différence donne le vecteur
    $\vb{\dd l} = \vb{v} \cdot \dd t$.

    Le travail des forces exercées sur la particule
    évoluant sur une trajectoire de $A$ à $B$
    s'écrit~:
    \begin{equation*}
        W = \int_A^B{\vb{F}\dotproduct\vb{\dd l}}
    \end{equation*}
    Cette première égalit
    semble indiquer un cas singulier puisque
    certaines forces
    peuvent s'exprimer sous la forme~:
    $\vb{F_c}(M) = -\grad E_p
    \iff
    \int_A^B{\vb{F_c} \vb{\dd l}} = \eval{E_p(M)}_A^B$.
    Ces forces sont dites
    \emph{forces conservatives}
    ou
    \emph{forces dérivant d'un potentiel}.
    Nous examinerons leur cas dans un instant.

    Reprenons~:
    \begin{equation*}
        W
        = \int_A^B{\vb{F}\dotproduct\vb{\dd l}}
        = \int_A^B{\dv{\vb{p}}{t'} \dotproduct \vb{v} \dd t'}
        = \int_A^B{m \dv{\vb{v}}{t'} \dotproduct \vb{v} \dd t'}
        = \int_A^B{m \vb{v} \dotproduct \dd \vb{v}}
        = \eval{\frac{1}{2} m \vb{v}^2}_A^B
        = \Delta E_c
    \end{equation*}
    Nous venons d'établir le
    théorème de l'énergie cinétique.

    Notons désormais
    $\vb{F_c}$ la résultante des forces conservatives
    appliquées sur la particule
    $\vb{F_{nc}}$ la résultante des autres forces,
    et reprennons le calcul~:
    \begin{equation*}
        W
        = \int_A^B{(\vb{F_c} + \vb{F_{nc}}) \dotproduct\vb{\dd l}}
        = -\int_A^B{\grad E_p \vb{\dd l}} + \int_A^B{\vb{F_{nc}} \vb{\dd l}}
        = -\Delta E_p + W_{nc}
    \end{equation*}
    Finalement,
    \begin{equation*}
        \Delta E_c
        = - \Delta E_p + W_{nc}
        \implies
        \Delta (E_m \defeq E_c + E_p)
        = W_{nc}
    \end{equation*}
    Il vient
    qu'en l'abscence de forces non conservatives
    l'énergie mécanique est une grandeur conservée,
    l'appelation \emph{forces conservatives}
    est ainsi justifiée.

    Les forces non conservatives
    sont des forces qui vont
    modifier l'énergie mécanique de notre particule,
    elles peuvent être \emph{motrices}
    si leur travail est positif~:
    elles font \emph{gagner} de la quantité de mouvement
    dans le cas contraire elles font
    perdre de la quantité de mouvement.

    La différence fondamentale
    entre forces conservatives
    et forces non conservatives
    est que
    le travail des forces non conservatives
    pour un déplacement de $A$ à $B$
    dépend du chemin suivi,
    alors que celui des forces conservatives
    n'en dépend pas.
    On donne comme bon exemple de force non conservatives
    les forces de frottements.
    Dans un véhicule
    elles sont compensées
    par des forces motrices
    générées par le moteur
    utilisant du carburant.
    Pour aller d'ici à la pièce voisine,
    la quantité de carburant consomée
    ne sera pas la même
    si j'y vais directement
    ou si je passe par \propername{Tokyo}.

  \section{Intéret des lois de conservations}

    Les lois de conservations
    permettent de résoudre des problèmes de mécanique
    sans intégrer les équations du mouvement.

    \subsection{Choc élastique}

    \subsection{Pendule ballistique}

    \subsection{Diffusion de \propername{Rutherford}}

    \subsection{Problème de \propername{Képler}}

    \subsection{Le pendule~: portrait de phases sans calculs}

\plusloin{
    \begin{todo}
        Problème à 3 puis n corps.
    \end{todo}
    \begin{todo}
        Bref récapitulatif
        sur la mécanique lagrangienne.
        Théorème de \propername[Emmy]{Noether}.
    \end{todo}
}
