\chapter{Induction électromagnétique}

\begin{draft}
    On ne dessine
    qu'une seule spire
    pour modéliser les bobines.
\end{draft}

\begin{draft}
    On insiste
    sur les conventions de signe
    choisies dans la leçon
    mais,
    en se référant aux expériences
    on fait attention
    à ne pas donner une idée
    de \emph{positif} ou \emph{négatif} absolu.
    La fem
    \emph{change de signe}
    lorsque l'on change
    le pôle de l'aimant.
\end{draft}

\niveau{PCSI (MPSI, PTSI, TSI)}

\prereqstart
{Électrocinétique~: bobines}
{Champ magnétique d'un aimant, d'une bobine}
{Action d'un champ magnétique sur un moment magnétique}
{Action d'un champ magnétique sur un circuit~: force de \propername{Laplace}}
\prereqstop

\begin{manip}
    On fait tourner un alternateur
    relié à une petite ampoule~:
    elle s'allume.
\end{manip}

\begin{manip}
    On approche un aimant
    d'un anneau en aluminium
    fixé à une tige en bois pivotante~:
    l'anneau s'éloigne
    (expérience de \propername{Faraday}).
\end{manip}

\begin{manip}
    On dispose
    deux bobines
    côtes à côtes et sur le même axe,
    l'une des deux
    étant alimentée par un générateur alternatif~:
    une tension est mesurée
    aux bornes de l'autre.
\end{manip}

\begin{manip}
    On approche
    le pôle Nord d'un aimant
    vers une bobine~:
    une différence de potentiel
    apparaît à ses bornes.
\end{manip}

\begin{manip}
    On fait l'expérience
    du rail de \propername{Laplace}
    avec les deux tiges mobiles.
\end{manip}

Jusqu'ici
dans le cours de physique
nous n'avons jamais vu d'explication
aux phénomènes présentés ici.
Les expériences
semblent assez différentes
mais
nous allons voir
dans cette leçon
qu'elle sont toutes expliquées
par la connaissance
d'une nouvelle notion~:
l'induction électromagnétique.

Ce phénomène
qui lie le magnétisme
à l'électrocinétique
va permettre d'expliquer
ces expériences
ainsi que le fonctionnement
des transformateurs,
des chargeurs sans fils,
ou des puces NFC.
Comme illustré
dans les expériences
la mécanique
s'associe parfois
au magnétisme et à l'électrocinétique~:
dans ce cas
on peut étudier
la conversion électromécanique
d'énergie
et explique
le fonctionnement
des moteurs,
des alternateurs,
ou des haut-parleurs.
Cette leçon
est riche en applications.

\begin{draft}
    Dans les moteurs
    à courant continu
    ou
    synchrones,
    il n'y a pas
    de courant induits.
    C'est seulement le cas
    dans les moteurs
    asynchrones.
\end{draft}

\section{Lois de l'induction \cite{dunodpcsi}}

    \subsection{Force électromotrice induite, loi de \propername{Faraday}}

    Dans les expériences introductives,
    la variation d'une quantité
    liée au champ magnétique $\vb{B}$
    produit un courant
    dans le circuit fermé.
    L'amplitude de ce courant
    va dépendre des autres constituants du circuit,
    mais des expériences quantiatives
    montreraient qu'elle est liée
    à la variation du \emph{flux}
    de $\vb{B}$
    à travers chaque spire la bobine.

    La loi de \propername{Faraday}
    indique que le courant est égal
    à celui que produirait un générateur
    dont la force électromotrice (fem) serait~:
    \begin{equation*}
        \qq{et}
        e_\textrm{bobine}
        = N e_\textrm{spire}
        \qq{avec}
        e_\textrm{spire}
        = -\dv{\Phi}{t}
        \qq{et}
        \Phi
        = \iint_S \vb{B}\dd\vb{S}
    \end{equation*}
    la surface $S$
    est orientée
    et les valeurs
    de la fem
    et
    du flux
    sont algébriques.
    L'orientation de la surface
    donne une orientation à son contour,
    le courant
    et la fem
    sont orientés dans ce sens,
    en convention générateur.

    \begin{manip}
        Refaire l'expérience
        avec l'aimant et la bobine
        en montrant
        l'approche de chaque pôle.

        Les lignes de champ
        sortent du pôle nord de l'aimant
        donc
        si l'on entre ce pôle (resp. l'autre) dans la bobine,
        le flux augmente (resp. diminue)
        donc la fem est négative (resp. positive).
    \end{manip}

    \begin{manip}
        Refaire l'expérience
        d'induction mutuelle
        en changeant la fréquence
        au générateur.
        Puis avec un signal triangle.
    \end{manip}

    \begin{draft}
        \paragraph{Induction de \propername{Neumann} \cite{feynmanem1}}
        La loi de \propername{Faraday}
        vient de l'équation de \propername{Maxwell-Faraday}~:
        \begin{equation*}
            \curl\vb{E}
            = - \pdv{B}{t}
        \end{equation*}
        La fem
        entre deux points d'un circuit
        n'est autre que
        l'intégrale~:
        \begin{equation*}
            e
            = \int_C \vb{E} \dotproduct \dd\vb{l}
        \end{equation*}
        or
        le théorème de \propername{Stockes}
        donne~:
        \begin{equation*}
            e
            = \int_C \vb{E} \dotproduct \dd\vb{l}
            = \int_S (\curl\vb{E}) \dotproduct \dd\vb{S}
            \implies
            e
            = - \int_S \pdv{\vb{B}}{t} \dotproduct \dd\vb{S}
        \end{equation*}
        pour une surface $S$ fixée
        on peut sortir
        la dérivée par rapport au temps
        de l'intégrale sur l'espace~:
        \begin{equation*}
            e
            = - \dv{t}(\int_S \vb{B} \dotproduct \dd\vb{S})
            = - \dv{\Phi}{t}
        \end{equation*}

        \paragraph{Induction de \propername{Lorentz}}
        Elle concerne les cas
        où le champ $\vb{B}$ est constant
        mais la surface $S$ change dans le temps.
        Dans ce dernier cas,
        les électrons
        animés d'une vitesse $\vb{v}$
        subissent
        la partie magnétique de la force de \propername{Lorentz}~:
        \begin{equation*}
            \vb{F}
            = q \vb{v} \crossproduct \vb{B}
        \end{equation*}
        perpendiculaire à $\vb{v}$
        à laquelle
        on associe
        le champ électromoteur~:
        $\vb{E}_m = \flatfrac{\vb{F}}{q}$.
        Le fil
        étant orienté selon $\dd\vb{l}$,
        on associe
        au champ
        la fem~:
        \begin{align*}
            e
            = \int_C \vb{E}_m \dotproduct \dd\vb{l}
            &= \int_C \qty(\vb{v} \crossproduct \vb{B}) \dotproduct \dd\vb{l}
            \\
            &= - \int_C \qty(\vb{v} \crossproduct \dd\vb{l}) \vb{B}
            \\
            &= - \int_C \vb{B} \qty(\dv{\vb{x}}{t} \crossproduct \dd\vb{l})
        \end{align*}
        comme $\vb{B}$ et $\dd\vb{l}$
        sont indépendants du temps~:
        \begin{align*}
            e
            = - \int_C \vb{B} \qty(\dv{\vb{x}}{t} \crossproduct \dd\vb{l})
            = - \frac{1}{\dd t} \int_C \vb{B} \qty(\dd\vb{x} \crossproduct \dd\vb{l})
            = - \frac{1}{\dd t} \int_C \vb{B} \dd\vb{S}
            = - \frac{1}{\dd t} \dd\Phi
        \end{align*}

        (Attention,
        les charges mises en mouvement
        par le champ
        vont avoir une composante de vitesse
        générant un effet \propername{Hall} dans les fils
        rapidement compensé.)
    \end{draft}

    \subsection{Courant induit et champ magnétique, loi de \propername{Lenz}}

    Nous avons déjà vu
    que la présence
    d'un courant dans une bobine
    entrainait la création
    d'un champ magnétique.

    Le champ magnétique
    créé par une bobine (infinie)
    est~: $\vb{B} = \mu_0 i n \vb{u}$,
    une analyse
    pas-à-pas
    du phénomène
    sur un schéma orienté
    montre que
    le courant qui s'établit
    dans le même sens que la fem
    va provoquer
    la création d'un champ magnétique
    orienté dans le sens opposé
    à celui de l'aimant qui s'approche
    (si l'on approche un nord d'un côté,
    la bobine créé un nord du même côté).
    En conséquence,
    l'intensité du champ magnétique
    dans la bobine
    va diminuer,
    et le flux aussi.

    La loi de \propername{Lenz}
    précise que
    les effets de l'induction
    sont tels que
    ces effets
    s'opposent toujours
    à la cause qui leur donne naissance.

    La discussion
    sur le flux
    est correcte
    mais difficile à appréhender~:
    on ne voit pas
    le champ magnétique~:
    raisonons plutôt
    sur la mécanique.
    En poursuivant le raisonement
    qui mêne à l'orientation
    du courant induit
    on peut calculer
    la force de \propername{Laplace}
    ($\dd \vb{F} = i \dd \vb{l} \crossproduct \vb{B}$)
    exercée par l'aimant
    sur la bobine.
    On concluera
    que cette force
    pousse la bobine
    loin de l'aimant~:
    les effets de l'induction
    s'opposent ici
    à l'approche de l'aimant
    qui créé ces effets.
    C'est ce que l'on observe
    dans l'expérience avec l'anneau d'aluminum.

    Dans l'expérience
    du rail de \propername{Laplace}
    on essaye d'augmenter le flux
    en augmantant la surface du circuit~:
    le système répond
    en s'opposant à cette augmentation.

    \begin{draft}
        On peut exprimer
        la force exercée
        entre l'aimant et une spire.
        Cette force
        est liée
        à l'interaction dipôlaire
        entre les moments magnétiques~:
        \begin{itemize}
            \item l'aimant génère
                un champ $\vb{B}_a$
                et porte un moment dipolaire $\vb{m}_a$,
            \item la bobine polarisée
                génère un champ $\vb{B}_b$
                porte un moment dipôlaire $\vb{m}_b = i \vb{S}$
                (que l'on assure constant ici)
        \end{itemize}

        L'énergie potentielle
        de l'aimant
        dans le champ magnétique de la bobine
        est~:
        $E_p = - \vb{m}_a \dotproduct \vb{B}_b$.

        À cette énergie
        on associe une force~:
        $\vb{F} = -\grad E_p = \grad(\vb{m}_a \dotproduct \vb{B}_b)$.

        Comme les deux vecteurs
        sont de sens opposés,
        si l'on fixe l'angle d'approche à nord contre nord,
        le produit scalaire
        est négatif~:
        $\vb{F} = - m_a \grad(B_b)$.
        La force est dirigée
        vers les faibles valeurs
        de $B_b$~:
        loin de la bobine.
    \end{draft}

    \subsection{Principe de relativité}

    Ici
    les expériences sont faites
    en déplacant l'aimant,
    la bobine étant fixe.
    On peut refaire ces expériences
    en fixant l'aimant
    et déplacant la bobine
    pour retrouver
    les mêmes résultats.

    \begin{draft}
        Les champs électrique et magnétique
        ne se transforment pas trivialement
        par changement de référentiel.
        On raisonne
        sur la force.
    \end{draft}

    \subsection*{*}

    Dans les sections suivantes
    nous préciserons des effets
    qui découlent de cette loi
    et les illustrerons
    par des applications technologiques.

\section{Induction mutuelle et autoinduction \cite{dunodpcsi}}

    On étudie dans cette section
    le cas
    de circuit fixe
    dans un champ magnétique qui dépend du temps.

    \subsection{Inductance mutuelle}

    \begin{manip}
        Remontrer l'expérience
        d'induction mutuelle.
    \end{manip}

    On considère maintenant
    deux circuits électriques.
    Le circuit $1$
    composé d'une bobine
    est parcouru par le courant $i_1$
    et créé donc un champ magnétique $\vb{B_1} \propto i_1 \vb{u}$
    dont les lignes de champ
    sont enlacées par une bobine
    dans le circuit $2$.
    Le circuit $1$
    créé donc un flux $\Phi_{12}$
    dans cette bobine.

    Dans le cas général
    l'expression de $\Phi_{12}$ est compliquée
    mais comme $\vb{B_1}$
    est proportionnel à $i_1$
    on pourra dire que~:
    \begin{equation*}
        \Phi_{12}
        = M_{12} i_1
    \end{equation*}

    La situation inverse
    est vraie égallement
    de sorte que
    le circuit $2$
    créé un flux
    dans le circuit $1$~:
    \begin{equation*}
        \Phi_{21}
        = M_{21} i_2
    \end{equation*}

    Le théorème de \propername{Neumann}
    indique que
    ces deux coefficients
    sont égaux
    (ils dépendent essentiellement
    de caractéristiques géométriques)~:
    on appelle \emph{inductance mutuelle}
    la grandeur
    $M = M_{12} = M_{21}$.
    Elle s'exprime
    en
    henrys~:
    $\si{\henry}
    = \si{\weber\per\ampere}
    = \si{\tesla\meter\squared\per\ampere}$.
    \begin{todo}
        Trouver la démonstration
        de ce théorème.
        Attention
        il n'est peut-être pas vrai
        dans le cas général.
    \end{todo}
    \begin{draft}
        $\si{\weber}
        \defeq \si{\volt\second}$
        et
        $\si{\tesla}
        \defeq \si{\weber\per\meter\squared}$
        et
        $\si{\henry}
        \defeq \si{\volt\per\ampere\second}$
    \end{draft}
    Connaissant ces flux
    on peut exprimer
    les fem induites
    par mutuelle induction
    dans l'un et l'autre circuit~:
    \begin{equation*}
        e_{12}
        = - \dv{\Phi_{12}}{t}
        = - M_{12} \dv{i_1}{t}
        \qq{et}
        e_{21}
        = - \dv{\Phi_{21}}{t}
        = - M_{21} \dv{i_2}{t}
    \end{equation*}

    On constate que
    par l'établissement d'un courant variable
    dans un circuit
    on peut transmettre
    de l'information
    ou de l'énergie
    dans un autre circuit.

    \subsection{Autoinductance}

    L'expression type
    $e = -M \dv*{i}{t}$
    doit rappeller
    la modélisation électrique
    des bobines
    où~:
    $e = L \dv*{i}{t}$.
    La ressemblance
    n'est pas un hasard.

    Dans l'étude précédente
    nous avons cherché
    à modéliser en termes d'électrocinétique
    les effets d'induction
    de la bobine $1$ sur la bobine $2$
    via la variations de flux magnétique $\Phi_{12}$
    mais,
    nous n'avons pas regardé
    les effet
    de la variation du flux magnétique
    créé par la bobine $1$
    dans la bobine $1$.

    En effet,
    le champ magnétique $B_1$
    créé par le courant $i_1$
    est aussi présent
    dans la bobine $1$
    et va créer
    des effets d'inductions
    dits d'\emph{autoinduction}.
    On peut calculer
    le flux $\Phi_{11}$
    créé par la bobine
    dans elle même~:
    comme toute à l'heure
    il n'est pas forcément trivial
    mais peut s'exprimer
    par proportionalité~:
    $\Phi_{11} = L_1 i_1$.

    Les variations de ce flux
    crééent une fem \emph{autoinduite}~:
    \begin{equation*}
        e_{11}
        = - L_1 \dv{i_1}{t}
    \end{equation*}

    Rappelons que
    dans cette modélisation
    la bobines est traitée
    comme un générateur (de fem $e_{11}$),
    d'où le signe opposé
    à celui usuel,
    quand la bobine est traitée
    comme un récepteur (tension aux bornes $u_L = L \dv*{i_1}{t} = -e_{11}$).

\section{Électrocinétique de deux circuits couplés \cite{dunodpcsi}}

    \subsection{Schémas électriques équivalents}

    \begin{manip}
        On s'appuie à nouveau
        sur l'expérience d'induction mutuelle.
    \end{manip}

    \begin{todo}
        Faire les schémas
        avec les générateurs
        mais aussi
        avec les bobines en mutuelle induction
        (flèche courbée).
    \end{todo}

    Dans une situation
    ou les deux bobines interagissent,
    aucun effet
    n'est négligeable devant l'autre.
    En conséquences,
    la fem
    qui existe au bornes de chacune des bobine
    traitée comme générateur
    correspond à la somme~:
    \begin{align*}
        e_1
        &= e_{12} + e_{11}
        = - M \dv{i_2}{t} - L_1 \dv{i_1}{t}
        \\
        e_2
        &= e_{21} + e_{22}
        = - M \dv{i_1}{t} - L_2 \dv{i_2}{t}
    \end{align*}
    On modélise les bobines~:
    soit par des générateurs (symbole d'une source de tension)
    avec ces fem,
    soit par des dipoles récepteurs (symbole d'une inductances)
    avec les fem opposées.

    Les deux circuits
    sont dit \emph{couplés}.
    Encore une fois,
    on constate que
    par l'établissement d'un courant variable
    dans un circuit
    on peut transmettre
    de l'information
    ou de l'énergie
    dans un autre circuit.

    \subsection{Grandeurs électriques}

    Le circuit $1$
    est composé
    d'un générateur
    et d'une bobine,
    le circuit $2$
    est composé
    d'une bobine fermée sur elle même.
    On tiendra compte
    des réistances internes
    $R_1$ de la bobine $1$ et du générateur
    et
    $R_2$ de la bobine $2$.

    On s'intéresse aux courants
    $i_1$ et $i_2$
    qui circulent.
    Pour les calculer
    on représente les éléments de chaque circuits
    par des éléments parfaits,
    les bobines étant modélisées
    par des sources de fem~:
    \begin{align*}
        0 &= u_g(t) - R_1 i_1(t) + e_1(t)
        \\
        0 &= e_2(t) - R_2 i_2(t)
    \end{align*}
    en remplaçant
    chaque fem
    par son expression~:
    \begin{align*}
        u_g(t) &= R_1 i_1(t) + L_1 \dv{i_1}{t} + M \dv{i_2}{t}
        \\
        0 &= L_2 \dv{i_2}{t} + M \dv{i_1}{t} + R_2 i_2(t)
    \end{align*}

    En régime sinusoidal forcé,
    on impose
    $u_g(t) = E_0 \cos(\omega t)$.
    En représentation harmonique~:
    \begin{align*}
        \z{u_g} &= (R_1 + j\omega L_1) \z{i_1} +  j\omega M \z{i_2}
        \\
        0 &= j\omega M \z{i_1} + (R_2 + j\omega L_2) \z{i_2}
    \end{align*}
    On peut résoudre
    ce système d'équations
    pour obtenir~:
    \begin{align*}
        \z{i_1}
        = - \frac{R_2 + j\omega L_2}{j\omega M} \z{i_2}
        \qq{puis}
        &\z{u_g}
        = \qty[- (R_1 + j\omega L_1)\frac{R_2 + j\omega L_2}{j\omega M} + j\omega M]
            \z{i_2}
        \\
        \qq{sans oublier}
        &\z{e_2}
        = R_2 \z{i_2}
    \end{align*}

    \subsection{Bilan d'énergie}

    Multiplions
    les équations
    temporelles
    couplées
    précédentes
    par $i_1$ et $i_2$~:
    \begin{align*}
        u_g(t) i_1(t)
        &= R_1 i_1(t) + \dv{t}(\frac{1}{2} L_1 i_1^2(t) + M i_1(t) i_2(t))
        \\
        0
        &= R_2 i_2(t) + \dv{t}(\frac{1}{2} L_2 i_2^2(t) + M i_1(t) i_2(t))
    \end{align*}
    on peut les combiner
    pour obtenir~:
    \begin{equation*}
        u_g(t) i_1(t) - R_1 i_1(t) - R_2 i_2(t)
        = \frac{1}{2} \dv{t}(L_1 i_1^2(t) + L_2 i_2^2(t) + 2 M i_1(t) i_2(t))
    \end{equation*}

    La puissance
    fournie par le générateur
    est~:
    \begin{equation*}
        P_g(t)
        = u_g(t) i_1(t)
    \end{equation*}
    La puissance
    dissipée par effet \propername{Joule}
    est~:
    \begin{equation*}
        P_J(t)
        = R_1 i_1(t) + R_2 i_2(t)
    \end{equation*}
    Le membre de droite
    dans le bilan de puissance
    correspond à
    la puissance fournie
    au champ électromagnétique
    par la bobine.
    Le champ emmagasine une énergie
    qui s'écrit~:
    \begin{equation*}
        W
        = \int_{t_0}^{t_1} P_{mag}(t) \dd t
        = \frac{1}{2}
            \qty[L_1 i_1^2(t) + L_2 i_2^2(t) + 2 M i_1(t) i_2(t)]_{t_i}^{t_f}
    \end{equation*}
    c'est cette énergie
    qui est libérée
    lorsque les effets inductifs
    de la bobine
    s'opposent aux variations
    de courant électrique.

\section{Exemples d'applications}

    \begin{todo}
        Se renseigner
        sur le chauffage par induction
        avant d'en parler~:
        il semblerait que
        le champ électrique variable
        qui accompagne le champ magnétique variable
        soit capable
        de retourner les spins
        du ferromagnétique
        et que c'est cet effet
        qui génère le plus
        d'énergie thermique,
        et non
        les courants de \propername{Foucault}.
    \end{todo}

    \subsection{Quelques exemples}

    \paragraph{Radio-étiquettes}
    Les étiquettes RFID («~radio frequency identification~»)
    sont de petits objets
    qui comprennent
    une antenne
    et
    une puce électronique,
    elles sont utilisées
    pour le suivi ou l'identification~:
    \begin{itemize}
        \item d'articles (bibliothèques, magasins)
        \item de véhicules (télépéage, trains)
        \item d'animaux (sous-cutanée ou bague)
        \item de personnes (cartes de transport en commun, cartes d'accès)
    \end{itemize}
    L'antenne sert
    à récupérer de l'énergie
    en se couplant à un émetteur,
    puis à transmettre un signal
    commandé par la puce embarquée.

    \paragraph{Technologie NFC}
    Les appareils mobiles
    peuvent communiquer
    entre eux
    ou avec des radio-étiquettes compatibles
    par une technologie proche de RFID.

    \paragraph{Rechargement sans fil}
    En plaçant
    une batterie rechargeable
    dans le circuit secondaire,
    l'énergie fournie par le générateur
    peut être utilisée
    pour la recharger.

    \subsection{Le transformateur de tension \cite{dunodpcsi}}

    On considère deux bobines
    (idéales, les réistances internes sont hors de vue)
    dans une situation d'induction mutuelle.
    Le circuit primaire ($1$)
    est soumis à une tension alternative $u_1(t)$
    et est parcouru par le courant $i_1$,
    il créé un champ magnétique variable.
    La bobine du circuit secondaire
    qui enlace les lignes du champ
    créé par le primaire,
    va alors induire
    une fem $e_2(t)$ à ses bornes.
    On cherche la relation
    entre la tension $u_1(t)$
    et la tension $u_2(t) = -e_2(t)$.

    Exprimons
    les deux fem~:
    \begin{align*}
        e_1(t)
        &= - \dv{\Phi_{11} + \Phi_{21}}{t}
        = - N_1 S_1 \dv{B}{t}
        \\
        e_2(t)
        &= - \dv{\Phi_{21} + \Phi_{22}}{t}
        = - N_2 S_2 \dv{B}{t}
    \end{align*}

    Par égalité
    du champ magnétique total~:
    \begin{equation*}
        \frac{u_2}{u_1}
        = \frac{N_2 S_2}{N_1 S_1}
        = \frac{N_2}{N_1}
    \end{equation*}
    les transformateurs
    étant souvent concus
    de sorte que
    $S_1 = S_2$.

    En partique
    les transformateurs électriques
    sont utilisés
    tout le long de la chaîne de transport
    de l'énergie électrique~:
    \begin{itemize}
        \item transformateur élévateur,
            en sortie de centrale de production
            pour abaisser l'intensité (en augmentant la tension)~:
            $\SI{20}{\kilo\volt} / \SI{400}{\kilo\volt}$
        \item transformateur ferroviaires~:
            $\SI{400}{\kilo\volt} / \SI{28}{\kilo\volt}$
        \item transformateur de distribution,
            à l'entrée des zones d'utilisation grand public~:
            $\SI{400}{\kilo\volt} / \SI{230}{\volt}$
        \item transformateur domestique
            (ordinateur portable)~:
            $\SI{230}{\volt} / \SI{12}{\volt}$
    \end{itemize}

    \begin{draft}
        On aura en tête
        les puissances typiques
        au niveau de la production~:
        \begin{table}[H]
            \centering
            \begin{tabular}{lS}
                \toprule
                Centale & {Puissance (\si{\mega\watt})} \\
                \midrule
                Éolienne & 200 \\
                Photovoltaïque & 250 \\
                Thermique & 500 \\
                Hydroélectrique & 1000 \\
                Nucléaire & 1600 \\
                \bottomrule
            \end{tabular}
        \end{table}

        On aura aussi en tête
        les pertes associées
        aux transformateurs~:
        \begin{itemize}
            \item par effet \propername{Joule}
            \item par fuites de champ
            \item par courants de \propername{Foucault}
            \item par hystérésis
        \end{itemize}
    \end{draft}


\plusloin{
    À la suite de cette leçon
    on pourra calculer
    une grandeur encore mystérieuse~:
    le coefficient de mutuelle induction $M$
    dans le cas particulier
    de deux bobines infinies
    qui partagent le même axe.

    Dans une leçon suivante,
    nous étudierons
    d'autres situations
    où l'induction électromagnétique
    joue un rôle primordial~:
    la conversion électromécanique de puissance,
    illustrée dans l'une des expériences.
    J'ai fait le choix
    de ne pas traîter ces application
    dans cette leçon
    car c'est le cœur
    de la leçon LP20
    et,
    le BO de PCSI
    suggère de traîter
    «~circuit fixe dans un champ magnétique qui dépend du temps~»
    avant
    «~circuit mobile dans un champ magnétique fixe~».

    Précisons que
    l'on retrouvera
    l'étude du transformateur
    en classe de PSI.
}
