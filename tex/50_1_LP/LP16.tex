\chapter{Facteur de \propername{Boltzmann}}

    La physique statistique
    dans l'ensemble microcanonique
    permet l'étude de système isolés et fermés.
    Dans cette leçon
    nous généralisons
    vers des systèmes qui peuvent
    échanger de l'énergie thermique avec un thermostat.

  \section{Loi de \propername{Boltzmann}}

    \subsection{Systèmes}

    On considère
    un thermostat $\tau$
    de température $T_\tau$
    d'énergie interne $E_\tau$,
    en contact avec
    le système $s$
    de température $T = T_\tau$
    d'énergie interne $E$.
    Le thermostat
    est constitué d'un nombre de particules
    grand devant celui du système
    ($N_\tau \gg N$).
    Puisque l'énergie interne de chaque système est extensives,
    on pourra dire que $E_\tau \propto N_\tau \gg N \propto E$.

    L'ensemble $\mathcal{S} = \{s \cup \tau\}$ est isolé,
    et a une énergie interne $E_\mathcal{S} = E + E_\tau$.

    \subsection{Thermostat}

    La température microcanonique $T_\tau^\ast$ du thermostat
    s'exprime par~:
    \begin{equation*}
        \frac{1}{T_\tau^\ast}
        = \eval{\pdv{S_\tau(E_\tau = E_\mathcal{S} - E)}{E_\tau}}_V
        \approx
        \eval{\pdv{S_\tau(E_\mathcal{S})}{E_\tau}}_V
        - E \eval{\pdv[2]{S_\tau(E_\mathcal{S})}{E_\tau}}_V
        \approx
        \eval{\pdv{S_\tau(E_\mathcal{S})}{E_\tau}}_V
    \end{equation*}
    on en déduit que $T_\tau^\ast$
    est indépendante de l'énergie du système $s$~:
    le thermostat peut échanger de l'énergie thermique avec $s$
    sans que sa température ne varie.

    \subsection{Statistique}

    Notons $(l)$, $(\lambda)$ les états microscopiques de $s$, $\tau$
    à l'énergie $E_l$, $E_\lambda$.
    Le système $\mathcal{S}$ est décrit
    par la donnée d'un état $(l, \lambda)$
    d'énergie $E_\mathcal{S} = E_l + E_\lambda$.
    Comme ce système est isolé,
    nous pouvons l'étudier à travers une description microcanonique,
    et ainsi dire que la probabilité
    de trouver $\mathcal{S}$ dans un microétat $(l, \lambda)$
    est~:
    \begin{equation*}
        \mathbb{P}_{(l, \lambda)}
        = \frac{1}{\Omega_\mathcal{S}(E_\mathcal{S} = E_l + E_\lambda)}
    \end{equation*}
    alors
    en relachant la contrainte explicite
    sur l'état $(\lambda)$ de $\tau$,
    la probabilité de trouver $\mathcal{S}$ dans un état
    où $s$ est dans un microétat $(l)$ d'énergie $E_l$
    (par conservation de l'énergie,
    $\tau$ est dans un état quelconque d'énergie $E_\mathcal{S} - E_l$)
    est~:
    \begin{equation*}
        \mathbb{P}_{(l)}
        \defeq \mathbb{P}_{(l,
            \{\forall (\lambda) \suchas E_\lambda = E_\mathcal{S} - E_l)\}}
        = \Omega_\tau(E_\mathcal{S} - E_l)
            \frac{1}{\Omega_\mathcal{S}(E_\mathcal{S})}
    \end{equation*}
    Vu l'expression statistique de l'entropie
    nous pourrons écrire~:
    \begin{equation*}
        \mathbb{P}_{(l)}
        = \frac{e^{\frac{S_\tau(E_\mathcal{S} - E_l)}{k_B}}}
            {\Omega_\mathcal{S}(E_\mathcal{S})}
        \approx
        \frac{
            e^{\frac{S_\tau(E_\mathcal{S})}{k_B}}
            \cdot
            e^{\frac{-E_l \pdv{S_\tau(E_\mathcal{S})}{E_\tau}}{k_B}}
            }{\Omega_\mathcal{S}(E_\mathcal{S})}
        = \frac{e^{\frac{S_\tau(E_\mathcal{S})}{k_B}}}
            {\Omega_\mathcal{S}(E_\mathcal{S})}
        \cdot
        e^{- \frac{E_l}{k_B T}}
        = \frac{1}{Z} e^{- \frac{E_l}{k_B T}}
    \end{equation*}
    Le dernier facteur est
    le \emph{facteur de \propername{Boltzmann}},
    la grandeur $Z$ dans le premier est
    la \emph{fonction de partition}
    qui dépend de toutes les contraintes externes.

    \begin{itemize}
        \item À température fixée,
            la probabilité de trouver un système
            dans un microétat d'énergie $E_l$
            suit une loi en exponentielle décroissante.
        \item Une augmentation de la température
            favorise la probabilité de trouver le système
            dans un micoétat de plus haute énergie.
    \end{itemize}

    La probabilité de trouver le système
    dans un macroétat d'énergie $E_L$ est~:
    \begin{equation*}
        \mathbb{P}(E_L)
        \defeq \mathbb{P}_{\{\forall (l) \suchas E_l = E_L\}}
        = \Omega_s(E_L) \frac{e^{-\flatfrac{E_L}{k_B T}}}{Z}
    \end{equation*}

    \section{Expérience de \propername[Jean]{Perrin} (1908) \cite{perrin}}

    Physicien français au début des années 1900,
    \propername[Jean]{Perrin}
    consacre une partie de ses travaux
    à la vérification de l'hypothèse atomique
    alors controversée.
    Il cherche à déterminer
    le nombre d'Avogadro $\glssymbol{Na}$
    par plusieurs méthodes expérimentales,
    espérant trouver des valeurs cohérentes.

    L'une des méthodes
    reposes sur l'observation
    de particules sphériques de caoutchouc végétal
    en suspension dans une haute éprouvette remplie d'eau
    et laissée au repos.
    \propername{Perrin} a constaté
    qu'il se réalise un état de régime permanent
    dans lequel la concentration en \emph{sphérules}
    décroît avec la hauteur.
    Expliquons ce phénomène.

    Le principe fondamental de la dynamique,
    appliqué à une sphérule donne~:
    \begin{equation*}
        \vb{F}
        = m_s\vb{g} + \vb{\pi}
        = (\rho_s - \rho_u) \frac{4 \pi r_s^3}{3} \vb{g}
    \end{equation*}
    On pourra exprimer
    l'énergie potentielle de la particule
    à l'altitude $z$~:
    \begin{equation*}
        E_p(z)
        = - \int_0^z{\vb{F}\vu{z}\dd z'}
        = (\rho_s - \rho_e) \frac{4 \pi r_s^3}{3} g z
    \end{equation*}

    En ne considérant que l'énergie potentielle,
    et vu les résultats de la partie précédente,
    la probabilité de trouver la particule
    dans un macroétat d'énergie $E_p(z)$
    s'exprime~:
    \begin{equation*}
        \mathbb{P}(E_p(z))
        = \frac{\Omega}{Z} e^{-\flatfrac{E_p(z)}{k_B T}}
    \end{equation*}
    où $\Omega$ correspond
    au nombre de microétats
    tels que la particule
    se trouve à l'altitude $z$.
    En considérant $N_0$ particules
    identiques
    et indépendantes,
    le nombre de particules à l'altitude $z$
    s'exprime~:
    \begin{equation*}
        N(z)
        = \frac{N_0 \Omega}{Z} e^{-\flatfrac{E_p(z)}{k_B T}}
    \end{equation*}
    \propername{Perrin} donne
    les résultats de mesure,
    à \SI{20}{\degreeCelsius}
    pour $\rho_s = \SI{1.194}{\kilo\gram\per\meter\squared}$
    et $r_s = \SI{0.212}{\micro\meter}$,
    suivants \cite{perrin}~:
    \begin{table}[H]
        \centering
        \begin{tabular}{SS}
            \toprule
            {Altitude (\si{\micro\meter})}
                & {Nombre de sphérules ($\propto$)} \\
            \midrule
            5 & 100 \\
            35 & 47 \\
            65 & 23 \\
            95 & 12 \\
            \bottomrule
        \end{tabular}
    \end{table}
    en remplacant $k_B = \flatfrac{\glssymbol{Na}}{R}$
    dans l'exposant
    et en ajustant un modèle aux valeurs,
    le calcul donnera~:
    \begin{equation*}
        \glssymbol{Na}^\mathrm{1908}
        = \SI{7.5e23}{\per\mol}
    \end{equation*}
    la valeur aujourd'hui définie est de~:
    \begin{equation*}
        \glsfull{Na}
    \end{equation*}

  \section{Capacité calorifique des solides}

    La thermodynamique,
    émergente au début du \century{19}
    introduit des grandeurs intensives
    associées aux matériaux
    caractérisant la variation de température
    observée lors d'un transfert d'énergie thermique.
    On distingue différentes grandeurs
    selon les conditions opératoires,
    et nous nous intéressons ici aux solides
    soumis à des transferts thermiques sous pression constante.
    C'est la capacité calorifique molaire $c_p$
    que nous allons étudier~:
    de nombreuses mesures ont été réalisées
    en 1819 par
    \propername[Louis]{Dulong}
    et \propername[Alexis]{Petit}.
    Ces derniers mirent en évidence
    une remarquable indépendance
    de la capacité calorifique molaire
    au matériaux considéré.

    La loi empirique de \propername{Dulong-Petit}
    donne que~:
    $c_p
    \approx (\num{3.0 +- 0.2}) R
    = \SI{25 +- 2}{\joule\per\kelvin\per\mol}$
    pour la majorité des solides \cite{handbook}.

    \subsection{Théorie cinétique des solides}

    Un modèle simple des solides
    permet de retrouver par la théorie
    la loi empirique de \propername{Dulong-Petit}.

    On considère un solide cristallin,
    où chaque atome (masse $m$)
    vibre autour d'une position d'équilibre
    sous l'influence de l'agitation thermique.
    On considère les atomes
    indépendants,
    et identiques.

    On repère un atome aux coordonnées $(x, y, z)$
    relatives à sa position d'équilibre,
    et on suppose l'oscillation harmonique à 3 dimensions.
    L'énergie de cet atome s'écrit~:
    \begin{equation*}
        E_1(t)
        = \frac{1}{2} m (\dt{x}^2 + \dt{y}^2 + \dt{z}^2)
        + \frac{1}{2} k (x^2 + y^2 + z^2)
    \end{equation*}
    L'énergie interne moyenne du cristal s'écrit donc~:
    \begin{equation*}
        \avg{E}
        = N \qty(\frac{1}{2} m (\dt{x}^2 + \dt{y}^2 + \dt{z}^2)
            + \frac{1}{2} k (x^2 + y^2 + z^2))
    \end{equation*}

    \subsection*{Entracte~: théorème d'équipartition de l'énergie}

    Un résultat général se présente
    lorsque l'énergie microscopique d'un système
    s'écrit comme une somme dont certains termes
    sont proportionnels au carré d'un degré de liberté
    et indépendants entre eux.

    Pour simplifier le calcul amorcé dans cette section,
    nous allons le démontrer maintenant.

    Soit un système d'énergie $E = aX^2 + E'$,
    où $X$ est un degré de liberté du système
    et $E'$ une énergie indépendante de $X$.
    La valeur moyenne de l'énergie $aX^2$
    s'écrit~:
    \begin{equation*}
        \avg{aX^2}
        = \int_\mathcal{X}{aX^2 \dd\mathbb{P}(E(X))}
        = \frac{1}{Z}
            \int_\mathbb{X}{aX^2} e^{\frac{-E}{k_B T}} \dd X
        =
    \end{equation*}
    \begin{todo}
        Trouver une bonne biblio (regarder \cite{diuphystat})
    \end{todo}

    \subsection*{Retour au calcul amorcé}

    On termine facilement
    en considérant une isotropie des trois directions~:
    \begin{equation*}
        \avg{E}
        = N \qty(6 \frac{k_B T}{2})
        = 3 N k_B T
        = 3 N \frac{R}{\glssymbol{Na}} T
        = 3 n R T
    \end{equation*}
    d'où la capacité calorifique molaire~:
    \begin{equation*}
        c_p
        = \frac{1}{n} \pdv{E}{T}
        = 3 R
    \end{equation*}
    Les écarts à l'expérience
    proviennent des approximations de ce modèle~:
    particules indépendantes,
    potentiel harmonique isotrope,
    trois directions indépendantes.
    \begin{todo}
        Y en a-t-il d'autres~?
    \end{todo}

    \subsection{Modèle d'\propername{Einstein} (1907)}
