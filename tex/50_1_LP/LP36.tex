\chapter{Diffraction par des structures périodiques}

\begin{draft}
    Ne pas passer
    trop de temps sur
    les choses simples.
\end{draft}

\section{Réseau optique}

    \subsection{Notions}

    En optique,
    on appelle réseau
    une structure périodique
    dont le motif
    interagit avec la lumière.
    Concrètement
    il peut s'agir~:
    \begin{itemize}
        \item d'un réseau d'amplitude
            dont le motif
            a une transparence variable
            (fentes, mirroirs)
        \item d'un réseau de phase
            dont le motif
            a un indice optique variable
            (verre d'épaisseur variable)
    \end{itemize}
    Dans cette leçon,
    on s'intéresse
    au cas des fentes,
    éclairées avec une onde plane.
    On note
    $\epsilon$ la largeur d'une fente
    et
    $a$ le pas du réseau.
    On s'intéresse aux situations
    où la diffraction de l'onde par les fentes
    n'est pas négligeable.

    \begin{manip}
        Montrer la diffraction
        en lumière blance
        par un réseau
        choisit pour bien montrer
        que l'on ne pourra pas faire
        l'approximation des petits angles.
    \end{manip}

    \subsection{Figure de diffraction}

    \begin{figure}[H]
        \centering
        \begin{tikzpicture}
            \coordinate (O) at (0,0);
            \coordinate (Y) at (0,1.3);

            \draw[dashed] (0,-2) -- (0,2);

            \foreach \x in {-2,0,2,4,6} {
                \draw[line width=3] (\x-1.7,0) -- (\x-.3,0);
                \draw (\x-.3,0) -- (\x+.3,0);
            }

            \draw (0,0) -- ++(130:2cm);
            \draw (0,0) -- ++(-30:2cm);

            \draw[red] ([shift=(90:1cm)]0,0) arc (90:130:1cm)
                node[midway,fill=white] {$i$};
            \draw[red] ([shift=(-90:1cm)]0,0) arc (-90:-30:1cm)
                node[midway,fill=white] {$\theta$};

            \draw (2,0) -- ++(130:2cm);
            \draw (2,0) -- ++(-30:2cm);
        \end{tikzpicture}
    \end{figure}

    \paragraph{Diffraction par une fente}
    L'amplitude de l'onde transmise
    par \emph{une} fente considérée
    et dans la direction $\theta$
    va s'écrire~:
    \begin{equation*}
        f(\theta)
        \propto \int_0^\epsilon s_i e^{jk\delta(x)} \dd x
    \end{equation*}
    avec $\delta(x)$
    la différence de marche
    entre les rayons qui traversent la fente
    qui s'écrit~:
    \begin{equation*}
        \delta(x)
        = \epsilon(\sin i - \sin \theta)
    \end{equation*}
    donc~:
    \begin{equation*}
        f(\sin\theta)
        \propto \int_0^\epsilon s_i e^{jkx(\sin i+\sin \theta)} \dd x
        \propto \sin_c\qty[\epsilon k (\sin i - \sin \theta)]
    \end{equation*}

    \paragraph{Diffraction par l'ensemble des fentes}
    L'amplitude de l'onde transmise
    par le réseau
    dans la direction $\theta$
    va s'écrire~:
    \begin{equation*}
        s(\sin\theta)
        = \sum_{\textrm{fentes}} f(\sin\theta) e^{jk\delta_n}
    \end{equation*}
    avec $\delta_n$
    la différence de marche
    entre les rayons qui traversent le réseau par chaque fente
    qui s'écrit~:
    \begin{equation*}
        \delta_n
        = na(\sin i - \sin \theta)
        = n \delta_1
    \end{equation*}
    donc~:
    \begin{equation*}
        s(\sin\theta)
        = \sum_{n \in N} f(\theta) e^{jkn\delta_1}
        = f(\sin\theta)\sum_{n \in N} (e^{jk\delta_1})^n
        = f(\sin\theta)\frac
            {\sin(N\frac{k}{2}\delta_1)}
            {\sin(\frac{k}{2}\delta_1)}
    \end{equation*}
    Finalement,
    l'intensité de la figure de diffraction angulaire
    formée par le réseau
    éclairé avec une onde plane
    (et $\sin i = 0$ pour alléger les écritures)
    est donnée par~:
    \begin{align*}
        I(\sin\theta)
        &= s^2(\sin\theta)
        \\
        &= I_0
            \sin_c^2(\epsilon k \sin\theta)
            \frac
                {\sin[2](N\frac{ka}{2}\sin\theta)}
                {\sin[2](\frac{ka}{2}\sin\theta)}
    \end{align*}
    on pose
    $\alpha = \frac{a}{2} k \sin\theta$
    et
    $\beta = \flatfrac{\epsilon}{a}$~:
    \begin{equation*}
        I(\alpha)
        = I_0
            \sin_c^2(\beta\alpha)
            \qty(\frac{\sin(N\alpha)}{\sin\alpha})^2
    \end{equation*}

    \pyimgen{diffraction_reseau}

    \begin{python}
        \software{./python/diffraction\_reseau.imgen.py}
    \end{python}

    L'enveloppe
    en $\sin_c$
    varie lentement
    pourvus que $\beta$ soit faible.
    Sur l'autre facteur,
    on distingue
    les \emph{maximas principaux}
    des \emph{maximas secondaires}.

    Les maximas principaux
    sont situés aux angles
    tels que $\sin(\alpha) = 0$
    donc aux angles~:
    \begin{equation*}
        \frac{k a \sin\theta}{2}
        = p \pi
        \implies
        \sin\theta_p
        = \frac{2 p \pi}{k a}
        = \frac{p\lambda}{a}
    \end{equation*}
    on appelle $p$
    l'ordre d'interférence.
    Pour rappel,
    les maximas de la figure de diffraction
    sont placés tous les $\flatfrac{\lambda}{2\epsilon}$~;
    et
    on peut s'intéresser à la position
    des maximas secondaires
    qui sont situés
    tous les $\flatfrac{\lambda}{Na}$.

    \subsection{Pouvoir de résolution}

    Les réseaux
    sont beaucoup utilisés en spectroscopie
    puisqu'ils peuvent,
    comme des prismes,
    séparer des longueurs d'ondes~:
    $I(\alpha)$ dépend de $k$ donc de $\lambda$.

    Pour ces applications
    il est intéressant
    de former des raies
    les plus fines possibles
    de manière à pouvoir
    en mesurer la position précisément
    ou même dans certains cas,
    en distinguer deux voisines.
    On veut donc minimiser
    la largeur angulaire $\delta\theta$
    des maximas principaux.
    Or concernant le pic central
    où $\alpha \approx 0$~:
    \begin{equation*}
        I(\alpha)
        \propto \frac{\sin[2](N\alpha)}{\sin[2](\alpha)}
        \propto \frac{\sin[2](N\alpha)}{\alpha}
        \propto \sin_c^2(N\alpha)
    \end{equation*}
    la largeur à mi hauteur
    du sinus cardinal
    est
    $\delta\sin\theta \approx \delta\theta = \frac{\lambda}{Na}$.
    Les raies sont donc
    d'autant plus fines
    que le nombre de traits éclairés
    est grand.
    On appelle \emph{finesse}
    du montage (réseau + source)
    le rapport entre
    l'écrat entre deux pics consécutifs
    et leur largeur à mi hauteur~:
    \begin{equation*}
        F
        = \frac{\Delta\theta}{\delta\theta}
        = N
    \end{equation*}

    Deux longueurs d'ondes consécutives
    séparées de $\delta\lambda$
    seront discernables dans le spectre
    selon le critère de \propername{Rayleigh}
    si
    $\Delta\theta > \delta\theta$
    avec
    $\Delta\theta = \frac{p\Delta\lambda}{a}$
    donc
    $\frac{p\Delta\lambda}{a} > \frac{\lambda}{Na}$
    donc si
    $pN > \frac{\lambda}{\delta\lambda}$.
    On appelle le produit $P = pN$
    \emph{pouvoir de résolution}
    du montage
    et en pratique
    on veut que
    $\delta\lambda > \frac{\lambda}{pN}$.
    On doit donc
    éclaier le plus de fentes possible
    ce qui peut se faire
    en élargissant la source
    ou en augmantant le nombre de traits par unité de longueur.

    \pyimgen{diffraction_reseau_spectro}

\section{Échographie}

    \begin{python}
        \software{./python/echographie.py}
    \end{python}

\section{Structures cristallines}

    \subsection{Loi de \propername{Bragg}}

\section{Diffraction acousto-optique}
