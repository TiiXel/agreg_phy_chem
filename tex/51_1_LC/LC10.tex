\chapter{Du macroscopique au microscopique dans les synthèses organiques
(Lycée)}

\niveau{TS}

\prereqstart
{Groupes caractéristiques (1S)}
{Polarisation et électronégativité (1S)}
\prereqstop

Dans le cours de chimie
depuis la seconde,
nous étudions
des réactions en chimie organique
au cours desquelles
des molécules se transforment
en échangeant des atomes~;
nous apprenons à reconnaître
certains «~groupes caractéristiques~»
et l'on apprend
qu'ils sont responsables
de la réactivité des molécules.

Dans cette leçon,
nous allons découvrir
la raison pour laquelle
les groupes caractéristiques
sont les lieux réactifs
des molécules,
et comment
ils interagissent entre eux
pour en former de nouvelles.

Nous allons nous appuyer
sur la synthèse du paracétamol
\tp{tpparacetamol}
ainsi qu'une autre réaction illustrative
\tp{tpdeshydratationalcool}.

\begin{manip}
    Lancer la synthèse \tp{tpparacetamol}~:
    ajouter l'excès d'anhydride
    et chauffer.
    Au cours de la leçon
    il faudra surveiller la réaction
    et filtrer le produit
    quand elle aura eu lieu,
    pour démarer la recristallisation.
\end{manip}

\begin{manip}
    Lancer la synthèse \tp{tpdeshydratationalcool}~:
    ajouter l'acide phosphorique
    et chauffer.
    Au cours de la leçon
    il faudra surveiller la réaction,
    la température ne doit pas dépasser \SI{95}{\degreeCelsius}
    mais le débit du distillat
    doit être correct.
\end{manip}

\section{La synthèse du paracétamol}

    La synthèse du paracétamol
    se fait
    à partir de deux réactifs~:
    le paraaminophénol
    et
    l'anhydride acétique.

    Sur le paraaminophénol
    on reconnaît~:
    un groupe amine,
    un groupe alcool,
    un cycle aromatique.
    Sur l'anhydride acétique
    on reconnaît~:
    un groupe anhydride.
    Sur le paracétamol
    on reconnaît~:
    un groupe amide,
    un cycle aromatique,
    un alcool.
    Sur l'acide éthanoïque
    on reconnaît~:
    un groupe acide carboxylique.

    La transformation
    ne se fait pas
    en une seule étape.
    Pendant la réaction chimique
    les molécules
    se déplacent
    et certains groupes d'atomes
    réagissent deux-à-deux.
    On peut écrire
    chacune des étapes
    de la réaction
    et montrer
    les espèces intermédiaires.

    \begin{draft}
        Dessiner les étapes du mécanisme
        indiquées dans \tp{tpparacetamol},
        mais en séparant la dernière étape acide-base
        en deux.
    \end{draft}

    Nous reviendrons
    sur ces étapes
    tout au cours de la leçon
    pour les comprendre.

\section{Catégories de réactions}

    On peut catégoriser
    les différentes réactions chimiques
	selon les liaisons
	qui sont modifiées.

    \paragraph{Le rôle de la spectroscopie}
    Mais au fait,
    comment sait-on
    quelles liaisons sont rompues
    et lesquelles sont formées~?

    C'est le rôle
    de la spectroscopie
    en particulier ici,
    la spectroscopie infrarouge
    nous renseigne sur
    la nature des liaisons
    qui composent les molécules.

    \begin{manip}
        Récupérer une goutte du distillat.
    \end{manip}

    Dans le cas de la synthèse \tp{tpdeshydratationalcool}
    le réactif est
    le 2-méthylcyclohexan-1-ol,
    son spectre IR
    montre la présence de la fonction alcool.
    Le produit de la réaction
    nous est inconnu
    mais son spectre IR
    montre que la fonction alcool à disparu,
    tandis que
    une double liaisons carbone-cabrone
    a été formée.

    \begin{media}
        2-méthylcyclohexan-1-ol~:\\
        \url{http://www.hanhonggroup.com/ir/img/583-59-5_IR1.gif}

        1-méthylcyclohexène~:\\
        \url{https://www.chemicalbook.com/Spectrum/591-48-0_IR1.gif}
    \end{media}

    \subsection{Élimination}

    Les réactions d'élimination
    sont celles au cours desquels
    une seule molécule
    se divise
    pour former
    deux molécules.
    Il y a alors
    rupture d'une liaision simple
    et
    formation d'une liaison multiple.
    Dans le cas
    de la synthèse du paracétamol,
    la deuxième étape
    est une élimination.

    \subsection{Addition}

    Les réactions d'addition
    sont celles au cours desquels
    deux molécules
    se combinent
    pour en former
    une seule.
    Il y a alors
    rupture d'une liaison multiple
    et
    formation d'une liaision simple.
    Dans le mécanisme
    de la synthèse du paracétamol,
    la première étape
    est une addition.

    Notons que,
    même si dans notre cas d'étude,
    l'addition est suivie d'une élimination,
    cela n'est pas toujours le cas.

    \begin{manip}
        Décoloration
        de l'eau de brome
        en présence d'alcène \tp{tpbromationalcene}.
        Sur cette réaction,
        on \emph{voit}
        les molécules colorées disparaître.
    \end{manip}

    On peut comparer
    le spectre de l'alcène
    à celui de l'alcane bromé.

    \begin{media}
        Table infrarouge~: \cite{bts2010chimie},
        spectre~:
        \software{./document/stilbene\_bromation.pdf}
    \end{media}

    De la même manière
    pour le paracétamol,
    le spectre IR ou RMN
    du produit
    nous renseigne
    sur la nature des liaisons
    qui la composent.

    \begin{media}
        \url{https://tice.ac-montpellier.fr/ABCDORGA/Famille/Tp/PARACETAMOL.GIF}
        \begin{table}[H]
            \begin{tabular}{ccl}
                \toprule
                Déplacement ($\si{\per\centi\meter}$)
                & Liaison
                & Groupe
                \\
                \midrule
                \num{3325.9}
                & \ce{N-H}
                & amide
                \\
                \num{3160.9}
                & \ce{O-H}
                & alcool
                \\
                \numrange{3000}{3100}
                & \ce{C-H}
                & aromatique
                \\
                \num{1654.4}
                & \ce{C=O}
                & amide
                \\
                \num{1610.8}
                & \ce{N-H}
                & amide
                \\
                \bottomrule
            \end{tabular}
        \end{table}
    \end{media}

    \begin{manip}
        Décolorer l'eau de brome
        avec le distillat de \tp{tpdeshydratationalcool}.
    \end{manip}

    \subsection{Substitution}

    Dans le cas
    de la synthèse du paracétamol
    il n'y a pas de substitution
    dans le mécanisme,
    même si l'équation globale
    y correspond.

\section{L'origine des réactions}

    \subsection{Sites donneurs, sites accepteurs}

    Lors de formation ou rupture
    de liaisons entre atomes,
    ce sont des doublets électrons
    qui se déplacent.

    Dans la première étape
    de la synthèse du méthylcyclohexène
    un doublet non liant d'électrons
    qui se trouve
    entre sur atome d'oxygène
    va se déplacer
    pour lier
    l'atome d'oxygène
    à un ion d'hydrogène.

    Si cette modification microscopique à lieu,
    c'est parce que
    l'ion hydrogène
    attire fortement les électrons
    en raison
    de sa charge positive.
    Dans cette situation
    on dit que
    l'hydrogène
    est un site accepteur
    de doublet d'électrons,
    alors que le doublet non liant
    de l'oxygène
    est un site donneur.

    On peut représenter
    le mouvement du doublet d'électron
    par une flèche courbe.

    \begin{draft}
        Le faire
        pour cette étape du mécanisme.
        Puis démarer
        pour celles du paracétamol
        dans la section suivante.
    \end{draft}

    La question est alors
    de savoir ce qui rend
    certains sites donneurs
    et d'autres accepteurs.

    \subsection{Électrénogativité des éléments et polarisation des liaisons}

    Dans la première étape
    de la synthèse du paracétamol
    l'azote se lie
    à un carbone,
    qui est un site accepteur.
    Pourquoi ce carbone est-il accepteur~?

    C'est à cause
    de la polarisation
    des liaions \ce{C-O} et \ce{C=O}.
    À chaque élément
    de la classification périodique
    on associe une grandeur
    appelée \emph{électronégativité}
    qui caractérise
    la «~force~» avec laquelle
    le noyau «~tire~» sur les électrons.

    \begin{media}
        \url{https://en.wikipedia.org/wiki/Electronegativities_of_the_elements_(data_page)}
    \end{media}

    L'oxygène
    est plus électronégatif
    que le carbone.
    Donc le carbone que nous étudions ici
    est très peu riche en électrons~:
    on dit
    que la liaison est polarisée
    (ce que l'on représente par une flèche sur la liaison),
    et
    que le carbone
    porte une \emph{charge partielle} positive $\delta^+$
    ce qui fait de lui
    un site accepteur de doublets
    (les électrons étants de charge négative).

    \begin{draft}
        Procéder
        à la description
        des étapes qui suivent
        en termes d'électronégativités et de polarisation.
    \end{draft}

    Dans la deuxième étape
    de la synthèse du paracétamol
    un oxygène à pourtant
    paratagé un doublet avec un carbone.
    Dans cette situation,
    la charge négative portée par l'oxygène
    change tout~:
    l'électronégativité de l'oxygène
    à peu d'importance
    par rapport à l'excès de charge négatives
    qui à tendance à repousser les doublets.
    L'oxygène donne donc un doublet au cabrone.
    Ce dernier
    doit respecter la règle de l'octet,
    et pour cela
    il doit repousser un autre doublet~:
    comme l'autre oxygène est un site accepteur
    il va en récupérer un
    en cassant la liaison.

    \begin{draft}
        L'azote peut aussi récupérer le doublet
        d'où l'aspect réversible de la première étape.
    \end{draft}

    \begin{draft}
        Notons que pour s'aider
        à déterminer quel site
        est le meilleur donneur,
        on peut regarder
        l'espèce chimique présente
        dans l'étape suivante~:
        c'est ce qui est demandé
        au programme de TS.
    \end{draft}

\begin{manip}
    Filtrer le produit recristallisé,
    dire qu'il faut le mettre à l'étuve.

    Récupérer le produit mis à l'étuve
    pendant la préparation,
    le peser,
    calculer le rendement.
\end{manip}

\begin{manip}
    Idem avec le distillat.
\end{manip}
