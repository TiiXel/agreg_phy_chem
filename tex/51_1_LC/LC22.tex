\chapter{Cinétique homogène (CPGE)}

\niveau{MPSI}

    Nous nous limitons
    aux réactions
    associées à un milieu réactionnel
    formé d'une seule phase,
    et allons s'intéresser
    aux facteurs qui influent
    sur la \emph{vitesse de réaction}.

    \begin{manip}
        Dans 3 béchers
        à des températures différentes,
        faire réagir
        environ au même moment
        des ions iodur  e
        avec des ions peroxodisulfate
        selon
        $\ce{3I- + S208^2- = I3- + 2SO4^2-}$.
        Penser à introduire les réactifs
        dans le bécher à la température la plus basse
        en premier.
    \end{manip}
    \begin{tpdo}
        Tester la manip en TP~!
    \end{tpdo}

  \section{Facteurs cinétiques}

    \subsection{Température}

    Discuter qualitativement,
    avec un schéma,
    que l'augmentation de la température
    associée à une augmentation de l'agitation thermique
    permet au réactifs de passer plus facilement
    la barrière de potentiel
    du complexe activé.

    \subsection{Catalyse}

    La catalyse
    comme facteur cinétique
    figure peu dans le programme de MPSI.
    Les élèves doivent juste savoir
    ce qu'est un catalyseur.

    \subsection{Concentration des réactifs}

    Une augementation de la concentration
    entraine une augmentation
    de la probabilité qu'un choc entre molécules arrive.
    Les réactions chimiques ont lieu
    lors des chocs.

  \section{Étude cinétique d'une réaction}

    On se concentre sur l'exemple
    de la manip d'introduction.

    \subsection{Définiton de la vitesse de réaction}

    Lorsque la réaction d'avancement $\xi$ à lieu,
    alors pendant chaque intervalle de temps $\dd t$,
    \begin{itemize}
        \item une quantité
            $\dd n_{\ce{I-}} = -3 \dd\xi$
            d'ions iodure
            disparait,
        \item une quantité
            $\dd n_{\ce{S2O8^2-}} = - \dd\xi$
            d'ions peroxodisulfate
            disparait,
        \item une quantité
            $\dd n_{I3-} = \dd\xi$
            d'ions triodure
            apprait,
        \item une quantité
            $\dd n_{SO4^2-} = 2 \dd\xi$
            d'ions sulfate
            apprait
    \end{itemize}

    Il semble alors naturel
    de définir la vitesse de la réaction par~:
    \begin{align*}
        u
        \defeq - \dv{\xi}{t}
        &= - \frac{1}{3} \dv{n_{\ce{I-}}}{t}\\
        &= - \dv{n_{\ce{S2O8^2-}}}{t}\\
        &= + \dv{n_{\ce{I3-}}}{t}\\
        &= + \frac{1}{2} \dv{n_{\ce{SO4^2-}}}{t}
    \end{align*}
    Elle renseigne
    sur la quantité
    de réactifs qui disparait
    ou sur la quantité
    de produits qui apparait
    au cours du temps.

    Remarquons que
    si l'on écrit la réaction comme~:
    $\ce{6I- + 2S208^2- = 2I3- + 4SO4^2-}$
    la valeur de la vitesse va changer ($\dd\xi \rightarrow 2 \dd\xi$).
    Cela pose problème
    concernant la tabulation de valeurs,
    la convention en cinétique
    est donc de définir la vitesse de réaction
    pour des équations ajustées avec
    tous les coefficients stoechiométriques
    entiers et le plus petit possible.

    \subsection{Vitesse de réaction en cinétique homogène isochore}

    Lorsqu'une réaction se fait à volume constant,
    on pourra définir une vitesse volumique de réaction
    $v = \frac{u}{V}$.
    L'intérêt est que l'on va pouvoir écrire la vitesse
    en fonction des concentrations
    des réactifs ou des produits~:
    \begin{equation*}
        v
        = \frac{u}{V}
        = - \frac{1}{V} \frac{1}{3} \dv{n_{\ce{I-}}}{t}
        = - \frac{1}{3} \dv{{\ce{[I^-]}}}{t}
    \end{equation*}

    \subsection{Ordre de la réaction}

    Certaines réactions chimiques admettent un \emph{ordre}.
    Lorsque c'est le cas
    on pourra vérifier expérimentalement
    que la vitesse
    peut s'écrire~:
    \begin{equation*}
        v = k {\ce{[I-]}}^\alpha {\ce{[S2O8^2-]}}^\beta
    \end{equation*}
    où $k$ est la constante cinétique,
    $\alpha$ et $\beta$ ($\geq 0$) sont les ordres partiels,
    $\alpha + \beta$ est l'odre global,
    de la réaction.

    Nous allons chercher
    si la réaction étudiée
    admet un ordre
    et si oui,
    quels sont les ordres partiels.

    \subsubsection{Dégénrescence d'ordre}

    Si l'on introduit l'un des réactifs
    en très petite quantité
    comparativement aux autres réactifs
    (par exemple ${\ce{[S2O8^2-]}}_0 \ll {\ce{[I-]}}_0$),
    nous pourrons considérer que
    sa concentration est la seule à varier
    (on suppose les autres constantes).
    Cela permet de réécrire
    une approximation de la vitesse~:
    \begin{align*}
        v(t)
        &= k {\ce{[I-]}}^\alpha(t) {\ce{[S2O8^2-]}}^\beta(t)\\
        &\approx k {\ce{[I-]}}^\alpha(t=0) {\ce{[S2O8^2-]}}^\beta(t)\\
        &= k' {\ce{[S2O8^2-]}}^\beta(t)
    \end{align*}
    où $k'$ est une constante cinétique \emph{apparente}.

    \subsubsection{Résolution expérimentale \cite{iodurecinetique}}

    \begin{manip}
        Montrer une échelle de teintes
        pour le triiodure en solution.
    \end{manip}

    On peut parfois suivre expérimentalement
    l'évolution de la concentration d'une espèce
    dans le milieu réactionnel
    par spectrophotométrie.
    Dans notre cas d'étude,
    l'espèce $\ce{I3^-}$
    colore la solution en jaune.
    Par la loi de \propername{Beer-Lambert}
    nous obtenons que l'absorbance
    et la concentration
    sont liées par
    $A_{\lambda}(t) = l \epsilon_{\lambda, {\ce{I3^-}}} {\ce{[I3^-]}}$
    donc que~:
    \begin{equation}
        v(t)
        = \frac{1}{l \epsilon_{\lambda, {\ce{I3^-}}}} \dv{A}{t}
    \end{equation}
    \begin{tpdo}
        Déterminer la valeur de $\lambda$ adéquat en TP~!
        Et faire toute cette manip.
    \end{tpdo}
    \begin{draft}
        Méthode différentielle~:
        établir les expressions de $\ce{[I3^-](t)}$
        pour $\beta = \{0, 1, 2\}$
        l'ordre partiel des ions peroxodisulfates
    \end{draft}
    \begin{manip}
        Se placer en dégénérescence d'ordre
        et réaliser un suivi spectro
        pour plusieurs concentrations initiales en réactifs.
        Attention,
        se souvenir qu'il y a un problème
        lié à l'écrat en temps
        entre le début de la réaction
        et le début d'acquisition dans le spectro~!
        Modéliser.
        Déterminer $\alpha$.
    \end{manip}

    \begin{todo}
        Terminer en parlant de la loi d'\propername{Arrhenius}
        qui est au programme.
    \end{todo}
