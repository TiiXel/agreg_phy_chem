SHELL := /bin/bash
# Latex files
TEX=$(wildcard tex/*.tex) $(wildcard tex/**/*.tex)
STY=$(wildcard tex/*.sty) $(wildcard tex/**/*.sty)

# Python image generating files
IMGEN=$(wildcard python/*.imgen.py)
IMG=$(patsubst python/%.imgen.py, .build/%_imgen.pdf, $(IMGEN))

###############################################################################
# Handy shortcuts that will always rebuild
whole: FORCE agreg_phy_chem.pdf

chapter: FORCE chapter.pdf  # Requires the $(CHAP) variable to be set

FORCE:
	touch tex/00_documentclass.tex

###############################################################################
# Recipes for TeX PDF  (has dependencies in other sections of this Makefile)
agreg_phy_chem.pdf: .build/document.pdf
	cp .build/document.pdf agreg_phy_chem.pdf

chapter.pdf: .build/document.pdf
	cp .build/document.pdf chapter.pdf

.build/document.pdf: .build/document.tex $(IMG) .build/styles.sty .build/bibliography.bib
	cd .build; latexmk -pdf -g document.tex

.build/document.tex: $(TEX) | .build
ifndef CHAP
# Make whole doc
	find tex/ -type f -name '*.tex' -print0 | sort -z | xargs -r0 cat > .build/document
else
ifneq ("$(wildcard tex/$(CHAP))", "")
	# Make only one chap
	find tex/ -type f -regextype sed -regex 'tex/[0-4].*.tex' -print0 | sort -z | xargs -r0 cat > .build/document
	find tex/ -type f -samefile tex/$(CHAP) -print0 | xargs -r0 cat >> .build/document
	find tex/ -type f -regextype sed -regex 'tex/[6-9].*.tex' -print0 | sort -z | xargs -r0 cat >> .build/document
endif
endif
	mv .build/document .build/document.tex

.build/styles.sty: $(STY) | .build
	find tex/ -type f -name '*.sty' -print0 | sort -z | xargs -r0 cat > .build/styles
	mv .build/styles .build/styles.sty

.build/bibliography.bib: tex/bibliography.bib | .build
	cp tex/bibliography.bib .build/bibliography.bib

###############################################################################
# Recipes for illustrations
images: $(IMG) | pythondeps

.build/%_imgen.pdf: python/%.imgen.py | .build pythondeps
	python3 $< --save

pythondeps: python/dependencies python/dependencies/wavelen2rgb.py

python/dependencies:
	mkdir python/dependencies

python/dependencies/wavelen2rgb.py: | python/dependencies
	cd python/dependencies; wget http://www.johnny-lin.com/py_code/wavelen2rgb.py

###############################################################################
# Recipes for the public folder to be deployed on hosting solutions
html: agreg_phy_chem.pdf $(IMG) | public public/python
	# Copy files from .build and sources to public
	@rm -fr python/dependencies
	@cp agreg_phy_chem.pdf public/
	@cp LICENSE* public/
	@cp .build/*_imgen.pdf public/python/
	@for filename in python/*.py; do \
		if [[ $$filename == *.imgen.py ]]; then \
			image=$$(echo "$${filename/python\//}"); \
			image=$$(echo "$${image/.imgen.py/_imgen.pdf}"); \
			sed -e "s|TITLE|$$filename|" \
				-e "s|IMAGE|$$image|" \
				-e "/CODE/{r $$filename" -e "d}" \
				.html-templates/python-template.html > public/$$filename.html; \
		else \
			sed -e "s|TITLE|$$filename|" \
				-e "/CODE/{r $$filename" -e "d}" \
				.html-templates/python-template.html > public/$$filename.html; \
		fi \
	done
	# Generate index.html
	@find public/ -maxdepth 1 -type f -print0 | sort -z | xargs -0 -I '{}' echo "<li><a href='{}'>{}</a></li>" > .build/list
	@sed -i -e "/index.html/d" .build/list
	@sed -i -e "s|public/||g" .build/list
	@echo "<li><a href='python/index.html'>python/</a></li>" >> .build/list
	@sed -e "s|TITLE|Index|" \
		-e "/LIST/{r .build/list" -e "d}" \
		.html-templates/index-template.html > public/index.html;
	# Generate python/index.html
	@echo "<li><a href='../index.html'>..</a></li>" > .build/list
	@find public/python/ -type f -exec echo "<li><a href='{}'>{}</a></li>" \; >> .build/list
	@sed -i -e "/index.html/d" .build/list
	@sed -i -e "s|public/python/||g" .build/list
	@sed -e "s|TITLE|/python|" \
		-e "/LIST/{r .build/list" -e "d}" \
		.html-templates/index-template.html > public/python/index.html;

public:
	mkdir public
	cp .html-templates/stylesheet.css public/stylesheet.css

public/python: | python
	mkdir public/python
	cp .html-templates/stylesheet.css public/python/stylesheet.css

###############################################################################
# More general matter
.build:
	mkdir .build

clean:
	rm -fr .build

mrproper:
	rm -fr .build
	rm -f agreg_phy_chem.pdf
	rm -f chapter.pdf
	rm -fr public
	rm -fr python/dependencies
