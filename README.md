This Gitlab repository gathers course notes related to my work preparing the french physics & chemistry teaching examination. All textual information is written in French, but you may find physics related python programs : either simulations or graphs.

# Leçons et montages à l'agrégation de physique-chimie option physique

Pour trouver le [document PDF](https://remiberthoz.gitlab.io/agreg_phy_chem/agreg_phy_chem.pdf) issu de la compilation de ce répertoire, rendez-vous sur la page  [Gitlab Pages](https://remiberthoz.gitlab.io/agreg_phy_chem/) associée. Vous y trouverez aussi les programmes python et les images qu'ils produisent.

Le dossier `tex` de ce répertoire contient du code LaTeX découpé pratiquement en plusieurs fichiers, qui sont manipulés par le script de compilation `Makefile` pour produire un seul document PDF. Ce dernier est illustré automatiquement par certains des programmes du dossier `python` et fait par endroit références aux documents qui se trouvent dans le dossier `documents`.

---------------------

J'ai préparé l'agrégation pour la session de 2018 puis après un échec aux épreuves orales j'ai repréparé le concours pour la session 2019 cette fois beaucoup plus sérieusement et avec succès.

Ce document est le fruit du travail réalisé pendant l'année universitaire 2018/2019. Un certain nombre de leçons de physiques sont rédigées, et certains montages comme certaines leçons de chimie sont pourvus d'un plan plus ou moins détaillé.

J'ai essayé d'accompagner un maximum de leçons de physique de fichiers python qui servent d'illustrations ou d'outils de calcul. Ces fichiers ont été écrits rapidement par moi et pour moi... si quelque chose n'est pas clair n'hésitez pas à m'écrire par email ou à poser une question dans la sections [Issues](https://gitlab.com/remiberthoz/agreg_phy_chem/issues) de Gitlab. Je vais essayer de les clarifier au fil du temps, notamment en en proposant une description et en intégrant leur code source commenté pas-à-pas dans le fichier PDF.

## Signalez les erreurs

Les plans et les explications des leçons ou des montages sont susceptibles de contenir des erreurs, et là encore, vous pouvez me contacter si une correction semble appropriée.

Il n'est normalement pas nécessaire de rappeler que si ce document peut être utile aux personnes qui préparent l'agrégation il ne doit pas être utilisé tel quel. Je me suis moi aussi inspirés d'autres plans que l'on trouve sur internet, mais chaque leçon est établie de manière personnelle. Utiliser les plans de quelqu'un d'autre n'est pas une bonne stratégie : chaque candidat doit s'approprier la leçon qu'il élabore pour être capable d'en justifier la structure et d'en expliquer le contenu de manière claire et fluide.

## Licence

Le document PDF généré par ce projet est sous licence [CC-BY-SA](LICENSE-CCBYSA), et le code source LaTeX utilisé pour le mettre en forme est sous la [MIT License](LICENSE-MIT).

Les codes source des programmes python qui accompagnent ce projet sont sous la [MIT License](LICENSE-MIT).